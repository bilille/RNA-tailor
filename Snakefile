configfile: "config.json"

DBREADS = config["DBREADS"]
SPECIE = config["SPECIE"]

rule ensembl_download:
    output:
        fasta="{ensemblId}/{ensemblId}.fa",gff="{ensemblId}/{ensemblId}.gff"
    message:
        "Downloading {wildcards.ensemblId} from Ensembl"
    shell:
        "python DownloadGFFfromEnsembl.py -t {wildcards.ensemblId} -g {output.gff} -f {output.fasta}"

rule repeat_masker:
    input:
        "{ensemblId}/{ensemblId}.fa"
    output:
        "{ensemblId}/{ensemblId}.fa.masked"
    message:
        "Copying {wildcards.ensemblId}.fa to {wildcards.ensemblId}.fa.masked"
    shell:
        "cp {input} {output}"


rule convert_fastq_to_fasta:
    input:
        "db/{DBREADS}.fastq"
    output:
        "db/{DBREADS}.fasta"
    shell:
        "seqtk seq -A {input} > {output}"

rule dustmasker:
    input:
        expand("db/{dbreads}.fasta",dbreads=DBREADS)
    output:
        expand("db/{dbreads}.fasta.asnb",dbreads=DBREADS)
    shell:
        'dustmasker -in {input} -infmt fasta -outfmt maskinfo_asn1_bin -out {output}'

rule build_blast_database:
    input:
        fasta=expand("db/{dbreads}.fasta",dbreads=DBREADS),
        asnb=expand("db/{dbreads}.fasta.asnb",dbreads=DBREADS)
    output:
        output_db = expand("db/{dbreads}.megablast_db", dbreads=DBREADS)
    shell:
        "makeblastdb -in {input.fasta} -dbtype nucl -mask_data {input.asnb} -out {output.output_db}; touch {output.output_db}"

rule extractexonsfromgff:
    input:
        "{ensemblId}/{ensemblId}.gff"
    output:
        "{ensemblId}/{ensemblId}.exon.txt"
    shell:
        "python ExtractExons.py -t {wildcards.ensemblId} -g {wildcards.ensemblId}/{wildcards.ensemblId}.gff -e {wildcards.ensemblId}/{wildcards.ensemblId}.exon.txt"

rule Alter_extractexonsfromgff:
     input:
          "{ensemblId}/{ensemblId}.gff"
     output:
          "{ensemblId}/{ensemblId}.split_exon.txt"
     shell:
          "python ExtractExons.py -t {wildcards.ensemblId} -g {wildcards.ensemblId}/{wildcards.ensemblId}.gff -e {wildcards.ensemblId}/{wildcards.ensemblId}.split_exon.txt -m splitter"

rule megablast:
    input:
        db = expand("db/{dbreads}.megablast_db", dbreads=DBREADS),
        gene="{ensemblId}/{ensemblId}.fa.masked"
    output:
        "{ensemblId}/{ensemblId}.megablast.xml"
    shell:
        "blastn -db {input.db} -query {input.gene} -max_target_seqs 1000000 -outfmt 5 -out {wildcards.ensemblId}/{wildcards.ensemblId}.megablast.xml"

rule megablast_parsing:
    input:
        "{ensemblId}/{ensemblId}.megablast.xml",
        "{ensemblId}/{ensemblId}.gff"
    output:
        "{ensemblId}/{ensemblId}.megablast.tsv"
    shell:
        "python CompareAnalysis2Ref.py -b {wildcards.ensemblId}/{wildcards.ensemblId}.megablast.xml -t {wildcards.ensemblId} -g {wildcards.ensemblId}/{wildcards.ensemblId}.gff -o {output}"

rule megablast_extract:
    input:
        tsv="{ensemblId}/{ensemblId}.megablast.tsv",
        reads=expand("db/{dbreads}.fasta",dbreads=DBREADS)
    output:
        "{ensemblId}/megablast.fa",
    params:
        READCOVER_THRESHOLD = config["READCOVER_THRESHOLD"],
        GENECOVER_THRESHOLD = config["GENECOVER_THRESHOLD"]
    shell:
        "python ExtractReadsFromFasta.py -r {params.READCOVER_THRESHOLD} -g {params.GENECOVER_THRESHOLD} -f {input.reads} -s {input.tsv} -o {output}"


rule exonerate:
    input:
        readsOfInterest="{ensemblId}/{alignment}.fa",
        gene="{ensemblId}/{ensemblId}.fa.masked"
    output:
        "{ensemblId}/{ensemblId}.{alignment}_exonerate.txt"
    shell:
        "exonerate --revcomp false --showvulgar no --showtargetgff y --model est2genome --bestn 1 {input.readsOfInterest} {input.gene} > {output}"

rule exonerate_filtered:
    input:
        readsOfInterest="{ensemblId}/{alignment}_filtered.fa",
        gene="{ensemblId}/{ensemblId}.fa.masked"
    output:
        "{ensemblId}/{ensemblId}.{alignment}_exonerate_filtered.txt"
    shell:
        "exonerate --revcomp false --showvulgar no --showtargetgff y --model est2genome --bestn 1 {input.readsOfInterest} {input.gene} > {output}"


rule exonerate_corrected:
    input:
        readsOfInterest="{ensemblId}/{alignment}_corrected.fa",
        gene="{ensemblId}/{ensemblId}.fa.masked"
    output:
        "{ensemblId}/{ensemblId}.{alignment}_exonerate_corrected.txt"
    shell:
        "exonerate --revcomp false --showvulgar no --showtargetgff y --model est2genome --bestn 1 {input.readsOfInterest} {input.gene} > {output}"

rule correct_reads:
    input:
        fastq=expand("db/{dbreads}.fastq",dbreads=DBREADS),
        readsOfInterest="{ensemblId}/{alignment}_filtered.fa",
    output:
        "{ensemblId}/{alignment}_corrected.fa"
    shell:
        "python exon_matrix/IsonCorrectRunner.py -g {wildcards.ensemblId} -q {input.fastq} -r {input.readsOfInterest} -o {output} -a {wildcards.alignment}"

rule filter_out_reads:
    input:
        readsOfInterest = "{ensemblId}/{alignment}.fa",
        exonerate_output="{ensemblId}/{ensemblId}.{alignment}_exonerate.txt",
        gene= "{ensemblId}/{ensemblId}.fa.masked",
        exonslist="{ensemblId}/{ensemblId}.exon.txt"
    output:
        "{ensemblId}/{alignment}_filtered.fa"
    shell:
        "python FilterOutReads.py -R {input.readsOfInterest} -r {input.gene} -E {input.exonerate_output} -o {output}"

rule exonerate_parsing:
    input:
        tsv="{ensemblId}/{ensemblId}.megablast.tsv",
        gene="{ensemblId}/{ensemblId}.fa.masked",
        exonerate="{ensemblId}/{ensemblId}.exonerate_{alignment}.txt",
        reads=expand("db/{dbreads}.fasta",dbreads=DBREADS)
    output:
        "{ensemblId}/{ensemblId}.exonerate.xml"
    shell:
        "python ParseExonerateOutput.py -r {input.reads} -b {input.tsv} -g {input.gene} -e {input.exonerate} -o {output}"

rule tsv_analysis:
    input:
        gene="{ensemblId}/{ensemblId}.fa.masked",
        readsOfInterest="{ensemblId}/megablast.fa",
        exonslist="{ensemblId}/{ensemblId}.exon.txt",
        exonerate_output="{ensemblId}/{ensemblId}.exonerate_{alignment}.txt"
    output:
        "{ensemblId}/{ensemblId}.compact.tsv"
    shell:
        "python PredictIsoform.py -r {input.gene} -R {input.readsOfInterest} -t {wildcards.ensemblId} -e {input.exonslist} -T {output} -E {input.exonerate_output}"



rule download_genome:
    output:
        "db/{SPECIE}.fa.gz"
    params:
        specie_name = config["SPECIE"]
    run:
        import ftplib
        ftp = ftplib.FTP("ftp.ensembl.org")
        ftp.login()
        ftp.cwd(f"/pub/current_fasta/{params.specie_name}/dna")
        for file in ftp.nlst():
            if ".dna.primary_assembly.fa.gz" in file:
                with open(f"{output}","wb") as f:
                    ftp.retrbinary(f"RETR {file}",f.write)
        ftp.quit()

rule mapping_to_genome:
    input:
        reads=expand("db/{dbreads}.fasta",dbreads=DBREADS),
        genome=expand("db/{specie}.fa.gz",specie=SPECIE)
    output:
        sam_file=expand("db/{dbreads}.sam",dbreads=DBREADS),
        bam_file=expand("db/{dbreads}.sorted.bam",dbreads=DBREADS)
    threads: 1

    shell:
        "minimap2 -a -x splice --split-prefix split_prefix -t {threads} {input.genome} {input.reads} > {output.sam_file} ; "
        "samtools sort {output.sam_file} -O bam > {output.bam_file} ; "
        "samtools index {output.bam_file}"



rule extract_bed_from_gff:
    input:
        "{ensemblId}/{ensemblId}.gff"
    output:
        "{ensemblId}/{ensemblId}.bed"
    params:
        key_word = "Gene"
    run:
        with open(f"{input}", "r") as input_file, open(f"{output}", "w") as output_file:
            for line in input_file:
                if params.key_word in line:
                     fields = line.strip().split("\t")
                     output_file.write("\t".join([fields[8].split(" ")[1], fields[3], fields[4]]) + "\n")


rule minimap2:
    input:
        reads=expand("db/{dbreads}.fasta",dbreads=DBREADS),
        genome=expand("db/{specie}.fa.gz",specie=SPECIE)
    output:
        sam_file="{ensemblId}/{ensemblId}.sam",
        bam_file="{ensemblId}/{ensemblId}.sorted.bam",
        paf_file="{ensemblId}/{ensemblId}.paf"
    params:
        threads="1"
    shell:
        "minimap2 -a -x splice --split-prefix split_prefix -o {output.paf_file} -t {params.threads} {input.genome} {input.reads} > {output.sam_file}  ;"
        " samtools sort {output.sam_file} -O bam > {output.bam_file} ; "
        " samtools index {output.bam_file} "


rule create_filtered_SAM_file:
    input:
        db_sam_file = expand("db/{dbreads}.sorted.bam",dbreads=DBREADS),
        region_of_interest = "{ensemblId}/{ensemblId}.gff"
    output:
        read_file= "{ensemblId}/minimap2.fa",
        gene_sam_file= "{ensemblId}/{ensemblId}.filtered.sam"
    params:
        read_cover_threshold = config['READCOVER_THRESHOLD'],
        gene_cover_threshold = config['GENECOVER_THRESHOLD']
    message:
        "python3 exon_matrix/SamParser.py --input {input.db_sam_file} --output {output.gene_sam_file} "
        "--gene_region {input.region_of_interest} --threshold {params.read_cover_threshold} "
        "--gene_cover_threshold {params.gene_cover_threshold} --gene_name {wildcards.ensemblId}"
    shell:
        "python3 exon_matrix/SamParser.py --input {input.db_sam_file} --output {output.gene_sam_file} "
        "--gene_region {input.region_of_interest} --threshold {params.read_cover_threshold} "
        "--gene_cover_threshold {params.gene_cover_threshold} --gene_name {wildcards.ensemblId}"

rule xlsx_analysis:
    input:
        gene="{ensemblId}/{ensemblId}.fa.masked",
        readsOfInterest="{ensemblId}/{alignment}_filtered.fa",
        exonslist="{ensemblId}/{ensemblId}.exon.txt",
        exonerate_output="{ensemblId}/{ensemblId}.{alignment}_exonerate_filtered.txt"
    output:
        xlsx_file = "{ensemblId}/{ensemblId}.{alignment}_compact.xlsx",
        json_file = "{ensemblId}/{ensemblId}.{alignment}_compact.json",
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}.gtf"

    shell:
        "python PredictIsoform.py -b -c config.json -J -r {input.gene} -R {input.readsOfInterest} -t {wildcards.ensemblId}"
        " -e {input.exonslist} -X {output.xlsx_file} -E {input.exonerate_output} -Z {output.json_file} "
        "-G {output.gtf_file} -S"


# question ? faut-il tout gèrer dans le config file, genre le filterOutReads et les préfixes

# -a : export alignments
# -b : compute and export realignments scores in a realign directory
# -d : generate a dot file based on a graph for every exonic binary vectors
# -e : exon file (a file with every known exons)
# -E : export Exonerate alignments between reference sequence and reads
# -J : filter on junction points
# -m : export msa alignment
# -r : the reference file (containing the reference sequence in a fasta format)
# -R : the reads file (containing every reads in a fasta format)
# -t : ID of the reference sequence
# -f : Reinforce block status before realignment
# -T : export the matrix in a tsv format
# -X : export the matrix in a .xlsx format
# -Z : export json format
# -S : performs the border smoothing


rule xlsx_analysis_with_correction:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked", \
        readsOfInterest = "{ensemblId}/{alignment}_corrected.fa", \
        exonslist = "{ensemblId}/{ensemblId}.exon.txt", \
        exonerate_output = "{ensemblId}/{ensemblId}.{alignment}_exonerate_corrected.txt"
    output:
        xlsx_file="{ensemblId}/{ensemblId}.{alignment}_compact_corrected.xlsx",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact_corrected.json",
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_corrected.gtf"
    params:
        REALIGNMENT = "-b" if config["REALIGNMENT"] else ""

    shell:
        "python PredictIsoform.py {params.REALIGNMENT} -c config.json -J -r {input.gene} -R {input.readsOfInterest} -t {wildcards.ensemblId}"
        " -e {input.exonslist} -X {output.xlsx_file} -E {input.exonerate_output}"
        " -G {output.gtf_file} -S -Z {output.json_file}"
    #-Z {output.json_file}

rule Isoform_Inclusion_method_from_corrected:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact_corrected.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_inclusion_GTAG_corrected.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -c -i"

rule Isoform_Inclusion_method_from_uncorrected:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_inclusion_GTAG.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -c -i"

    # -c GT AG correction
    # -i inclusion_process
    # -r orf_finder
rule Isoform_orf_Inclusion_method_from_corrected:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact_corrected.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_included_orf_classification_GTAG_corrected.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -c -i -r"


rule Isoform_filtered_according_to_support:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact_corrected.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_included_filtered_according_to_support_GTAG_corrected.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -c -i -r -s"


rule Isoform_corrected_GT_AG_read_support_filter:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact_corrected.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_GTAG_corrected_read_support.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -c -s"


rule Isoform_corrected_included_GT_AG_read_support_filter:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact_corrected.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_included_GTAG_corrected_read_support.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -c -s -i"

rule Isoform_corrected_GT_AG_no_read_support_filter:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact_corrected.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_GTAG_corrected.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -c"

rule Isoform_uncorrected_GT_AG_no_read_support_filter:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_GTAG_uncorrected.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -c"

rule Read_assignment_to_Isoform:
    input:
        gtf = "{ensemblId}/{ensemblId}.{alignment}_inclusion_GTAG.gtf",
        reads = "{ensemblId}/{alignment}_filtered.fa",
        genome="/home/lilian/homo_sapiens.fa"
    output:
        reduced_gtf = "{ensemblId}/{ensemblId}.{alignment}_assigned_reads_inclusion_GTAG.gtf",
        paf_file = "{ensemblId}/{ensemblId}.{alignment}_assigned_reads_inclusion_GTAG.paf"
    message:
        "<preset> use asm10 for around 10% error and asm5 for around 5%"
    shell:
        "bash ReadsToIsoformsAssignment.sh asm10 {output.paf_file} {input.gtf} {output.reduced_gtf} {input.reads} {input.genome}"


rule Read_assignment_to_all_Isoforms:
    input:
        gtf = "{ensemblId}/{ensemblId}.{alignment}_GTAG_uncorrected.gtf",
        reads = "{ensemblId}/{alignment}_filtered.fa",
        genome = "/home/lilian/homo_sapiens.fa"
        # genome=expand("db/{specie}.fa.gz",specie=SPECIE)
    output:
        reduced_gtf = "{ensemblId}/{ensemblId}.{alignment}_assigned_reads_GTAG_corrected.gtf",
        paf_file = "{ensemblId}/{ensemblId}.{alignment}_assigned_reads_GTAG_corrected.paf"
    message:
        "<preset> use asm10 for around 10% error and asm5 for around 5%"
    shell:
        "bash ReadsToIsoformsAssignment.sh asm10 {output.paf_file} {input.gtf} {output.reduced_gtf} {input.reads} {input.genome}"


rule Read_assignment_to_all_Isoforms_asmb5:
    input:
        gtf = "{ensemblId}/{ensemblId}.{alignment}_GTAG_corrected.gtf",
        reads = "{ensemblId}/{alignment}_corrected.fa",
        genome = "/home/lilian/homo_sapiens.fa"
        # genome=expand("db/{specie}.fa.gz",specie=SPECIE)
    output:
        reduced_gtf = "{ensemblId}/{ensemblId}.{alignment}_assigned_reads_GTAG_corrected_asmb5.gtf",
        paf_file = "{ensemblId}/{ensemblId}.{alignment}_assigned_reads_GTAG_corrected_asmb5.paf"
    message:
        "<preset> use asm10 for around 10% error and asm5 for around 5%"
    shell:
        "bash ReadsToIsoformsAssignment.sh asm5 {output.paf_file} {input.gtf} {output.reduced_gtf} {input.reads} {input.genome}"



rule Read_assignment_to_all_Isoforms_asmb5_test_read_support:
    input:
        gtf = "{ensemblId}/{ensemblId}.{alignment}_GTAG_corrected.gtf",
        reads = "{ensemblId}/{alignment}_corrected.fa",
        genome = "/home/lilian/homo_sapiens.fa"
        # genome=expand("db/{specie}.fa.gz",specie=SPECIE)
    output:
        reduced_gtf = "{ensemblId}/{ensemblId}.{alignment}_assigned_corrected_reads_GTAG_corrected_asmb5_test_readsupportN2S3.gtf",
        paf_file = "{ensemblId}/{ensemblId}.{alignment}_assigned_corrected_reads_GTAG_corrected_asmb5_test_readsupportN2S3.paf"
    message:
        "<preset> use asm10 for around 10% error and asm5 for around 5%"
    shell:
        "bash ReadsToIsoformsAssignment_test_read_support.sh asm5 {output.paf_file} {input.gtf} {output.reduced_gtf} {input.reads} {input.genome}"


rule Isoform_inclusion:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_inclusion.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -i -s 1"
        
        
        
rule Isoform_GT_AG_correction:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_GT_AG_correction.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -c -s 1"
        
        
rule Isoform_inclusion_UTR:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_inclusion_UTR.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -u -s 1"

rule Isoform_inclusion_ORF:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_inclusion_ORF.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -p -s 1"

rule Isoform_inclusion_ORF_clustering:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_clustering_ORF.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -r -s 1"


rule Isoform_inclusion_from_corrected:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact_corrected.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_inclusion_from_corrected.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -i"
        
        
        
rule Isoform_GT_AG_correction_from_corrected:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact_corrected.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_GT_AG_correction_from_corrected.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -c"
        
        
rule Isoform_inclusion_UTR_from_corrected:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact_corrected.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_inclusion_UTR_from_corrected.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -u"


rule Isoform_inclusion_UTR_from_corrected_read_support:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact_corrected.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_inclusion_UTR_from_corrected_read_support_1_fix.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -u"

rule Isoform_inclusion_orf:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}_inclusion_orf.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -p"



rule Isoform_corrected_GT_AG_read_support_1:
    input:
        gene = "{ensemblId}/{ensemblId}.fa.masked",
        json_file="{ensemblId}/{ensemblId}.{alignment}_compact_corrected.json"
    output:
        gtf_file= "{ensemblId}/{ensemblId}.{alignment}Isoform_corrected_GT_AG_read_support_1.gtf"
    shell:
        "python IsoformClustering.py -j {input.json_file} -f {input.gene} -o {output.gtf_file} -c -s 1"




