from Bio import SeqRecord
from Bio import SearchIO
from enum import Enum, auto

class ReadStatus(Enum):
	SOLID = auto()
	DEFAULT = auto()

class Read(object):
    """
    A read is made of :
    - the read sequence : SeqRecord
    - the query result from exonerate, if any : QueryResult
    """

    def __init__(self, read: SeqRecord):
        self.read = read
        self.id = read.id
        self.queryresult = None
        self.read_solidity = ReadStatus.DEFAULT

    def set_read_solidity(self,read_solidity):
        self.read_solidity= read_solidity

    def get_read_solidity(self):
        return self.read_solidity

    def is_read_solid(self):
        if self.get_read_solidity() == ReadStatus.SOLID:
            return True

    def set_exonerate_result(self, queryresult: SearchIO.QueryResult):
        self.queryresult = queryresult

    def get_hits(self):
        return self.queryresult

    def get_read_name(self):
        return self.id

    def __getitem__(self, key):
        return self.read.__getitem__(key)

    def __len__(self):
        return len(self.read)

    def get_read_coverage(self) -> float:
        """
            The read coverage is the ratio between the read alignment and the read size.
        """
        assert self.queryresult is not None
        return (self.queryresult[-1][-1][-1].query_range[1] - \
                self.queryresult[-1][-1][0].query_range[0]) / \
               (self.queryresult[-1][-1][-1].query_range[1])
