from Bio import SeqIO


class Reference(object):
    """
    A Reference is made of :
    - the sequence itself : SeqRecord
    - its id
    - the filename
    """

    def __init__(self, filename: str):
        self.ref = next(SeqIO.parse(filename, 'fasta'))
        self.id = self.ref.id
        self.filename = filename

    def __getitem__(self, key):
        return self.ref.__getitem__(key)

    def __len__(self):
        return len(self.ref)

    def get_filename(self):
        return self.filename
