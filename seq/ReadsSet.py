from Bio import SeqIO
from seq.Read import Read

"""
A ReadSet is a set of reads built from a fasta file.
"""


class ReadsSet:
    """
    A ReadsSet is an ordered collection of Read
    """

    def __init__(self):
        self.filename = None
        # reads are stores in a list
        self.reads = []
        # readnames are stored in a dictionary a readname (key) associated to its index in self.reads (value)
        # this is for compatibility with existing code that often uses readname to access reads
        self.readnames = {}
        # readindex stores in a dictionary an int (key) associated to a readname (value)
        self.readindexes = {}
        # sued for iteration
        self.num = 0

    @classmethod
    def make_from_fasta(cls, filename: str):
        r = ReadsSet()
        r.filename = filename
        reads = list(SeqIO.parse(filename, 'fasta'))
        for read in reads:
            r.add(Read(read))
        return r

    def add(self, read: Read):
        index = len(self.reads)
        self.reads.append(read)
        self.readindexes[index] = read.id
        self.readnames[read.id] = index
        self.num = 0

    def get_read_from_index(self, readindex: int)->Read:
        assert 0 <= readindex < len(self.reads)
        return self.reads[readindex]

    def get_list_of_reads_from_indexes(self, indexes: [ int ]) -> [ Read ]:
        """

        @param indexes: list of 0/1 of length number of reads
        @return: the list of Read for each position containg 1 in indexes
        """
        assert len(indexes) == len(self)
        list_of_reads = []
        for readindex in range(len(self)):
            if indexes[readindex] == 1:
                list_of_reads.append(self.get_read_from_index(readindex))
        return list_of_reads

    def get_readname_from_index(self, readindex: int)->str:
        assert readindex in self.readindexes
        readname = self.readindexes[readindex]
        return readname

    def get_index_from_readname(self, readname:str)-> int:
        if readname in self.readnames:
            return self.readnames[readname]
        return -1

    def get_read(self, readname: str)->Read:
        assert readname in self.readnames
        return self.get_read_from_index(self.readnames[readname])

    def get_readnames(self)->[str]:
        return list(self.readnames.keys())

    def get_filename(self)->str:
        return self.filename

    def remove(self, id: str):
        print(f"Removing read {id} from {self}")
        index = self.get_index_from_readname(id)
        assert index != -1, f"read {id} not in  {self}"
        del self.readnames[id]
        del self.reads[index]
        del self.readindexes[index]
        self.__reindex()

    def __len__(self):
        return len(self.reads)

    def __iter__(self):
        return self

    def __next__(self):
        if self.num >= len(self.reads):
            self.num = 0
            raise StopIteration
        self.num += 1
        return self.get_read_from_index(self.num - 1)

    def __reindex(self):
        self.readindexes = {}
        self.readnames = {}
        for index, read in enumerate(self.reads):
            self.readnames[read.id] = index
            self.readindexes[index] = read.id
        self.num = 0

    def sort_and_reindex_according_to(self, sorted_list_of_isoforms):
        """
        sort and reindex according to sorted_list of isoforms and update indexes.
        """
        # sort self.reads according to sorted_list_of_isoforms
        sorted_reads = []
        for isoform in sorted_list_of_isoforms:
            read_index = self.readnames.get(isoform)
            if read_index is not None:
                sorted_reads.append(self.reads[read_index])
            else:
                print(f"Warning: Read '{isoform}' not found in ReadsSet.")

        # Update list of Read object
        self.reads = sorted_reads
        # Reindex ReadsSet
        self.__reindex()


