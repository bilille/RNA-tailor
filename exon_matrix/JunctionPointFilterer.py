from exon_matrix.AnnotatedCompactMatrix import AnnotatedCompactMatrix
from exon_matrix.ReadJunctionPoint import *
import config
import numpy
from PredictIsoform import Logger

# The length of the window used to check if a read shares a beginning or and ending position with another read.
# Half of this value is used to check before and after the position.
# default value is JUNCTION_WINDOW = 20

# When filtering reads, the read that have a 'common junction' percentage above or equal to this value will be 
# dismissed from the matrix.
# Default value is FILTER_READ_JUNCTION_PERCENT = 25

class JunctionPointFilterer():
	def __init__(self, ACM : AnnotatedCompactMatrix):

		self.ACM = ACM
		self.LOGGER = ACM.LOGGER
		self.iterative_junction_points_filter()
	
	def iterative_junction_points_filter(self) :
		"""
			Apply the filter in an iterative way : as long as at least one read has an insolated junction point
			above FILTER_READ_JUNCTION_PERCENT.
		"""

		filter_necessary = True
		filter_loop = 0
		while filter_necessary :

			filter_necessary = False
			self.ACM.compute_junction_points_neighbours(config.JUNCTION_WINDOW)
			#
			rjps = self.ACM.get_read_junction_points()


			index = 0
			# Filter is necessary if for at least one read has an isolated junction percentage above
			# the one we want.
			while not filter_necessary and index < len(rjps) :
				if not rjps[index].get_read().is_read_solid():
					filter_necessary = (rjps[index].get_isolated_percentage(ignore_junction_start_and_end = False) >= config.FILTER_READ_JUNCTION_PERCENT)
				if not filter_necessary:
					index = index + 1

			if index <= len(rjps) and filter_necessary :
				filter_loop = filter_loop + 1
				self.LOGGER.add_comment_warning(("filtering iteration n°{} (config.FILTER_READ_JUNCTION_PERCENT:{})".format(filter_loop,config.FILTER_READ_JUNCTION_PERCENT)))
				self.junction_filtering()

				assert len(self.ACM.get_read_junction_points()) > 0, "All reads have been dismissed after JunctionPointFiltering."

		for rjp in rjps :
			self.LOGGER.add_comment_warning(rjp.get_read_name() + "\t\t" + str(rjp.get_isolated_amount(ignore_junction_start_and_end = False)) + "/" + str(len(rjp.get_junction_points()) - 2) + "\t" + str(round(rjp.get_isolated_percentage(ignore_junction_start_and_end = True), 2)))

	def junction_filtering(self) :
		"""
			Filter the binary (or inner) matrix by removing every read line
			where a read does not carry enough junction parts with the others
			reads.

			N. B. The read junction points must have been computed behorehand.
		"""
		rjp_to_remove = []
		read_junction_points = self.ACM.get_read_junction_points()
		for rjp in read_junction_points :
			if rjp.get_isolated_percentage(ignore_junction_start_and_end = False) >= config.FILTER_READ_JUNCTION_PERCENT and not rjp.get_read().is_read_solid():
				# remove the read from the ExonerateParser
				# self.exonerate_parser.removeReadResultsFromName(rjp.get_read_name())
				# recall to remove the RPJ at the end of the process
				rjp_to_remove.append(rjp)
				self.LOGGER.add_comment_warning("remove read {}, isolated junction rate is {} ({}/{})".format(rjp.get_read_name(), rjp.get_isolated_percentage(ignore_junction_start_and_end = True),rjp.get_isolated_amount(ignore_junction_start_and_end = True),len(rjp.get_junction_points()) - 2))
			else:
				self.LOGGER.add_comment_warning("keep read {}, isolated junction rate is {} ({}/{})".format(rjp.get_read_name(),
																			rjp.get_isolated_percentage(
																				ignore_junction_start_and_end=True),rjp.get_isolated_amount(ignore_junction_start_and_end = True),len(rjp.get_junction_points()) - 2))
		# effectively remove the RPJs
		for rjp in read_junction_points :
			if rjp not in rjp_to_remove :
				for jp in rjp.get_junction_points() :
					for rjp2 in rjp_to_remove :
						for jp2 in rjp2.get_junction_points() :
							jp.remove_neighbour(jp2)

		for rjp in rjp_to_remove :
			self.ACM.remove_read_junction_point(rjp.get_read_name())
			self.LOGGER.add_comment_warning(f"removing read {rjp.get_read_name()}")
			self.ACM.remove_read(rjp.get_read_name())
		self.LOGGER.add_comment_warning("reads in acm:")
		for r in self.ACM.get_read_names():
			self.LOGGER.add_comment_warning(r)