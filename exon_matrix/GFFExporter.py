import csv
import os
import pandas as pd
import gffutils


class GFFExporter(object):
    """
		This class will export an AlignedMatrixContent object to a .gff3 file. It will especially takes
		the result of the mechanism determination classmethod.
	"""

    def __init__(self, dico_clustering: dict, db_name: str, seqid: str, export_gtf_name : str, ISOFORM_READ_SUPPORT : str):
        self.ISOFORM_READ_SUPPORT = ISOFORM_READ_SUPPORT
        self.read_number = self.get_read_number(dico_clustering)
        self.seqid = seqid
        self.read_support = {'Read_name': [], 'Transcript_id': []}
        self.export_gtf_name = export_gtf_name

        gff_df = pd.read_csv(os.path.join(os.getcwd(), seqid, f"{seqid}.gff"), sep="\t", header=None,
                             names=["seqname", "source", "feature", "start", "end", "score", "strand", "frame",
                                    "attribute"])
        gtf_dict = gff_df.to_dict("records")

        for dct in gtf_dict:
            if dct['feature'] == 'Gene':
                self.start = dct['start']
                self.end = dct['end']
                self.seqname = dct['seqname']
                self.strand = dct['strand']
            if dct['strand'] == '-':
                self.start = -self.end

        self.score = '.'
        self.frame = '.'
        self.export_transcripts_according_to_support(above_threshold=True, dico_clustering=dico_clustering)
        self.export_transcripts_according_to_support(above_threshold=False, dico_clustering=dico_clustering)

        with open(os.path.join(os.getcwd(), export_gtf_name.replace('.gtf','.read_support.tab')), 'w') as f:
            f.write("Transcript_ID\tRead_Name\n")
            for key, value in dico_clustering.items():
                # for r_name in value['reads_names']:
                # f.write()
                # f.write(f"{key} supported by {value['read_support']} reads:\n")
                for read in value['reads_names']:
                    f.write(f"{seqid}.{key}\t{read}\n")
                # f.write("\n")


    def export_transcripts_according_to_support(self, dico_clustering, above_threshold : bool):
        isoforms = {'seqname': [],
                    'source': [],
                    'feature': [],
                    'start': [],
                    'end': [],
                    'score': [],
                    'strand': [],
                    'frame': [],
                    'attribute': []}

        if self.strand == '-':
            for transcript in dico_clustering:
                sorted_data = sorted(dico_clustering[transcript]['exon_list'], key=lambda x: x[1], reverse=True)
                result = [[b, a] for a, b in sorted_data]
                dico_clustering[transcript]['exon_list'] = result

        index = -1
        if above_threshold:
            for transcript in dico_clustering:
                    if dico_clustering[transcript]['read_support'] > self.trusted_support():
                        index = index + 1
                        read_names = dico_clustering[transcript]['reads_names']
                        # adding the transcript line
                        isoforms['seqname'].append(self.seqname)
                        isoforms['source'].append('RNA-tailor')
                        isoforms['feature'].append('transcript')
                        isoforms['start'].append(min(abs(self.start + max(
                            [value for sub_list in dico_clustering[transcript]['exon_list'] for value in sub_list])),
                                                     abs(self.start + min(
                                                         [value for sub_list in dico_clustering[transcript]['exon_list']
                                                          for value in sub_list]))))
                        isoforms['end'].append(max(abs(self.start + max(
                            [value for sub_list in dico_clustering[transcript]['exon_list'] for value in sub_list])),
                                                   abs(self.start + min(
                                                       [value for sub_list in dico_clustering[transcript]['exon_list']
                                                        for value in sub_list]))))
                        isoforms['score'].append(self.score)
                        isoforms['strand'].append(self.strand)
                        isoforms['frame'].append(self.frame)
                        isoforms['attribute'].append(
                            'gene_id "{}"; transcript_id "{}.{}"; read_support "{}";'.format(self.seqid, self.seqid,
                                                                                             transcript,
                                                                                             dico_clustering[
                                                                                                 transcript][
                                                                                                 'read_support']))

                        # self.read_support['Transcript_id'].append(f"{self.seqid}.{transcript} supported by {dico_clustering[transcript]['read_support']} reads:\n")
                        # self.read_support['Read_support_names'].append(read_names)
                        # adding the exon lines
                        exon_index = 0
                        for exon in range(len(dico_clustering[transcript]['exon_list'])):
                            exon_index = exon_index + 1
                            #mecanisme = dico_clustering[transcript]['mechanism'][exon]
                            isoforms['seqname'].append(self.seqname)
                            isoforms['source'].append('RNA-tailor')
                            isoforms['feature'].append('exon')
                            # isoforms['feature'].append(dico_clustering[transcript]['description'][exon].lower())
                            isoforms['start'].append(min(abs(self.start + dico_clustering[transcript]['exon_list'][exon][0])
                                                     , abs(self.start + dico_clustering[transcript]['exon_list'][exon][1])))
                            isoforms['end'].append(max(abs(self.start + dico_clustering[transcript]['exon_list'][exon][0])
                                                     , abs(self.start + dico_clustering[transcript]['exon_list'][exon][1])))
                            isoforms['score'].append(self.score)
                            isoforms['strand'].append(self.strand)
                            isoforms['frame'].append(self.frame)
                            isoforms['attribute'].append(
                                f"gene_id \"{self.seqid}\"; transcript_id \"{self.seqid}.{transcript}\"; exon_number \"{exon_index}\""
                                f"; exon_biotype \"{dico_clustering[transcript]['description'][exon].lower()}\";")
            df = pd.DataFrame(isoforms)
            # Define the columns to use in the GTF file
            columns = ['seqname', 'source', 'feature', 'start', 'end', 'score', 'strand', 'frame', 'attribute']
            # Write the DataFrame to a GTF file

            df.to_csv(
                os.path.join(os.getcwd(), self.export_gtf_name),
                sep='\t',
                header=False,
                index=False,
                columns=columns,
                quoting=csv.QUOTE_NONE,
                escapechar='\\'  # Use a backslash to escape quote characters in the data
            )

        if not above_threshold:
            for transcript in dico_clustering:
                if dico_clustering[transcript]['read_support'] <= self.trusted_support():
                    index = index + 1
                    read_names = dico_clustering[transcript]['reads_names']
                    # adding the transcript line
                    isoforms['seqname'].append(self.seqname)
                    isoforms['source'].append('RNA-tailor')
                    isoforms['feature'].append('transcript')
                    isoforms['start'].append(min(abs(self.start + max(
                        [value for sub_list in dico_clustering[transcript]['exon_list'] for value in sub_list])),
                                                 abs(self.start + min(
                                                     [value for sub_list in dico_clustering[transcript]['exon_list']
                                                      for value in sub_list]))))
                    isoforms['end'].append(max(abs(self.start + max(
                        [value for sub_list in dico_clustering[transcript]['exon_list'] for value in sub_list])),
                                               abs(self.start + min(
                                                   [value for sub_list in dico_clustering[transcript]['exon_list']
                                                    for value in sub_list]))))
                    isoforms['score'].append(self.score)
                    isoforms['strand'].append(self.strand)
                    isoforms['frame'].append(self.frame)
                    isoforms['attribute'].append(
                        'gene_id "{}"; transcript_id "{}.{}"; read_support "{}";'.format(self.seqid, self.seqid,
                                                                                         transcript,
                                                                                         dico_clustering[
                                                                                             transcript][
                                                                                             'read_support']))

                    # self.read_support['Transcript_id'].append(
                    #     f"{self.seqid}.{transcript} supported by {dico_clustering[transcript]['read_support']} reads:\n")
                    # self.read_support['Read_support_names'].append(read_names)
                    # adding the exon lines
                    exon_index = 0
                    for exon in range(len(dico_clustering[transcript]['exon_list'])):
                        exon_index = exon_index + 1
                        # mecanisme = dico_clustering[transcript]['mechanism'][exon]
                        isoforms['seqname'].append(self.seqname)
                        isoforms['source'].append('RNA-tailor')
                        isoforms['feature'].append('exon')
                        # isoforms['feature'].append(dico_clustering[transcript]['description'][exon].lower())
                        isoforms['start'].append(min(abs(self.start + dico_clustering[transcript]['exon_list'][exon][0])
                                                     , abs(self.start + dico_clustering[transcript]['exon_list'][exon][
                                1])))
                        isoforms['end'].append(max(abs(self.start + dico_clustering[transcript]['exon_list'][exon][0])
                                                   ,
                                                   abs(self.start + dico_clustering[transcript]['exon_list'][exon][1])))
                        isoforms['score'].append(self.score)
                        isoforms['strand'].append(self.strand)
                        isoforms['frame'].append(self.frame)
                        isoforms['attribute'].append(
                            f"gene_id \"{self.seqid}\"; transcript_id \"{self.seqid}.{transcript}\"; exon_number \"{exon_index}\""
                            f"; exon_biotype \"{dico_clustering[transcript]['description'][exon].lower()}\";")
            df = pd.DataFrame(isoforms)
            # Define the columns to use in the GTF file
            columns = ['seqname', 'source', 'feature', 'start', 'end', 'score', 'strand', 'frame', 'attribute']
            # Write the DataFrame to a GTF file

            df.to_csv(
                os.path.join(os.getcwd(), self.export_gtf_name.replace(".gtf", "_untrusted.gtf")),
                sep='\t',
                header=False,
                index=False,
                columns=columns,
                quoting=csv.QUOTE_NONE,
                escapechar='\\'  # Use a backslash to escape quote characters in the data
            )



    def get_read_number(self, dico_clustering: dict):
        total_read_nb = 0
        for transcript in dico_clustering:
            total_read_nb = total_read_nb + dico_clustering[transcript]['read_support']
        return total_read_nb

    def trusted_support(self):
        #return self.ISOFORM_READ_SUPPORT
        return max(self.ISOFORM_READ_SUPPORT, round(0.01 * self.read_number))