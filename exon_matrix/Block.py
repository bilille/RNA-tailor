from enum import Enum, auto
import config

from exon_matrix import GeneJunctionPoint
from exon_matrix.BlockAlignment import *
from seq.Read import Read


class BlockStatus(Enum):
    DEFAULT = auto()
    SOLID = auto()
    SEMI_SOLID = auto()
    UNCERTAIN = auto()

class BlockReadStatus(Enum):
    UTR5 = auto()
    EXON = auto()
    INTRON_RETENTION = auto()
    UTR3 = auto()

class Block(object):
    """
		This object gathers every alignment for each read aligning at the same
		interval on the reference sequence.
	"""

    def __init__(self, begin: int, end: int, known_status: bool = False, smoothed: bool = False, smoothable: bool = False):
        self.entering_junction_point = None
        self.leaving_junction_point = None
        self.entering_splice_junction_point = None
        self.leaving_splice_junction_point = None
        self.begin = begin
        self.end = end
        self.index = None
        self.realignable_interval = None
        self.reads_positions = {} # { str : (int,int) } str = read_name
        self.block_alignments = {} # { str : BlockAlignment } str = read_name
        self.reads_status = {} # { str : BlockReadStatus } str = read_name
        self.multiple_sequence_alignment = None
        self.known = known_status
        self.smoothed = smoothed
        self.smoothable = False
        self.status = BlockStatus.DEFAULT

        self.realignable_read_amount = 0
        self.previous_block = None
        self.next_block = None
        self.previous_nucleotides = None
        self.next_nucleotides = None

    def __len__(self):
        """
        @return: the length of the block on the reference (gene)
        """
        return self.get_end() - self.get_begin() + 1

    def set_begin(self, new_begin : int):
        self.begin = new_begin

    def set_end(self, new_end: int):
        self.end = new_end

    # TODO chek usage of set index in ACM_smoothing
    def set_block_index(self, index:int):
        self.index = index

    def get_previous_nucleotides(self) -> str:
        return self.previous_nucleotides
    def set_previous_nucleotides(self, previous_nucleotides : str):
        self.previous_nucleotides = previous_nucleotides

    def get_next_nucleotides(self) -> str:
        return self.next_nucleotides

    def set_next_nucleotides(self, next_nucleotides : str):
        self.next_nucleotides = next_nucleotides

    def set_reads_name(self,list_of_read_name: [ str ]):
        assert self.block_alignments == {}, f"block_alignments {self.block_alignments}"
        assert self.reads_positions == {}, f"reads_coordinates {self.reads_positions}"
        for read_name in list_of_read_name:
            self.add_read(read_name)

    def set_read_status(self, read_name: str, status : BlockReadStatus):
        assert read_name in self.reads_status
        assert read_name in self.reads_positions
        assert read_name in self.block_alignments
        self.reads_status[read_name] = status

    def get_read_status(self, read_name: str) -> BlockReadStatus:
        assert read_name in self.reads_status, print(f"read not in read status {read_name}")
        assert read_name in self.reads_positions
        assert read_name in self.block_alignments
        return self.reads_status[read_name]

    def add_read(self, read_name:str):
        self.block_alignments[read_name] = None
        self.reads_positions[read_name] = None
        self.reads_status[read_name] = None

    def set_previous_block(self, block : object):
        self.previous_block = block

    def set_next_block(self, block : object):
        self.next_block = block

    def get_previous_block(self):
        return self.previous_block

    def get_previous_block_containing_read_name(self, read_name: str):
        b = self.get_previous_block()
        while b != None and not b.contain_read(read_name):
            b = b.get_previous_block();
        return b

    def get_next_block(self):
        return self.next_block

    def get_next_block_containing_read_name(self, read_name: str):
        b = self.get_next_block()
        while b != None and not b.contain_read(read_name):
            b = b.get_next_block();
        return b

    def set_smoothed_status(self, status: bool):
        self.smoothed = status

    def get_smoothed_status(self):
        return self.smoothed

    def set_smoothable_status(self, status: bool):
        self.smoothable = status

    def set_status(self, status : BlockStatus):
        self.status = status

    def get_smoothable_status(self):
        return self.smoothable

    def get_entering_junction_point(self) -> GeneJunctionPoint:
        return self.entering_junction_point

    def get_leaving_junction_point(self) -> GeneJunctionPoint:
        return self.leaving_junction_point

    def set_entering_junction_point(self, junction_point : GeneJunctionPoint):
        self.entering_junction_point = junction_point

    def set_leaving_junction_point(self, junction_point : GeneJunctionPoint):
        self.leaving_junction_point = junction_point

    def get_entering_splice_junction_point(self):
        return self.entering_splice_junction_point

    def get_leaving_splice_junction_point(self):
        return self.leaving_splice_junction_point

    def set_entering_splice_junction_point(self, splice_junction_point):
        self.entering_splice_junction_point = splice_junction_point

    def set_leaving_splice_junction_point(self, splice_junction_point):
        self.leaving_splice_junction_point = splice_junction_point
    def set_realignable_interval(self, interval: (int, int)):
        self.realignable_interval = interval

    def get_realignable_interval(self):
        return self.realignable_interval

    def add_alignment(self, block_alignment: BlockAlignment):
        """
			Add a ref/read BlockAlignment to the list
			@requires block_alignement.get_read_name() to be a key of self.block_alignments
		"""
        assert block_alignment.get_read_name() in self.block_alignments
        assert block_alignment.get_read_name() in self.reads_positions
        assert block_alignment.get_read_begin() == self.reads_positions[block_alignment.get_read_name()][0] and \
               block_alignment.get_read_end() == self.reads_positions[block_alignment.get_read_name()][1]
        self.block_alignments[block_alignment.get_read_name()] = block_alignment

    def add_read_positions(self, read_name: str, begin: int, end: int):
        """
            Add a ref/read BlockAlignment to the list
            @requires read_name to be a key of self.reads_positions
        """
        assert read_name in self.reads_positions
        self.reads_positions[read_name] = (begin, end)

    def __str__(self):
        """
			Return this object as a str
		"""
        res = "Block : (" + str(self.begin) + ", " + str(self.end) + ")"
        res = res + " known : " + str(self.known)
        res = res + " alignments : " + str(self.get_population()) + " solidity : " + str(self.status)
        res = res + " smoothed : " + str(self.smoothed)
        return res

    def contain_read(self, read_name: str) -> bool:
        """
			Return True if this block contains a given read name, False otherwise.
		"""

        return read_name in self.block_alignments


    def remove_read(self, read_name: str):
        """
        Remove the read reda_name if it exists. Do nothing ortherwise.
        @param read_name: The name of the read to be removed
        """
        if self.contain_read(read_name):
            # by removing BlockAlignement population is automatically updated (defined as len(self.block_alignments))
            del self.block_alignments[read_name]
            # update junction points
            self.update_junction_points()

    def update_junction_points(self):
        if self.entering_junction_point is not None:
            self.entering_junction_point.update(self.get_population(), self.get_diff_population_with_previous_block())
        if self.leaving_junction_point is not None:
            self.leaving_junction_point.update(self.get_population(), self.get_diff_population_with_next_block())

    def get_read_coordinates(self, read_name: str) -> (int, int):
        """
			Return the read coordinates if it is presents on this block,
			None otherwise.
		"""
        if self.contain_read(read_name):
            return (self.reads_positions[read_name][0],
                    self.reads_positions[read_name][1])
        return None

    def Is_entering_splice_junction_Solid(self):
        return self.get_entering_splice_junction_point() >= max(config.SOLID_JUNCTION_MINIMUM, config.PERCENT * self.get_population())

    def Is_leaving_splice_junction_Solid(self):
        return self.get_leaving_splice_junction_point() >= max(config.SOLID_JUNCTION_MINIMUM, config.PERCENT * self.get_population())

    def get_begin(self) -> int:
        return self.begin

    def get_end(self) -> int:
        return self.end

    def is_known(self) -> bool:
        return self.known

    def is_missing(self) -> bool:
        return not self.known

    def set_known(self, known_status: bool):
        self.known = known_status

    def get_block_alignments(self) -> { BlockAlignment }:
        return self.block_alignments

    def get_block_alignment(self, read_name: str) -> BlockAlignment:
        """
			Return a BlockAlignment object for the given read
			or None if there is no object with such read name.
		"""
        if self.contain_read(read_name):
            return self.block_alignments[read_name]
        return None

    def get_population(self) -> int:
        """
			Return the number of reads appearing inside this block.
		"""
        return len(self.block_alignments)

    def get_diff_population_with_previous_block(self) -> int:
        if self.get_previous_block() is None:
            return self.get_population()
        # has a previous block but not contiguous
        if self.get_previous_block().get_end() < self.get_begin() - 1:
            return self.get_population()

        current_reads = self.get_reads_name()
        previous_reads = self.get_previous_block().get_reads_name()
        return len(list(set(current_reads) - set(previous_reads)))

    def get_diff_population_with_next_block(self) -> int:
        if self.get_next_block() is None:
            return self.get_population()
        # has a next block but not contiguous
        if self.get_end() < self.get_next_block().get_begin() - 1:
            return self.get_population()

        current_reads = self.get_reads_name()
        previous_reads = self.get_next_block().get_reads_name()
        return len(list(set(current_reads) - set(previous_reads)))

    def is_empty(self) -> bool:
        return self.get_population() == 0

    def set_block_status(self, status : BlockStatus):
        """
			Set the status (BlockStatus) for the object.
		"""
        self.status = status

    def is_solid(self) -> bool:
        return self.status == BlockStatus.SOLID
    def is_semi_solid(self) -> bool:
        return self.status == BlockStatus.SEMI_SOLID

    def is_uncertain(self) -> bool:
        return self.status == BlockStatus.UNCERTAIN

    def is_surrounded_by_semi_solid(self):
        if self.get_next_block().is_semi_solid():
            return True
        if self.get_previous_block().is_semi_solid():
            return True

    def is_read_in_block(self, readname :str):
        return readname in self.get_reads_name()

    def get_reads_name(self) -> [str]:
        """
			Return the names of the reads present in the Block.
		"""
        return self.block_alignments.keys()

    def set_realignable_read_amount(self, amount):
        assert 0 <= amount <= self.get_population(), "possible read amount must be between 0 and {}, given is {}".format(
            self.get_population(), amount)
        self.realignable_read_amount = amount

    def get_realignable_read_amount(self) -> int:
        return self.realignable_read_amount

    def get_length(self):
        return self.get_end() - self.get_begin() + 1

    def get_represented_genomic_interval(self):
        return [self.get_begin(), self.get_end()]
