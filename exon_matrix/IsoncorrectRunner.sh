#!/bin/bash

# This script executes the isONcorrect package on the filtered reads, and in output writes the
#
#
#
#

usage() { echo "Usage: $0 [-d <database of reads>] [-g <gene name>] [-a <used tools>]" 1>&2; exit 1; }
while getopts ":d:g:a:" option; do
    case "${option}" in
        d)
            d=${OPTARG}
            ;;
        g)
            g=${OPTARG}
            ;;
        a)
            a=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

#echo "g = ${g}"
#echo "vd = ${d}"
#echo "seqtk subseq db/${d}.fastq ${g}/${a}_read_to_correct.txt > ${g}/${a}_read_to_correct.fq;
#"

seqtk subseq ${d} ${g}/${a}_read_to_correct.txt > ${g}/${a}_read_to_correct.fq;
isONcorrect --fastq ${g}/${a}_read_to_correct.fq --outfolder ${g}/${a}_ison_correct;
cp ${g}/${a}.fa ${g}/${a}_uncorrected.fa;
seqtk seq -A ${g}/${a}_ison_correct/corrected_reads.fastq > ${g}/${a}_ison_correct/corrected_reads.fa;
