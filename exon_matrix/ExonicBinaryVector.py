import numpy

class ExonicBinaryVector():
	"""
		Represents for a read the presence or absence for each predicted exon.

	"""

	def __init__(self, read_name : str,  vector : numpy.ndarray):
		"""
			read_name : str (the read represented by this vector)
			vector : numpy.ndarray or list (the vector itself)
		"""
		self.read_name = read_name
		self.vector = numpy.array(vector)

	def is_included_exon_BV(self, other_ebv):
		"""
			Compare the current object and another ExonicBinaryVector object. This method
			will return True if the current object exon are present in the other object.
			If that's not the case, False is returned. Moreover, the other vector can have
		"""

		other_vector = other_ebv.get_binary_vector()
		# This vector can't be included in the other vector if it's smaller.
		if len(self.vector) > len(other_vector) :
			return False

		index = 0
		print(self.read_name)
		print(self.vector)
		print(len(self.vector))
		# TODO : adress the problem, 1pts for exonic binary vector.
		# If a read is monoexonic, it is considered to be included in no isoforms
		if sum(self.vector) == 0:
			return False
		while self.vector[index] == 0 and index < len(self.vector) :
			index += 1
			print(index)
		start = index
		
		index = len(self.vector) - 1
		while index >= 0 and self.vector[index] == 0 :
			index -= 1
		end = index 

		inclusion = True
		index = 0
		while inclusion and index < len(self.vector) :
			if index <= start or index >= end:
				inclusion = (self.vector[index] == 0) or (self.vector[index] == other_vector[index])
			else :
				inclusion = self.vector[index] == other_vector[index]
			index += 1
		return inclusion

	def is_included_intronic_BV(self, other_ebv):
		"""
        Compare the current object and another ExonicBinaryVector object. This method
        will return True if the current object exon are present in the other object.
        If that's not the case, False is returned. Moreover, the other vector can have
        additional exons that are not present in the current vector.
        """

		other_vector = other_ebv.get_binary_vector()
		# This vector can't be included in the other vector if it's smaller.
		if len(self.vector) > len(other_vector):
			return False

		# Check if the lengths of intronic structures are compatible
		if len(self.vector) > len(other_vector):
			return False

		for splice_site_A_index in range(len(other_vector) - len(self.vector) + 1):
			included_splice_site_number = 0
			current_index = splice_site_A_index
			for splice_site_B in self.vector:
				if splice_site_B == other_vector[current_index] or splice_site_B == 0:
					included_splice_site_number += 1
				current_index += 1

			if included_splice_site_number == len(self.vector):
				return True
		return False

	def is_included(self, other_ebv):
			"""
	        Compare the current object and another ExonicBinaryVector object. This method
	        will return True if the current object exon are present in the other object.
	        If that's not the case, False is returned. Moreover, the other vector can have
	        additional exons that are not present in the current vector.
	        """

			other_vector = other_ebv.get_binary_vector()
			# This vector can't be included in the other vector if it's smaller.
			if len(self.vector) > len(other_vector):
				return False

			# Check if the lengths of intronic structures are compatible
			if len(self.vector) > len(other_vector):
				return False
			for splice_site_A_index in range(len(other_vector) - len(self.vector) + 1):
				included_splice_site_number = 0
				current_index = splice_site_A_index
				for splice_site_B in self.vector:
					if splice_site_B == other_vector[current_index] or splice_site_B == 0:
						included_splice_site_number += 1
					current_index += 1

				if included_splice_site_number == len(self.vector):
					return True
			return False
	def __str__(self) :
		return str(self.vector)

	def get_binary_vector(self):
		return self.vector

	def get_read_name(self):
		return self.read_name