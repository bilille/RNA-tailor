import numpy
import os
import subprocess
import json
import re
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
import config
from exon_matrix.KnownExon import *
from exon_matrix.BinaryMatrix import BinaryMatrix
from exon_matrix.PredictedExon import *
from exon_matrix.PredictedIntron import *
from exon_matrix.Block import *
from exon_matrix.BlockAlignment import *
from exon_matrix.HSPFragmentSequenceExtracter import *
from exon_matrix.ReadJunctionPoint import *
from exon_matrix.JunctionPoint import *
from exon_matrix.ExonicBinaryVector import *
from exon_matrix.EBVGraph import *
from exon_matrix.GeneJunctionPoint import *
from exon_matrix.MSAFromSeq import MSAFromSeq
from networkx.drawing.nx_pydot import *
from exon_matrix.XLSXExporter import *
from exonerate_alignment.ExonerateParser import ExonerateParser
from PredictIsoform import Logger
from seq import ReadsSet
from seq.Read import ReadStatus

"""
SUSPICIOUS_THRESHOLD is the threshold from which a block is decided to
be suspicious or not. If the population inside the block (the number of reads
having an alignment with the reference inside the block) is below or equal that
threshold, the block is said to be 'suspicious'.
"""

# default value of SUSPICIOUS_THRESHOLD = 1 /!\ not used

"""
SUSPICIOUS_DELTA_THRESHOLD acts the same as SUSPICIOUS_THRESHOLD, except that it 
compare consecutive blocks on an exon sides. This threshold tell if a block is suspicious
or not based on the difference of population between two consecutives blocks.
"""
# default value of SUSPICIOUS_DELTA_THRESHOLD = 1 /!\ not used

""" 
MIN_REALIGNABLE_LENGTH is the minimum length from which a realignable area
will be approved or not.
"""
# default value of MIN_REALIGNABLE_LENGTH = 6

# default value of REALIGNMENT_ERROR_EDGE = 5

"""

MAX_SMOOTHING_LEN is minimal length of a bloc to be excluded from the border smoothing method.
default value of MAX_SMOOTHING_LEN = 6

"""


"""
THRESHOLD_SMOOTHING_RATIO is the constant used to choose whether or not a smoothable block passes the criteria for 
smoothing.
It is calculated as the ratio between smoothable block population over max block population of the exon > threshold
THRESHOLD_SMOOTHING_RATIO default value is 0.5

"""


class AnnotatedCompactMatrix():
    """
        This object has the same content as AlignedMatrixContent.compact_matrix with few extra.
        These extra help to represent the compact_matrix in another format.
    """

    def __init__(self,binary_matrix:BinaryMatrix, gene_filename, known_exons_file: str = "", generate_dot_file: bool = False,
                 export_name : str = ""):

        self.binary_matrix = binary_matrix
        self.gene_filename = gene_filename
        self.LOGGER= self.binary_matrix.get_log()
        self.LOGGER.add_comment_debug("Start building AnnotatedCompactMatrix")
        self.sorted_list_of_isoforms = []

        # the ReadsSet of the AnnotatedCompactMatrix is the one of the BinaryMatrix at begining
        self.reads = self.binary_matrix.get_reads()
        self.LOGGER.add_comment_debug(f"Reads at the beginning of the ACM construction {len(self.reads)} {self.reads}")
        self.reference = self.binary_matrix.get_reference()
        self.read_junction_points = []

        self.predicted_exons = []
        self.predicted_introns = []
        self.known_exons = []
        self.annotated_compact_matrix = []
        self.exonic_binary_vectors = []
        self.generate_dot_file = generate_dot_file
        self.known_exons_file = known_exons_file

        # Compute list of consecutive blocks.
        # If blocks are consecutive they belong to same list
        # otherwise they do not.
        # Type of exons : { int : [ Block ] ]
        exons = self.__compute_exons(self.binary_matrix.get_blocks())
        # Then create PredictedExon objects from the list of consecutive blocks
        self.predicted_exons = self.__build_predicted_exons(exons)
        self.__compute_gene_junction_points()

        self.LOGGER.add_comment_warning("[" + __name__ + "] " + "predicted exons after predicted blocks computation :")
        for x in self.predicted_exons:
            self.LOGGER.add_comment_warning(str(x))

        # Insert known exons if provided
        # self.do_insert_known_exons( known_exons_file, generate_dot_file)

        # set an id for each predicted exon (based on their order in the list)
        self.set_predicted_exons_id()

        # Create PredictedIntrons object for each gaps between PredictedExons objects.
        self.insert_predicted_introns()



        self.set_blocks_status()

        # Merge the predicted exons and the predicted introns into a single list
        # and order them according to their starting positions
        self.annotated_compact_matrix = sorted(self.predicted_exons + self.predicted_introns,
                                               key=lambda x: x.get_begin())
        self.tag_exon_blocks()
        self.tag_utr_blocks()
        self.tag_intron_retention_blocks()
        # self.compute_exonic_binary_vector()
        self.compute_intronic_binary_vector()
        self.export_to_xlsx(os.path.join(export_name.replace("compact", "compact-before-merging")))
        self.merging_solid_blocks()

        # TODO check if this is required
        # rechaining blocks
        l = []
        for pe in self.predicted_exons:
            for b in pe.get_blocks():
                l.append(b)
        l[0].set_previous_block(None)
        l[-1].set_next_block(None)
        for p,n in zip(l[0:-1],l[1:]):
            p.set_next_block(n)
            n.set_previous_block(p)

        self.tag_exon_blocks()
        self.tag_utr_blocks()
        self.tag_intron_retention_blocks()

        # self.compute_exonic_binary_vector()
        self.compute_intronic_binary_vector()
        # self.exon_determination(binary_matrix)
        self.export_to_xlsx(os.path.join(export_name.replace("compact", "compact-after-merging")))

    def do_insert_known_exons(self, known_exons_file, generate_dot_file):
        # if file is provided
        if len(known_exons_file) > 0:

            # extract known exons coordinates from file, build KnownExon objects,
            # append them to self.known_exons, and set for each of them mapping
            # PredictedExon objects (overlapping coordinates).
            self.known_exons = self.build_known_exons(known_exons_file)

            # self.compute_exonic_binary_vector()
            self.compute_intronic_binary_vector()

            # self.compute_block_binary_vector()

            self.graph = EBVGraph(self.exonic_binary_vectors)
            #
            if generate_dot_file:
               self.graph.write_dot_file(f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}_graph_binary.dot")
            # for each KnownExon object that does not map on any PredictedExon,
            # create a 'fake' PredictedExon with one Block with the 'KNOWN'
            # status and reinsert this PredictedExon with the others in self.predicted_exons
            self.insert_known_exons_into_predicted_list()

            # remap every KnownExon object with the 'artificial'/'fake' PredictedExon object.
            self.known_exons = self.build_known_exons(known_exons_file)

            self.set_predicted_exons_id()
            self.insert_predicted_introns()
            self.annotated_compact_matrix = sorted(self.predicted_exons + self.predicted_introns,
                                                   key=lambda x: x.get_begin())
            # self.compute_exonic_binary_vector()
            self.compute_intronic_binary_vector()

    def do_clustering(self):
        return self.cluster_rna_isoforms_according_to_introns_juction(self.create_dictionnary_read_to_nucleotidic_vector())


    def compute_block_binary_vector(self):
        self.block_binary_vector = []
        for read in self.get_reads():
            ebv = ExonicBinaryVector(read.id, [1 if b.contain_read(read.id) else 0 for e in self.get_predicted_exons() for b in e.get_blocks()])
            self.block_binary_vector.append(ebv)

    def tag_exon_blocks(self):
        for exon in self.predicted_exons:
            for b in exon.get_blocks():
                for read in b.get_reads_name():
                    b.set_read_status(read,BlockReadStatus.EXON)



    def do_realignment(self,threshold : int) -> BinaryMatrix:
        self.LOGGER.add_comment_debug(f"ACM: Starting realignment")
        from realignment.RealignmentFactory import RealignmentFactory
        realignable_areas = RealignmentFactory.compute_realignable_areas(self)
        self.LOGGER.add_comment_debug(f"ACM: Number of realignable areas  = "
                                      f"{len(realignable_areas)} ")
        self.export_to_xlsx(output_filename=f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}before_hard.xlsx")
        RealignmentFactory.hard_realignment(realignable_areas, threshold, self.binary_matrix.get_reads(), self.reference,
                                             self.gene_filename, self.binary_matrix)
        self.export_to_xlsx(output_filename=f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}after_hard.xlsx")


        RealignmentFactory.write_realignment_scores_on_ACM(realignable_areas,self.binary_matrix.get_reads(),self.reference,self.gene_filename,self.binary_matrix, self.get_predicted_exons())

        self.export_to_xlsx(output_filename=f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}after_soft.xlsx")
        # self.export_to_xlsx(output_filename= "strange_compact.xlsx")
        self.LOGGER.add_comment_debug(f"ACM: End of realignment")
        for e in self.get_predicted_exons():
            for b in e.get_blocks():
                print(b)
        self.fusion_similar_blocks()
        self.export_to_xlsx(output_filename=f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}fusion_similar_blocks.xlsx")

        # for e in self.get_predicted_exons():
        #     for b in e.get_blocks():
        #         print(b)
        self.delete_fragile_reads()
        self.export_to_xlsx(output_filename=f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}delete_fragile_reads.xlsx")

        self.delete_empty_blocks()
        self.export_to_xlsx(output_filename=f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}delete_empty_blocks.xlsx")

        self.merging_solid_blocks()
        self.export_to_xlsx(output_filename=f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}after_merging_s.xlsx")

        # self.delete_duplicate_block()
        self.do_update_PredictedExon_and_Block()
        # for e in self.get_predicted_exons():
        #     for b in e.get_blocks():
        #         print(b)

    def do_smoothing(self):
        self.LOGGER.add_comment_debug(f"ACM: starting border smoothing")
        #self.smoothed_BinaryM = self.refined_border_smoothing(self.binary_matrix)
        # return self.smoothed_BinaryM
        self.do_update_PredictedExon_and_Block()

        # self.ACM_border_smoothing()
        self.smoothing_solid_GeneJunctionPoint()
        self.delete_empty_blocks()
        self.fusion_similar_blocks()
        self.delete_fragile_reads()
        self.delete_empty_blocks()
        self.do_update_PredictedExon_and_Block()
        # for exon in self.predicted_exons:
        #     # if self.Is_there_a_solid_block_in_the_exon(exon):
        #     self.find_max_leaving_entering_junction_around_solid_block(exon)
        self.LOGGER.add_comment_debug(f"ACM: number of smoothed blocks = {self.get_smoothed_blocks_number()}")



    def do_update_PredictedExon_and_Block(self):

        blocks = self.chaining_blocks()
        dblocks = { i : b for i,b in enumerate(blocks)}
        exons = self.__compute_exons(dblocks)
        self.predicted_exons = self.__build_predicted_exons(exons)

        self.__compute_gene_junction_points()
        # reset block status
        for exon in self.get_predicted_exons():
            for block in exon.get_blocks():
                block.status = BlockStatus.DEFAULT
        self.set_blocks_status()
        # self.do_insert_known_exons(self.known_exons_file,self.generate_dot_file)
        self.set_predicted_exons_id()
        self.insert_predicted_introns()
        self.annotated_compact_matrix = sorted(self.predicted_exons + self.predicted_introns,
                                               key=lambda x: x.get_begin())
        self.tag_exon_blocks()
        self.tag_utr_blocks()
        self.tag_intron_retention_blocks()
        # self.compute_exonic_binary_vector()
        self.compute_intronic_binary_vector()
        # self.compute_block_neighourhood_Seq()


    def do_pruning(self):
        '''
            /!\ Take into account read solidity now
            The pruning process aims to delete reads that are the only ones supporting a block,
             and that block is neither their first nor their last.
        '''

        for exon in self.get_predicted_exons():
            for block in exon.get_blocks():
                if block.get_population() == 1:
                    if self.get_read_first_block(list(block.get_reads_name())[0]) == block \
                            or block == self.get_read_last_block(list(block.get_reads_name())[0]):
                        continue
                    else:
                        if not self.reads.get_read(list(block.get_reads_name())[0]).is_read_solid():
                            self.remove_read(list(block.get_reads_name())[0])
        self.fusion_similar_blocks()
        self.delete_fragile_reads()
        self.delete_empty_blocks()
        self.do_update_PredictedExon_and_Block()

    def delete_fragile_reads(self):
        '''
        A read is fragile, if all the block it contains are uncertain.
        '''
        for r in self.get_read_names():
            if self.is_read_fragile(r):
                self.remove_read(r)

    def is_read_fragile(self, read_name : str):
        '''
        Returns False if the read has at least one solid block
        '''
        for pe in self.get_predicted_exons():
            for block in pe.get_blocks():
                if block.contain_read(read_name):
                    if block.is_solid():
                        return False
        return True
    def create_dictionnary_read_to_nucleotidic_vector(self) -> dict:
        '''
        Output only with EXON, with everything, possibility to take out UTR.

        '''
        dico_read_nucleotidic = {}
        for read in self.reads:
            dico_read_nucleotidic[read.id] = {}
            dico_read_nucleotidic[read.id]['intervals'] = []
            dico_read_nucleotidic[read.id]['status'] = []
            current_intervals = []
            current_status = []

            for exon in self.get_predicted_exons():
                for i, block in enumerate(exon.get_blocks()):
                    if block.contain_read(read.id):
                        # if block.get_read_status(read.id).name.startswith('UTR'):
                        #     continue
                        #
                        # # If it is tagged as UTR, then pass
                        # #
                        # else:
                        current_intervals.append([block.begin, block.end])
            dico_read_nucleotidic[read.id]['intervals'] = self.merge_contiguous_intervals(current_intervals)
            dico_read_nucleotidic[read.id]['status'] = ['EXON' for e in dico_read_nucleotidic[read.id]['intervals']]

        return dico_read_nucleotidic

    def merge_contiguous_intervals(self, intervals : list):
        if not intervals:
            return []

        intervals.sort(key=lambda x: x[0])

        merged_intervals = [intervals[0]]
        for current in intervals[1:]:
            prev = merged_intervals[-1]
            if current[0] == prev[1] + 1:
                merged_intervals[-1] = [prev[0], current[1]]
            else:
                merged_intervals.append(current)
        return merged_intervals

    def create_dictionnary_read_to_nucleotidic_vector_old(self) -> dict:
        dico_read_nucleotidic = {}
        for read in self.reads:
            dico_read_nucleotidic[read.id] = {}
            dico_read_nucleotidic[read.id]['intervals'] = []
            dico_read_nucleotidic[read.id]['status'] = []
            current_interval = []
            current_status = []

            for exon in self.get_predicted_exons():
                for i, block in enumerate(exon.get_blocks()):
                    if block.contain_read(read.id):

                        # If current_interval is not initialized, start with the current block,
                        # If it is tagged as UTR, then add a line for this block in the GFF
                        #
                        if current_interval == []:
                            current_interval = block.get_represented_genomic_interval()
                            current_status = [block.get_read_status(read.id).name]
                            if block.get_read_status(read.id).name.startswith('UTR'):
                                dico_read_nucleotidic[read.id]['intervals'].append(current_interval)
                                dico_read_nucleotidic[read.id]['status'].append(block.get_read_status(read.id).name)
                                current_interval = []
                                current_status = []
                            continue
                        # If the current block and next block are contiguous merge their intervals
                        # but if the block is UTR 5 , then add the current interval to the GFF
                        if current_interval[1] + 1 == block.get_begin():
                            if block.get_read_status(read.id).name.startswith('UTR'):
                                dico_read_nucleotidic[read.id]['intervals'].append(current_interval)
                                dico_read_nucleotidic[read.id]['status'].append(sorted(current_status)[-1])
                                dico_read_nucleotidic[read.id]['intervals'].append(block.get_represented_genomic_interval())
                                dico_read_nucleotidic[read.id]['status'].append(block.get_read_status(read.id).name)
                                current_interval = []
                                current_status = []
                                continue

                            current_interval = [current_interval[0], block.get_end()]
                            current_status.append(block.get_read_status(read.id).name)

                        else:
                            # If the current block and next block are not contiguous add the current interval
                            # and starts a new one to account for an intron.

                            dico_read_nucleotidic[read.id]['intervals'].append(current_interval)
                            dico_read_nucleotidic[read.id]['status'].append(sorted(current_status)[-1])
                            current_interval = block.get_represented_genomic_interval()
                            current_status = [block.get_read_status(read.id).name]

                if current_interval:
                    if current_interval not in dico_read_nucleotidic[read.id]['intervals']:
                        dico_read_nucleotidic[read.id]['intervals'].append(current_interval)
                        dico_read_nucleotidic[read.id]['status'].append(sorted(current_status)[-1])
                        current_interval = []
                        current_status = []

        # Merge contiguous UTR
        for read in self.reads:
            utr3_index_to_merge = []
            utr5_index_to_merge = []
            for interval_number in range(len(dico_read_nucleotidic[read.id]['intervals'])):
                if dico_read_nucleotidic[read.id]['status'][interval_number].startswith('UTR3'):
                    utr3_index_to_merge.append(interval_number)
                if dico_read_nucleotidic[read.id]['status'][interval_number].startswith('UTR5'):
                    utr5_index_to_merge.append(interval_number)
            if len(utr3_index_to_merge)>1:

                new_interval = [dico_read_nucleotidic[read.id]['intervals'][utr3_index_to_merge[0]][0],
                                dico_read_nucleotidic[read.id]['intervals'][utr3_index_to_merge[-1]][1]]


                dico_read_nucleotidic[read.id]['intervals'][sorted(utr3_index_to_merge)[0]:sorted(utr3_index_to_merge)[-1]+1]=[new_interval]
                dico_read_nucleotidic[read.id]['status'][sorted(utr3_index_to_merge)[0]:sorted(utr3_index_to_merge)[-1]+1]=[dico_read_nucleotidic[read.id]['status'][sorted(utr3_index_to_merge)[0]]]
            if len(utr5_index_to_merge)>1:
                # accounting for len variation if merging before
                if len(utr3_index_to_merge) > 1:
                    utr5_index_to_merge = [x - (len(utr3_index_to_merge) - 1) for x in utr5_index_to_merge]

                new_interval = [dico_read_nucleotidic[read.id]['intervals'][utr5_index_to_merge[0]][0],
                                dico_read_nucleotidic[read.id]['intervals'][utr5_index_to_merge[-1]][1]]
                dico_read_nucleotidic[read.id]['intervals'][sorted(utr5_index_to_merge)[0]:sorted(utr5_index_to_merge)[-1]+1]=[new_interval]
                dico_read_nucleotidic[read.id]['status'][sorted(utr5_index_to_merge)[0]:sorted(utr5_index_to_merge)[-1]+1]=[dico_read_nucleotidic[read.id]['status'][sorted(utr5_index_to_merge)[0]]]
        return dico_read_nucleotidic


    def cluster_rna_isoforms_according_to_introns_juction_new(self, dico_rna: dict) -> dict:
        dico2 = {}
        final_dico = {}
        for k in dico_rna.keys():
            inner_junctions = self.create_inner_junctions_vector(self.get_intervals_list_without_utr(k, dico_rna))
            str_inner_junctions = str(inner_junctions)
            if str_inner_junctions not in dico2:
                dico2[str_inner_junctions] = [k]
            else:
                dico2[str_inner_junctions].append(k)

        # cluster with by total inclusion:
        dico_all_inclusive = {}
        for str_inner_junctions1 in dico2.keys():
            for str_inner_junctions2 in dico2.keys():
                if str_inner_junctions1 != str_inner_junctions2:
                    # dico2[str_inner_junctions1][0] is the read name.
                    inner_junctions1 = self.create_inner_junctions_vector(self.get_intervals_list_without_utr(dico2[str_inner_junctions1][0], dico_rna))
                    inner_junctions2 = self.create_inner_junctions_vector(self.get_intervals_list_without_utr(dico2[str_inner_junctions2][0], dico_rna))
                    # if inner_junctions1 is included inner_junctions2
                    if self.is_included(inner_junctions1, inner_junctions2):
                        if str(inner_junctions2) not in dico_all_inclusive:
                            dico_all_inclusive[str(inner_junctions2)] = [str(inner_junctions2)]
                        else:
                            dico_all_inclusive[str(inner_junctions2)].append(str(inner_junctions2))

                # if self.is_included(inner_junctions2, inner_junctions1):

        # ADAPT THIS PART OF THE SCRIPT PLEASE

        transcript_number = 0
        for k, v in dico2.items():
            # clust_isof = "_".join(v)
            # final_dico[clust_isof] = dico_rna[v[0]]
            final_dico[f"transcript_{transcript_number}"] = {}
            final_dico[f"transcript_{transcript_number}"]['reads_names'] = v
            final_dico[f"transcript_{transcript_number}"]['read_support'] = len(v)
            final_dico[f"transcript_{transcript_number}"]['exon_list'] = dico_rna[v[0]]['intervals']
            final_dico[f"transcript_{transcript_number}"]['description'] = dico_rna[v[0]]['status']
            transcript_number += 1


        # sorted_list_of_read_per_isoforms = [final_dico[t]['reads_names'] for t in
        #                       sorted(final_dico.keys(), key=lambda x: final_dico[x]['read_support'], reverse=True)]
        # self.sorted_list_of_isoforms = [r  for t in sorted_list_of_read_per_isoforms for r in t]
        #
        # self.order_ReadsSet()

        return final_dico

    def cluster_rna_isoforms_according_to_introns_juction(self, dico_rna: dict) -> dict:
        dico2 = {}
        final_dico = {}
        for k in dico_rna.keys():
            inner_junctions = self.create_inner_junctions_vector(self.get_intervals_list_without_utr(k, dico_rna))
            str_inner_junctions = str(inner_junctions)
            if str_inner_junctions not in dico2:
                dico2[str_inner_junctions] = [k]
            else:
                dico2[str_inner_junctions].append(k)


        transcript_number = 0
        for k, v in dico2.items():
            # clust_isof = "_".join(v)
            # final_dico[clust_isof] = dico_rna[v[0]]
            final_dico[f"transcript_{transcript_number}"] = {}
            final_dico[f"transcript_{transcript_number}"]['reads_names'] = v
            final_dico[f"transcript_{transcript_number}"]['read_support'] = len(v)
            final_dico[f"transcript_{transcript_number}"]['exon_list'] = dico_rna[v[0]]['intervals']
            final_dico[f"transcript_{transcript_number}"]['description'] = dico_rna[v[0]]['status']
            transcript_number += 1


        # sorted_list_of_read_per_isoforms = [final_dico[t]['reads_names'] for t in
        #                       sorted(final_dico.keys(), key=lambda x: final_dico[x]['read_support'], reverse=True)]
        # self.sorted_list_of_isoforms = [r  for t in sorted_list_of_read_per_isoforms for r in t]
        #
        # self.order_ReadsSet()

        return final_dico

    def is_included(self, list1, list2):

        """
        Check if list1 is totally included in list2.
        Each list is a list of intervals, and list1 is included in list2 if all intervals in list1
        are exactly the same in list2, allowing list2 to have additional intervals.

        Args:
        - list1: a list of intervals.
        - list2: a second list of intervals.

        Returns:
        - True, if included, False otherwise
        """
        # Sort the lists to ensure a consistent order for comparison
        for intervals in list1:
            if intervals not in list2:
                return False
        return True


    def get_intervals_list_without_utr(self, transcript_name : str, dico_rna : dict):
        without_utr_intervals_list = []
        for i, exon in enumerate(dico_rna[transcript_name]['intervals']):
            if not dico_rna[transcript_name]['status'][i].startswith('UTR5') or dico_rna[transcript_name]['status'][i].startswith('UTR3'):
                without_utr_intervals_list.append(exon)
        return without_utr_intervals_list

    def get_sorted_list_of_isoforms(self):
        return self.sorted_list_of_isoforms

    def order_ReadsSet(self):
        self.reads.sort_and_reindex_according_to(self.get_sorted_list_of_isoforms())

    def cluster_rna_isoforms_according_to_introns_juction_experimental(self, dico_rna: dict) -> dict:
        dico2 = {}
        final_dico = {}

        for k in dico_rna.keys():
            dico_rna[k]['intronic_intervals'] = self.create_inner_junctions_vector(dico_rna[k]['intervals'])

        transcript_number = 0

        # dico2 = self.Explore_intronic_vector_inclusion(dico_rna)

        for k, v in dico2.items():
            # clust_isof = "_".join(v)
            # final_dico[clust_isof] = dico_rna[v[0]]
            final_dico[f"transcript_{transcript_number}"] = {}
            final_dico[f"transcript_{transcript_number}"]['reads_names'] = v
            final_dico[f"transcript_{transcript_number}"]['read_support'] = len(v)
            final_dico[f"transcript_{transcript_number}"]['exon_list'] = dico_rna[v[0]]['intervals']
            final_dico[f"transcript_{transcript_number}"]['description'] = dico_rna[v[0]]['status']
            transcript_number += 1
        return final_dico

    # def Explore_intronic_vector_inclusion(self, dict_str_inner_junctions : dict):
    #     '''
    #     Uses graph object to search for inclusion between isoforms.
    #     Two isoforms are included if their intronic vectors overlaps and if the totally included vector (the vector that misses
    #     positions, is not flanked by an UTR before or after the missing position).
    #     '''
    #     import ast
    #
    #     list_of_interval = []
    #
    #     for k in dict_str_inner_junctions:
    #         for i in ast.literal_eval(k):
    #             if i not in list_of_interval:
    #                 list_of_interval.append(i)
    #
    #     list_of_interval = sorted(list_of_interval)
    #
    #     binary_to_dict_str_inner_junctions_keys = {}
    #     self.intronic_binary_vector = [] # a list of intronic binary vector, one per clustered isoform.
    #     for inner_junctions,reads in dict_str_inner_junctions.items():
    #         isoform_binary = []
    #         for itrn in list_of_interval:
    #
    #             if itrn in ast.literal_eval(inner_junctions):
    #                 isoform_binary.append(1)
    #             else:
    #                 isoform_binary.append(0)
    #         binary_to_dict_str_inner_junctions_keys[str(isoform_binary)]=inner_junctions
    #         ijgraph = ExonicBinaryVector(reads,isoform_binary)
    #         self.intronic_binary_vector.append(ijgraph)
    #     import matplotlib.pyplot as plt
    #     # create graph
    #     self.graph = EBVGraph(self.intronic_binary_vector)
    #
    #     self.graph.write_dot_file(f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}_graph_intronic.dot")
    #     print(jfoirej)
    #     # update dict_str_inner_junctions with inclusion information.
    #


    def Explore_intronic_vector_inclusion(self, dict_str_inner_junctions : dict):
        '''
        Uses graph object to search for inclusion between isoforms.
        Two isoforms are included if their intronic vectors overlaps and if the totally included vector (the vector that misses
        positions, is not flanked by an UTR before or after the missing position).
        '''
        import ast

        list_of_interval = []
        for k in dict_str_inner_junctions:
            for i in dict_str_inner_junctions[k]['intronic_intervals']:
                if i not in list_of_interval:
                    list_of_interval.append(i)

        list_of_interval = sorted(list_of_interval)

        print(list_of_interval)
        print(f"len of intervals : {len(list_of_interval)} {type(list_of_interval)}")

        binary_to_dict_str_inner_junctions_keys = {}
        self.intronic_binary_vector = [] # a list of intronic binary vector, one per clustered isoform.
        for read in dict_str_inner_junctions.keys():
            print(f"dict_str_inner_junctions[read]['intronic_intervals'] {dict_str_inner_junctions[read]['intronic_intervals']}")
            print(f"len {len(dict_str_inner_junctions[read]['intronic_intervals'])}")
            print(f"type {type(dict_str_inner_junctions[read]['intronic_intervals'])}")
            isoform_binary = []
            for itrn in list_of_interval:
                if itrn in dict_str_inner_junctions[read]['intronic_intervals']:
                    isoform_binary.append(1)
                else:
                    isoform_binary.append(0)
            print(f"isoform_binary {isoform_binary}")
            ijgraph = ExonicBinaryVector(read,isoform_binary)
            self.intronic_binary_vector.append(ijgraph)
        # import matplotlib.pyplot as plt
        # create graph
        self.graph = EBVGraph(self.intronic_binary_vector)
        print(self.graph.get_graph())
        self.graph.write_dot_file(f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}_graph_intronic.dot")
        # update dict_str_inner_junctions with inclusion information.







    def is_included(self, nucleotid_list1 : [[int,int]], nucleotid_list2 :  [[int,int]]):
        """
        If every items of nucleotid_list1 is included into nucleotid_list2, then returns True
        """
        for sub_list in nucleotid_list1:
            if sub_list not in nucleotid_list2:
                return False
        return True




    def create_inner_junctions_vector(self, exon_list: list) -> list:
        '''
        joining the inner junctions of an exon list
        :param exon_list:
        :return: inner_junctions as a list
        '''
        inner_junctions = []
        for i in range(len(exon_list) - 1):
            inner_junctions.append([exon_list[i][1], exon_list[i + 1][0]])
        return inner_junctions

    def merge_intervals(self, intervals: [[int, int]], status: ['BlockReadStatus']):
        if not intervals or not status:
            return []

        # pair sorting of intervals and statuses, then sort by the first value of each interval
        sorted_pairs = sorted(zip(intervals, status), key=lambda x: x[0][0])
        sorted_intervals, sorted_status = zip(*sorted_pairs)

        # Initialize merged lists with the first interval and status
        merged = [sorted_intervals[0]]
        overlapping_statuses = []
        current_status = [sorted_status[0]]
        # Iterate through the intervals starting from the second one
        for i in range(1, len(sorted_intervals)):
            current = sorted_intervals[i]
            previous = merged[-1]

            # If the current interval overlaps the previous one
            if current[0] <= previous[1] + 1:
                # Extend the last interval to encompass the current interval
                merged[-1] = (previous[0], max(previous[1], current[1]))
                # Add the current status to the list of overlapping statuses
                current_status.append(sorted_status[i])
            else:
                # When intervals no longer overlap, sort statuses,
                # take the last one, and append it to the final status list
                overlapping_statuses.append(sorted(current_status)[-1])
                current_status = []
                merged.append(current)

        # # Handle the last set of overlapping statuses
        # if len(overlapping_statuses) < len(merged):
        #     overlapping_statuses.append(sorted(current_status)[-1])

        return merged, overlapping_statuses

    def remove_read(self, read_name: str):
        """
        Remove the read read_name from teh AnnotatedCompactMatrix.

        Note:
        - as a side effect, the BinaryMatrix is also modified
        - this may lead to empty Block and thus empty PredictedExon

        @param read_name: the read to remove
        @return:
        """
        self.binary_matrix.remove_read(read_name)
        # remove the read from the annotated matrix itself, that is remove the read from blocks
        for exon in self.get_predicted_exons():
             for block in exon.get_blocks():
                 block.remove_read(read_name)

    def remove_read_junction_point(self, r_name):
        to_remove = [i for i, rjp in enumerate(self.get_read_junction_points()) if rjp.get_read_name() == r_name]
        for i in reversed(to_remove):
            del self.read_junction_points[i]

    def get_smoothed_blocks_number(self):
        number_of_smoothed_blocks = 0
        for exon in self.predicted_exons:  # First and last exon are taken
            for block in exon.get_blocks():
                if block.get_smoothed_status():
                    number_of_smoothed_blocks+=1
        return number_of_smoothed_blocks

    def get_logger(self):
        return self.LOGGER


    def pre_filtering(self):
        '''
        Apply filtering to the reads similar as JunctionPointFilterer but some read are possible to save

        - firstly, we test which read will be removed if we apply the JunctionPointFilterer : number of isolated
          junction points greater than FILTER_READ_JUNCTION_PERCENT
        - secondly, we estimate which isolated junction point could disappear thanks to realignment
        - thirdly, reads for which the estimated number of isolated junction points remains greater
          than FILTER_READ_JUNCTION_PERCENT are definitively removed
        '''
        read_to_pre_filter = []
        read_to_rebuy = []
        read_to_discard_before_realignement = []
        number_of_junction_point = 0

        # firstly, we test which read will be removed if we apply the JunctionPointFilterer
        # done in do_filtering
        # self.binary_matrix.compute_junction_points_neighbours(config.JUNCTION_WINDOW)
        rjps = self.get_read_junction_points()
        dico_list_of_isolated_jp = {}
        for index in range(len(rjps)):
            dico_list_of_isolated_jp[rjps[index].get_read_name()]=[]
            if (rjps[index].get_isolated_percentage(ignore_junction_start_and_end = True) >= config.FILTER_READ_JUNCTION_PERCENT):
                for jp in rjps[index].get_junction_points():
                    if jp.is_isolated():
                        dico_list_of_isolated_jp[rjps[index].get_read_name()].append(jp)
                read_to_pre_filter.append(rjps[index].get_read_name())

        from realignment.RealignmentFactory import RealignmentFactory
        realignable_areas = RealignmentFactory.compute_realignable_areas(self)

        # secondly, we estimate which isolated junction point could disappear thanks to realignment
        for read_name in read_to_pre_filter:

            list_of_isolated_jp = dico_list_of_isolated_jp[read_name]
            if read_name in realignable_areas.keys():
                # for each realignable area, we compute which junction points could be rebought
                for realignment_area in realignable_areas[read_name]:
                    first_block = realignment_area.get_first_block()
                    end_position = realignment_area.get_reference_end()
                    # stop_block is the first solid block after the realignment_range
                    stop_block = self.get_block_at_begining_reference_position(end_position+1)#(realignment_range[0][-1]).get_next_block()
                    # If there is no solid block after the realignment range,
                    # it means the last block of the read is being realigned
                    # therefore, the new end of realignment region is the last predicted block for now.
                    if stop_block is None:

                        self.LOGGER.add_comment_warning(f"first_block = {first_block}")
                        self.LOGGER.add_comment_warning(f"end_position = {end_position}")

                        stop_block=self.predicted_exons[-1].get_blocks()[-1]
                        self.LOGGER.add_comment_warning(f"new stop _ block = {stop_block}")
                        self.LOGGER.add_comment_warning(f"stop_block.get_next_block( = {stop_block.get_next_block()}")
                        self.LOGGER.add_comment_warning(f"stop_block.get_previous_block( = {stop_block.get_previous_block()}")


                    # compute the number of junction point for this read in this realignment area
                    # (unused)
                    # each time a block is not contigous then add a number_of_junction_point to number_of_junction_point
                    number_of_junction_point = 0
                    # compute the length of available blocks for realigning :
                    # - the block should have more than 2 reads
                    # - or the block should contain exactly one read, diffrent from read_name
                    receptor_area_len = 0
                    block_jp = first_block
                    while block_jp != stop_block :
                        # if the blocs or not contigous and curent block doesn't contain the read
                        # then we count a jp
                        if block_jp.get_end() + 1 != block_jp.get_next_block().get_begin() \
                                and not block_jp.contain_read(read_name):
                            number_of_junction_point = number_of_junction_point +1

                        if block_jp.get_population() > 1 or (block_jp.get_population() == 1 and not block_jp.contain_read(read_name)):
                            receptor_area_len = receptor_area_len + block_jp.get_end() - block_jp.get_begin() + 1

                        # go to the next block
                        block_jp = block_jp.get_next_block()

                    consecutive_blocks_length = realignment_area.get_consecutive_block_length()
                    # if the length of the realignable read portion is higher than the area not containing the read
                    # (receptor_area_len) plus half the window size per jp
                    # it means that it exists an area where the read can be realigned and rebuy at least one isolated jp
                    if consecutive_blocks_length <= receptor_area_len : # jp are rebuyable if condition true
                        for jp in list_of_isolated_jp.copy():
                            if first_block.get_begin() <= jp.get_reference_position() < stop_block.get_begin():
                                list_of_isolated_jp.remove(jp)

                # get number of jp : TODO make a method on ReadJunctionPoint
                for readjp in self.get_read_junction_points():
                    if readjp.get_read_name() == read_name:
                        nb_jp = len(readjp.get_junction_points())

                if len(list_of_isolated_jp) / nb_jp * 100 <= config.FILTER_READ_JUNCTION_PERCENT:
                    if read_name not in read_to_rebuy:
                        read_to_rebuy.append(read_name)
                else:
                    if read_name not in read_to_discard_before_realignement:
                        read_to_discard_before_realignement.append(read_name)

            else:
                # there's no realignment area for this read, it will be removed
                read_to_discard_before_realignement.append(read_name)


        self.LOGGER.add_comment_warning("read_to_pre_filter " + str(read_to_pre_filter))
        self.LOGGER.add_comment_warning("read_to_rebuy " + str(read_to_rebuy))
        self.LOGGER.add_comment_warning("read_to_discard_before_realignement " + str(read_to_discard_before_realignement))


        # thirdly, reads are definitively removed

        if read_to_discard_before_realignement:
            # create list rjp_to_remove
            for read_name in read_to_discard_before_realignement:
                rjp_to_remove=[]
                for rjp in self.get_read_junction_points():
                    if read_name == rjp.read_name:
                        rjp_to_remove.append(rjp)

            # Remove jp in neighbour of other jp
            for read_name in read_to_discard_before_realignement:
                for rjp in self.get_read_junction_points():
                    if rjp not in rjp_to_remove:
                        for jp in rjp.get_junction_points():
                            for rjp2 in rjp_to_remove:
                                for jp2 in rjp2.get_junction_points():
                                    jp.remove_neighbour(jp2)

            # Remove rjp
            for read_name in read_to_discard_before_realignement:
                for rjp in self.get_read_junction_points():
                    if read_name == rjp.read_name:
                        self.get_read_junction_points().remove(rjp)
                        self.remove_read(read_name)


    def get_exonic_vector(self):
        return [ ebv for ebv in self.exonic_binary_vectors if ebv.read_name in self.get_read_names() ]



    def get_previous_solid_block(self, read: str, position: int):
        """
            Return the first solid block found for a read right before a given position.
            If no solid block has been found before the position on that read, None is
            returned.
        """
        solid_block = None
        for exon in self.predicted_exons:
            for block in exon.get_blocks():
                if block.get_end() <= position and block.contain_read(read) and block.is_solid():
                    solid_block = block
                elif block.get_end() > position:
                    return solid_block
        return None

    def get_next_solid_block(self, read: str, position: int):
        """
            Return the first solid block found for a read after a position.
            If no solid block has been found after this position for the given read,
            None is returned.
        """
        solid_block = None
        position_found = False
        for exon in self.predicted_exons:
            for block in exon.get_blocks():
                if block.get_begin() <= position <= block.get_end():
                    position_found = True
                if position_found and solid_block == None and block.is_solid() and block.contain_read(read):
                    return block

        return solid_block

    def compute_exonic_binary_vector(self):
        """
            Compute the ExonicBinaryVector for each read.
        """
        self.exonic_binary_vectors = []
        for read_name in self.get_read_names():
            binary_vector = numpy.zeros(len(self.predicted_exons), dtype=numpy.ubyte)
            for index, exon in enumerate(self.predicted_exons):
                if exon.contains_read(read_name):
                    binary_vector[index] = 1

            ebv = ExonicBinaryVector(read_name, binary_vector)
            self.exonic_binary_vectors.append(ebv)

    def compute_intronic_binary_vector(self):
        """
            Compute the ExonicIntronicVector for each read.
        """
        dico_rna = self.create_dictionnary_read_to_nucleotidic_vector()
        dico2 = {}
        final_dico = {}
        innner_junction_list = []
        for k in dico_rna.keys():
            inner_junctions = self.create_inner_junctions_vector(self.get_intervals_list_without_utr(k, dico_rna))
            innner_junction_list.extend(inner_junctions)
        inner_junctions_unique = list(set(tuple(inner) for inner in innner_junction_list))
        inner_junctions_unique = [list(intron) for intron in inner_junctions_unique]
        inner_junctions_unique = sorted(inner_junctions_unique, key=lambda x: x[0])
        self.exonic_binary_vectors = []
        for read_name in self.get_read_names():
            read_inner_junctions = self.create_inner_junctions_vector(
                self.get_intervals_list_without_utr(read_name, dico_rna))
            binary_vector = numpy.zeros(len(inner_junctions_unique), dtype=numpy.ubyte)
            for index, intron in enumerate(inner_junctions_unique):
                if intron in read_inner_junctions:
                    binary_vector[index] = 1
            ebv = ExonicBinaryVector(read_name, binary_vector)
            self.exonic_binary_vectors.append(ebv)

    def insert_predicted_introns(self):
        """
            Compute the predicted introns from the gaps between the gaps between the
            predicted exons.
        """
        self.predicted_introns = []
        p_e = list(self.predicted_exons)
        if p_e[0].get_begin() > 0:
            self.predicted_introns.append(PredictedIntron(0, p_e[0].get_begin() - 1))

        for index, predicted_exon in enumerate(p_e[1:], 1):
            self.predicted_introns.append(PredictedIntron(p_e[index - 1].get_end() + 1, predicted_exon.get_begin() - 1))

    def set_predicted_exons_id(self):
        """
            Set an id for each PredictedExon in self.predicted_exons based on their order
            in the list.
        """
        for counter, exon in enumerate(self.predicted_exons):
            exon.set_id(counter)
            assert isinstance(exon.get_id(), int), "get_id() does not return an integer"


    def insert_known_exons_into_predicted_list(self):
        """
            For each KnownExon object that has no mapping among the predicted exons, a
            PredictedExon object is created with a single Block with the 'KNOWN'
            block status.
            This PredictedExon is then inserted into the PredictedExon list (self.predicted_exons)
            and list is sorted on the beginning position.
        """
        for k in self.known_exons:
            if len(k.get_mappings()) == 0:
                # add a 'fake' predicted exon into self.predicted_exons
                # with a special status
                b = Block(k.get_begin(), k.get_end(), known_status=False) # TODO why False ?
                b.set_block_status(BlockStatus.UNCERTAIN)
                p = PredictedExon(k.get_begin(), k.get_end(), [b])
                self.predicted_exons.append(p)
            else:
                for exon in self.predicted_exons:
                    if k.contains(exon):
                        blocks = exon.get_blocks()
                        for b in blocks:
                            if k.get_begin() <= b.get_begin() <= k.get_end():
                                b.set_known(True)

                            # resort predicted exon list based on beginning position
        self.predicted_exons = sorted(self.predicted_exons, key=lambda p: p.get_begin())
        list_of_all_blocks = []
        for exon in self.predicted_exons:
            for block in exon.get_blocks():
                list_of_all_blocks.append(block)
        list_of_all_blocks[0].set_next_block(list_of_all_blocks[1])
        list_of_all_blocks[-1].set_previous_block(list_of_all_blocks[-2])
        for b_index in range(1,len(list_of_all_blocks)-1):
            list_of_all_blocks[b_index].set_next_block(list_of_all_blocks[b_index+1])
            list_of_all_blocks[b_index].set_previous_block(list_of_all_blocks[b_index - 1])


    def build_known_exons(self, known_exons_file: str) -> [KnownExon]:
        """
            Read informations from the gff file containing the exons positions
            and save them as a list of KnownExon objects in self.known_exons.
        """

        # Build KnownExon object for each exon found in the gff file and
        # add them into 'result' list.
        with open(known_exons_file, "r") as exons_file:
            result = []
            for line in exons_file:
                words = line.split("\t")
                k = KnownExon(int(words[1]), int(words[2]))
                result.append(k)

        # Determine for each predicted exon if there is an area covered
        # by a known exon (in the 'result list').
        for k in result:
            for p in self.predicted_exons:
                map_area = False
                map_begin = 0
                map_end = 0
                if p.get_begin() <= k.get_begin() <= p.get_end():
                    map_area = True
                    map_begin = k.get_begin()
                elif k.get_begin() <= p.get_begin() <= k.get_end():
                    map_area = True
                    map_begin = p.get_begin()
                if p.get_begin() <= k.get_end() <= p.get_end():
                    map_area = True
                    map_end = k.get_end()
                elif k.get_begin() <= p.get_end() <= k.get_end():
                    map_area = True
                    map_end = p.get_end()
                if map_area:
                    map_begin = k.get_begin() if map_begin == 0 else map_begin
                    map_end = k.get_end() if map_end == 0 else map_end
                    k.add_mapping(p, (map_begin, map_end))
        return result

    def compute_block_neighourhood_Seq(self):
        '''
        if a block has every one of its junction
        '''
        record = list(SeqIO.parse(f"{self.gene_filename}/{self.gene_filename}.fa", "fasta"))[0]
        for exon in self.predicted_exons:
            for block_index, b in enumerate(exon.get_blocks()):
                range1, range2 = (0, 0), (0, 0)
                if b.get_begin() > 4:
                    range1 = (b.get_begin() - 4, b.get_begin())
                if b.get_end() < self.predicted_exons[-1].get_blocks()[-1].get_end() - 4:
                    range2 = (b.get_end(), b.get_end() + 4)
                edge_begin = str(record.seq)[range1[0]:range1[1]]
                edge_end = str(record.seq)[range2[0] + 1:range2[1] + 1]
                b.set_previous_nucleotides(edge_begin)
                b.set_next_nucleotides(edge_end)

    def compute_read_solidity(self):
        rjps = self.get_read_junction_points()
        read_names = self.get_read_names()
        for read_index, read_junction_points in enumerate(rjps):
            found_weak_splice_junction = False
            read_junction_points.sort_junction_points()
            for rjp in read_junction_points.get_junction_points()[1:-1]:
                rjp_ref_pos = rjp.reference_position
                rjp_status = rjp.get_status()
                for exon in self.predicted_exons:
                    for block in exon.get_blocks():
                        block_begin, block_end = block.get_begin(), block.get_end()
                        if block_begin <= rjp_ref_pos <= block_end:
                            previous_nucleotides = block.get_previous_nucleotides()
                            if rjp_status == JunctionStatus.ENTERING and block.get_previous_nucleotides()[2:] != 'AG':
                                found_weak_splice_junction = True
                                break
                            elif rjp_status == JunctionStatus.LEAVING and block.get_next_nucleotides()[:2] != 'GT':
                                found_weak_splice_junction = True
                                break
                    if found_weak_splice_junction:
                        break
            if not found_weak_splice_junction:
                self.get_reads().get_read_from_index(read_index).set_read_solidity(ReadStatus.SOLID)

    def __compute_splice_junction_points(self):
        # Compute each ENTERING and LEAVING splice_junction_points for every blocks.
        rjps = self.get_read_junction_points()
        for exon in self.predicted_exons:
            for block_index, block in enumerate(exon.get_blocks()):
                entering_splice_junction=0
                leaving_splice_junction=0
                block.entering_splice_junction_point = None
                block.leaving_splice_junction_point = None
                for read_index in range(len(rjps)):
                    rjps[read_index].sort_junction_points()
                    for rjp in rjps[read_index].get_junction_points()[1:-1]:
                        if rjp.reference_position >= block.get_begin() and rjp.reference_position <= block.get_end():
                            if rjp.get_status() == JunctionStatus.ENTERING:
                                entering_splice_junction+=1
                            elif rjp.get_status() == JunctionStatus.LEAVING:
                                leaving_splice_junction += 1
                            else:
                                assert rjp.get_status() == JunctionStatus.LEAVING or rjp.get_status() == JunctionStatus.ENTERING, "read junction point should have a status."

                block.set_entering_splice_junction_point(entering_splice_junction)
                block.set_leaving_splice_junction_point(leaving_splice_junction)
    def __compute_gene_junction_points(self):
        # check that the block has at least one read
        for exon in self.predicted_exons:
            for block_index, block in enumerate(exon.get_blocks()):
                assert block.get_population() != 0, print(f"Block {block} has no read")
        # Compute each ENTERING GeneJunctionPoint for every blocks.
        for exon in self.predicted_exons:
            for block_index, block in enumerate(exon.get_blocks()):

                entering_gene_junction = GeneJunctionPoint(block.get_begin(), block.get_population(),
                                                           block.get_diff_population_with_previous_block(), JunctionStatus.ENTERING)
                block.set_entering_junction_point(entering_gene_junction)

        # Compute each LEAVING GeneJunctionPoint for every blocks.
        #for exon in self.predicted_exons:
        #    for block_index, block in enumerate(reversed(exon.get_blocks())):

                leaving_gene_junction = GeneJunctionPoint(block.get_end(), block.get_population(),
                                                          block.get_diff_population_with_next_block(), JunctionStatus.LEAVING)
                block.set_leaving_junction_point(leaving_gene_junction)

        # Calculate splice_junction from rjp. Values are stored in blocks

        self.__compute_splice_junction_points()


    def __build_predicted_exons(self, exons: { int : [ Block ]}):
        """
            Build PredictedExon objects from a dictionary where each key is a number and value
            associated a list of Block.
            The predicted exon contains this list of blocks that are changed into Block
            objects. However these Block objects does not contain information on the block
            content, only the beginning and ending position in the reference sequence.
        """
        predicted_exons = []
        # build a list of blocks for each exon,reuse the same Block instance
        for key in exons:
            pe = PredictedExon(exons[key][0].begin, exons[key][-1].end, exons[key])
            predicted_exons.append(pe)
        return predicted_exons

    def __compute_exons(self, blocks : {int : Block}) -> { int : [ Block ] }:
        """
            Build a dictionary where each key is a basic number and for each corresponding value, there is a
            list of blocks on the gene (this methods requires that a call to self.compute_blocks() has been made before).
            A block is defined to belong to an exon if its beginning positions is exactly 1 nucleotide after the stop of the
            previous block.
        """
        exons = {}
        block_index = 0
        group_number = 0
        while block_index < len(blocks):
            group = []
            group.append(blocks[block_index])
            i = 1

            while block_index + i < len(blocks) and blocks[block_index + i].begin == group[-1].end + 1:
                group.append(blocks[block_index + i])
                i = i + 1

            block_index = block_index + i
            exons[group_number] = group
            group_number = group_number + 1

        # sanity check for refactoring
        for k in exons:
            for b in exons[k]:
                assert isinstance(b, Block)

        return exons

    def __str__(self):
        """
            Return this object as a string.
        """
        res = "AnnotatedCompactMatrix : "
        for predicted_intron in self.predicted_introns:
            res = res + "\n\t" + str(predicted_intron)
        for predicted_exon in self.predicted_exons:
            res = res + "\n\t" + str(predicted_exon)
        for known_exon in self.known_exons:
            res = res + "\n\t" + str(known_exon)
        return res

    def set_blocks_status(self):
        """
            So far the block status are solid and uncertain.
            Basicly, solid blocks are caried by a great amount of reads and
            they suggest known exon areas.
        """

        # Set the first exon status apart
        first_exon_index = 0
        first_solid_block = -1
        # loop until a solid leaving junction point is found inside a predicted exon
        while first_exon_index < len(self.predicted_exons) and first_solid_block == -1:
            # skip blocks that are empty (a missing exon or a PredictedIntron for example)
            if self.predicted_exons[first_exon_index].is_empty():
                first_exon_index += 1
                continue
            first_solid_block = self.predicted_exons[first_exon_index].get_first_solid_leaving_junction_point()
            # increment exon index if the current one does not contain a solid leaving junction point
            first_exon_index += 1 if first_solid_block == -1 else 0

        if first_solid_block == -1:
            self.LOGGER.add_comment_warning(
                "A solid leaving junction point could not be found for the first exon, impossible to set block "
                "status, make sure that the gene has a splicing dynamic.")

        assert first_solid_block != -1, "A solid leaving junction point could not be found for the first exon, " \
                                        "impossible to set block status, make sure that the gene has a splicing " \
                                        "dynamic."

        # All blocks from the exon beginning until the solid leaving junction point (included) are considered solid
        for block in self.predicted_exons[first_exon_index].get_blocks()[:first_solid_block + 1]:
            block.set_block_status(BlockStatus.SOLID)

        # Set the last exon status apart
        last_exon_index = len(self.predicted_exons) - 1
        last_solid_block = -1
        # backward loop until a solid entering junction point is found inside a predicted exon
        while last_exon_index >= first_exon_index and last_solid_block == -1:
            # skip blocks that are empty (a missing exon or a PredictedIntron for example)
            if self.predicted_exons[last_exon_index].is_empty():
                last_exon_index -= 1
                continue
            last_solid_block = self.predicted_exons[last_exon_index].get_last_solid_entering_junction_point()
            # decrease last exon index if the current one does not contain a solid entering junction point
            last_exon_index -= 1 if last_solid_block == -1 else 0

        if last_solid_block == -1:
            self.LOGGER.add_comment_warning(
                "A solid entering junction point could not be found for the last exon, impossible to set block "
                "status, make sure that the gene has a splicing dynamic.")

        assert last_solid_block != -1,  "A solid entering junction point could not be found for the last exon, " \
                                        "impossible to set block status, make sure that the gene has a splicing " \
                                        "dynamic."

        # All blocks from the last solid entering junction point (included) to the last is considered solid
        for block in self.predicted_exons[last_exon_index].get_blocks()[last_solid_block:]:
            block.set_block_status(BlockStatus.SOLID)

        # Set block status for predicted exons in between the first one having an leaving junction point and the last
        # one having an entering junction point
        for exon in self.predicted_exons[first_exon_index:last_exon_index + 1]:

            # Don't set block's solidity status if the exon does not contain appropriate
            # blocks (a missing exon or a PredictedIntron for example).
            if exon.is_empty():
                continue

            blocks = exon.get_blocks()

            block_index = 0
            last_solid_entering_index = -1
            while block_index < len(blocks):

                # save the last solid entering junction point met
                if blocks[block_index].get_entering_junction_point().is_solid():
                    last_solid_entering_index = block_index

                if last_solid_entering_index != -1 and blocks[block_index].get_leaving_junction_point().is_solid():
                    blocks[last_solid_entering_index].set_block_status(BlockStatus.SOLID)
                    blocks[block_index].set_block_status(BlockStatus.SOLID)
                    # block_index should be read as first_solid_leaving_index

                    # set solid blocks from the last block with a solid entering junction point
                    # (the last_solid_entering_index block) to at most the first block with a solid leaving junction
                    # point (the block_index block) with at least the same cover (population)
                    entering_pop = blocks[last_solid_entering_index].get_population()
                    while last_solid_entering_index < len(blocks) and last_solid_entering_index < block_index \
                            and blocks[last_solid_entering_index].get_population() >= entering_pop:
                        blocks[last_solid_entering_index].set_block_status(BlockStatus.SOLID)
                        last_solid_entering_index += 1
                        # at this step, last_solid_entering_index is the index of the last block tagged SOLID

                    # set solid blocks from the first block with a solid leaving junction point (the block_index block)
                    # to the last block tagged SOLID with at least the same cover (population)
                    j = block_index
                    leaving_pop = blocks[block_index].get_population()
                    while j > last_solid_entering_index and blocks[j].get_population() >= leaving_pop:
                        blocks[j].set_block_status(BlockStatus.SOLID)
                        j = j - 1

                    # resets the index to detect solid entering junction point anew
                    last_solid_entering_index = -1

                block_index += 1

        # final loop to set unknown blocks status to uncertain
        for exon in self.predicted_exons:
            blocks = exon.get_blocks()
            for block in blocks:
                if not block.is_solid():
                    block.set_block_status(BlockStatus.UNCERTAIN)

    # nucleotidic_vector[[blocks[i].get_begin(),blocks[j].get_end()] for i,j in global_vector]

    def fusion_blocks(self, list_of_blocks : [ Block ]) -> Block:
        '''
        Make the fusion between block1 and block 2.
        Return the merged Block object
        It requires the block to be contiguous and to have the same reads.
        block_1 is the previous of block_2.
        '''
        list_of_blocks= sorted(list_of_blocks, key=lambda b: b.get_begin())
        list_of_status = [ block.status for block in list_of_blocks]
        print(f"list_of_status {list_of_status}")
        print(f"list of bocl {[str(b) for b in list_of_blocks ]}")
        merge_status = BlockStatus.DEFAULT
        if BlockStatus.SOLID  in list_of_status:
            merge_status = BlockStatus.SOLID
        elif BlockStatus.SEMI_SOLID  in list_of_status:
            merge_status = BlockStatus.SEMI_SOLID
        else :
            merge_status = BlockStatus.UNCERTAIN
        assert merge_status != BlockStatus.DEFAULT, "merged blocks should have a status"

        merge_B = Block(begin= list_of_blocks[0].get_begin(),end = list_of_blocks[-1].get_end())
        merge_B.set_status(merge_status)
        # merge_B.set_reads_name(list_of_blocks[0].get_reads_name())
        merged_reads_name = list(set(list(list_of_blocks[0].get_reads_name()) + list(list_of_blocks[-1].get_reads_name())))
        merge_B.set_reads_name(merged_reads_name)

        for read_name in merged_reads_name:
            list_of_ba = []
            #if both blocks contains  the read.
            if list_of_blocks[0].contain_read(read_name) and list_of_blocks[1].contain_read(read_name):
                list_of_ba = [b.get_block_alignment(read_name) for b in list_of_blocks ]
            # if only one contains the read
            if not list_of_blocks[0].contain_read(read_name):
                list_of_ba = [list_of_blocks[1].get_block_alignment(read_name)]
            if not list_of_blocks[1].contain_read(read_name):
                list_of_ba = [list_of_blocks[0].get_block_alignment(read_name)]


            for A in list_of_ba:
                assert isinstance(A, BlockAlignment), "Object of incorrect type"


            new_ba: BlockAlignment = self.fusion_block_alignments(list_of_ba)
            merge_B.add_read_positions(read_name, new_ba.get_read_begin(), new_ba.get_read_end())
            merge_B.add_alignment(new_ba)
            # merge_B.set_block_status(BlockStatus.SOLID)
            merge_B.entering_junction_point = GeneJunctionPoint(gene_position=list_of_blocks[0].get_begin(),
                                                                current_bloc_population= merge_B.get_population(),
                                                                delta_read=-1,
                                                                junction_status=JunctionStatus.ENTERING)
            merge_B.leaving_junction_point = GeneJunctionPoint(gene_position=list_of_blocks[-1].get_end(),
                                                                current_bloc_population= merge_B.get_population(),
                                                                delta_read=-1,
                                                                junction_status=JunctionStatus.LEAVING)
        return merge_B

    def fusion_block_alignments(self, ba_list : [BlockAlignment]) -> BlockAlignment:
        '''
        Return the merged BlockAlignment
        '''
        reference_name = ba_list[0].reference_name
        reference_sequence = "".join([ba.reference_sequence for ba in ba_list])
        reference_begin = ba_list[0].reference_begin
        reference_end = ba_list[-1].reference_end
        read_name = ba_list[0].read_name
        read_sequence = "".join([ba.read_sequence for ba in ba_list])
        read_begin = ba_list[0].read_begin
        read_end = ba_list[-1].read_end
        return BlockAlignment(reference_name, reference_sequence, reference_begin, reference_end, read_name, read_sequence, read_begin, read_end)

    def smoothing_fusion_block_alignments(self, ba_list: [BlockAlignment], ba_to_merge_in : BlockAlignment) -> BlockAlignment:
        '''
        Return the merged BlockAlignment
        '''
        sorted_ba_list = sorted(ba_list, key=lambda ba: ba.reference_begin)
        reference_name = ba_to_merge_in.reference_name
        reference_sequence = "".join([ba.reference_sequence for ba in sorted_ba_list])
        reference_begin = ba_to_merge_in.reference_begin
        reference_end = ba_to_merge_in.reference_end
        read_name = ba_to_merge_in.read_name
        read_sequence = "".join([ba.read_sequence for ba in sorted_ba_list])
        read_begin = sorted_ba_list[0].read_begin
        read_end = sorted_ba_list[-1].read_end


        return BlockAlignment(reference_name, reference_sequence, reference_begin, reference_end, read_name,
                              read_sequence, read_begin, read_end)

    def merge_sublists(self, sublist : [Block]):
        if len(sublist) < 2:
            return sublist[0]

        merged_block = self.fusion_blocks([sublist[0],sublist[1]])

        for block in sublist[2:]:
            merged_block = self.fusion_blocks([merged_block, block])


        return merged_block


    def merging_solid_blocks(self):
        return
        # TODO unmerging
        # for exon in self.predicted_exons:
        #     solid_sublists = self.create_sublist_of_solid_blocks(exon)
        #
        #
        #     for solid_sublist in solid_sublists:
        #         print(f"solid_sublist {[b for b in solid_sublist]}")
        #
        #         if len(solid_sublist) > 1:
        #             merged_solid_block = self.merge_sublists(solid_sublist)
        #             assert merged_solid_block.status == BlockStatus.SOLID, f"{merged_solid_block}"
        #             self.replace_blocks_by_merged_block(merged_solid_block,exon)
        #     for blok in exon.get_blocks():
        #         blok.update_junction_points()
        # self.chaining_blocks()



    def chaining_blocks(self):
        blocks = [] # global block list
        for exon in self.predicted_exons:
            blocks.extend(exon.get_blocks())

        unique_blocks = set()
        sorted_blocks = []

        for block in blocks:
            if block not in unique_blocks:
                unique_blocks.add(block)
                sorted_blocks.append(block)

        assert blocks != [], "All reads have been discarded. Make sure that the gene is expressed."
        blocks = sorted(sorted_blocks, key=lambda b: b.begin)

        assert len(blocks)>1, "Make sure that your gene is expressed or has splicing dynamic"

        blocks[0].set_previous_block(None)
        blocks[0].set_next_block(blocks[1])
        blocks[-1].set_next_block(None)
        blocks[-1].set_previous_block(blocks[-2])

        for block_index in range(1,len(blocks) - 1):
            blocks[block_index].set_next_block(blocks[block_index + 1])
            blocks[block_index + 1].set_previous_block(blocks[block_index])
        return blocks

    def update_blocks(self):
        # Chaining blocks
        for exon in self.predicted_exons:
            if len(exon.get_blocks()) > 1:
                exon.get_blocks()[-1].set_previous_block(exon.get_blocks()[-2])
                exon.get_blocks()[0].set_next_block(exon.get_blocks()[1])
            for block_index, block in enumerate(exon.get_blocks()[1:-1]):
                block.set_next_block(exon.get_blocks()[block_index + 2])
                block.set_previous_block(exon.get_blocks()[block_index])

        # Compute each ENTERING GeneJunctionPoint for every blocks.
        for exon in self.predicted_exons:
            for block_index, block in enumerate(exon.get_blocks()):
                entering_gene_junction = GeneJunctionPoint(block.get_begin(), block.get_population(),
                                                           block.get_diff_population_with_previous_block(),
                                                           JunctionStatus.ENTERING)
                block.set_entering_junction_point(entering_gene_junction)

        # Compute each LEAVING GeneJunctionPoint for every blocks.
        for exon in self.predicted_exons:
            for block_index, block in enumerate(reversed(exon.get_blocks())):
                leaving_gene_junction = GeneJunctionPoint(block.get_end(), block.get_population(),
                                                          block.get_diff_population_with_next_block(),
                                                          JunctionStatus.LEAVING)
                block.set_leaving_junction_point(leaving_gene_junction)

        # reset block status
        for exon in self.get_predicted_exons():
            for block in exon.get_blocks():
                block.status = BlockStatus.DEFAULT

        self.set_blocks_status()

    def replace_blocks_by_merged_block(self,merged_bloc : Block, exon : PredictedExon):
        '''
        Find the location of the block in the list of blocks of the exon and insert it in it
        '''

        merged_interval = [None,None]


        for i, block in enumerate(exon.get_blocks()):
            print("block.begin == merged_bloc.begin")
            print(f"{block.begin} == {merged_bloc.begin}")
            if block.begin == merged_bloc.begin:
                print("block.begin == merged_bloc.begin")
                print(f"{block.begin} == {merged_bloc.begin}")
                merged_interval[0] = i
            if block.end == merged_bloc.end:
                merged_interval[1] = i
                break
        print("previous merged interval")
        for e in exon.get_blocks():
            print(e)
        if None in merged_interval:
            for idx, interval in enumerate(merged_interval):
                if interval is None:
                    merged_interval[idx] = self.get_first_previous_or_next_block(idx, merged_bloc)

        assert None not in merged_interval, f"{merged_interval} {merged_bloc}"
        print(f"merged_interval {merged_interval}")
        exon.replace_blocks(merged_interval, merged_bloc)


    def get_first_previous_or_next_block(self, idx, merged_bloc):

        if idx == 0:
            for exon in self.get_predicted_exons():
                if merged_bloc.begin >= exon.get_begin() and merged_bloc.begin <= exon.get_end():
                    distance = merged_bloc.begin - exon.get_begin()
                    previous_block = exon.get_blocks()[0]
                    for i, block in enumerate(exon.get_blocks()):
                        if merged_bloc.begin - block.get_begin() < distance and merged_bloc.begin - block.get_begin() > 0:
                            distance = merged_bloc.begin - block.get_begin()
                            previous_block = i
            return previous_block

        if idx == 1:
            for exon in self.get_predicted_exons():
                if merged_bloc.end >= exon.get_begin() and merged_bloc.end <= exon.get_end():
                    distance =  exon.get_end() - merged_bloc.end
                    next_block = exon.get_blocks()[0]
                    for i, block in enumerate(exon.get_blocks()):
                        if  block.get_end() - merged_bloc.end < distance and   block.get_end() - merged_bloc.end> 0:
                            distance = block.get_end() - merged_bloc.end
                            next_block = i
            return next_block





    def refined_border_smoothing(self, binary_object : BinaryMatrix):

        for exon in self.predicted_exons:
            for i in range(len(exon.get_blocks())):
                if (exon.get_blocks()[i].get_end() - exon.get_blocks()[i].get_begin() + 1) < config.MAX_SMOOTHING_LEN:
                    exon.get_blocks()[i].set_smoothed_status(True)

            solid_sublists = self.create_sublist_of_solid_blocks(exon)

            for solid_blocks in solid_sublists:
                max_entering_index, max_leaving_index, max_pop =\
                    self.find_max_leaving_point_entering_point_and_max_pop(solid_blocks)
                ent_block = exon.get_blocks()[max_entering_index]
                lv_block = exon.get_blocks()[max_leaving_index]
                for blok in exon.get_blocks():
                    blok.update_junction_points()
                self.smooth_from_max_entering_index(max_entering_index,max_pop,exon.get_blocks(),binary_object)
                self.smooth_from_max_leaving_index(max_leaving_index, max_pop, exon.get_blocks(), binary_object)
        return binary_object.get_matrix()

    def ACM_border_smoothing(self):

        self.merging_solid_blocks()
        self.chaining_blocks()
        self.export_to_xlsx(
            output_filename=f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}strange_compact0.xlsx")
        for exon in self.predicted_exons:
            if self.is_block_solid_in_exon(exon):
                for b_index, block in enumerate(exon.get_blocks()):
                    if block.get_leaving_junction_point().is_solid():
                        # if block.is_surrounded_by_semi_solid():
                        downstream_terminal_alternative_bound, downstream_index_BA_to_move_in_BSJP, downstream_BA_to_move_in_alternative_bound = self.downstream_smoothing_old( block, b_index)
                        if len(downstream_index_BA_to_move_in_BSJP) > 0:
                            self.newJS_merging_smoothing_out_blocks(True, b_index,
                                                                    downstream_index_BA_to_move_in_BSJP, exon)

                        if len(downstream_BA_to_move_in_alternative_bound) > 0:
                            self.newJS_merging_smoothing_out_blocks(True, downstream_terminal_alternative_bound,
                                                                    downstream_BA_to_move_in_alternative_bound, exon)
                    if block.get_entering_junction_point().is_solid():

                        # downstream_terminal, downstream_index_to_merge_out, downstream_index_to_merge_in, \
                        #     upstream_terminal, upstream_index_to_merge_out, upstream_index_to_merge_in \
                        #     = self.smoothing_around_solid_block(b_index, block)
                        uptream_terminal_alternative_bound, uptream_index_BA_to_move_in_BSJP, uptream_BA_to_move_in_alternative_bound = self.uptream_BSJP_smoothing( block, b_index)

                        if len(uptream_index_BA_to_move_in_BSJP) > 0:
                            self.newJS_merging_smoothing_out_blocks(False, b_index,
                                                                    uptream_index_BA_to_move_in_BSJP, exon)

                        if len(uptream_BA_to_move_in_alternative_bound) > 0:
                            self.newJS_merging_smoothing_out_blocks(False, uptream_terminal_alternative_bound,
                                                                    uptream_BA_to_move_in_alternative_bound, exon)

                    for blok in exon.get_blocks():
                        blok.update_junction_points()
            else:
                continue

        self.export_to_xlsx(
            output_filename=f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}strange_compact1.xlsx")

        # for exon in self.predicted_exons:
        #     # if self.Is_there_a_solid_block_in_the_exon(exon):
        #     self.find_max_leaving_entering_junction_around_solid_block(exon)
        # self.chaining_blocks()
        # self.export_to_xlsx(output_filename=f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}strange_compact2.xlsx")

        self.chaining_blocks()

    def ACM_border_smoothing_old(self):

        self.merging_solid_blocks()
        self.chaining_blocks()
        self.export_to_xlsx(output_filename=f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}strange_compact0.xlsx")
        for exon in self.predicted_exons:
            if self.is_block_solid_in_exon(exon):
                for b_index, block in enumerate(exon.get_blocks()):
                    if block.is_solid() and self.Is_next_contiguous_block_uncertain(current_block=block):#or block.is_semi_solid():
                        # if block.is_surrounded_by_semi_solid():
                        downstream_terminal, downstream_index_to_merge_out, downstream_index_to_merge_in = self.downstream_smoothing(
                            b_index, block)
                        if len(downstream_index_to_merge_out) > 0:
                            self.newJS_merging_smoothing_out_blocks(True, downstream_terminal, downstream_index_to_merge_out, exon)
                        if len(downstream_index_to_merge_in)>1: # >1 because the first index is the solid and the additional index is merged in
                            self.merging_smoothing_blocks(True, downstream_terminal, downstream_index_to_merge_in,exon)

                    if block.is_solid() and self.Is_previous_contiguous_block_uncertain(current_block=block):  # or block.is_semi_solid():

                        # downstream_terminal, downstream_index_to_merge_out, downstream_index_to_merge_in, \
                        #     upstream_terminal, upstream_index_to_merge_out, upstream_index_to_merge_in \
                        #     = self.smoothing_around_solid_block(b_index, block)
                        upstream_terminal, upstream_index_to_merge_out, upstream_index_to_merge_in = self.upstream_smoothing(
                            b_index, block)

                        if len(upstream_index_to_merge_in)>1:
                            self.merging_smoothing_blocks(False, upstream_terminal, upstream_index_to_merge_in,exon)
                            # update terminal index and  downstream_index_to_merge_out to compensate for list of block and therefore index modification

                        if len(upstream_index_to_merge_out) > 0:
                            self.newJS_merging_smoothing_out_blocks(False, upstream_terminal, upstream_index_to_merge_out, exon)

                    for blok in exon.get_blocks():
                        blok.update_junction_points()
            else:
                continue

        self.export_to_xlsx(output_filename=f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}strange_compact1.xlsx")

        # for exon in self.predicted_exons:
        #     # if self.Is_there_a_solid_block_in_the_exon(exon):
        #     self.find_max_leaving_entering_junction_around_solid_block(exon)
        # self.chaining_blocks()
        # self.export_to_xlsx(output_filename=f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}strange_compact2.xlsx")

        self.chaining_blocks()

    # def ACM_border_smoothing_new(self):
    #     self.chaining_blocks()
    #     for exon in self.predicted_exons:
    #         if self.Is_there_several_contiguous_SOLID_blocks_in_exon():
    #             self.merging_solid_blocks()
    #         if self.Is_there_several_SOLID_blocks_in_exon():

    def ACM_border_smoothing_old(self):
        self.chaining_blocks()
        self.export_to_xlsx(output_filename=f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}strange_compact0.xlsx")
        for exon in self.predicted_exons:
            if self.is_block_solid_in_exon(exon):
                for b_index, block in enumerate(exon.get_blocks()):
                    if block.is_solid() and block.get_previous_block():#or block.is_semi_solid():
                        # if block.is_surrounded_by_semi_solid():

                        downstream_terminal, downstream_index_to_merge_out, downstream_index_to_merge_in, \
                            upstream_terminal, upstream_index_to_merge_out, upstream_index_to_merge_in \
                            = self.smoothing_around_solid_block(b_index, block)

                        if len(downstream_index_to_merge_in)>1:
                            print('ici')
                            self.merging_smoothing_blocks(True, downstream_terminal, downstream_index_to_merge_in,exon)
                            # update downstream_index_to_merge_out to compensate for list of block and therefore index modification
                            if len(downstream_index_to_merge_out) > 0:
                                 downstream_index_to_merge_out = [x - (len(downstream_index_to_merge_in)) for x in downstream_index_to_merge_out]

                        if len(upstream_index_to_merge_in)>1:
                            self.merging_smoothing_blocks(False, upstream_terminal, upstream_index_to_merge_in,exon)
                            # update terminal index and  downstream_index_to_merge_out to compensate for list of block and therefore index modification
                            if len(downstream_index_to_merge_out) > 0:
                                downstream_index_to_merge_out = [x - (downstream_terminal - upstream_terminal) for x in downstream_index_to_merge_out]
                            downstream_terminal = upstream_terminal
                        self.chaining_blocks()
                        if len(downstream_index_to_merge_out) > 0:
                            print('la bas')
                            self.newJS_merging_smoothing_out_blocks(True, downstream_terminal, downstream_index_to_merge_out, exon)
                        if len(upstream_index_to_merge_out) > 0:
                            self.newJS_merging_smoothing_out_blocks(False, upstream_terminal, upstream_index_to_merge_out, exon)

                    for blok in exon.get_blocks():
                        blok.update_junction_points()
            else:
                continue

        self.export_to_xlsx(output_filename=f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}strange_compact1.xlsx")

        for exon in self.predicted_exons:
            # if self.Is_there_a_solid_block_in_the_exon(exon):
            self.find_max_leaving_entering_junction_around_solid_block(exon)
        self.chaining_blocks()
        self.export_to_xlsx(output_filename=f"{self.get_binary_matrix().get_geneId()}/{self.get_binary_matrix().get_geneId()}strange_compact2.xlsx")

        self.chaining_blocks()

    def is_block_solid_in_exon(self, exon : PredictedExon):
        for b in exon.get_blocks():
            if b.is_solid():
                return True
        return False

    def merging_smoothing_blocks(self,downstream : bool, terminal_index : int, index_to_merge_in : list,exon : PredictedExon):
        print("here")
        print(exon)
        print(index_to_merge_in)
        print(exon.get_blocks()[index_to_merge_in[0]])
        print(exon.get_blocks()[index_to_merge_in[1]])
        merged_smoothing_block = self.fusion_blocks([exon.get_blocks()[index_to_merge_in[0]],
                                                      exon.get_blocks()[index_to_merge_in[1]]])
        if len(index_to_merge_in)>2:
            for index in index_to_merge_in[2:]:
                merged_smoothing_block = self.fusion_blocks([merged_smoothing_block, exon.get_blocks()[index]])
        if downstream == True:
            merged_smoothing_block.end = exon.get_blocks()[terminal_index].get_end()
        else:
            merged_smoothing_block.begin = exon.get_blocks()[terminal_index].get_begin()
        exon.replace_blocks([index_to_merge_in[0], index_to_merge_in[-1]], merged_smoothing_block)

    def merging_semisolid_blocks(self,downstream : bool, terminal_index : int, index_to_merge_in : list,exon : PredictedExon):
        print("here")
        print(exon)
        print(index_to_merge_in)
        print(exon.get_blocks()[index_to_merge_in[0]])
        print(exon.get_blocks()[index_to_merge_in[1]])
        merged_smoothing_block = self.fusion_blocks([exon.get_blocks()[index_to_merge_in[0]],
                                                      exon.get_blocks()[index_to_merge_in[1]]])
        merged_smoothing_block.set_block_status(BlockStatus.SEMI_SOLID)
        if len(index_to_merge_in)>2:
            for index in index_to_merge_in[2:]:
                merged_smoothing_block = self.fusion_blocks([merged_smoothing_block, exon.get_blocks()[index]])
        if downstream == True:
            merged_smoothing_block.end = exon.get_blocks()[terminal_index].get_end()
        else:
            merged_smoothing_block.begin = exon.get_blocks()[terminal_index].get_begin()
        exon.replace_blocks([index_to_merge_in[0], index_to_merge_in[-1]], merged_smoothing_block)
    def merging_smoothing_out_blocks(self, downstream : bool ,terminal_index : int, index_to_merge_out : list,
                                     exon : PredictedExon):
        if index_to_merge_out: # if list is not empty
            if downstream == True: # start from first element
                # pour les reads dans ce bloc, si le read appartient au bloc suivant
                for block_index_out in index_to_merge_out:  # Itérez sur chaque index
                    read_to_remove = []
                    for read in exon.get_blocks()[block_index_out].get_reads_name():
                        block_index_to_remove=[]
                        if exon.get_blocks()[block_index_out].get_next_block() is not None:
                            if not exon.get_blocks()[block_index_out].get_next_block().contain_read(read):
                                list_of_ba = [exon.get_blocks()[terminal_index].get_block_alignment(read)]
                                block_index_to_remove.append(block_index)
                                for block_index in index_to_merge_out:
                                    list_of_ba.append(exon.get_blocks()[block_index].get_block_alignment(read))
                                new_BA = self.smoothing_fusion_block_alignments(list_of_ba, exon.get_blocks()[terminal_index].block_alignments[read])
                                sorted_ba_list = sorted(list_of_ba, key=lambda ba: ba.reference_begin)

                                exon.get_blocks()[terminal_index].block_alignments[read].reference_sequence = "".join(
                                    [ba.reference_sequence for ba in sorted_ba_list])
                                exon.get_blocks()[terminal_index].block_alignments[read].read_sequence = "".join([ba.read_sequence for ba in sorted_ba_list])
                                exon.get_blocks()[terminal_index].block_alignments[read].read_begin = sorted_ba_list[0].read_begin
                                exon.get_blocks()[terminal_index].block_alignments[read].read_end = sorted_ba_list[-1].read_end
                                read_to_remove.append(read)

                                    # supprimer les BA outter

                    for read in read_to_remove:
                        for block_index in block_index_to_remove:
                            exon.get_blocks()[block_index].remove_read(read)


            else: # start from last element
                reversed_index_out = reversed(index_to_merge_out)  # Inversez l'ordre des indices

                for block_index_out in reversed_index_out:  # Itérez sur chaque index dans le sens inverse
                    read_to_remove = []

                    for read in exon.get_blocks()[block_index_out].get_reads_name():
                        block_index_to_remove = []
                        if exon.get_blocks()[block_index_out].get_previous_block() is not None:
                            if not exon.get_blocks()[block_index_out].get_previous_block().contain_read(read):
                                list_of_ba = [exon.get_blocks()[terminal_index].get_block_alignment(read)]

                                for block_index in reversed(index_to_merge_out):
                                    if exon.get_blocks()[block_index].get_block_alignment(read) is not None:
                                        list_of_ba.append(exon.get_blocks()[block_index].get_block_alignment(read))
                                        block_index_to_remove.append(block_index)

                                    new_BA = self.smoothing_fusion_block_alignments(list_of_ba,exon.get_blocks()[terminal_index].block_alignments[read])
                                    exon.get_blocks()[terminal_index].block_alignments[read] = new_BA
                                    read_to_remove.append(read)

                    for read in read_to_remove:
                        for block_index in block_index_to_remove:
                            exon.get_blocks()[block_index].remove_read(read)






    def newJS_merging_smoothing_out_blocks(self, downstream : bool ,terminal_index : int, index_to_merge_out : list,
                                     exon : PredictedExon):

        # Find the reads to be removed
        # TODO UNLESS THEY ARE NOT IN THE corresponding solid block
        reads_to_be_remove = self.find_reads_to_be_removed(downstream , exon, terminal_index, index_to_merge_out)
        # Find most upstream block containing read
        # dict_most_extreme = self.find_extreme_coordinate_of_read_to_remove(downstream, reads_to_be_remove ,index_to_merge_out)
        # Calculate new BA and insert it
        self.insert_new_BA(reads_to_be_remove, exon, index_to_merge_out, terminal_index)
        # Delete read to remove from blocks_index_out
        self.remove_read_from_the_blocks( exon, index_to_merge_out, reads_to_be_remove)



    def remove_read_from_the_blocks(self,exon, index_to_merge_out, reads_to_be_remove):
        for index in index_to_merge_out:
            for read in reads_to_be_remove:
                if exon.get_blocks()[index].contain_read(read):
                    exon.get_blocks()[index].remove_read(read)

    def find_reads_to_be_removed(self,downstream : bool , exon, terminal_index, index_to_merge_out):
        if downstream:
            reads_to_remove = []
            # If the downstream index is the last block of the exon and is to be merged out
            # then all the reads it contains are to be removed
            if index_to_merge_out[-1] == len(exon.get_blocks())-1:
                for index in index_to_merge_out:
                    reads_to_remove.extend(exon.get_blocks()[index].get_reads_name())
                # using list() to create a copy of the list so it doesn't affect iteration
                for r in list(reads_to_remove):
                    if not exon.get_blocks()[terminal_index].contain_read(r):
                        reads_to_remove.remove(r)
                return reads_to_remove
            else:

                # Union des reads de l'intervalles merged_out
                union_merged_out_reads = []
                # print(exon)
                # print(index_to_merge_out)
                # print(len(exon.get_blocks()))
                for index in index_to_merge_out:
                    union_merged_out_reads.extend(exon.get_blocks()[index].get_reads_name())
                union_merged_out_reads = list(set(union_merged_out_reads))
                reads_to_discard = exon.get_blocks()[index_to_merge_out[-1] + 1].get_reads_name()
                union_merged_out_reads = [read for read in union_merged_out_reads if read not in reads_to_discard]
                read_to_keep = exon.get_blocks()[terminal_index].get_reads_name()
                return [read for read in union_merged_out_reads if read in read_to_keep]
        else:
            reads_to_remove = []
            # If the upstream index is the first block of the exon and is to be merged out
            # then all the reads it contains are to be removed
            if index_to_merge_out[0] == 0:
                for index in index_to_merge_out:
                    reads_to_remove.extend(exon.get_blocks()[index].get_reads_name())
                    for r in list(reads_to_remove):
                        if not exon.get_blocks()[terminal_index].contain_read(r):
                            reads_to_remove.remove(r)
                return reads_to_remove
            else:
                union_merged_out_reads = []
                for index in index_to_merge_out:
                    union_merged_out_reads.extend(exon.get_blocks()[index].get_reads_name())
                union_merged_out_reads = list(set(union_merged_out_reads))
                reads_to_discard = exon.get_blocks()[index_to_merge_out[0] - 1].get_reads_name()
                union_merged_out_reads = [read for read in union_merged_out_reads if read not in reads_to_discard]
                read_to_keep = exon.get_blocks()[terminal_index].get_reads_name()
                reads_to_remove = [read for read in union_merged_out_reads if read  in read_to_keep]
                return reads_to_remove


    def find_extreme_coordinate_of_read_to_remove(self, downstream : bool, reads_to_be_removed : [],index_to_merge_out):
        if downstream:
            dict_most_extreme = {}
            for read in reads_to_be_removed:
                for index in index_to_merge_out:
                    if read in dict_most_extreme.keys():
                        if dict_most_extreme[read] < index:
                            dict_most_extreme[read] = index
                    else:
                        dict_most_extreme[read] = index
        else:
            dict_most_extreme = {}
            for read in reads_to_be_removed:
                for index in index_to_merge_out:
                    if read in dict_most_extreme.keys():
                        if dict_most_extreme[read] > index:
                            dict_most_extreme[read] = index
                    else:
                        dict_most_extreme[read] = index
        return dict_most_extreme


    def insert_new_BA(self, reads_to_be_removed, exon, index_to_merge_out, terminal_index):
        # print(f"reads_to_be_removed {reads_to_be_removed}\n"
        #       f"index_to_merge_out {index_to_merge_out}\n"
        #       f"terminal_index {terminal_index}\n"
        #       f"block terminal index {exon.get_blocks()[terminal_index]}")

        for read in reads_to_be_removed:
            list_of_ba = []
            list_of_ba = [exon.get_blocks()[terminal_index].block_alignments[read]]
            for index in index_to_merge_out:
                if exon.get_blocks()[index].contain_read(read):
                    list_of_ba.append(exon.get_blocks()[index].block_alignments[read])
            new_BA = None
            new_BA = self.smoothing_fusion_block_alignments(list_of_ba, exon.get_blocks()[terminal_index].block_alignments[read])
            exon.get_blocks()[terminal_index].block_alignments[read] = new_BA
            exon.get_blocks()[terminal_index].reads_positions[read] = (new_BA.read_begin,new_BA.read_end)

    def smoothing_around_solid_block(self, b_index : int, block : Block):
        assert block.is_solid(), "selected Block is not solid. Smoothing Error."
        # self.downstream_smoothing(b_index, block)
        downstream_terminal, downstream_index_to_merge_out, downstream_index_to_merge_in = self.downstream_smoothing(b_index, block)

        upstream_terminal, upstream_index_to_merge_out, upstream_index_to_merge_in = self.upstream_smoothing(b_index , block)
        return downstream_terminal, downstream_index_to_merge_out, downstream_index_to_merge_in,\
            upstream_terminal, upstream_index_to_merge_out, upstream_index_to_merge_in


    def fusion_block_alignment_for_merging(self, ba_list: [BlockAlignment], downstream : bool) -> BlockAlignment:
        '''
        Return the merged BlockAlignment
        '''
        sorted_ba_list = sorted(ba_list, key=lambda x: x.begin)
        reference_name = sorted_ba_list[0].reference_name
        reference_sequence = "".join([ba.reference_sequence for ba in sorted_ba_list])
        reference_begin = sorted_ba_list[0].reference_begin
        reference_end = sorted_ba_list[-1].reference_end
        read_name = sorted_ba_list[0].read_name
        read_sequence = "".join([ba.read_sequence for ba in sorted_ba_list])
        read_begin = sorted_ba_list[0].read_begin
        read_end = sorted_ba_list[-1].read_end
        return BlockAlignment(reference_name, reference_sequence, reference_begin, reference_end, read_name,
                              read_sequence, read_begin, read_end)


    def find_fragile_block_upstream(self, solid_block : Block, solid_block_index : int):
        current_block = solid_block.get_previous_block()
        max_fragile_block = None
        max_fragile_block_index = solid_block_index - 1

        while current_block is not None and current_block.is_fragile() \
                and self.Are_blocks_contiguous(current_block.get_previous_block,current_block):
            if current_block.entering_junction_point >= solid_block.entering_junction_point:
                if max_fragile_block is None or current_block.entering_jp > max_fragile_block.entering_jp:
                    max_fragile_block = current_block
                    max_fragile_block_index = current_block.get_index()
            current_block = current_block.get_previous_block()

        return max_fragile_block, max_fragile_block_index




    # def find_max_leaving_entering_junction_around_solid_block(self, exon : PredictedExon):
    #     block_number = -1
    #     solid_block_index = 0
    #     block_number_max_jp = None
    #     for block in exon.get_blocks():
    #         block_number += 1
    #         if block.is_solid():
    #             solid_block_index = block_number
    #             max_entering_jp = block.get_entering_junction_point().get_read_amount()
    #             block_number_max_jp = block_number
    #             current_block = block
    #             if current_block.get_previous_block() is not None :
    #                 while self.Are_blocks_contiguous(current_block.get_previous_block(),current_block) and current_block.get_previous_block().is_uncertain():
    #                     if current_block.get_previous_block().get_entering_junction_point().get_read_amount() >= max_entering_jp:
    #                         max_entering_jp = current_block.get_previous_block().get_entering_junction_point().get_read_amount()
    #                         block_max_jp = current_block.get_previous_block()
    #                         block_number_max_jp-=1
    #                     if current_block.get_previous_block() is not None:
    #                         current_block=current_block.get_previous_block()
    #                         if current_block.get_previous_block() is not None:
    #                             continue
    #                         else:
    #                             break
    #                     else:
    #                         break
    #             # if block_number_max_jp < block_number:
    #             #     for block_index in range(block_number, block_number_max_jp):
    #             #         exon.get_blocks()[block_index].status = BlockStatus.SEMI_SOLID
    #
    #             # if block_number_max_jp < block_number:
    #             #     for block_index in range(block_number_max_jp ,block_number ):
    #             #         self.merging_smoothing_blocks(True, block_number_max_jp, [block_number_max_jp, block_number],exon)
    #
    #             if block_number_max_jp is not None:
    #                 if block_number_max_jp < solid_block_index:
    #                     if block_number_max_jp +1 < solid_block_index:
    #                         for block_index in range(block_number_max_jp, block_number):
    #                             self.merging_semisolid_blocks(False, block_number_max_jp, [block_number_max_jp,solid_block_index-1],exon)
    #                     else:
    #                         exon.get_blocks()[block_number_max_jp].status = BlockStatus.SEMI_SOLID
    #
    #     block_number = -1
    #
    #     for block in exon.get_blocks():
    #         block_number += 1
    #         if block.is_solid():
    #             solid_block_number = block_number
    #             max_leaving_jp = block.get_leaving_junction_point()
    #             block_max_ljp = block
    #             block_number_max_ljp = block_number
    #             current_block = block
    #             if current_block.get_next_block() is not None :
    #                 while current_block.get_next_block().is_uncertain() and self.Are_blocks_contiguous(current_block, current_block.get_next_block()):
    #                     if current_block.get_next_block().get_entering_junction_point().get_read_amount() >= block_max_ljp.get_leaving_junction_point().get_read_amount():
    #                         # max_leaving_jp = current_block.get_next_block().get_entering_junction_point().get_read_amount()
    #                         block_max_ljp = current_block.get_next_block()
    #                         block_number_max_ljp+=1
    #                         current_block=current_block.get_next_block()
    #
    #                     elif current_block.get_next_block() is not None:
    #                         current_block = current_block.get_next_block()
    #                         if current_block.get_next_block() is not None:
    #                             continue
    #                         else:
    #                             break
    #
    #                     else:
    #                         break
    #             # if block_number_max_ljp > block_number:
    #             #     for block_index in range(block_number, block_number_max_ljp):
    #             #         exon.get_blocks()[block_index].status = BlockStatus.SEMI_SOLID
    #             # if block_number_max_ljp > block_number+1:
    #             #     self.merging_semisolid_blocks(True, block_number_max_ljp, [block_number+1, block_number_max_ljp], exon)
    #             # if block_number_max_jp < block_number:
    #             #     print('la bas')
    #             #     self.merging_smoothing_blocks(False, block_number_max_jp, [block_number_max_jp,block_number],exon)
    #
    #             if block_number_max_ljp > block_number:
    #                 if block_number_max_ljp -1 > block_number:
    #                     print(exon.get_blocks())
    #                     print("merging")
    #                     print(exon.get_blocks()[block_number+1])
    #                     print("block_number_max_ljp")
    #                     print(block_number_max_ljp)
    #                     print(exon.get_blocks()[block_number_max_ljp])
    #                     print("tag234")
    #
    #
    #                     self.merging_semisolid_blocks(False, block_number_max_ljp, [block_number+1,block_number_max_ljp],exon)
    #                 else:
    #                     exon.get_blocks()[block_number_max_ljp].status = BlockStatus.SEMI_SOLID

                # if block_number_max_ljp > solid_block_index:
                #     if block_number_max_ljp -1 > solid_block_index:
                #         for block_index in range(solid_block_index+1, block_number_max_ljp):
                #             self.merging_semisolid_blocks(False, block_number_max_jp, [solid_block_index+1, block_number_max_ljp],exon)
                #     else:
                #         exon.get_blocks()[block_number_max_jp].status = BlockStatus.SEMI_SOLID

    def find_max_leaving_entering_junction_around_solid_block(self, exon : PredictedExon):
        block_number = -1
        for block in exon.get_blocks():
            block_number += 1
            if block.is_solid():
                max_entering_jp = block.get_entering_junction_point().get_read_amount()
                block_number_max_jp = block_number
                current_block_index = block_number
                current_block = block
                if current_block.get_previous_block() is not None and not current_block.get_previous_block().is_solid():
                    while self.Are_blocks_contiguous(current_block.get_previous_block(),current_block) and current_block.get_previous_block().is_uncertain():
                        if current_block.get_previous_block().get_entering_junction_point().get_read_amount() >= max_entering_jp:
                            max_entering_jp = current_block.get_previous_block().get_entering_junction_point().get_read_amount()
                            block_max_jp = current_block.get_previous_block()
                            current_block_index-=1
                            block_number_max_jp = current_block_index
                            current_block = current_block.get_previous_block()
                        else:
                            if current_block.get_previous_block() is not None:
                                current_block = current_block.get_previous_block()
                                current_block_index -= 1
                                if current_block.get_previous_block().is_solid():
                                    break
                                else:
                                    continue

                            else:
                                break
                if block_number_max_jp < block_number-1:
                    self.merging_semisolid_blocks(False, block_number_max_jp, [block_number_max_jp,block_number-1],exon)
                elif block_number_max_jp < block_number:
                    exon.get_blocks()[block_number_max_jp].set_block_status(BlockStatus.SEMI_SOLID)



                max_leaving_jp = block.get_leaving_junction_point()
                block_max_ljp = block
                block_number_max_ljp = block_number
                current_block_index = block_number
                current_block = block
                if current_block.get_next_block() is not None :
                    while current_block.get_next_block().is_uncertain() and self.Are_blocks_contiguous(current_block, current_block.get_next_block()):
                        if current_block.get_next_block().get_entering_junction_point().get_read_amount() >= block_max_ljp.get_leaving_junction_point().get_read_amount():
                            # max_leaving_jp = current_block.get_next_block().get_entering_junction_point().get_read_amount()
                            block_max_ljp = current_block.get_next_block()
                            current_block_index+=1
                            block_number_max_ljp = current_block_index
                            current_block=current_block.get_next_block()

                        elif current_block.get_next_block() is not None:
                            current_block = current_block.get_next_block()
                            if current_block.get_next_block() is not None:
                                continue
                            else:
                                break

                        else:
                            break
                if block_number_max_jp > block_number +1:
                    self.merging_semisolid_blocks(True, block_number_max_ljp, [block_number +1, block_number_max_ljp],[],exon)
                elif block_number_max_jp > block_number:
                    exon.get_blocks()[block_number_max_jp].set_block_status(BlockStatus.SEMI_SOLID)

    # def downstream_smoothing(self,  b_index : int, block : Block) :
    #     current_block = block
    #     smoothed_interval = 0
    #     block_index_to_merge = [b_index]
    #     block_index_to_merge_out = []
    #     if not self.should_smooth_block(current_block,True):
    #         return b_index, [], []
    #
    #     while current_block.get_next_block() is not None and \
    #             current_block.get_next_block().get_length() <= config.MAX_SMOOTHING_LEN \
    #             and self.Are_blocks_contiguous(current_block, current_block.get_next_block()):
    #         b_index=b_index+1
    #         smoothed_interval = smoothed_interval + current_block.get_next_block().get_length()
    #         if smoothed_interval <= config.MAX_SMOOTHING_LEN:
    #             if block.get_population()*config.THRESHOLD_SMOOTHING_RATIO <= current_block.get_population():
    #                 block_index_to_merge.append(b_index)
    #             else:
    #                 block_index_to_merge_out.append(b_index)
    #         else:
    #             break
    #
    #         current_block = current_block.get_next_block()
    #     downstream_terminal = block_index_to_merge[-1]
    #     return downstream_terminal, block_index_to_merge, block_index_to_merge_out

    def upstream_BSJP_smoothing(self, block_solid_entering_jp :Block, block_solid_entering_jp_index:int):
        current_block = block_solid_entering_jp
        smoothed_interval = 0
        block_index_BA_to_move_in_BSJP = []  # BSJP = Block with Solid Junction Point
        block_index_BA_to_move_in_alternative_bound = []
        max_entering_splice_junction = 0
        initial_block_index = block_solid_entering_jp_index
        b_index = block_solid_entering_jp_index
        max_entering_splice_junction_index = None
        if not self.should_smooth_block(current_block, False):
            return block_solid_entering_jp, [], []
        # while a block has a next one
        # his length is below the smoothing limit
        # and the blocks are contiguous
        while current_block.get_previous_block() is not None and \
                current_block.get_previous_block().get_length() <= config.MAX_SMOOTHING_LEN and \
                self.Are_blocks_contiguous(current_block, current_block.get_previous_block()):
            b_index = b_index - 1
            smoothed_interval = smoothed_interval + current_block.get_previous_block().get_length()
            if current_block.get_previous_block().get_entering_splice_junction_point() > 3:
                if max_entering_splice_junction < current_block.get_previous_block().get_entering_splice_junction_point():
                    max_entering_splice_junction = current_block.get_previous_block().get_entering_splice_junction_point()
                    max_entering_splice_junction_index = b_index
            # else:
            #     block_index_to_merge.append(b_index)
            if smoothed_interval <= config.MAX_SMOOTHING_LEN:
                block_index_BA_to_move_in_BSJP[0:0]=b_index
            else:
                break

            current_block = current_block.get_previous_block()
        # downstream_terminal = block_index_to_merge[-1]
        if max_entering_splice_junction_index is None:
            max_entering_splice_junction_index = initial_block_index
        else:
            # if the max_entering_splice_junction_index bound is the first block, then there is no BA to move into it:
            if max_entering_splice_junction_index == block_index_BA_to_move_in_BSJP[0]:
                block_index_BA_to_move_in_alternative_bound = []
                return max_entering_splice_junction_index, block_index_BA_to_move_in_BSJP, block_index_BA_to_move_in_alternative_bound
            else:
                block_index_BA_to_move_in_alternative_bound = block_index_BA_to_move_in_BSJP[
                                                              (max_entering_splice_junction_index + 1)::]
                block_index_BA_to_move_in_BSJP = block_index_BA_to_move_in_BSJP[0:max_entering_splice_junction_index]
        return max_entering_splice_junction_index, block_index_BA_to_move_in_BSJP, block_index_BA_to_move_in_alternative_bound


    def upstream_SJP_smoothing(self, block_solid_entering_jp: Block, block_solid_entering_jp_index: int):
        current_block = block_solid_entering_jp
        smoothed_interval = 0
        block_index_BA_to_move_in_BSJP = []  # block_solid_leaving_jp
        initial_block_index = block_solid_entering_jp_index
        b_index = block_solid_entering_jp_index
        if not self.should_smooth_block(current_block, False):
            return []
        # while a block has a previous one
        # his length is below the smoothing limit
        # and the blocks are contiguous
        while current_block.get_previous_block() is not None and \
                current_block.get_previous_block().get_length() <= config.MAX_SMOOTHING_LEN and \
                self.Are_blocks_contiguous(current_block.get_previous_block(), current_block ) and not \
                current_block.get_previous_block().Is_entering_splice_junction_Solid():
                # current_block.get_previous_block().get_entering_junction_point().is_solid() and not \

            b_index = b_index - 1
            smoothed_interval = smoothed_interval + current_block.get_previous_block().get_length()
            current_block = current_block.get_previous_block()
            # block_index_to_merge.append(b_index)
            if smoothed_interval <= config.MAX_SMOOTHING_LEN:
                block_index_BA_to_move_in_BSJP.append(b_index)
            else:
                break
            if current_block.get_previous_block() is None:
                break
        return block_index_BA_to_move_in_BSJP
    def downstream_SJP_smoothing(self, block_solid_leaving_jp: Block, block_solid_leaving_jp_index: int):
        current_block = block_solid_leaving_jp
        smoothed_interval = 0
        block_index_BA_to_move_in_BSJP = []  # block_solid_leaving_jp
        initial_block_index = block_solid_leaving_jp_index
        b_index = block_solid_leaving_jp_index
        if not self.should_smooth_block(current_block, True):
            return []
        # while a block has a next one
        # his length is below the smoothing limit
        # and the blocks are contiguous
        while current_block.get_next_block() is not None and \
                current_block.get_next_block().get_length() <= config.MAX_SMOOTHING_LEN and \
                self.Are_blocks_contiguous(current_block, current_block.get_next_block()) and not \
                current_block.get_next_block().Is_leaving_splice_junction_Solid():
            # current_block.get_next_block().get_leaving_junction_point().is_solid() and not \

            b_index = b_index + 1
            smoothed_interval = smoothed_interval + current_block.get_next_block().get_length()
            current_block = current_block.get_next_block()
            #     block_index_to_merge.append(b_index)
            if smoothed_interval <= config.MAX_SMOOTHING_LEN:
                block_index_BA_to_move_in_BSJP.append(b_index)
            else:
                break
            if current_block.get_next_block() is None:
                break
        return block_index_BA_to_move_in_BSJP

    # def downstream_SJP_GT_signals_smoothing(self, block_solid_leaving_jp: Block, block_solid_leaving_jp_index: int):
    #     '''
    #     Within the smoothing interval, is there any other solid splice junction?
    #     If there is then, is there any supported by a GT acceptor signal ? (those are kept)
    #     Is there any not supported by a signal ? Those are modified.
    #     Depending on the contextual structure of the blocks, they can either be merged in the next block, or there
    #     BlockAlignment moved to the previous block.
    #     '''
    #     current_block = block_solid_leaving_jp
    #     smoothed_interval = 0
    #     block_index_BA_to_move_in_BSJP = []  # block_solid_leaving_jp
    #     initial_block_index = block_solid_leaving_jp_index
    #     b_index = block_solid_leaving_jp_index
    #     if not self.should_smooth_block(current_block, True):
    #         return []
    #     # while a block has a next one
    #     # his length is below the smoothing limit
    #     # and the blocks are contiguous
    #     while current_block.get_next_block() is not None and \
    #             current_block.get_next_block().get_length() <= config.MAX_SMOOTHING_LEN and \
    #             self.Are_blocks_contiguous(current_block, current_block.get_next_block()):
    #         if current_block.get_next_block().Is_leaving_splice_junction_Solid() or D:
    #         # current_block.get_next_block().get_leaving_junction_point().is_solid() and not \
    #
    #         b_index = b_index + 1
    #         smoothed_interval = smoothed_interval + current_block.get_next_block().get_length()
    #         current_block = current_block.get_next_block()
    #         #     block_index_to_merge.append(b_index)
    #         if smoothed_interval <= config.MAX_SMOOTHING_LEN:
    #             block_index_BA_to_move_in_BSJP.append(b_index)
    #         else:
    #             break
    #         if current_block.get_next_block() is None:
    #             break
    #     return block_index_BA_to_move_in_BSJP


    def smoothing_solid_GeneJunctionPoint(self):
        self.compute_read_junction_points()
        self.__compute_splice_junction_points()
        for exon in self.get_predicted_exons():
            for block in exon.get_blocks():
                print(f" get_entering_junction_point {block.get_entering_junction_point().get_read_amount()}")
                print(f" get_leaving_junction_point {block.get_leaving_junction_point().get_read_amount()}")
                print(f" get_entering_splice_junction_point {block.get_entering_splice_junction_point()}")
                print(f" get_leaving_splice_junction_point {block.get_leaving_splice_junction_point()}")

            for couple_of_Solid_Blocks in self.find_solid_blocks_bounds(exon):
                #for all the blocks upstream from the Entering Solid block and until a Leaving Solid block is met
                for block_with_SEGJP in [couple_of_Solid_Blocks[0]]+self.find_entering_solid_gene_junction_point_upstream_from_Sblock(couple_of_Solid_Blocks[0]):
                    block_index_BA_to_move_in_BSEJP=self.upstream_SJP_smoothing(block_with_SEGJP,exon.get_block_index_from_block(block_with_SEGJP))


                    if len(block_index_BA_to_move_in_BSEJP) > 0:
                        self.newJS_merging_smoothing_out_blocks(downstream=False,terminal_index=exon.get_block_index_from_block(block_with_SEGJP),
                                                                index_to_merge_out= block_index_BA_to_move_in_BSEJP, exon= exon)
                for block_with_SLGJP in [couple_of_Solid_Blocks[-1]]+self.find_leaving_solid_gene_junction_point_downstream_from_Sblock(couple_of_Solid_Blocks[-1]):
                    block_index_BA_to_move_in_BSLJP=self.downstream_SJP_smoothing(block_with_SLGJP,exon.get_block_index_from_block(block_with_SLGJP))
                    print(block_index_BA_to_move_in_BSLJP)
                    for b in block_index_BA_to_move_in_BSLJP:
                        print(exon.get_blocks()[b])
                    if len(block_index_BA_to_move_in_BSLJP) > 0:
                        print("terminal index")
                        print(exon.get_block_index_from_block(block_with_SLGJP))
                        self.newJS_merging_smoothing_out_blocks(downstream=True,terminal_index=exon.get_block_index_from_block(block_with_SLGJP),
                                                                index_to_merge_out= block_index_BA_to_move_in_BSLJP, exon= exon)


    def find_solid_blocks_bounds(self, exon : PredictedExon) -> [[Block,Block]]:
        '''
        Returns the list of couples [First_BlockSolid, LastBlockSolid] for each group of solid blocks
        '''
        solid_bounds = []
        entering_solid_block = None
        found_Entering_Solid_Block = False
        for b in exon.get_blocks():
            if b.is_solid() and b.get_entering_junction_point().is_solid():
                entering_solid_block = b
                found_Entering_Solid_Block = True
            if found_Entering_Solid_Block and b.is_solid() and b.get_leaving_junction_point().is_solid():
                found_Entering_Solid_Block = False
                solid_bounds.append([entering_solid_block,b])
                entering_solid_block = None
        return solid_bounds

    def find_entering_solid_gene_junction_point_upstream_from_Sblock(self, bloc : Block):
        '''
        Find all the block containg a solid_entering_gene_junction_point and situated before a solid_leaving_gene_junction_point
        '''
        list_of_bloc_with_SEGJP = [] # solid_entering_gene_junction_point
        if bloc.get_previous_block() is not None:
            while self.Are_blocks_contiguous(bloc.get_previous_block(), bloc) and not bloc.get_previous_block().get_leaving_junction_point().is_solid():
                if bloc.get_previous_block().get_entering_junction_point().is_solid():
                    list_of_bloc_with_SEGJP.append(bloc)
                if bloc.get_previous_block() != None:
                    bloc = bloc.get_previous_block()
                    if bloc.get_previous_block() is None:
                        break
                else:
                    break
        return list_of_bloc_with_SEGJP


    def find_leaving_solid_gene_junction_point_downstream_from_Sblock(self, bloc: Block):
        list_of_bloc_with_SLGJP = []  # solid_entering_gene_junction_point
        if bloc.get_next_block() is not None:
            while self.Are_blocks_contiguous(bloc,
                                             bloc.get_next_block()) and not bloc.get_next_block().get_entering_junction_point().is_solid():
                if bloc.get_next_block().get_leaving_junction_point().is_solid():
                    list_of_bloc_with_SLGJP.append(bloc)
                if bloc.get_next_block() != None:
                    bloc = bloc.get_next_block()
                else:
                    break
        return list_of_bloc_with_SLGJP

    def downstream_smoothing_jdh(self, b_index: int, block: Block) -> list:
        current_block = block
        smoothed_interval = 0
        block_index_to_merge = [b_index]
        block_index_to_merge_out = []
        max_leaving_splice_junction = block.get_leaving_splice_junction_point()
        initial_block_index = b_index
        max_leaving_splice_junction_index = b_index
        if not self.should_smooth_block(current_block, True):
            return b_index, [], []

        # while a block has a next one
        # his length is below the smoothing limit
        # and the blocks are contiguous
        while current_block.get_next_block() is not None and \
                current_block.get_next_block().get_length() <= config.MAX_SMOOTHING_LEN and \
                self.Are_blocks_contiguous(current_block, current_block.get_next_block()):

            b_index = b_index + 1
            smoothed_interval = smoothed_interval + current_block.get_next_block().get_length()
            if current_block.get_next_block().get_leaving_splice_junction_point() > max_leaving_splice_junction:
                max_leaving_splice_junction = current_block.get_next_block().get_leaving_splice_junction_point()
                max_leaving_splice_junction_index = b_index

            if smoothed_interval <= config.MAX_SMOOTHING_LEN:
                if initial_block_index <  max_leaving_splice_junction_index :
                    block_index_to_merge.append(b_index)
                else:
                    block_index_to_merge_out.append(b_index)
            else:
                break

            current_block = current_block.get_next_block()
        downstream_terminal = block_index_to_merge[-1]
        return downstream_terminal, block_index_to_merge_out, block_index_to_merge


    def downstream_smoothing_old(self, b_index: int, block: Block) -> list:
        current_block = block
        smoothed_interval = 0
        block_index_to_merge = [b_index]
        block_index_to_merge_out = []
        if not self.should_smooth_block(current_block, True):
            return b_index, [], []

        # while a block has a next one
        # his length is below the smoothing limit
        # and the blocks are contiguous
        while current_block.get_next_block() is not None and \
                current_block.get_next_block().get_length() <= config.MAX_SMOOTHING_LEN and \
                self.Are_blocks_contiguous(current_block, current_block.get_next_block()):

            b_index = b_index + 1
            smoothed_interval = smoothed_interval + current_block.get_next_block().get_length()

            if smoothed_interval <= config.MAX_SMOOTHING_LEN:
                if block.get_population() * config.THRESHOLD_SMOOTHING_RATIO <= current_block.get_next_block().get_population():
                    block_index_to_merge.append(b_index)
                else:
                    block_index_to_merge_out.append(b_index)
            else:
                break

            current_block = current_block.get_next_block()
        downstream_terminal = block_index_to_merge[-1]
        return downstream_terminal, block_index_to_merge_out, block_index_to_merge
    def should_smooth_block(self, block: Block, downstream : bool) -> bool:

        if downstream:
            if block.get_next_block() is not None:
                if self.Are_blocks_contiguous(block,block.get_next_block()):
                    if block.get_next_block().get_length() <= config.MAX_SMOOTHING_LEN:
                        return True
        else:
            if block.get_previous_block() is not None:

                if self.Are_blocks_contiguous(block.get_previous_block(),block):
                    if block.get_previous_block().get_length() <= config.MAX_SMOOTHING_LEN:
                        return True
        return False

    def delete_empty_blocks(self):
        updated_exons = []
        for exon in self.get_predicted_exons():
            blocks = exon.get_blocks()
            blocks_to_keep = [block for block in blocks if block.get_population() != 0]

            if len(blocks_to_keep) > 0:
                exon.set_blocks(blocks_to_keep)
                updated_exons.append(exon)

        # update exon list if there was an empty exon
        self.set_predicted_exons(updated_exons)

    def update_predicted_exons(self, list_of_blocks : [Block]):
        dico_exon = {}
        for exon in self.get_predicted_exons():
            pass

        self.__build_predicted_exons(dico_exon)

    def set_predicted_exons(self, exon_list : [PredictedExon]):
        self.predicted_exons = exon_list
        self.set_predicted_exons_id()


    def fusion_similar_blocks(self):
        '''
        In each PredictedExon, groups of contiguous and similar Block
        '''
        for exon in self.get_predicted_exons():
            contiguous_groups = self.find_contiguous_blocks_with_same_reads(exon)
            print("contiguous_groups")
            print(contiguous_groups)
            for group in contiguous_groups:
                print(group)
                for exond in group:
                    print(exond)

            for group in contiguous_groups:
                if len(group)>1:
                    print("block with same reads : ")
                    for block_to_m in group:
                        print(block_to_m)
                    merged_block = self.merge_sublists(sorted(group, key = lambda x: x.get_begin()))
                    assert isinstance(merged_block, Block), "merged_block is not a Block object"
                    print(f"merged block {merged_block}")
                    self.replace_blocks_by_merged_block(merged_block, exon)


    def get_read_junction_points(self):
        return self.read_junction_points


    def do_filtering(self, redemption : bool):
        from exon_matrix.JunctionPointFilterer import JunctionPointFilterer
        ## 1 - Compute ReadJunctionPoint and its JunctionPoints
        self.compute_read_junction_points()
        self.compute_block_neighourhood_Seq()
        self.compute_read_solidity()
        # Compute JP solidity
        self.compute_junction_points_neighbours(config.JUNCTION_WINDOW)
        ## 2 - Read redemption attempt

        # A Read can be redempted if there is a possible realignment area that is large enough to receive the realignable blocks
        # and that would turn an isolated JP into a Solid Jp.
        if redemption:

            self.LOGGER.add_comment_debug(f"ACM: Number of reads before pre_filtering() : {len(self.reads)}")
            self.pre_filtering()
            self.LOGGER.add_comment_debug(f"ACM: Number of reads after pre_filtering() : {len(self.reads)}")
        else:
        # Remove isolated Reads without redemption
            self.LOGGER.add_comment_debug(f"Number of Reads before JunctionPointFilterer : {len(self.reads)} ")
            JunctionPointFilterer(self)
            self.LOGGER.add_comment_debug(f"Number of Reads after JunctionPointFilterer : {len(self.reads)} ")
        self.delete_empty_blocks()
        self.fusion_similar_blocks()
        self.delete_fragile_reads()
        self.delete_empty_blocks()
        self.do_update_PredictedExon_and_Block()


    def compute_read_junction_points(self):
        # A read has a JunctionPoint each time it is not continuous with its next block.
        self.read_junction_points = []

        for read in self.get_reads():
            # initializing : Set last leaving JP and First Entering JP
            last_block = self.get_read_last_block(read.get_read_name())
            starting_block = self.get_read_first_block(read.get_read_name())
            rjp = ReadJunctionPoint(read)
            rjp.add_junction_point(JunctionPoint(reference_position=starting_block.get_begin(), read_name=read.get_read_name(),
                                                 junction_status=JunctionStatus.ENTERING))
            rjp.add_junction_point(
                JunctionPoint(reference_position=last_block.get_end(), read_name=read.get_read_name(),
                              junction_status=JunctionStatus.LEAVING))
            while starting_block.get_next_block_containing_read_name(read.get_read_name()) is not None:
                if self.Are_blocks_contiguous(starting_block, starting_block.get_next_block_containing_read_name(read.get_read_name())):
                    starting_block = starting_block.get_next_block_containing_read_name(read.get_read_name())
                else:
                    rjp.add_junction_point(
                        JunctionPoint(reference_position=starting_block.get_end(), read_name=read.get_read_name(),
                                      junction_status=JunctionStatus.LEAVING))
                    starting_block = starting_block.get_next_block_containing_read_name(read.get_read_name())
                    rjp.add_junction_point(
                        JunctionPoint(reference_position=starting_block.get_begin(), read_name=read.get_read_name(),
                                      junction_status=JunctionStatus.ENTERING))
            self.read_junction_points.append(rjp)

    def compute_junction_points_neighbours(self, window_range: int):
        '''
            Checks for each junction points of each read if it exists another junction point withing an
            given window range on their genomic position.
        '''
        pos_dict = {}
        for rjp in self.read_junction_points:
            for jp in rjp.get_junction_points():
                pos = jp.get_reference_position()
                if pos not in pos_dict:
                    pos_dict[pos] = []
                pos_dict[pos].append(jp)

        # Adding a neighbour jp for each jp with same status at the same pos.
        for pos in pos_dict.keys():
            for jp in pos_dict[pos]:
                for other_jp in pos_dict[pos]:
                    if jp.get_status() == other_jp.get_status() and other_jp.get_read_name() != jp.get_read_name() :
                        jp.add_neighbour(other_jp)
                        other_jp.add_neighbour(jp)

        for rjp in self.read_junction_points:
            for jp in rjp.get_junction_points():
                gene_position = jp.get_reference_position()
                if gene_position < int(window_range / 2):
                    position_start = 0
                else:
                    position_start = gene_position - int(window_range / 2)

                if gene_position > self.get_predicted_exons()[-1].get_end() - 1 - int(window_range / 2):
                    position_end = self.get_predicted_exons()[-1].get_end() - 1
                else:
                    position_end = gene_position + int(window_range / 2)

                # Adding a neighbour jp for each jp with same status at a pos within a window.
                for other_gene_position in range(position_start, position_end+1):
                    if other_gene_position != gene_position:
                        for other_jp in pos_dict.get(other_gene_position, []):
                            if jp.get_status() == other_jp.get_status() and other_jp.get_read_name() != jp.get_read_name() :
                                jp.add_neighbour(other_jp)
                                other_jp.add_neighbour(jp)


                # TODO: Mais on ne checke pas dans la window avec pos_dict.get(gene_position, [])? il manque une boucle for non ?
                # for other_jp in pos_dict.get(gene_position, []):
                #     print(other_jp)
                #     print(jp)
                #     if jp.get_status() == other_jp.get_status() and other_jp.get_read_name() != jp.get_read_name() and\
                #             position_start <= other_jp.get_reference_position() <= position_end:
                #         jp.add_neighbour(other_jp)
                #         other_jp.add_neighbour(jp)



    def find_contiguous_blocks_with_same_reads(self, exon):
        '''
        returns the list of similar block to merge
        '''
        contiguous_groups = []

        current_group = [exon.get_blocks()[0]]
        for i in range(1, len(exon.get_blocks())):
            current_block = exon.get_blocks()[i]
            previous_block = current_group[-1]
            if (current_block.get_reads_name() == previous_block.get_reads_name() and
                    self.Are_blocks_contiguous(previous_block, current_block)):
                current_group.append(current_block)
            else:
                contiguous_groups.append(current_group)
                current_group = [current_block]
        contiguous_groups.append(current_group)
        return contiguous_groups

    def Are_blocks_contiguous(self, current_block : Block, current_next_block : Block):
        if current_next_block== None:
            return None
        if current_block.get_end() + 1 == current_next_block.get_begin():
            return True
        return False

    def Is_next_contiguous_block_uncertain(self, current_block):
       if current_block.get_next_block() == None:
           return False
       elif current_block.get_next_block().is_uncertain() and self.Are_blocks_contiguous(current_block, current_block.get_next_block()):
           return True

    def Is_previous_contiguous_block_uncertain(self, current_block):
       if current_block.get_previous_block() == None:
           return False
       elif current_block.get_previous_block().is_uncertain() and self.Are_blocks_contiguous(current_block.get_previous_block(), current_block):
           return True

    def create_sublist_of_solid_blocks(self, exon: PredictedExon):
        solid_sublists = []
        current_sublist = []
        for i, block in enumerate(exon.get_blocks()):
            if block.is_solid():
                current_sublist.append(block)
            else:
                if len(current_sublist) > 0:
                    solid_sublists.append(current_sublist)
                current_sublist = []
        if len(current_sublist) > 0:
            solid_sublists.append(current_sublist)
        return solid_sublists


    def get_reads(self) -> ReadsSet:
        return self.reads


    def export_to_xlsx(self, output_filename: str = "compact.xlsx"):
        xlsx = XLSXExporter(self, output_filename, str(self.gene_filename)+'/'+str(self.gene_filename)+'.fa.masked')
        xlsx.export()

    def get_read_names(self):
        return self.reads.get_readnames() #self.read_names

    def get_known_exons(self):
        return self.known_exons

    def get_predicted_exons(self):
        return self.predicted_exons

    def get_read_predicted_exons(self, readname : str) -> [PredictedExon]:
        list_of_exons = []
        for exon in self.predicted_exons:
            if exon.contains_read(readname):
                list_of_exons.append(exon)
        return list_of_exons

    def get_read_first_block(self, read_name: str) -> Block:
        """
            Return the first Block for a read or None if nothing has been
            found for the given read name.
        """
        for pe in self.predicted_exons:
            for b in pe.get_blocks():
                if b.contain_read(read_name):
                    return b
        return None

    def get_read_last_block(self, read_name: str) -> Block:
        """
            Return the last Block for a read or None if nothing has been
            found for the given read name.
        """
        for pe in reversed(self.predicted_exons):
            for b in reversed(pe.get_blocks()):
                if b.contain_read(read_name):
                    return b
        return None

    def get_annotated_compact_matrix(self):
        return self.annotated_compact_matrix

    def get_exonic_binary_vectors(self):
        return self.exonic_binary_vectors

    def get_binary_matrix(self):
        return self.binary_matrix


    def get_block_at_begining_reference_position(self, begin: int):
        for exon in self.predicted_exons:
            for block in exon.get_blocks():
                if begin == block.get_begin():
                    return block
        return None



    def export_to_json(self, output_filename: str):
        '''
        creates the dictionnary as a sum up of ACM object and exports it to a json file.

        '''
        result = {}
        record = self.binary_matrix.gene_seq
        # we also need to have information about the gene : start and end pos on the chromosome
        filename = os.path.join(self.binary_matrix.reference.id,self.binary_matrix.reference.id+".gff")
        with open(filename) as fileId:
            line = fileId.readline()
            t = line.split("\t")
            chromosome, _, _, start, end, _, strand, _, identifier = t
        print(chromosome, start, end, strand, identifier)

        # gene information
        result["gene"] = {
            "chromosome" : chromosome,
            "id": self.binary_matrix.reference.id,
            "start": int(start),
            "end": int(end),
            "strand": strand}

        # read dictionary creation
        result["reads"] = {}
        hsp_read_dictionnary = {}
        for r_index, read in enumerate(self.get_reads(), start=0):
            result["reads"][r_index] = {"name" : "", "coverage" : "","exon_vector" : "","block_vector" : ""}
            result["reads"][r_index]["name"] = read.id
            result["reads"][r_index]["coverage"] = int(read.get_read_coverage() * 100)
            result["reads"][r_index]["exon_vector"]=str(self.get_exonic_vector()[r_index])
            result["reads"][r_index]["block_vector"]= str([1 if b.contain_read(read.id) else 0 for e in self.get_predicted_exons() for b in e.get_blocks()])
            hsp_read_dictionnary[read.id] = HSPFragmentSequenceExtracter(
                self.binary_matrix.get_reads().get_read(read.id).get_hits(), self.get_logger())

        for k in hsp_read_dictionnary.keys():
            print(hsp_read_dictionnary[k])

        result["exons"] = {}
        for e_index, exon in enumerate(self.get_predicted_exons()):
            result["exons"][e_index]= { "begin" : exon.get_begin(), "end" : exon.get_end()}

        result["blocks"] = {}
        block_index=0
        for exon in self.get_predicted_exons():
            for block in exon.get_blocks():

                block_index+=1
                result["blocks"][block_index] = { "begin" : block.get_begin(), "end" : block.get_end(),
                                                  "reads" : {} , "exon" : exon.get_id(),
                                                  "population" : block.get_population(),
                                                  "right_seq" : record[block.get_end()+1: block.get_end() + 5],
                                                  # "leaving_left_seq" :  record[block.get_end() - 4: block.get_end()],
                                                  # "entering_right_seq": record[block.get_begin() + 1: block.get_begin() + 5],
                                                  "left_seq": record[block.get_begin() - 4: block.get_begin()],
                                                  "solid" : block.is_solid(),
                                                  "sequences": record[block.get_begin(): block.get_end()],
                                                  "entering_junction_points" : block.get_entering_junction_point().get_read_amount() if block.get_entering_junction_point() is not None else -1,
                                                  "leaving_junction_points": block.get_leaving_junction_point().get_read_amount() if block.get_leaving_junction_point() is not None else -1,
                                                  "entering_splice_junctions": block.get_entering_splice_junction_point(),
                                                  "leaving_splice_junctions": block.get_entering_splice_junction_point()
                                                  }
                for read_in_block_index, read_in_block in enumerate(block.get_reads_name()):
                    result["blocks"][block_index]["reads"][read_in_block_index] = {"begin" : block.get_block_alignments()[read_in_block].get_read_begin(),
                                                                          "end" : block.get_block_alignments()[read_in_block].get_read_end()
                                                                          #   ,
                                                                          # "sequence" : hsp_read_dictionnary[read_in_block].get_fragment_from_read_position(
                                                                          #     block.get_block_alignments()[
                                                                          #         read_in_block].get_read_begin()
                                                                          # )
                                                                          }
                    # print(f"begin {block.get_block_alignments()[read_in_block].get_read_begin()}")
                    # print(f"end {block.get_block_alignments()[read_in_block].get_read_end()}")
                    #
                    # print(hsp_read_dictionnary[read_in_block].get_fragment_from_read_position(
                    #                                                           block.get_block_alignments()[
                    #                                                               read_in_block].get_read_begin()))
        with open(output_filename, "w+") as of:
            of.write(json.dumps(result, indent=2))


    def export_to_json_old(self, output_filename: str):
        result = {}
        record = self.binary_matrix.gene_seq

        # result["data-type"] = "compact"
        #
        # self.graph = EBVGraph(self.exonic_binary_vectors)
        # result["ebv-graph"] = to_pydot(self.graph.graph).to_string()
        #
        # binary_vectors = [ str(b) for b in self.get_exonic_binary_vectors()]
        # result["binary_vectors"] = str(binary_vectors)

        # extraction des reads et affectation d'un id
        reads = self.get_read_names()
        reads_dict = {"{}".format(id): {
            "name": reads[id],
            "coverage": int(self.get_binary_matrix().get_reads().get_read(reads[id]).get_read_coverage() * 100),
            "block_vector": None,
            "exon_vector": None} for id in range(len(reads))}
        reads2ids = {reads_dict[id]["name"]: id for id in reads_dict}
        result["reads"] = reads_dict



        # extraction des blocks
        ## on ne prend que les exons réellement prédits, pas ceux provenant des annotations
        ## on reconstruit les numeros des exons
        exons_idx = [-1 for _ in range(len(self.get_predicted_exons()))]
        idx = 0
        for e in self.get_predicted_exons():
            if not e.is_empty():
                exons_idx[e.get_id()] = idx
                idx += 1
        exons = [e for e in self.get_predicted_exons() if not e.is_empty()]
        # print("==> {} {}".format(len(exons),len(self.get_predicted_exons())))

        exons_dict = {}
        result["exons"] = exons_dict
        blocks = []
        all_uncertain = {e.get_id(): True for e in exons}
        for e in exons:
            exons_dict["{}".format(exons_idx[e.get_id()])] = {"begin": e.get_begin(), "end": e.get_end()}
            if not e.is_empty():
                for b in e.get_blocks():
                    if b.is_solid():
                        all_uncertain[e.get_id()] = False
                    blocks.append((b, e))

        nb_blocks = len(blocks)
        nb_exons = len(exons)

        # initialise les vecteurs de blocks pour chaque read
        for read_id in reads_dict:
            reads_dict[read_id]["block_vector"] = [0 for b in range(nb_blocks)]
            reads_dict[read_id]["exon_vector"] = [0 for b in range(nb_exons)]

        # construction du dictionnaire des blocks
        blocks_dicts = {}
        for block_idx in range(len(blocks)):
            b, e = blocks[block_idx]
            # extraction des bordures dans la reference
            range1, range2 = (0, 0), (0, 0)
            if b.get_begin() > 4:
                range1 = (b.get_begin() - 4, b.get_begin())
            if b.get_end() < self.get_annotated_compact_matrix()[-1].get_end() - 4:
                range2 = (b.get_end(), b.get_end() + 4)
            edge_begin = record[range1[0]:range1[1]]
            edge_end = record[range2[0] + 1:range2[1] + 1]
            eid = exons_idx[e.get_id()]
            # if b.get_population() <= 1:
            #	print(b)
            #	eid = None
            # if all_uncertain[e.get_id()]:
            #		eid = None
            d = {
                # "id" : block_idx,
                "begin": b.get_begin(),
                "end": b.get_end(),
                "reads": {},
                "exon": eid,  # exons_idx[e.get_id()],
                "left_seq": edge_begin,
                "right_seq": edge_end,
                "solid": b.is_solid()
            }
            alignments = []
            for read_name in b.get_reads_name():
                if read_name not in reads2ids.keys():
                    continue
                read_id = reads2ids[read_name]
                # extraction de la séquence alignée du read pour le block
                h = HSPFragmentSequenceExtracter(
                    self.get_binary_matrix().get_reads().get_read(read_name).get_hits(), log_object=self.get_logger())
                sequences = h.extract_sequences_from_reference_positions(b.get_begin(), b.get_end())
                alignments.append(
                    (SeqRecord(Seq(sequences[0]), id="reference"), SeqRecord(Seq(sequences[1]), id=read_name)))
                d["reads"][read_id] = {"begin": b.block_alignments[read_name].get_read_begin(),
                                       "end": b.block_alignments[read_name].get_read_end(),
                                       "sequence": None
                                       }
                # enregistre que ce read est dans ce block
                reads_dict[read_id]["block_vector"][block_idx] = 1
                reads_dict[read_id]["exon_vector"][exons_idx[e.get_id()]] = 1
            msa = MSAFromSeq(alignments)
            d["sequence"] = msa.msa_reference_sequence
            for index in range(0, len(msa.msa_reads_sequences)):
                read_name = msa.alignments[index][1].id
                d["reads"][reads2ids[read_name]]["sequence"] = msa.msa_reads_sequences[index]

            blocks_dicts["{}".format(block_idx)] = d

        result["blocks"] = blocks_dicts

        # result["exons"] = []
        # for e in exons:
        # 	if not e.is_empty():
        # 		result["exons"].append({ k : v  for k,v in zip(["id","begin","end","nb","blocks"],[e.get_id(),
        # 	e.get_begin(),
        # 	e.get_end(),
        # 	e.get_read_number(),
        # 	[ { kk : vv for kk,vv in zip(["begin","end","reads"],[b.get_begin(),b.get_end(),
        # 														  {
        # 								b.block_alignments[index].get_read_name() :
        # 									(b.block_alignments[index].get_read_begin(),
        # 							b.block_alignments[index].get_read_end()) for index in range(len(b.block_alignments)) }
        #
        # 														  ])} for b in e.get_blocks()] ] ) } )
        #
        #
        # reads
        # result["compact"] = {
        # 	"blocs" : {
        #
        # 	}
        # }

        with open(output_filename, "w+") as of:
            of.write(json.dumps(result, indent=2))


    def tag_intron_retention_blocks (self):
        for pe in self.get_predicted_exons():
            blocks = pe.get_blocks()
            i = 0
            # skip uncertain blocks at the beginning, i.e. find the first solid block
            while i < len(blocks) and blocks[i].is_uncertain():
                i += 1
            while i < len(blocks): #if not, there is no solid block
                # print(i,blocks[i])
                # skip solid blocks
                while i < len(blocks) and blocks[i].is_solid():
                    i += 1
                if i < len(blocks):
                    j = i
                    # skip uncertain blocks until the next solid block
                    while j < len(blocks) and blocks[j].is_uncertain():
                        j += 1
                    # [i,j] contains only uncertain blocks
                    if j == len(blocks):
                        # we are at the end of the pe, this not an intron retention
                        i = j
                    else:
                        assert blocks[i].is_uncertain()
                        assert blocks[j].is_solid()
                        s = set(blocks[i].get_reads_name())
                        for b in blocks[i+1:j+1]:
                            s = s.intersection(set(b.get_reads_name()))
                        #print(f"Reads to tag : {s}")
                        for b in blocks[i:j]:
                           # print(f"Tag block {b}")
                            for read_name in s:
                                b.set_read_status(read_name,BlockReadStatus.INTRON_RETENTION)
                        i = j
                else:
                    # the last block is solid, nothing to do
                    pass

    def tag_utr_blocks (self):
        """
        We define as UTR blocks for a given read all blocks that are before (after) the first (last) solid block
        @return: nothing
        """

        for read_name in self.get_read_names():
            print(read_name)
            if not self.is_read_fragile(read_name):
                pe = self.get_read_predicted_exons(read_name)
                # go through blocks until the first solid block
                first_pe = pe[0].get_blocks()
                i = 0
                block = first_pe[0]
                stop = False
                while block is not None and not stop:
                    if block.contain_read(read_name):
                        if block.is_uncertain():
                            block.set_read_status(read_name,BlockReadStatus.UTR3)
                        else:
                            stop = True
                    block = block.get_next_block()
                # go through blocks in reverse until the last solid block
                last_pe = pe[-1].get_blocks()
                i = len(last_pe) - 1
                block = last_pe[i]
                stop = False
                while block is not None and not stop:
                    print("current block")
                    print(block)
                    if block.contain_read(read_name):
                        if block.is_uncertain():
                            block.set_read_status(read_name, BlockReadStatus.UTR5)
                        else:
                            stop = True
                    block = block.get_previous_block()
                    print(f"previous block is : {block}")
                    print(read_name)


    def for_tag_utr_blocks (self):
        """

        We define as UTR blocks for a given read all blocks that are before (after) the first (last) solid block
        @return: nothing
        """

        for read_name in self.get_read_names():
            # Process the first predicted exon
            first_pe = self.get_read_predicted_exons(read_name)[0]
            self.find_utr_blocks(read_name, first_pe.get_blocks(), BlockReadStatus.UTR3)

            # Process the last predicted exon in reverse
            last_pe = self.get_read_predicted_exons(read_name)[-1]
            self.find_utr_blocks(read_name, reversed(last_pe.get_blocks()), BlockReadStatus.UTR5)


    def find_utr_blocks(self, read_name, blocks, status):
        for block in blocks:
            if block.contain_read(read_name):
                if block.is_uncertain():
                    block.set_read_status(read_name, status)
                else:
                    break

    def Is_there_several_contiguous_SOLID_blocks_in_exon(self):
        pass

    def Is_there_several_SOLID_blocks_in_exon(self):
        pass

    # def delete_duplicate_block(self):
    #
    #     for exon in self.get_predicted_exons():
    #         list_with_potential_duplicate = [ b.begin for b in exon]
    #         # for blk in list_with_potential_duplicate:
    #         my_dict = {i: list_with_potential_duplicate.count(i) for i in list_with_potential_duplicate}


    def delete_duplicate_block(self):
        blocks = [] # global block list
        for exon in self.predicted_exons:
            blocks.extend(exon.get_blocks())

        unique_blocks = set()
        sorted_blocks = []

        for block in blocks:
            if block not in unique_blocks:
                unique_blocks.add(block)
                sorted_blocks.append(block)

        self.binary_matrix.blocks = sorted_blocks
        exons = self.__compute_exons(self.binary_matrix.get_blocks())
        # Then create PredictedExon objects from the list of consecutive blocks
        self.predicted_exons = self.__build_predicted_exons(exons)
        self.__compute_gene_junction_points()