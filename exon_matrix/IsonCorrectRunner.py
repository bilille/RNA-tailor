#!/usr/bin/python3.8
import argparse
import shlex
import subprocess
import os

from Bio import SeqIO


parser = argparse.ArgumentParser()
parser.add_argument('-g','--gene_id',action="store", help="store the ENSEMBL ID of the gene")
parser.add_argument('-q','--fastq_database', action="store", help="raw fastq reads sequences")
parser.add_argument('-r','--reads_of_interest',action="store", help="FASTA file containing the reads to correct")
parser.add_argument('-o','--output_file',action="store", help="Output FASTA file with the corrected reads")
parser.add_argument('-a','--alignement_file',action="store", help="input the fasta file prefix")

args = parser.parse_args()
gene_name = args.gene_id
fasta_prefix = args.alignement_file

name_read_to_correct = f"{args.gene_id}/{args.alignement_file}_read_to_correct.txt"
read_to_correct = f"{args.gene_id}/{args.alignement_file}_read_to_correct.fq"
reverted_read: list = []
read_name: list = []
fasta_sequences = SeqIO.parse(open(args.reads_of_interest), 'fasta')
f1 = open(name_read_to_correct, "w")

for fasta in fasta_sequences:
    f1.write(fasta.id + '\n')
    read_name.append(fasta.id)
    if fasta.description[-10:] == '(reversed)':
        # print(fasta.description)
        reverted_read.append(fasta.id)
f1.close()

if not (os.path.exists(f"{gene_name}/{args.alignement_file}_ison_correct")):
    subprocess.run(["mkdir", f"{gene_name}/{args.alignement_file}_ison_correct"])

command_line = f"bash exon_matrix/IsoncorrectRunner.sh -d {args.fastq_database} -g {gene_name} -a {args.alignement_file}"
argument = shlex.split(command_line)
subprocess.run(argument)

read_ison_corrected = SeqIO.parse(open(f"{args.gene_id}/{args.alignement_file}_ison_correct/corrected_reads.fa"), 'fasta')
f = open(f"{args.gene_id}/{args.alignement_file}_corrected.fa", "w")

for fasta in read_ison_corrected:
    if fasta.id in reverted_read:
        f.write(f">{fasta.description} (reversed) \n")
        f.write(f"{str(fasta.reverse_complement().seq)}\n")

    else:
        f.write(f">{fasta.description}\n")
        f.write(f"{str(fasta.seq)}\n")
f.close()


