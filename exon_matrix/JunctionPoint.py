from enum import Enum, auto


class JunctionStatus(Enum):
	ENTERING = auto()
	LEAVING = auto()

"""
	The minimum amount of other read alignments that have to start on the exact
	same reference position to consider this JunctionPoint as a solid one.
"""
JUNCTION_POINT_SOLIDITY = 2

class JunctionPoint():

	def __init__(self, reference_position, read_name : str, junction_status):
		"""
			:param reference_position: The position of that junction point in the reference sequence (starts from 0).
			:param read_line: The read line for that junction point.
			:param junction_status: The status defined for that junction point.
		"""
		self.reference_position = reference_position
		self.read_name = read_name
		self.junction_status = junction_status
		self.neighbours = []
		self.solid = False

	def add_neighbour(self, neighbour_point):
		if neighbour_point not in self.neighbours :
			self.neighbours.append(neighbour_point)

	def remove_neighbour(self, neighbour_point):
		if neighbour_point in self.neighbours :
			self.neighbours.remove(neighbour_point)

	def get_reference_position(self):
		return self.reference_position

	def get_status(self):
		return self.junction_status

	def get_neighbours(self):
		return self.neighbours

	def get_read_name(self):
		return self.read_name

	def is_isolated(self) :
		"""
			Return True if this junction point has no neighbours. False otherwise.
		"""
		return len(self.neighbours) == 0

	def __str__(self) :
		return "read : {}, pos : {}, solidity : {}, neighbours : {}, isolated : {}".format(self.read_name, self.reference_position, self.solid, len(self.neighbours),self.is_isolated())

	def is_solid(self) -> bool :
		return self.solid

	def is_fragile(self) -> bool :
		return not self.solid

	def compute_solidity(self) :
		"""
			Compute the solidity state of the JunctionPoint. If there are more than JUNCTION_POINT_SOLIDITY JunctionPoint
			belonging to other reads and starting at the same reference position, this JunctionPoint is considered solid.
			Otherwise, by default the JunctionPoint solidity is considered to be weak. 
		"""
		solidity = False

		# To make sure a JunctionPoint is never met twice.
		tracked_read_lines = [self.read_line]

		# While loop index
		n = 0

		# JunctionPoint objects starting at the same reference position
		common_junction_points = 0

		while not solidity and n < len(self.neighbours) :
			current_neighbour = self.neighbours[n]
			if current_neighbour.get_reference_position() == self.reference_position and \
				current_neighbour.get_status() == self.get_status() and current_neighbour.get_read_line() not in tracked_read_lines :
				common_junction_points += 1
				tracked_read_lines.append(current_neighbour.get_read_line())
			solidity = common_junction_points >= JUNCTION_POINT_SOLIDITY
			n = n + 1

		self.solid = solidity
