class BlockAlignment(object) :
	"""
		Represents an alignment between the reference sequence and a read on a 
		specific block. Many informations about that alignment are stored to ease
		some calculations.
	"""

	def __init__(self, reference_name : str, reference_sequence : str, 
					   reference_begin : int, reference_end : int,
					   read_name : str, read_sequence : str,
					   read_begin : int, read_end : int):
		self.reference_name = reference_name
		self.reference_sequence = reference_sequence
		self.reference_begin = reference_begin
		self.reference_end = reference_end
		self.read_name = read_name
		self.read_sequence = read_sequence
		self.read_begin = read_begin
		self.read_end = read_end

	def get_read_name(self) -> str:
		return self.read_name

	def get_read_begin(self) -> int:
		return self.read_begin

	def get_read_end(self) -> int:
		return self.read_end

	def get_reference_end(self):
		return self.reference_end

	def get_read_sequence(self, ignore_gaps : bool = False) -> str :
		"""
			Return the sequence corresponding to the read portion in this alignment.
			If the ignore_gaps parameter is set to True the gaps are omitted from the result.
		"""
		return self.read_sequence if not ignore_gaps else self.read_sequence.replace('-', '')

	def describe(self):
		description = f"Reference Name: {self.reference_name}\n" \
					  f"Reference Sequence: {self.reference_sequence}\n" \
					  f"Reference Begin: {self.reference_begin}\n" \
					  f"Reference End: {self.reference_end}\n" \
					  f"Read Name: {self.read_name}\n" \
					  f"Read Sequence: {self.read_sequence}\n" \
					  f"Read Begin: {self.read_begin}\n" \
					  f"Read End: {self.read_end}"
		return description