from exon_matrix.PredictedExon import *

class KnownExon(object):
	"""
		Represents a known exon, an exon specified by a gff file that
		we should find back in matrix.
	"""

	def __init__(self, begin : int, end : int):
		self.begin = begin
		self.end = end
		self.mappings = {} # key : PredictedExon -> (int,int)

	def get_begin(self) -> int :
		return self.begin

	def get_end(self) -> int :
		return self.end

	def add_mapping(self, predicted_exon : PredictedExon, positions : (int, int)) :
		"""
			add a match between a predicted exon and a known exon
		"""
		self.mappings[predicted_exon] = positions

	def get_mappings(self):
		return self.mappings

	def contains(self, predicted_exon : PredictedExon) :
		return predicted_exon in self.mappings
		
	def __str__(self):
		res ="KnownExon : (" + str(self.begin) + ", " + str(self.end) + ")"
		for key, value in self.mappings.items() :
			res = res + "\n\t\tmap on predicted exon n°" + str(key.get_id()) + " : " + str(value)
		return res 