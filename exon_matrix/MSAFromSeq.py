import os
from Bio import AlignIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Align import MultipleSeqAlignment

class MSAFromSeq():
	"""
		Build a Multiple sequence alignment from a group of pairwise alignments
		sharing the same reference sequence.
	"""

	def __init__(self, alignments) :
		"""
			Initialize the current object with a list of alignments.
		"""
		self.alignments = alignments				# list containing all alignments reference/read
		self.reference_sequence = ""		# reference sequence with the nucleotids only
		self.msa_reference_sequence = ""	# reference sequence in the msa
		self.msa_reads_sequences = []		# read sequences in the msa
		self.reference_gaps = []			# gaps for each reference sequence character
		self.build_msa_reference_sequence()
		self.build_msa_reads_sequences()
		self.sanity_check()

	def sanity_check(self) :
		"""
			Check msa validity.
		"""
		for sequence in self.msa_reads_sequences :
			if len(sequence) != len(self.msa_reference_sequence) :
				print("sequence length differ from reference")
				break

	def export_msa(self, output_filename : str = "msa.aln", clustal_format : bool = True):
		"""
			Export computed msa to a file. The msa can either be written in a raw format
			or in a clustal format.
		"""
		if len(self.msa_reference_sequence) == 0 :
			print("Can't export empty msa for " + self.alignments[0][0].id)
		elif clustal_format :
			# to export to clustal, biopython requires a sequence to be converted to
			# a Seq, then a SeqRecord and finally a MultipleSeqAlignment object.
			
			records = []
			
			# add reference sequence
			seq_id = self.alignments[0][0].id
			s = Seq(self.msa_reference_sequence) #, generic_dna)
			record = SeqRecord(s, id=seq_id)
			records.append(record)

			# add reads sequences
			for index in range(0, len(self.msa_reads_sequences)):
				sequence = self.msa_reads_sequences[index]
				seq_id = self.alignments[index][1].id
				s = Seq(sequence) #, generic_dna)
				record = SeqRecord(s, id=seq_id)
				records.append(record)

			fixed_msa = MultipleSeqAlignment(records)
			o = open(output_filename, "w+")
			o.write(format(fixed_msa,"clustal") + "\n")
			o.close()
		else :
			o = open(output_filename, "w+")
			o.write(self.msa_reference_sequence + "\n")
			for sequence in self.msa_reads_sequences :
				o.write(sequence + "\n")
			o.close()


	def build_msa_reference_sequence(self) :
		"""
		Build the reference sequence from the alignments. All of these alignments muse have in common
		the same reference sequence (with more or less nucleotides but with the same beginning).
		"""

		self.maximize_reference_sequence()
		self.compute_reference_gaps()

		# put every gaps for each position on the reference sequence
		for char_index in range(len(self.reference_sequence)) :
			self.msa_reference_sequence = self.msa_reference_sequence + self.reference_sequence[char_index]
			for gap in range(self.reference_gaps[char_index]) :
				self.msa_reference_sequence = self.msa_reference_sequence + "-"

	def build_msa_reads_sequences(self):
		"""
			Adapt every read sequence to the reference one.
		"""
		for a in self.alignments :
			alignment_ref_sequence = str(a[0].seq)
			alignment_read_sequence = str(a[1].seq)
			
			read_msa = ""
			total_gap = 0

			# go through every character in the read sequence and copy each letter when there is
			# the same letter on the reference sequence at the same position, or a '-' char if the
            # letters differ from each other;
			for char_index in range(len(self.msa_reference_sequence)) :
				if char_index - total_gap < len(alignment_ref_sequence) and  self.msa_reference_sequence[char_index] == alignment_ref_sequence[char_index - total_gap] :
					read_msa = read_msa + alignment_read_sequence[char_index - total_gap]
				else :
					read_msa = read_msa + "-"
					total_gap = total_gap + 1
			self.msa_reads_sequences.append(read_msa)

	def maximize_reference_sequence(self):
		"""
		Among all alignments, take the reference sequence that contain the most nucleotides.
		"""
		for a in self.alignments :
			current_reference_sequence = str(a[0].seq)
			if len(current_reference_sequence) - current_reference_sequence.count('-') > len(self.reference_sequence) :
				self.reference_sequence = current_reference_sequence.replace('-','')

	def compute_reference_gaps(self):
		"""
		For each nucleotide in the class reference sequence, check the maximum number of nucleotides found in an alignment
		reference sequence. 
		Note : the reference sequence must have been computed (maximize_reference_gaps) before gaps can be counted.
		"""

		# go through all nucleotides in reference sequence
		for reference_nt_index in range(len(self.reference_sequence)) :

			# check the maximum gap found for the same nucleotide on all alignments
			current_gap_max = 0
			for a in self.alignments :

				alignment_ref_sequence = str(a[0].seq)

				alignment_char_index = 0 # the index for characters on the reference sequence in alignment
				alignment_nt_index = 0	 # the index for nucleotides on the reference sequence in alignment
				# loop on sequence characters until the corresponding nucleotide is found
				while alignment_nt_index < reference_nt_index and alignment_char_index < len(alignment_ref_sequence) :
					if alignment_ref_sequence[alignment_char_index] != '-' :
						alignment_nt_index = alignment_nt_index + 1
					alignment_char_index = alignment_char_index + 1

				# purge remaining gap characters
				while alignment_char_index < len(alignment_ref_sequence) and alignment_ref_sequence[alignment_char_index] == '-' :
					alignment_char_index = alignment_char_index + 1

				if not alignment_char_index < len(alignment_ref_sequence) :
					current_gap_max = 0
					continue 

				if alignment_ref_sequence[alignment_char_index] != 'N' and alignment_ref_sequence[alignment_char_index] != self.reference_sequence[reference_nt_index] :
					print("Error : current character in alignment differs from reference sequence character")
					print("alignment sequence : " + alignment_ref_sequence + " (aligned with {})".format(a[1].id))
					print("reference sequence : " + self.reference_sequence)
					print("alignment_char_index : " + str(alignment_char_index))
					print("reference_nt_index : "  + str(reference_nt_index))
					print("directory : " + self.fasta_directory)
					exit()

				gaps = 0
				while alignment_char_index + 1 + gaps < len(alignment_ref_sequence) and alignment_ref_sequence[alignment_char_index + 1 + gaps] == '-' :
					gaps = gaps + 1

				current_gap_max = max(current_gap_max, gaps)

			# once maximal gap for position has been found, add it to the vector
			self.reference_gaps.append(current_gap_max)

	def get_alignments(self) -> list:
		return self.alignments

	def get_reference_gaps(self) -> list:
		return self.reference_gaps

	def get_reference_sequence(self) -> str:
		return self.reference_sequence

	def get_msa_reference_sequence(self) -> str:
		return self.msa_reference_sequence

	def get_msa_reads_sequences(self) -> list:
		return self.msa_reads_sequences
