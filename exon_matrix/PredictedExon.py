from exon_matrix.Block import Block
class PredictedExon():
	"""
		Contains blocks and information about them in the
		final matrix.
	"""

	def __init__(self, begin : int, end : int, blocks : list):
		self.id = 0,
		self.blocks = blocks
		self.predicted_sub_units = []
		self.begin = begin
		self.end = end
		self.nucleotidic_vector = []


	def __str__(self):
		"""
			Return this object as a str
		"""
		res = "PredictedExon n°" + str(self.id) + " : (" + str(self.begin) + ", " + str(self.end) + ")"
		for block in self.blocks :
			res = res + "\n\t\t" + str(block)
		return res

	def replace_blocks(self, coordinate_to_remove : [int], block_to_insert ):
		'''
		replace blocks by a single block by slicing the list.
		'''

		if self.blocks[coordinate_to_remove[-1]].get_next_block() is not None:
			block_to_insert.set_next_block(self.blocks[coordinate_to_remove[-1]].get_next_block())
		else:
			block_to_insert.set_next_block(None)
		if self.blocks[coordinate_to_remove[0]].get_previous_block() is not None:
			block_to_insert.set_previous_block(self.blocks[coordinate_to_remove[0]].get_previous_block())
		else:
			block_to_insert.set_previous_block(None)
		#insert Block
		self.blocks[coordinate_to_remove[0]: coordinate_to_remove[-1]+1] = [block_to_insert]

	def insert_blocks_in_exon(self, acceptor_blocks: [Block]):
		"""
        Insert new_blocks into the exon, replacing any existing blocks in case of overlap.
        """
		# Filter out any existing block that overlaps with a new block
		non_overlapping_old_blocks = [
			block for block in self.blocks
			if not any(
				nb.begin <= block.end and nb.end >= block.begin
				for nb in acceptor_blocks
			)
		]

		# Concatenate non-overlapping old blocks and new blocks
		all_blocks = non_overlapping_old_blocks + acceptor_blocks
		self.set_blocks(sorted(all_blocks, key=lambda b: b.begin))

	def get_blocks(self) -> [ Block ]:
		return self.blocks

	def get_block_index_from_block(self, target_block) -> int:
		index = 0
		for b in self.get_blocks():
			if b == target_block:
				return index
			index+=1
		return None


	def add_block(self,block : Block):
		self.blocks.append(block)
		self.blocks.sort(key=lambda k: k.begin)

	def set_blocks(self, new_blocks_list):
		self.blocks = new_blocks_list


	def get_begin(self) -> int:
		return self.begin

	def get_end(self) -> int:
		return self.end

	def get_id(self) -> int :
		return self.id

	def set_id(self, id) -> int :
		self.id = id

	def get_population(self) -> int :
		reads = set()
		for block in self.blocks :
			for read_name in block.get_block_alignments():
				reads.add(read_name)
		return len(reads)

	def contains_read(self, read_name) -> bool :
		read_found = False
		block_index = 0
		while not read_found and block_index < len(self.blocks) :
			read_found = self.blocks[block_index].contain_read(read_name)
			block_index = block_index + 1
			
		return read_found

	def is_empty(self) -> bool :
		return len(self.blocks) == 1 and self.blocks[0].get_population() == 0

	def get_first_solid_leaving_junction_point(self) -> int :
		"""
			Return the first block index inside this exon where there is a solid leaving
			junction point.If nothing has been found, -1 is returned.
		"""
		res = -1
		block_index = 0
		while block_index < len(self.blocks) and res == -1 :
			res = block_index if self.blocks[block_index].get_leaving_junction_point().is_solid() else -1
			block_index += 1 if res == -1 else 0
		return res

	def get_last_solid_entering_junction_point(self) -> int :
		"""
			Return the last block index in the exon with a solid entering junction point.
			If no such block is found, -1 is returned.
		"""
		res = -1
		block_index = len(self.blocks) - 1
		while block_index >= 0 and res == -1 :
			res = block_index if self.blocks[block_index].get_entering_junction_point().is_solid() else -1
			block_index -= 1 if res == -1 else 0
		return res
		