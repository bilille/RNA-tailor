import os
from Bio import AlignIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Align import MultipleSeqAlignment
from exon_matrix.MSAFromSeq import MSAFromSeq


class MSAFromFasta(MSAFromSeq):
	"""
		Build a Multiple sequence alignment from a group of pairwise alignments
		sharing the same reference sequence.
	"""

	def __init__(self, directory : str) :
		"""
			Initialize the current object with a given directory.
		"""
		self.fasta_directory = directory 	# directory with every pairwise alignments as fasta files
		alignments = self.read_directory()
		super().__init__(alignments)


	def read_directory(self) :
		"""
		Read the directory meant to contain fasta files. This directory must contain only fasta files with each of them being
		a pairwise alignment with the same reference sequence. 
		"""
		assert len(self.fasta_directory) > 0, "Provided directory name is empty, fasta files can't be retrieved to create MSA."
		alignments = []
		# loop over all files in given directory
		for filename in os.listdir(self.fasta_directory) :
			if filename.endswith(".fasta") :
				# create pairwise alignment object from file and add it to the list
				try :
					a = AlignIO.read(self.fasta_directory + "/" + filename, format = "fasta", seq_count = 2)
					alignments.append(a)
				except ValueError as v:
					print("Invalid fasta file " + str(self.fasta_directory) + "/" +  str(filename))
					print("AlignIO: " + str(v))
		return alignments

