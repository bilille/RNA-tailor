import os
import os.path

import numpy

from exon_matrix.Block import *
from exon_matrix.ReadJunctionPoint import *
from exon_matrix.XLSXExporter import XLSXExporter
from seq.ReadsSet import ReadsSet
from seq.Reference import Reference
from exonerate_alignment.ExonerateParser import ExonerateParser
from exon_matrix.HSPFragmentSequenceExtracter import HSPFragmentSequenceExtracter
from PredictIsoform import Logger
# This constant is used to specify the numbers of nucleotides that are looked in the matrix to apply correction.
# default value of REGROUP_GLOBALVECTOR_VALUE = 6

# When realigning block, this value is used to check that a block can be realigned if its length is at least
# equal to this value.
# default value of REALIGNABLE_BLOCK_LENGTH = 10


# create liste de chainage et test function
# set_previous et set_next


class BinaryMatrix():
    """
        This class represents a 2D array (numpy array).
        This matrix has as many lines as there are reads and has as many columns as there are nucleotides in the reference
        sequence. For each read, at each reference position, 1 means that the read is aligned with the reference sequence,
        0 means that there is not alignment at this position.
        
        :param sGeneId: The id of the reference sequence.
        :param reads:  The ReadsSet containing the reads matching with reference sequence.
        :param exon_file: The name of the gff file containing exon coordinates for the reference sequence.
        :param exonerate_parser: An ExonerateParser object built on exonerate output for the alignments between gene and reads.
        :param export_alignments: A boolean to tell to export reads/reference alignment (optional)
        :param msa_alignment: A boolean to compute or not a multiple sequence alignment for each area where read shares an alignment with the reference sequence.
        :param junction_filter: A boolean to tell or not to filter reads out of the matrix based on their junction points.
    """

    def __init__(self, sGeneId: str, reads: ReadsSet, reference: Reference,
                 exonerate_parser: ExonerateParser,
                 junction_filter: bool = False,
                 annotated_compact_matrix : classmethod = None,
                 log_object : Logger = None):

        if log_object is None:
            self.LOGGER=Logger(sGeneId)
        else:
            self.LOGGER=log_object
        self.LOGGER.add_comment_debug("Start building BinaryMatrix")
        self.LOGGER.add_comment_warning("Start building BinaryMatrix")
        self.annotated_compact_matrix = annotated_compact_matrix
        self.exonerate_parser = exonerate_parser
        print(exonerate_parser.getAllQueryResults()[0].hits[0])
        self.gene_length = None
        self.gene_id = sGeneId
        self.inner_matrix = None

        self.reads = reads
        self.reference = reference

        self.gene_seq = str(self.reference.ref.seq)
        self.gene_length = len(self.reference)
        self.build_matrix()
        # self.compute_read_junction_points()
        self.LOGGER.add_comment_debug(f"Number of Reads in BM : {len(self.reads)} ")
        # Apply filter only if there is more than 1 read
        #from exon_matrix.JunctionPointFilterer import JunctionPointFilterer
        #if junction_filter and self.get_height() > 1 :
        #   JunctionPointFilterer(self, self.exonerate_parser)
        #   self.LOGGER.add_comment_debug(f"Number of Reads after JunctionPointFilterer : {len(self.reads)} ")
        # self.compute_read_junction_points_solidity() # filtre de nature différente ?
        # Not necessary jamais utilisé par le filtre. La solidité d'un point de junction n'est pas utilisé
        # compute the dictionnary of blocks, which associates a block number to a Block
        self.blocks = self.__compute_blocks()

    def get_reads(self)->ReadsSet:
        """
        Return the set of reads of the BinaryMatrix
        @return: the ReadSet
        """
        return self.reads

    def remove_read(self, read_name: str):
        """
        Remove a read from the BinaryMatrix : the line in the matrix is removed as well as
        the read in the ReadSet.

        Note: if the read does not exist, nothing happens

        @param read_name: The read to remove
        @return: nothing
        """
        if read_name in self.get_reads().get_readnames():
            line_to_remove = self.get_reads().get_index_from_readname(read_name)
            if line_to_remove != -1:
                self.set_matrix(numpy.delete(self.get_matrix(), line_to_remove, axis=0))
                self.get_reads().remove(read_name)


    def get_log(self):
        return self.LOGGER

    def get_reference(self):
        return self.gene_seq

    def compute_read_junction_points_solidity(self):
        for rjp in self.read_junction_points:
            for jp in rjp.get_junction_points():
                jp.compute_solidity()

    def get_annotated_compact_matrix(self):
        return self.annotated_compact_matrix

    def get_column_population(self, gene_position: int) -> int:
        """
            Return the population inside a matrix column. The population corresponds
            to the number of reads aligned with the reference sequence on that position.
        """

        # Assert that the given position is inside the gene length.
        assert 0 <= gene_position < len(self.inner_matrix[0, :]), \
            "Reference position must be inside [{}: {}]".format(0, len(self.inner_matrix[0, :]))

        # Return the population
        return sum(self.inner_matrix[:, gene_position])

    def build_matrix(self):
        """
            Build the inner matrix from an ExonerateParser object.
            
            This inner matrix is a 2D numpy array where the first dimension (the rows) corresponds
            to the number of reads aligned against the reference sequence, and the second dimension
            (the columns) corresponds to the reference sequence length.
            Hence, if there is an alignment between the reference sequence and a read, the cell (x,y)
            is filled with 1, 0 otherwise (x being the row of a read and y the position on the reference sequence).
        """
        number_of_reads = len(self.reads)
        gene_length = len(self.reference) #self.get_gene_length()

        #print(f"{number_of_reads} {gene_length}")
        # To prevent filling a 2D numpy array with 0 and 1 at the same time, we instantiate a 
        # numpy array filled with 0. The 1 will be put afterward.
        matrix = numpy.zeros((number_of_reads, gene_length), dtype=numpy.ubyte)

        # loop over every read that have an alignment with the reference sequence
        for index in range(number_of_reads):
            # print(index)
            hit = self.reads.get_read_from_index(index).queryresult[0]

            hsp = hit[0]

            # for every part of the alignment between the read and the gene, fill matrix with 1 at the 
            # correspond columns.
            for hsp_fragment in hsp:

                # fill with 1 on a range
                matrix[index, hsp_fragment.hit_range[0]:hsp_fragment.hit_range[1]] = 1

        self.inner_matrix = matrix


    def get_geneId(self) -> str:
        """ return gene id as a string"""
        return self.gene_id

    def __compute_blocks(self, check_fragment=True,smoothing = False) -> {int: Block}:
        """
            The purpose of this method is to build basic gene blocks, it means tuple of positions (x, y)
            where there is an alignment between the gene and at least a read.
            It will create a dictionary where the key is a gene block index and values
            are tuple of positions on the gene.
            The 'check_fragment' parameter will verify that between two consecutive and similar columns inside
            a block, if the columns are part of the same fragment. If not, a new block is created. This option
            can help to have more accurate results but it can drastically slow down execution process despite 
            this function implemented optimizations.
        """

        # The final blocks returned.
        blocks = {}

        # A dictionary containing HSPFragmentSequenceExtracter objects for each read.
        # Storing them in a dictionary optimizes execution time since only one object
        # is created for each read, whereas creating HSPFragmentSequenceExtracter object
        # in the loop below would consume much more execution time.
        hsp_fragment_extracters = {}

        # Position on the matrix column.
        position = 0

        # Position for the previous matrix column.
        previous_position = position

        # Index on blocks which is used to create new blocks or modify an existing one.
        # The blocks index will start from 0.
        number_of_blocks = -1

        # Iterate over inner matrix columns
        while position < self.get_width():


            # No block if column is empty.
            if self.get_column_population(position) == 0:
                previous_position = position
                position = position + 1
                continue
            # if self.get_column_population(position) == 0
            # If two consecutive columns does not have the same reads, a new block is created.
            elif not numpy.array_equal(self.inner_matrix[:, position], self.inner_matrix[:, previous_position]):
                number_of_blocks = number_of_blocks + 1
                blocks[number_of_blocks] = Block(position, position)
                blocks[number_of_blocks].set_reads_name([r.id for r in self.get_reads().get_list_of_reads_from_indexes(self.inner_matrix[:, position])])

            # initilisation for first column if some reads are aligned at the first gene position
            elif position == 0 and self.get_column_population(position) > 1:
                number_of_blocks = number_of_blocks + 1
                blocks[number_of_blocks] = Block(position, position)
                blocks[number_of_blocks].set_reads_name([r.id for r in self.get_reads().get_list_of_reads_from_indexes(self.inner_matrix[:, position])])
            # If two consecutive columns are the same.
            else:

                # If 'check_fragment' parameter is given
                if check_fragment:
                    same_fragment = True
                    read_index = 0

                    # Iterate over the reads in the current column and compare for each of them
                    # if they share the same alignment part as the one in the previous column.
                    while same_fragment and read_index < self.inner_matrix.shape[0]:
                        if self.inner_matrix[read_index, position] == 1 and self.inner_matrix[
                            read_index, previous_position] == 1:

                            # If a HSPFragmentSequenceExtracter as already been created for the read, use it.
                            # Otherwise create one and save it for later. This operation is done to prevent the program
                            # from creating HSPFragmentSequenceExtracter object every time there is a new loop.
                            if read_index in hsp_fragment_extracters:
                                h = hsp_fragment_extracters[read_index]
                            else:
                                h = HSPFragmentSequenceExtracter(
                                    self.reads.get_read_from_index(read_index).queryresult, self.get_log()) #exonerate_parser.getReadResultFromIndex(read_index))
                                hsp_fragment_extracters[read_index] = h
                            current_fragment = h.get_fragment_from_reference_position(position)
                            previous_fragment = h.get_fragment_from_reference_position(previous_position)
                            if current_fragment == None or previous_fragment == None or current_fragment != previous_fragment:
                                same_fragment = False
                                break
                        read_index = read_index + 1


                    if not same_fragment:
                        number_of_blocks = number_of_blocks + 1
                        blocks[number_of_blocks] = Block(position, position)
                        blocks[number_of_blocks].set_reads_name([r.id for r in
                                                                 self.get_reads().get_list_of_reads_from_indexes(
                                                                     self.inner_matrix[:, position])])

                    else:
                        # If the alignment parts are the same, the current block is extended.
                        if number_of_blocks in blocks:
                            blocks[number_of_blocks].set_end(blocks[number_of_blocks].end + 1)

                # If the 'check_fragment' parameter is False, we simply extend the current block without
                # checking for the fragments integrity.
                else:
                    if number_of_blocks in blocks:
                        blocks[number_of_blocks].set_end(blocks[number_of_blocks].end + 1)

            previous_position = position
            position = position + 1

        # chaining blocks
        for block_index in range(len(blocks)-1):
            # couple (block_index, block_index +1)
            # where b1 = blocks[block_index] and b2 = blocks[block_index +1]
            # b1 is the previous block of b2
            # b2 is the next block of b1
            blocks[block_index].set_next_block(blocks[block_index + 1])
            blocks[block_index + 1].set_previous_block(blocks[block_index])

        self.__compute_read_positions_on_blocks(blocks)
        self.__compute_block_alignments_in_blocks(blocks)

        return blocks

    def __compute_read_positions_on_blocks(self, blocks: { int : Block }):
        for block in blocks.values():
            position = block.get_begin()
            # get the column at position
            indexes = self.inner_matrix[:, position]
            for row in range(len(indexes)):
                if indexes[row] == 1 and self.is_read_on_gene(row, block.get_begin(), block.get_end()):
                    # older method
                    # read_begin = self.get_read_position_from_gene(row, block.get_begin())
                    # read_end = self.get_read_position_from_gene(row, block.get_end())

                    HSP = self.get_reads().get_read_from_index(row).queryresult[0][0]
                    fragments = ExonerateParser.get_fragments_from_gene_positions(HSP, block.get_begin(), block.get_end())

                    # TODO 23_5_rea : deal with read position when we do not have a HSP in exonerate alignement because of realignment,
                    if len(fragments) == 0:
                        block.add_read_positions(self.get_reads().get_read_from_index(row).get_read_name(), 0,
                                                 0)
                    else:
                        # print(HSP)
                        # print(fragments)
                        # assert len(fragments) == 1

                        # TODO 23_5_rea : deal with read position when we do not have a HSP in exonerate alignement because of realignment,
                        try:
                            read_begin,read_end = ExonerateParser.get_read_positions_from_gene_positions(block.get_begin(), block.get_end(),fragments[0])
                        except:
                            read_begin, read_end = (-1,-1)

                        block.add_read_positions(self.get_reads().get_read_from_index(row).get_read_name(),read_begin, read_end)


    def __compute_block_alignments_in_blocks(self, blocks: { int : Block }):
        """
            Fill every Block
            with informations (pairwise alignments, multiple sequence alignment...)
        """
        reference_name = self.get_geneId()
        for block in blocks.values():
            for read_name in block.get_reads_name():
                read_begin = block.reads_positions[read_name][0]
                read_end = block.reads_positions[read_name][1]
                reference_begin = block.get_begin()
                reference_end = block.get_end()
                h = HSPFragmentSequenceExtracter(
                    self.get_reads().get_read(read_name).queryresult,
                    log_object=self.LOGGER)
                alignment_sequences = h.extract_sequences_from_reference_positions(reference_begin,
                                                                                   reference_end)
                reference_sequence = alignment_sequences[0]
                read_sequence = alignment_sequences[1]

                pba = BlockAlignment(reference_name, reference_sequence, reference_begin,
                                     reference_end,
                                     read_name, read_sequence, read_begin, read_end)
                block.add_alignment(pba)


    # TODO Ask JSV for removal
    def write_block_fasta_files(self, read_name: str, ref_block_begin: int, ref_block_end: int):
        """
            Write an alignment for a read in a block in a fasta format.
            The written file is located at gene_name/msa/x_y/readname.fasta
            with x and y being the beginning and ending positions (on the reference sequence) on the block.
        """

        block_folder = self.get_geneId() + "/msa/" + str(ref_block_begin) + "_" + str(ref_block_end)
        if not os.path.exists(block_folder):
            os.makedirs(block_folder)

        fasta_file = open(block_folder + "/" + read_name + ".fasta", "w+")

        h = HSPFragmentSequenceExtracter(self.reads.get_read(read_name).queryresult) #self.exonerate_parser.getReadResultFromName(read_name))
        sequences = h.extract_sequences_from_reference_positions(ref_block_begin, ref_block_end)
        fasta_file.write("> " + self.get_geneId() + "\n")
        fasta_file.write(sequences[0] + "\n")
        fasta_file.write("> " + read_name + "\n")
        fasta_file.write(sequences[1] + "\n")
        fasta_file.close()

    #TODO Ask JSV for removal
    def write_block_alignment(self, filename: str, read_index: int, gene_positions: (int, int),
                              read_positions: (int, int)):
        """
            Write the alignment content in a file between a read and a gene.
        """

        read_dir = self.get_geneId() + "/reads"

        try:
            if not os.path.exists(read_dir):
                os.mkdir(read_dir)
            f = open(read_dir + "/" + filename, "a+")
        except FileNotFoundError:
            return

        f.write("> " + str(gene_positions) + ", " + str(read_positions) + "\n")
        query = self.reads.get_read_from_index(read_index).queryresult #self.exonerate_parser.getAllQueryResults()[read_index]
        hit = query[0]
        hsp = hit[0]
        for hsp_fragment in hsp:
            if hsp_fragment.hit_range[0] <= gene_positions[0] <= hsp_fragment.hit_range[1]:
                begin = read_positions[0] - hsp_fragment.query_range[0]
                end = read_positions[1] - hsp_fragment.query_range[0] + 1
                f.write(hsp_fragment[begin:end]._str_aln() + "\n")
                break
        f.close()


    def get_read_position_from_gene(self, read_index: int, gene_position: int) -> int:
        """
            Return a position in the read alignment which is aligned
            against the given gene_position.
        """

        HSP = self.reads.get_read_from_index(read_index).queryresult[0][0] #self.exonerate_parser.getReadResultFromIndex(read_index)[0][0]
        fragments = ExonerateParser.get_fragments_from_gene_positions(HSP, gene_position, gene_position)


        assert len(fragments) < 2, self.LOGGER.add_comment_warning("Multiple fragments contain the same gene position : {}, HSP {}".format(
            gene_position, HSP))
        assert len(fragments) > 0, self.LOGGER.add_comment_warning("No fragments containing the gene position {} exists.".format(gene_position))
        hit_seq = str(fragments[0].hit.seq)
        query_seq = str(fragments[0].query.seq)
        nt_count = gene_position - fragments[0].hit_start
        ct_count = 0

        while ct_count < len(query_seq) and nt_count > 0:
            if hit_seq[ct_count] != '-':
                nt_count -= 1
            ct_count += 1

        if ct_count >= len(query_seq):
            return fragments[0].query_end - 1
        return fragments[0].query_start + ct_count - query_seq[:ct_count].count('-')

    def is_read_on_gene(self, read_index: int, gene_start: int, gene_stop: int) -> bool:
        """
            Return True if for the given read, there is an alignement between two
            positions on the gene, False otherwise.

            N.B. : The inner matrix must be built before using this function.

            read_index : The read index (or the row number) corresponding to the read in the inner matrix
            gene_start : The starting position from which an alignment will be looked at.
            gene_stop : The ending position from which an alignment will be looked at.
        """

        i = gene_start
        while (i <= gene_stop and self.inner_matrix[read_index, i] != 1):
            i = i + 1
        return i <= gene_stop

    def export_to_tsv(self, output_filename: str = "compact.tsv"):
        self.raw_compact_matrix.export_to_tsv(output_filename)

    def get_exonerate_parser(self):
        return self.exonerate_parser

    def get_matrix(self) -> numpy.ndarray:
        """
            Return the complete matrix where every row is dedicated to an alignment between a read
            and the gene. The row contains as many columns as nucleotids in the gene. The row is
            made of two values : 1 and 0. 1 means that at such position there is an alignement between
            the gene and the read, while 0 means that at this particular position there is no alignment
            between the read and the gene.
        """
        return self.inner_matrix

    def set_matrix(self, new_matrix: numpy.ndarray):
        self.inner_matrix = new_matrix

    def get_blocks(self) -> [Block]:
        return self.blocks

    def get_gene_length(self) -> int:
        """
            Return the gene length
        """
        return self.gene_length

    def get_width(self) -> int:
        """
            Return the matrix width
        """
        return len(self.inner_matrix[0, :])

    def get_height(self) -> int:
        """
            Return the matrix height
        """
        return len(self.get_matrix()[:, 0])

    def get_value(self, row: int, column: int) -> int:
        """
            Return the value stored at the given coordinates in the matrix.
        """
        return self.inner_matrix[row, column]

    def get_read_name(self, read_row: int) -> str:
        """
            Return the name of a read for a given row.
        """
        return self.reads.get_readname_from_index(read_row) #self.exonerate_parser.getReadResultFromIndex(read_row).id

    def get_read_junction_points(self):
        return self.read_junction_points

    def compute_junction_points_neighbours(self, window_range: int):
        '''
            Checks for each junction points of each read if it exists another junction point withing an
            given window range on their genomic position.
        '''
        pos_dict = {}
        for rjp in self.read_junction_points:
            for jp in rjp.get_junction_points():
                pos = jp.get_reference_position()
                if pos not in pos_dict:
                    pos_dict[pos] = []
                pos_dict[pos].append(jp)

        for rjp in self.read_junction_points:
            for jp in rjp.get_junction_points():
                gene_position = jp.get_reference_position()
                if gene_position < int(window_range / 2):
                    position_start = 0
                else:
                    position_start = gene_position - int(window_range / 2)

                if gene_position > self.get_width() - 1 - int(window_range / 2):
                    position_end = self.get_width() - 1
                else:
                    position_end = gene_position + int(window_range / 2)

                for other_jp in pos_dict.get(gene_position, []):
                    # print(other_jp)
                    # print(jp)
                    if jp.get_status() == other_jp.get_status() and other_jp.get_read_line() != jp.get_read_line() and\
                            position_start < other_jp.get_reference_position() < position_end:
                        jp.add_neighbour(other_jp)


    def compute_read_junction_points(self):
        self.read_junction_points = []
        for row in range(self.get_height()):
            self.LOGGER.add_comment_warning("create a junction point read object for {}".format(self.get_read_name(row)))
            rjp = ReadJunctionPoint(self.reads.get_readname_from_index(row))
            previous_column_value = 0
            for column in range(self.get_width()):
                current_column_value = self.get_value(row, column)

                # If there is an alignment beginning at the current column : create a junction point
                # If this is the first column and there is 1, it is also considered as an alignment beginning.
                if previous_column_value == 0 and current_column_value == 1:
                    junction_point = JunctionPoint(column, row, JunctionStatus.ENTERING)
                    rjp.add_junction_point(junction_point)

                # If there is an alignment ending at the current column : create a junction point
                # If this is the last column and there is 1, it is also considered as an alignment ending.
                elif column > 0 and (column == self.get_width() - 1 and current_column_value == 1 or \
                                     previous_column_value == 1 and current_column_value == 0):
                    junction_point = JunctionPoint(column - 1, row, JunctionStatus.LEAVING)
                    rjp.add_junction_point(junction_point)
                else:
                    # don't create junction point
                    pass
                previous_column_value = current_column_value
            self.read_junction_points.append(rjp)
