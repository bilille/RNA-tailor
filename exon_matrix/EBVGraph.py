from exon_matrix.ExonicBinaryVector import *

import networkx as nx
from networkx.drawing.nx_agraph import write_dot

class EBVGraph():
	"""
		Build a directed graph from the exonic binary vectors.
		Every node is lebeled as a binary vector with the amount of
		read sharing it. 
	"""

	def __init__(self, ebv_objects : list):
		self.ebv_objects = ebv_objects
		self.graph = nx.DiGraph()
		self.ebv_pop = self.gather_ebv()

		self.build_nodes()
		self.build_edges()
		self.graph = nx.algorithms.dag.transitive_reduction(self.graph)


	def gather_ebv(self) -> dict:
		"""
			Create a dictionary where the key is the binary vector and the value is the amount
			of read with this vector.
		"""
		d = {}
		for ebv in self.ebv_objects :
			if str(ebv) not in d :
				d[str(ebv)] = 1
			else :
				d[str(ebv)] += 1
		return d

	def build_nodes(self):
		"""
			Build nodes for each binary vector.
		"""
		for vector, population in self.ebv_pop.items() :
			self.graph.add_node(vector + "\n" + str(population))


	def build_edges(self):
		"""
			Build edges between each nodes. An edge is built when a vector is included
			into another one.
		"""
		for x, e1 in enumerate(self.ebv_objects) :
			for y, e2 in enumerate(self.ebv_objects) :
				if x != y  and str(e1) != str(e2):
					if e1.is_included(e2) :
						self.graph.add_edge(str(e1) + "\n" + str(self.ebv_pop[str(e1)]), str(e2) + "\n" + str(self.ebv_pop[str(e2)]))

	def write_dot_file(self, output : str = "graph.dot") :
		write_dot(self.graph, output)

	def get_graph(self):
		return self.graph

	import networkx as nx

	def merge_nodes(self):
		'''
		while there are leaf in the graph with one predecessor, merge the leaf with its predecessor
		'''
		while True:
			leaf_nodes = [node for node, out_degree in self.graph.out_degree() if out_degree == 0]

			if not leaf_nodes:
				break

			for leaf in leaf_nodes:
				predecessors = list(self.graph.predecessors(leaf))

				if not predecessors:
					continue

				# Only consider the first predecessor for simplicity
				pred = predecessors[0]

				# Merge: Add the labels of the two nodes
				self.graph.nodes[pred]['label'] += self.graph.nodes[leaf]['label']

				# Remove the edge between predecessor and leaf
				self.graph.remove_edge(pred, leaf)

				# Transfer all incoming edges of the leaf to its predecessor
				for incoming_node in self.graph.predecessors(leaf):
					self.graph.add_edge(incoming_node, pred)

				# Remove the leaf node
				self.graph.remove_node(leaf)

