import openpyxl
from openpyxl import Workbook
from openpyxl.styles import *
from openpyxl.styles.borders import Border, Side

from exon_matrix import AnnotatedCompactMatrix
from exon_matrix.Block import BlockReadStatus
from exon_matrix.PredictedIntron import *
from Bio import SeqIO
import random

class XLSXFont(object):
    """Represents every font used during the .xlsx export."""
    PLAIN_STANDARD = Font(name='Arial', size=12)
    PLAIN_BLUE = Font(name='Arial', size=12, color="4472c4")
    BOLD_STANDARD = Font(name='Arial', size=12, bold=True)


def random_color():
    r = lambda: random.randint(0, 255)
    RdColor = PatternFill(fgColor=str('%02X%02X%02X' % (r(), r(), r())), fill_type="solid")
    return RdColor



class XLSXCellColor(object):
    """Represents every color used during the .xlsx export."""
    LIGHT_GREEN = PatternFill(fgColor="c2d87f", fill_type="solid")
    GREEN = PatternFill(fgColor="bcbe59", fill_type="solid")
    LIGHT_BLUE = PatternFill(fgColor="ccccff", fill_type="solid")
    BLUE = PatternFill(fgColor="2a849e", fill_type="solid")
    GREY = PatternFill(fgColor="a9a9a9", fill_type="solid")
    LIGHT_RED = PatternFill(fgColor="f9b39d", fill_type="solid")
    VERY_LIGHT_RED = PatternFill(fgColor="fac4ae", fill_type="solid") # for INTRON RETENTION
    GRAY = PatternFill(fgColor="909090", fill_type="solid")
    LIGHT_GRAY = PatternFill(fgColor="c0c0c0", fill_type="solid")
    YELLOW = PatternFill(fgColor="eac300", fill_type="solid")
    LIGHT_YELLOW = PatternFill(fgColor="e5cf62", fill_type="solid")
    VERY_LIGHT_YELLOW = PatternFill(fgColor="f6df73", fill_type="solid") # for UTR
    WHITE = PatternFill(fgColor="ffffff", fill_type="lightGray")
    SURPRISE3 = random_color()


    def __init__(self):
        SURPRISE = self.get_random_color()
        SURPRISE1 = self.get_random_color()
        SURPRISE4 = self.get_random_color()

    def get_random_color(self):
        r = lambda: random.randint(0, 255)
        RdColor = PatternFill(fgColor=str('%02X%02X%02X' % (r(), r(), r())), fill_type="solid")
        return RdColor





class XLSXBorder(object):
    """Represents every border used during the .xlsx export."""
    THIN = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
    LEFT_THIN = Border(left=Side(style='thin'))
    RIGHT_THIN = Border(right=Side(style="thin"))
    LEFT_AND_RIGHT_THIN = Border(left=Side(style='thin'), right=Side(style='thin'))


class XLSXAlignment(object):
    """Represents every alignment used during the .xlsx export."""
    CENTER = Alignment(horizontal='center')
    LEFT = Alignment(horizontal='left')
    RIGHT = Alignment(horizontal='right')


class XLSXExporter(object):
    """
		This class will export an AlignedMatrixContent object to a .xlsx file. It will espcially takes
		the content of the compact matrix, so the compact matrix had to be built and setup, otherwise the
		following export will not work.
	"""

    def __init__(self, annotated_compact_matrix : AnnotatedCompactMatrix, output_filename: str = "compact_matrix.xlsx",
                 gene_filename=""):
        self.annotated_compact_matrix = annotated_compact_matrix
        self.output_filename = output_filename
        self.gene_filename = gene_filename
        self.wb = Workbook()
        self.ws = self.wb.active
        self.ws.title = "exonic matrix"
        self.ws.freeze_panes = "D2"  # freeze first columns and rows

        self.CONNECTION_SYMBOL = "———————"

        self.SCALE_LEVEL = 65  # scale used in the export file

        # starting cell from where matrix content will be written
        # self.MATRIX_KNOWN_EXON_ROW = 1
        self.MATRIX_PREDICTED_EXON_ROW = 1
        self.MATRIX_PREDICTED_COORDINATES_ROW = self.MATRIX_PREDICTED_EXON_ROW + 1
        self.MATRIX_PREDICTED_LENGTH_ROW = self.MATRIX_PREDICTED_COORDINATES_ROW + 1
        self.MATRIX_PREDICTED_SUBUNIT_ROW = self.MATRIX_PREDICTED_LENGTH_ROW + 1
        self.MATRIX_GFF_EXON_ROW = self.MATRIX_PREDICTED_SUBUNIT_ROW + 1
        self.MATRIX_BLOCK_LENGTH = self.MATRIX_GFF_EXON_ROW + 1
        self.MATRIX_SEQUENCES_ROW = self.MATRIX_BLOCK_LENGTH + 1
        self.MATRIX_BLOCK_COORDINATES = self.MATRIX_SEQUENCES_ROW + 1

        self.READ_BINARY_VECTOR_COLUMN = 1
        self.READ_COVERAGE_COLUMN = self.READ_BINARY_VECTOR_COLUMN + 1
        self.READ_COLUMN = self.READ_COVERAGE_COLUMN + 1
        self.READ_ROW = self.MATRIX_BLOCK_COORDINATES
        self.MATRIX_BLOCK_ROW = self.READ_ROW + 1
        self.MATRIX_COLUMN = self.READ_COLUMN + 1

        self.EXON_READ_POPULATION_ROW = self.MATRIX_SEQUENCES_ROW + len(
            self.annotated_compact_matrix.get_read_names()) + 2
        self.REALIGNABLE_ROW = self.EXON_READ_POPULATION_ROW + 1
        self.ENTERING_READ_JUNCTION_POINTS = self.REALIGNABLE_ROW + 1
        self.LEAVING_READ_JUNCTION_POINTS = self.ENTERING_READ_JUNCTION_POINTS + 1
        self.ENTERING_SPLICE_JUNCTION_POINTS = self.LEAVING_READ_JUNCTION_POINTS + 1
        self.LEAVING_SPLICE_JUNCTION_POINTS = self.ENTERING_SPLICE_JUNCTION_POINTS + 1
        # Write AnnotatedMatrix object content into a xlsx file object
        self.write_annotated_matrix()

        # optimize cells width after content has been written
        self.optimize_width()

    def apply_style_to_cell(self,
                            cell: openpyxl.cell.cell,
                            font: XLSXFont = None,
                            background_color: XLSXCellColor = None,
                            border: XLSXBorder = None,
                            alignment: XLSXAlignment = None):
        """Apply styles to a specific given cell. """
        if font:
            cell.font = font
        if background_color:
            cell.fill = background_color
        if border:
            cell.border = border
        if alignment:
            cell.alignment = alignment

    def write_annotated_matrix(self):
        """
			The XLSX file is written line per line. The main steps are separated
			in functions.
			It would be faster to write everything using two for loops, but it would
			seriously affect code readability. Hence, the steps are separated even though
			the functions are actually very very similar.
		"""
        self.write_read_names()
        self.write_read_binary_vectors()
        self.write_read_coverage()
        self.write_predicted_exon_intron_status()
        self.write_predicted_exon_intron_coordinates()
        #self.write_predicted_sub_units_coordinates()

        ### predicted sub unit coordinate et dupliquer cette fonction plus loin pour réussir à exporter.
        self.write_predicted_exon_intron_length()
        self.write_GFF_exon_intron_status()
        self.write_block_length()
        if self.gene_filename != "":
            self.write_block_sequences_border()
        self.write_block_coordinates()
        self.colorize_exons_introns()
        self.write_read_coordinates()
        self.write_in_between_reads_symbols()
        self.borderize_exons_introns()
        self.write_exons_population()
        self.write_realignable_intervals()
        self.write_entering_junction_points()
        self.write_leaving_junction_points()
        self.write_entering_splice_junction_points()
        self.write_leaving_splice_junction_points()



    def write_read_coverage(self):
        c = self.ws.cell(row=self.MATRIX_BLOCK_COORDINATES, column=self.READ_COVERAGE_COLUMN, value="read coverage (%)")
        self.apply_style_to_cell(c, alignment=XLSXAlignment.CENTER, font=XLSXFont.BOLD_STANDARD)

        reads = self.annotated_compact_matrix.get_reads()
        for index, read in enumerate(reads, start=1):
            coverage = int(read.get_read_coverage() * 100)
            c = self.ws.cell(row=self.MATRIX_BLOCK_COORDINATES + index, column=self.READ_COVERAGE_COLUMN,
                             value=str(coverage) + "%")
            self.apply_style_to_cell(c, alignment=XLSXAlignment.CENTER, font=XLSXFont.PLAIN_STANDARD)


    def write_block_sequences_border(self):
        block_index = 0
        record = list(SeqIO.parse(self.gene_filename, "fasta"))[0]
        for p in self.annotated_compact_matrix.get_annotated_compact_matrix():
            if isinstance(p, PredictedIntron):
                block_index += 1
            else:
                for b in p.get_blocks():
                    range1, range2 = (0, 0), (0, 0)
                    if b.get_begin() > 4:
                        range1 = (b.get_begin() - 4, b.get_begin())
                    if b.get_end() < self.annotated_compact_matrix.get_annotated_compact_matrix()[-1].get_end() - 4:
                        range2 = (b.get_end(), b.get_end() + 4)
                    edge_begin = str(record.seq)[range1[0]:range1[1]]
                    edge_end = str(record.seq)[range2[0] + 1:range2[1] + 1]
                    c = self.ws.cell(row=self.MATRIX_SEQUENCES_ROW, column=self.MATRIX_COLUMN + block_index,
                                     value=str(edge_begin) + ", " + str(edge_end))
                    self.apply_style_to_cell(c, alignment=XLSXAlignment.CENTER)
                    block_index += 1

    def write_read_binary_vectors(self):
        #for index, ebv in enumerate(self.annotated_compact_matrix.get_exonic_binary_vectors(), 1):
        for index, ebv in enumerate(self.annotated_compact_matrix.get_exonic_vector(), 1):

            c = self.ws.cell(row=self.READ_ROW + index, column=self.READ_BINARY_VECTOR_COLUMN, value=str(ebv))
            self.apply_style_to_cell(c, alignment=XLSXAlignment.CENTER)

        c = self.ws.cell(row=self.MATRIX_BLOCK_COORDINATES, column=self.READ_BINARY_VECTOR_COLUMN,
                         value="exonic binary vector")
        self.apply_style_to_cell(c, alignment=XLSXAlignment.CENTER, font=XLSXFont.BOLD_STANDARD)

    def write_leaving_junction_points(self):
        c = self.ws.cell(row=self.LEAVING_READ_JUNCTION_POINTS, column=self.READ_COLUMN,
                         value="leaving junction points")
        block_index = 0
        for p in self.annotated_compact_matrix.get_annotated_compact_matrix():
            # If it is an intron
            if isinstance(p, PredictedIntron):
                block_index = block_index + 1
            # if it is an exon
            else:
                for i in p.get_blocks():
                    leaving_junction_point = i.get_leaving_junction_point()
                    amount = str(leaving_junction_point.get_read_amount()) if leaving_junction_point != None else "?"
                    c = self.ws.cell(row=self.LEAVING_READ_JUNCTION_POINTS, column=self.MATRIX_COLUMN + block_index,
                                     value=amount)
                    self.apply_style_to_cell(c, alignment=XLSXAlignment.RIGHT)
                    block_index = block_index + 1

    def write_entering_junction_points(self):
        c = self.ws.cell(row=self.ENTERING_READ_JUNCTION_POINTS, column=self.READ_COLUMN,
                         value="entering junction points")
        block_index = 0
        for p in self.annotated_compact_matrix.get_annotated_compact_matrix():
            # If it is an intron
            if isinstance(p, PredictedIntron):
                block_index = block_index + 1
            # if it is an exon
            else:
                for i in p.get_blocks():
                    entering_junction_point = i.get_entering_junction_point()
                    amount = str(entering_junction_point.get_read_amount()) if entering_junction_point != None else "?"
                    c = self.ws.cell(row=self.ENTERING_READ_JUNCTION_POINTS, column=self.MATRIX_COLUMN + block_index,
                                     value=amount)
                    self.apply_style_to_cell(c, alignment=XLSXAlignment.LEFT)
                    block_index = block_index + 1

    def write_entering_splice_junction_points(self):
        c = self.ws.cell(row=self.ENTERING_SPLICE_JUNCTION_POINTS, column=self.READ_COLUMN,
                         value="entering splice junction points")
        block_index = 0
        for p in self.annotated_compact_matrix.get_annotated_compact_matrix():
            # If it is an intron
            if isinstance(p, PredictedIntron):
                block_index = block_index + 1
            # if it is an exon
            else:
                for i in p.get_blocks():
                    entering_splice_junction_point = i.get_entering_splice_junction_point()
                    amount = str(entering_splice_junction_point) if entering_splice_junction_point != None else "?"
                    c = self.ws.cell(row=self.ENTERING_SPLICE_JUNCTION_POINTS, column=self.MATRIX_COLUMN + block_index,
                                     value=amount)
                    self.apply_style_to_cell(c, alignment=XLSXAlignment.LEFT)
                    block_index = block_index + 1

    def write_leaving_splice_junction_points(self):
        c = self.ws.cell(row=self.LEAVING_SPLICE_JUNCTION_POINTS, column=self.READ_COLUMN,
                         value="leaving splice junction points")
        block_index = 0
        for p in self.annotated_compact_matrix.get_annotated_compact_matrix():
            # If it is an intron
            if isinstance(p, PredictedIntron):
                block_index = block_index + 1
            # if it is an exon
            else:
                for i in p.get_blocks():
                    leaving_splice_junction_points = i.get_leaving_splice_junction_point()
                    amount = str(leaving_splice_junction_points) if leaving_splice_junction_points != None else "?"
                    c = self.ws.cell(row=self.LEAVING_SPLICE_JUNCTION_POINTS, column=self.MATRIX_COLUMN + block_index,
                                     value=amount)
                    self.apply_style_to_cell(c, alignment=XLSXAlignment.LEFT)
                    block_index = block_index + 1

    def write_realignable_intervals(self):
        c = self.ws.cell(row=self.REALIGNABLE_ROW, column=self.READ_COLUMN, value="realignable interval")

        block_index = 0
        for p in self.annotated_compact_matrix.get_annotated_compact_matrix():
            # If it is an intron
            if isinstance(p, PredictedIntron):
                block_index = block_index + 1
            # if it is an exon
            else:
                for i in range(len(p.get_blocks())):
                    c = self.ws.cell(row=self.REALIGNABLE_ROW, column=self.MATRIX_COLUMN + block_index,
                                     value=str(p.get_blocks()[i].get_realignable_interval()))
                    self.apply_style_to_cell(c, alignment=XLSXAlignment.CENTER)
                    block_index = block_index + 1

    def write_exons_population(self):
        """
			Write on a line the coordinates of every predicted intron/exons.
			Moreover, for a predicted exon, if it spans on multiple blocks the cells
			will be merged before the coordinates are written.
		"""
        c = self.ws.cell(row=self.EXON_READ_POPULATION_ROW, column=self.READ_COLUMN, value="number of reads")

        block_index = 0
        for p in self.annotated_compact_matrix.get_annotated_compact_matrix():
            # If it is an intron
            if isinstance(p, PredictedIntron):
                intron_value = "0"
                c = self.ws.cell(row=self.EXON_READ_POPULATION_ROW, column=self.MATRIX_COLUMN + block_index,
                                 value=intron_value)
                self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD, alignment=XLSXAlignment.CENTER)
                block_index = block_index + 1
            # if it is an exon
            else:
                exon_value = str(p.get_population())
                # Merging columns in necessary if the exon has many predicted blocks
                if len(p.get_blocks()) > 1:
                    c = self.ws.cell(row=self.EXON_READ_POPULATION_ROW, column=self.MATRIX_COLUMN + block_index,
                                     value=exon_value)
                    self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD, alignment=XLSXAlignment.CENTER)
                    self.ws.merge_cells(start_row=self.EXON_READ_POPULATION_ROW,
                                        start_column=self.MATRIX_COLUMN + block_index,
                                        end_row=self.EXON_READ_POPULATION_ROW,
                                        end_column=self.MATRIX_COLUMN + block_index + len(p.get_blocks()) - 1)
                    block_index = block_index + len(p.get_blocks())
                else:
                    c = self.ws.cell(row=self.EXON_READ_POPULATION_ROW, column=self.MATRIX_COLUMN + block_index,
                                     value=exon_value)
                    self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD, alignment=XLSXAlignment.CENTER)
                    block_index = block_index + 1

    def borderize_exons_introns(self):
        """
			Add borders for columns
		"""
        block_number = 0
        for segment in self.annotated_compact_matrix.get_annotated_compact_matrix():
            # Intron case : no color
            if isinstance(segment, PredictedIntron):
                border = XLSXBorder.LEFT_AND_RIGHT_THIN
                for row_index in range(self.MATRIX_BLOCK_ROW + len(self.annotated_compact_matrix.get_read_names()) + 3):
                    c = self.ws.cell(row=row_index + 1, column=self.MATRIX_COLUMN + block_number)
                    self.apply_style_to_cell(c, border=border)
                block_number = block_number + 1
            # Exon case
            else:
                for block_index, block in enumerate(segment.get_blocks()):
                    # find the accurate background color for the column corresponding to the block
                    border = None
                    if block_index == 0:
                        for row_index in range(
                                self.MATRIX_BLOCK_ROW + len(self.annotated_compact_matrix.get_read_names()) + 3):
                            c = self.ws.cell(row=row_index + 1, column=self.MATRIX_COLUMN + block_number)
                            self.apply_style_to_cell(c, border=XLSXBorder.LEFT_THIN)
                    if block_index == len(segment.get_blocks()) - 1:
                        for row_index in range(
                                self.MATRIX_BLOCK_ROW + len(self.annotated_compact_matrix.get_read_names()) + 3):
                            c = self.ws.cell(row=row_index + 1, column=self.MATRIX_COLUMN + block_number)
                            self.apply_style_to_cell(c, border=XLSXBorder.RIGHT_THIN)

                    block_number = block_number + 1

    def colorize_exons_introns(self):
        """
			Add colors for each column according to their status.
		"""
        block_number = 0
        for segment in self.annotated_compact_matrix.get_annotated_compact_matrix():
            # Intron case : no color
            if isinstance(segment, PredictedIntron):
                block_number = block_number + 1
            # Exon case
            else:
                for block in segment.get_blocks():
                    # find the accurate background color for the column corresponding to the block
                    background_color = XLSXCellColor.WHITE

                    if block.is_uncertain():
                        background_color = XLSXCellColor.LIGHT_YELLOW
                    elif block.is_solid():
                        background_color = XLSXCellColor.YELLOW
                    elif block.is_semi_solid():
                        background_color = XLSXCellColor.LIGHT_BLUE
                    else:
                        background_color = XLSXCellColor.WHITE

                    for row_index in range(len(self.annotated_compact_matrix.get_read_names())):
                        c = self.ws.cell(row=self.MATRIX_BLOCK_ROW + row_index,
                                         column=self.MATRIX_COLUMN + block_number)
                        self.apply_style_to_cell(c, background_color=background_color)
                    block_number = block_number + 1

    def write_block_coordinates(self):
        c = self.ws.cell(row=self.MATRIX_BLOCK_COORDINATES, column=self.READ_COLUMN,
                         value="block coordinates (reference)")

        block_index = 0
        for segment in self.annotated_compact_matrix.get_annotated_compact_matrix():
            if isinstance(segment, PredictedIntron):
                intron_value = str((segment.get_begin(), segment.get_end()))
                c = self.ws.cell(row=self.MATRIX_BLOCK_COORDINATES, column=self.MATRIX_COLUMN + block_index,
                                 value=intron_value)
                self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD, alignment=XLSXAlignment.CENTER)
                block_index = block_index + 1
            else:
                for block in segment.get_blocks():
                    exon_value = str((block.get_begin(), block.get_end()))
                    c = self.ws.cell(row=self.MATRIX_BLOCK_COORDINATES, column=self.MATRIX_COLUMN + block_index,
                                     value=exon_value)
                    self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD, alignment=XLSXAlignment.CENTER)
                    block_index = block_index + 1

    def write_block_length(self):
        c = self.ws.cell(row=self.MATRIX_BLOCK_LENGTH, column=self.READ_COLUMN, value="block length (bp)")

        block_index = 0
        for segment in self.annotated_compact_matrix.get_annotated_compact_matrix():
            if isinstance(segment, PredictedIntron):
                intron_value = str(segment.get_end() - segment.get_begin() + 1)
                c = self.ws.cell(row=self.MATRIX_BLOCK_LENGTH, column=self.MATRIX_COLUMN + block_index,
                                 value=intron_value)
                self.apply_style_to_cell(c, font=XLSXFont.PLAIN_BLUE, alignment=XLSXAlignment.CENTER)
                block_index = block_index + 1
            else:
                for block in segment.get_blocks():
                    exon_value = str(block.get_end() - block.get_begin() + 1)
                    c = self.ws.cell(row=self.MATRIX_BLOCK_LENGTH, column=self.MATRIX_COLUMN + block_index,
                                     value=exon_value)
                    self.apply_style_to_cell(c, font=XLSXFont.PLAIN_BLUE, alignment=XLSXAlignment.CENTER)
                    block_index = block_index + 1

    def write_GFF_exon_intron_status(self):
        """
			Write on a line the status of the current predicted intron/exon with the one found in the
			gff file.
		"""
        c = self.ws.cell(row=self.MATRIX_GFF_EXON_ROW, column=self.READ_COLUMN, value="GFF introns/exons")

        block_index = 0
        for p in self.annotated_compact_matrix.get_annotated_compact_matrix():
            # If it is an intron
            if isinstance(p, PredictedIntron):
                intron_value = "intron"
                c = self.ws.cell(row=self.MATRIX_GFF_EXON_ROW, column=self.MATRIX_COLUMN + block_index,
                                 value=intron_value)
                self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD, alignment=XLSXAlignment.CENTER)
                block_index = block_index + 1
            # if it is an exon
            else:
                map_known_exon = []
                for k in self.annotated_compact_matrix.get_known_exons():
                    if k.contains(p):
                        map_known_exon.append((k.get_begin(), k.get_end()))
                exon_value = str(map_known_exon)

                # If the exon contains only one block that is missing and contains no inner alignments, it's a missing exon.
                if p.get_blocks()[0].is_missing() and p.get_blocks()[0].get_population() == 0:
                    exon_value = "missing exon"

                # Merging columns in necessary if the exon has many predicted blocks
                if len(p.get_blocks()) > 1:
                    c = self.ws.cell(row=self.MATRIX_GFF_EXON_ROW, column=self.MATRIX_COLUMN + block_index,
                                     value=exon_value)
                    self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD, alignment=XLSXAlignment.CENTER)
                    self.ws.merge_cells(start_row=self.MATRIX_GFF_EXON_ROW,
                                        start_column=self.MATRIX_COLUMN + block_index,
                                        end_row=self.MATRIX_GFF_EXON_ROW,
                                        end_column=self.MATRIX_COLUMN + block_index + len(p.get_blocks()) - 1)
                    block_index = block_index + len(p.get_blocks())
                else:
                    c = self.ws.cell(row=self.MATRIX_GFF_EXON_ROW, column=self.MATRIX_COLUMN + block_index,
                                     value=exon_value)
                    self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD, alignment=XLSXAlignment.CENTER)
                    block_index = block_index + 1

    def write_predicted_exon_intron_length(self):
        """
			Write on a line the length of every predicted intron/exons.
			Moreover, for a predicted exon, if it spans on multiple blocks the cells
			will be merged before the coordinates are written.
		"""
        c = self.ws.cell(row=self.MATRIX_PREDICTED_LENGTH_ROW, column=self.READ_COLUMN, value="predicted length (bp)")

        block_index = 0
        for p in self.annotated_compact_matrix.get_annotated_compact_matrix():
            # If it is an intron
            if isinstance(p, PredictedIntron):
                intron_value = str(p.get_end() - p.get_begin() + 1)
                c = self.ws.cell(row=self.MATRIX_PREDICTED_LENGTH_ROW, column=self.MATRIX_COLUMN + block_index,
                                 value=intron_value)
                self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD, alignment=XLSXAlignment.CENTER)
                block_index = block_index + 1
            # if it is an exon
            else:
                exon_value = str(p.get_end() - p.get_begin() + 1)
                # Merging columns in necessary if the exon has many predicted blocks
                if len(p.get_blocks()) > 1:
                    c = self.ws.cell(row=self.MATRIX_PREDICTED_LENGTH_ROW, column=self.MATRIX_COLUMN + block_index,
                                     value=exon_value)
                    self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD, alignment=XLSXAlignment.CENTER)
                    self.ws.merge_cells(start_row=self.MATRIX_PREDICTED_LENGTH_ROW,
                                        start_column=self.MATRIX_COLUMN + block_index,
                                        end_row=self.MATRIX_PREDICTED_LENGTH_ROW,
                                        end_column=self.MATRIX_COLUMN + block_index + len(p.get_blocks()) - 1)
                    block_index = block_index + len(p.get_blocks())
                else:
                    c = self.ws.cell(row=self.MATRIX_PREDICTED_LENGTH_ROW, column=self.MATRIX_COLUMN + block_index,
                                     value=exon_value)
                    self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD, alignment=XLSXAlignment.CENTER)
                    block_index = block_index + 1

    def write_predicted_exon_intron_coordinates(self):
        """
			Write on a line the coordinates of every predicted intron/exons.
			Moreover, for a predicted exon, if it spans on multiple blocks the cells
			will be merged before the coordinates are written.
		"""
        c = self.ws.cell(row=self.MATRIX_PREDICTED_COORDINATES_ROW, column=self.READ_COLUMN,
                         value="predicted positions")

        block_index = 0
        for p in self.annotated_compact_matrix.get_annotated_compact_matrix():
            # If it is an intron
            if isinstance(p, PredictedIntron):
                intron_value = str((p.get_begin(), p.get_end()))
                c = self.ws.cell(row=self.MATRIX_PREDICTED_COORDINATES_ROW, column=self.MATRIX_COLUMN + block_index,
                                 value=intron_value)
                self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD, alignment=XLSXAlignment.CENTER)
                block_index = block_index + 1
            # if it is an exon
            else:
                exon_value = str((p.get_begin(), p.get_end()))
                # Merging columns in necessary if the exon has many predicted blocks
                if len(p.get_blocks()) > 1:
                    c = self.ws.cell(row=self.MATRIX_PREDICTED_COORDINATES_ROW, column=self.MATRIX_COLUMN + block_index,
                                     value=exon_value)
                    self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD, alignment=XLSXAlignment.CENTER)
                    self.ws.merge_cells(start_row=self.MATRIX_PREDICTED_COORDINATES_ROW,
                                        start_column=self.MATRIX_COLUMN + block_index,
                                        end_row=self.MATRIX_PREDICTED_COORDINATES_ROW,
                                        end_column=self.MATRIX_COLUMN + block_index + len(p.get_blocks()) - 1)
                    block_index = block_index + len(p.get_blocks())
                else:
                    c = self.ws.cell(row=self.MATRIX_PREDICTED_COORDINATES_ROW, column=self.MATRIX_COLUMN + block_index,
                                     value=exon_value)
                    self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD, alignment=XLSXAlignment.CENTER)
                    block_index = block_index + 1

    def write_predicted_exon_intron_status(self):
        """
			Write on a line if the current column(s) corresponds to a predicted exon
			or a predicted intron. If it corresponds to a predicted intron, the splice sites
			are given. If it corresponds to a predicted exon, an exon id is added.
		"""
        c = self.ws.cell(row=self.MATRIX_PREDICTED_EXON_ROW, column=self.READ_COLUMN, value="Predicted exons/introns")
        self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD)

        block_index = 0
        exon_index = 1  # exon starts from 1 for human readability
        for p in self.annotated_compact_matrix.get_annotated_compact_matrix():
            # If it is an intron
            if isinstance(p, PredictedIntron):
                intron_value = "intron"
                c = self.ws.cell(row=self.MATRIX_PREDICTED_EXON_ROW, column=self.MATRIX_COLUMN + block_index,
                                 value=intron_value)
                self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD, alignment=XLSXAlignment.CENTER)
                block_index = block_index + 1
            # if it is an exon
            else:
                exon_value = "exon {}".format(exon_index)
                # Merging columns in necessary if the exon has many predicted blocks
                if len(p.get_blocks()) > 1:
                    c = self.ws.cell(row=self.MATRIX_PREDICTED_EXON_ROW, column=self.MATRIX_COLUMN + block_index,
                                     value=exon_value)
                    self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD, alignment=XLSXAlignment.CENTER)
                    self.ws.merge_cells(start_row=self.MATRIX_PREDICTED_EXON_ROW,
                                        start_column=self.MATRIX_COLUMN + block_index,
                                        end_row=self.MATRIX_PREDICTED_EXON_ROW,
                                        end_column=self.MATRIX_COLUMN + block_index + len(p.get_blocks()) - 1)
                    block_index = block_index + len(p.get_blocks())
                else:
                    c = self.ws.cell(row=self.MATRIX_PREDICTED_EXON_ROW, column=self.MATRIX_COLUMN + block_index,
                                     value=exon_value)
                    self.apply_style_to_cell(c, font=XLSXFont.BOLD_STANDARD, alignment=XLSXAlignment.CENTER)
                    block_index = block_index + 1
                exon_index = exon_index + 1

    def write_in_between_reads_symbols(self):
        """
			For every reads, write a symbol/string between the first and last block
			that is not covered by an alignment.
		"""
        reads = self.annotated_compact_matrix.get_read_names()
        for read_index, r in enumerate(reads):
            first_block = self.annotated_compact_matrix.get_read_first_block(r)
            last_block = self.annotated_compact_matrix.get_read_last_block(r)
            block_number = 0
            for index, segment in enumerate(self.annotated_compact_matrix.get_annotated_compact_matrix()):
                # Intron case
                if isinstance(segment, PredictedIntron):
                    if first_block.get_begin() < segment.get_begin() < last_block.get_begin():
                        c = self.ws.cell(row=read_index + self.MATRIX_BLOCK_ROW,
                                         column=self.MATRIX_COLUMN + block_number, value=self.CONNECTION_SYMBOL)
                        self.apply_style_to_cell(c, alignment=XLSXAlignment.CENTER)
                    block_number = block_number + 1
                # Exon case
                else:
                    for block in segment.get_blocks():
                        if first_block.get_begin() < block.get_begin() < last_block.get_begin() \
                                and not block.contain_read(r):
                            c = self.ws.cell(row=read_index + self.MATRIX_BLOCK_ROW,
                                             column=self.MATRIX_COLUMN + block_number, value=self.CONNECTION_SYMBOL)
                            self.apply_style_to_cell(c, alignment=XLSXAlignment.CENTER)
                        block_number = block_number + 1

        #exit(0)

    def write_read_names(self):
        """
			write read names in a single column
		"""
        # print("reads: " + str(len(self.annotated_compact_matrix.get_read_names())))
        for index, name in enumerate(self.annotated_compact_matrix.get_read_names(), 1):
            self.ws.cell(row=self.READ_ROW + index, column=self.READ_COLUMN, value=name)

        self.ws.cell(row=self.MATRIX_BLOCK_COORDINATES, column=self.READ_COLUMN, value="Reference positions")

    def write_read_coordinates(self):
        """
			write every read coordinates for each read in a Block
		"""
        block_number = 0
        for segment in self.annotated_compact_matrix.get_annotated_compact_matrix():
            if isinstance(segment, PredictedIntron):
                block_number = block_number + 1
            else:
                for block in segment.get_blocks():
                    for read_index, read_name in enumerate(self.annotated_compact_matrix.get_read_names()):
                        cell_value = ""

                        if block.contain_read(read_name):
                            cell_value = str(block.get_read_coordinates(read_name)) + " : " + str(
                                block.get_read_coordinates(read_name)[1] - block.get_read_coordinates(read_name)[0] + 1)
                            c = self.ws.cell(row=self.MATRIX_BLOCK_ROW + read_index,
                                             column=self.MATRIX_COLUMN + block_number, value=cell_value)
                            self.apply_style_to_cell(c, alignment=XLSXAlignment.CENTER)
                            if block.get_read_status(read_name) in [BlockReadStatus.UTR3, BlockReadStatus.UTR5]:
                                background = XLSXCellColor.VERY_LIGHT_YELLOW
                                self.apply_style_to_cell(c, alignment=XLSXAlignment.CENTER, background_color=background)
                            elif block.get_read_status(read_name) in [BlockReadStatus.INTRON_RETENTION]:
                                background = XLSXCellColor.VERY_LIGHT_RED
                                self.apply_style_to_cell(c, alignment=XLSXAlignment.CENTER, background_color=background)

                    block_number = block_number + 1

    def optimize_width(self):
        """
			Optimize column width after everything has been written.
		"""
        for column_cells in self.ws.columns:
            length = max(len(str(cell.value)) for cell in column_cells)
            self.ws.column_dimensions[openpyxl.utils.get_column_letter(column_cells[0].column)].width = length + 3

    def export(self):
        self.ws.sheet_view.zoomScale = self.SCALE_LEVEL
        self.wb.save(self.output_filename)
