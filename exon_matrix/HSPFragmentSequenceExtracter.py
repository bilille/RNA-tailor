import Bio
from Bio import SearchIO
from PredictIsoform import Logger

class HSPFragmentSequenceExtracter():
	"""
		This class can extract sequences on given positions for 
		a SearchIO.QueryResult object.
	"""

	def __init__(self, query_result : SearchIO.QueryResult, log_object : Logger = None) :

		self.LOGGER = log_object
		self.query_result = query_result
		self.read_name = query_result.id
		self.hit = self.query_result[0]
		self.hsp = self.hit[0]


	def get_read_name(self):
		return self.read_name


	def extract_sequences_from_reference_positions(self, reference_begin : int, reference_end : int) -> (str, str):
		"""
			Return the Hit and the Query sequences as a Tuple of two str between the two given positions.
			First this methods checks if the two positions can be found among the HSPFragments. If that's the case,
			and the same HSPFragment is to be found for the positions, the extract_sequence_from_fragment_on_reference_positions
			is called. If two differents fragments are found for the two positions an exception is thrown.
		"""
		reference_begin_fragment = self.get_fragment_from_reference_position(reference_begin)
		reference_end_fragment = self.get_fragment_from_reference_position(reference_end)
		
		# If no HSPFragment could not be found for both reference_begin and reference_end
		if not isinstance(reference_begin_fragment, Bio.SearchIO.HSPFragment) and not isinstance(reference_begin_fragment, Bio.SearchIO.HSPFragment) :
			self.LOGGER.add_comment_warning("Both positions on read : {}, {} and {} does not appear in any HSPFragment.".format(self.read_name, reference_begin, reference_end))
		
		# If HSPFragment could be found for reference_end but not reference_begin
		elif not isinstance(reference_begin_fragment, Bio.SearchIO.HSPFragment) and isinstance(reference_begin_fragment, Bio.SearchIO.HSPFragment):
			self.LOGGER.add_comment_warning("None fragments has been found on read {} for the reference begin position : {}".format(self.read_name, reference_begin))
		
		# If HSPFragment could be found for reference_begin but not reference_end
		elif not isinstance(reference_end_fragment, Bio.SearchIO.HSPFragment) and isinstance(reference_begin_fragment, Bio.SearchIO.HSPFragment) :
			self.LOGGER.add_comment_warning("None fragments has been found on read {} for the reference end position : {}".format(self.read_name, reference_end))
		
		# If HSPFragment could be found for reference_begin and reference_end
		elif isinstance(reference_begin_fragment, Bio.SearchIO.HSPFragment) and isinstance(reference_end_fragment, Bio.SearchIO.HSPFragment):
			
			# If the HSPFragment found for the two positions is the same, we just need to extract the good alignment portion
			# in the sequences
			if reference_begin_fragment == reference_end_fragment :
				return self.extract_sequence_from_fragment_on_reference_positions(reference_begin_fragment, reference_begin, reference_end)
			
			# If the HSPFragment found for the two positions is different, there is a problem. What should we put for nucleotids characters
			# on the positions between the two HSPFragments ?
			else :
				self.LOGGER.add_comment_warning("HSPFragment found is different on begin position {} and end position {} for read {}".format(reference_begin, reference_end, self.read_name))
				return self.extract_sequence_from_fragments_on_reference_positions(reference_begin_fragment, reference_end_fragment, reference_begin, reference_end)
		else :
			self.LOGGER.add_comment_warning("An unpexpected error happened. reference_begin_fragment and reference_end_fragment should both be a HSPFragment object or None :")
			self.LOGGER.add_comment_warning("reference_begin_fragment : {}".format(reference_begin_fragment))
			self.LOGGER.add_comment_warning("reference_end_fragment : {}".format(reference_end_fragment))
			self.LOGGER.add_comment_warning("full HSP: " + str(self.hsp))

		return ("","")

	def extract_sequence_from_fragments_on_reference_positions(self, reference_begin_fragment, reference_end_fragment, reference_begin, reference_end) :
		if reference_begin_fragment.hit_range[1] == reference_end_fragment.hit_range[0] and \
			reference_begin_fragment.query_range[1] == reference_end_fragment.query_range[0] :
			fragment_hit_seq = str(reference_begin_fragment.hit.seq) + str(reference_end_fragment.hit.seq)
			fragment_query_seq = str(reference_begin_fragment.query.seq) + str(reference_end_fragment.query.seq)
			
			# compute reference beginning position
			# -> count the number of characters between before the alignment on the reference sequence
			nt_count = 0
			char_count = 0
			while char_count < len(fragment_hit_seq) and nt_count < reference_begin - reference_begin_fragment.hit_range[0] :
				if fragment_hit_seq[char_count] != '-' :
					nt_count = nt_count + 1
				char_count = char_count + 1

			alignment_ref_begin = char_count

			# compute reference ending position
			# -> count the number of characters between the beginning and ending position on the reference alignment sequence
			nt_count = 0
			char_count = 0
			while char_count + alignment_ref_begin < len(fragment_hit_seq) and nt_count < reference_end - reference_begin + 1:
				if fragment_hit_seq[alignment_ref_begin + char_count] != '-' :
					nt_count = nt_count + 1
				char_count = char_count + 1

			alignment_ref_end = alignment_ref_begin + char_count
			alignment_read_begin = alignment_ref_begin
			alignment_read_end = alignment_read_begin + (alignment_ref_end - alignment_ref_begin)


			if alignment_ref_end - alignment_ref_begin != alignment_read_end - alignment_read_begin :
				self.LOGGER.add_comment_warning("Warning : alignment length mismatch between positions {} and {} on read {}".format(reference_begin, reference_end, self.read_name))
			ref_sequence = str(fragment_hit_seq)[alignment_ref_begin:alignment_ref_end]
			read_sequence = str(fragment_query_seq)[alignment_read_begin:alignment_read_end]
	  	
			self.LOGGER.add_comment_warning("HSPFragments could be mixed")
			self.LOGGER.add_comment_warning("reference_begin_fragment: " + str(reference_begin_fragment))
			self.LOGGER.add_comment_warning("reference_end_fragment: " + str(reference_end_fragment))
			self.LOGGER.add_comment_warning("ref_sequence: " + str(ref_sequence))
			self.LOGGER.add_comment_warning("read_sequence: " + str(read_sequence))
			return (ref_sequence, read_sequence)
		else :
			self.LOGGER.add_comment_warning("HSPFragments can't be mixed :\n" + str(reference_begin_fragment) + "\n"+ str(reference_end_fragment))
			self.LOGGER.add_comment_warning("reference_begin_fragment.hit_range[0] " + str(reference_begin_fragment.hit_range[0]))
			self.LOGGER.add_comment_warning("reference_begin_fragment.hit_range[1]: " + str(reference_begin_fragment.hit_range[1]))
			self.LOGGER.add_comment_warning("reference_end_fragment.hit_range[0]: " + str(reference_end_fragment.hit_range[0]))
			self.LOGGER.add_comment_warning("reference_end_fragment.hit_range[1]: " + str(reference_end_fragment.hit_range[1]))

			self.LOGGER.add_comment_warning("reference_begin_fragment.query_range[0] " + str(reference_begin_fragment.query_range[0]))
			self.LOGGER.add_comment_warning("reference_begin_fragment.query_range[1]: " + str(reference_begin_fragment.query_range[1]))
			self.LOGGER.add_comment_warning("reference_end_fragment.query_range[0]: " + str(reference_end_fragment.query_range[0]))
			self.LOGGER.add_comment_warning("reference_end_fragment.query_range[1]: " + str(reference_end_fragment.query_range[1]))
			exit()

	def extract_sequence_from_fragment_on_reference_positions(self, fragment : Bio.SearchIO.HSPFragment, reference_begin : int, reference_end : int) -> (str, str):
		assert fragment != None, "Fragment is missing"

		# compute reference beginning position
		# -> count the number of characters between before the alignment on the reference sequence
		nt_count = 0
		char_count = 0
		while char_count < len(fragment.hit.seq) and nt_count < reference_begin - fragment.hit_range[0] :
			if fragment.hit.seq[char_count] != '-' :
				nt_count = nt_count + 1
			char_count = char_count + 1

		alignment_ref_begin = char_count

		# compute reference ending position
		# -> count the number of characters between the beginning and ending position on the reference alignment sequence
		nt_count = 0
		char_count = 0
		while char_count + alignment_ref_begin < len(fragment.hit.seq) and nt_count < reference_end - reference_begin + 1:
			if fragment.hit.seq[alignment_ref_begin + char_count] != '-' :
				nt_count = nt_count + 1
			char_count = char_count + 1

		alignment_ref_end = alignment_ref_begin + char_count
		alignment_read_begin = alignment_ref_begin
		alignment_read_end = alignment_read_begin + (alignment_ref_end - alignment_ref_begin)


		if alignment_ref_end - alignment_ref_begin != alignment_read_end - alignment_read_begin :
			self.LOGGER.add_comment_warning("Warning : alignment length mismatch between positions {} and {} on read {}".format(reference_begin, reference_end, self.read_name))
		ref_sequence = str(fragment.hit.seq)[alignment_ref_begin:alignment_ref_end]
		read_sequence = str(fragment.query.seq)[alignment_read_begin:alignment_read_end]
  
		return (ref_sequence, read_sequence)

	def get_fragment_from_reference_position(self, reference_position : int) -> SearchIO.HSPFragment :
		"""
			Return a HSPFragment where the reference_position is, or None if no HSPFragments contain
			the reference position.
		"""
		fragment_index = -1
		is_on_fragment = False
		while not is_on_fragment and fragment_index < len(self.hsp.fragments) :
			fragment_index = fragment_index + 1
			is_on_fragment = self.hsp.fragments[fragment_index - 1].hit_range[0] <= reference_position <= self.hsp.fragments[fragment_index - 1].hit_range[1]

		return self.hsp.fragments[fragment_index - 1] if is_on_fragment else None

	def get_fragment_from_read_position(self, read_position : int) -> SearchIO.HSPFragment : 
		"""
			Return a HSPFragment where the read_position is, or None if no HSPFragments contain
			the read position.
		"""
		fragment_index = -1
		is_on_fragment = False
		while fragment_index < len(self.hsp.fragments) and not is_on_fragment :
			fragment_index = fragment_index + 1
			is_on_fragment = self.hsp.fragments[fragment_index - 1].query_range[0] <= read_position <= self.hsp.fragments[fragment_index - 1].query_range[1]

		if is_on_fragment :
			return self.hsp.fragments[fragment_index]
		return None

	def is_reference_position_on_fragments(self, reference_position : int) -> bool :
		"""
			Scan every HSPFragment object until one is found containing the reference position.
			If one fragment contains the reference position, True is returned, if none of the 
			HSPFragments contains the reference position, False is returned.
		"""

		return isinstance(self.get_fragment_from_reference_position(reference_position), Bio.SearchIO.HSPFragment)

	def is_read_position_on_fragments(self, read_position : int) -> bool :
		"""
			Scan every HSPFragment object until one is found containing the read position.
			If one fragment contains the read position, True is returned, if none of the 
			HSPFragments contains the read position, False is returned.
		"""
		return isinstance(self.get_fragment_from_read_position(read_position), Bio.SearchIO.HSPFragment)
