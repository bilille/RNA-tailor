from exon_matrix.JunctionPoint import *
from seq.Read import *
class ReadJunctionPoint() :
	"""
		Gather every informations for a read about its junction points.
		The junction points are the beginning and the ending of an alignment
		between that read and the reference sequence.
		Moreover, a junction point is considered as "isolated" if it is not shared by
		any other read.
	"""
	
	def __init__(self, read : Read):
		self.read = read
		self.read_name = read.get_read_name()
		self.junction_points = []

	def add_junction_point(self, junction_point : JunctionPoint):
		if isinstance(junction_point, JunctionPoint) and junction_point not in self.junction_points :
			self.junction_points.append(junction_point)

	def get_read(self):
		return self.read

	def get_read_name(self) -> str :
		return self.read_name

	def get_junction_points(self) -> [JunctionPoint] :
		return self.junction_points

	def get_isolated_percentage(self, ignore_junction_start_and_end : bool = False ) -> int :
		"""
			return the percentage of isolated junction points among the junction points.
		"""
		isolated_junction_points = 0
		for jp in self.junction_points if not ignore_junction_start_and_end else self.junction_points[1:-2]:
			if jp.is_isolated() :
				isolated_junction_points = isolated_junction_points + 1

		if ignore_junction_start_and_end :
			return (isolated_junction_points / (len(self.junction_points) - 2)) * 100 if len(self.junction_points) > 2 else 0
		return	isolated_junction_points / len(self.junction_points)  * 100 

	def get_isolated_amount(self, ignore_junction_start_and_end : bool = False) -> int :
		isolated_junction_points = 0
		for jp in self.junction_points if not ignore_junction_start_and_end else self.junction_points[1:-2] :
			isolated_junction_points += 1 if jp.is_isolated() else 0
		return isolated_junction_points

	def sort_junction_points(self):
		self.junction_points = sorted(self.junction_points, key=lambda x: x.reference_position)

	def __str__(self) :
		res = self.read_name + "\t" + str(self.get_isolated_percentage()) +  "\n"
		for jp in self.junction_points :
			res = res + "\t" + str(jp) + "\n"
		return res