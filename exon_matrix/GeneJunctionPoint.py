'''
default value is SOLID_JUNCTION_MINIMUM = 2

This constant represents the percentage which will be multiplied by the bloc_population to increase allowability
of a bloc to become solid when the previous/next bloc population has a high read population.

SOLID_JUNCTION_MINIMUM value becomes PERCENT * self.current_bloc_population if this new value is higher than
SOLID_JUNCTION_MINIMUM,
with PERCENT = 0.05
'''
import config


class GeneJunctionPoint() :
	"""
		A junction point on the gene.
	"""
	
	def __init__(self, gene_position : int, current_bloc_population : int, delta_read : int, junction_status) :
		"""
			gene_position (int) : the position on the gene
			current_bloc_population (int) : the population of the bloc
			delta_read (int) : the amount of read starting/ending on that position, ie the delta between either
			entering or leaving status
			junction_status (JunctionStatus) : JunctionStatus.ENTERING or JunctionStatus.LEAVING
		"""
		self.gene_position = gene_position
		self.delta_read = delta_read
		self.junction_status = junction_status
		self.current_bloc_population = current_bloc_population

	def is_solid(self) -> bool :
		"""
			A junction point is considered solid if there is a minimum amount
			of reads starting/ending for that position. Otherwise it is considerd
			fragile.
		"""
		return self.delta_read >= max(config.SOLID_JUNCTION_MINIMUM, config.PERCENT * self.current_bloc_population)
		#return self.delta_read >= SOLID_JUNCTION_MINIMUM

	def is_fragile(self) -> bool :
		return not self.is_solid()

	def get_read_amount(self) -> int :
		return self.delta_read

	def update(self, current_bloc_population : int, delta_read : int):
		self.delta_read = delta_read
		self.current_bloc_population = current_bloc_population