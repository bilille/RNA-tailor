class PredictedIntron():

	def __init__(self, begin : int, end : int):
		self.begin = begin
		self.end = end

	def __str__(self) :
		return "PredictedIntron : (" + str(self.begin) + ", " + str(self.end) + ")"

	def get_begin(self):
		return self.begin

	def get_end(self):
		return self.end