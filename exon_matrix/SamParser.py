import argparse
import pysam
from Bio.Seq import reverse_complement


class SamParser:
    """
    	Parse a SAM file output from Minimap2, filters its reads by query coverage and save its content.
        It calculates the read alignment coverage computing the ratio (aligned_bases / read_lenght).
        If an aligned read to the target gene passes this ratio, it is kept in the output filter SAM file.
    """
    def __init__(self, input_sam : str, output_sam : str, gene_region : str, alignment_coverage : int, gene_cover_threshold : int, gene_name : str):
        self.input_sam = input_sam
        self.output_sam = output_sam
        self.start = 0
        self.stop = 0
        self.sens = ""
        self.gene_region = self.set_region_of_interest(gene_region)
        self.threshold = float(alignment_coverage)
        self.gene_cover_threshold = float(gene_cover_threshold)
        self.output_fasta = f"{gene_name}/minimap2.fa"
        self.window = 50
        #print(self.gene_region)

    def set_region_of_interest(self, gff_file_path : str):
        with open(gff_file_path, 'r') as gff_file:
            for line in gff_file:
                if 'Gene' in line:
                    chr = line.split()[0]
                    self.start = int(line.split()[3])
                    self.stop = int(line.split()[4])
                    self.sens = str(line.split()[6])
                    return f'{chr}:{self.start}-{self.stop}'


    # process the read

    def filter_reads(self):
        '''
        Loads the input SAM file and GFF file
        '''
        samfile = pysam.AlignmentFile(self.input_sam, 'r')
        with open(self.output_fasta, 'w') as fasta_out:
            with pysam.AlignmentFile(self.output_sam, 'w', template=samfile) as outfile:
                for read in samfile.fetch(region=self.gene_region):
                    if read.reference_length == 0:
                        continue
                    blocks = read.get_blocks()
                    aligned_bases=0
                    for i in range(len(blocks)):
                        if blocks[i][0] >= self.start- self.window:
                            if blocks[i][1] <= self.stop + self.window:
                                aligned_bases=aligned_bases+(blocks[i][1] - blocks[i][0])
                    if read.query_sequence is not None:
                        if self.is_read_above_ref_cover(read):
                            if aligned_bases >= self.threshold/100 * read.query_length:
                                outfile.write(read)
                                if self.sens == "+":
                                    if read.is_reverse:
                                        fasta_out.write(">{} (reversed)\n{}\n".format(read.query_name, read.query_sequence))
                                    else:
                                        fasta_out.write(">{}\n{}\n".format(read.query_name, read.query_sequence))
                                if self.sens == "-":
                                    if not read.is_reverse:
                                        fasta_out.write(">{} (reversed)\n{}\n".format(read.query_name, reverse_complement(read.query_sequence)))
                                    else:
                                        fasta_out.write(">{}\n{}\n".format(read.query_name, reverse_complement(read.query_sequence)))

    def is_read_above_ref_cover(self,read) -> bool:
        '''
        The  method calculates the reference coverage, i.e. the proportion of the reference sequence covered by the alignment.
        '''
        idelta = float((max(read.get_blocks()[-1][1], read.get_blocks()[0][0]) - min(read.get_blocks()[-1][1], read.get_blocks()[0][0])) \
            / (max(self.start,self.stop) - min(self.start,self.stop)))
        return self.gene_cover_threshold <= idelta

    def compute_exon_cover_threshold(self, read):
        if self.liste_of_known_exons:
            for exon in self.liste_of_known_exons:
                blocks = read.get_blocks()
                aligned_bases = 0
                for i in range(len(blocks)):
                    if blocks[i][0] >= exon[0]:
                        if blocks[i][1] <= exon[1]:
                            aligned_bases = aligned_bases + (blocks[i][1] - blocks[i][0])

            # if aligned_bases >= self.exon_cover_threshold / 100 * read.query_length:
            #     continue


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', required=True, help='Input SAM file from minimap2 mapping')
    parser.add_argument('--output', required=True, help='Output SAM file')
    parser.add_argument('--gene_region', required=True, help='Region of interest defined in a GFF file')
    parser.add_argument('--gene_exon', required=False, help='Exons coordinates from gene start')
    parser.add_argument('--threshold', required=True, help='Threshold of read alignment coverage on the gene locus')
    parser.add_argument('--gene_cover_threshold', required=True, help='Threshold of read alignment coverage on the gene s exon')
    parser.add_argument('--gene_name', required=True, help='Name of the gene of interest (Ensembl ID)')
    args = parser.parse_args()
    filter_reads = SamParser(args.input, args.output, args.gene_region, args.threshold, args.gene_cover_threshold, args.gene_name)
    filter_reads.filter_reads()