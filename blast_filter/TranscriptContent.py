FEATURE_TRANSCRIPT="transcript"
FEATURE_EXON="exon"
FEATURE_GENE="gene"
STRAND_TO_INT={"+":1,"-":-1}

EXON_BASENAME="Ex"
EXON_SEPARATOR="."
EXON_INTERNALSEPARATOIR="-"
GFF_COLUMN=["seqname","source","feature","start","end","score","strand","frame","attribute"]

class TranscriptContent:
    
    def __init__(self,sGeneId,sRefFile):
        self.gene_start=None
        self.gene_end=None
        self.gene_strand=None
        self.transcript_to_exon={}
        self.set_emptyExonList()
        
        self.parse_gffFile(sRefFile,sGeneId)
        self.fusion_firstCommonStart()
        self.fusion_lastCommonStop()
        self.sort_exonList()
        self.assign_exonId()
        self.update_transcriptList()
        self.correct_exonCoord()
    
    def parse_gffFile(self,sPathFile,sGeneId):
        sTrId=None
        
        for sLine in open(sPathFile):
            sLine=sLine.replace("\n","")
            tLine=sLine.split("\t")
            #tRef=["seqname","source","feature","start","end","score","strand","frame","attribute"]
            dRef2Content=dict(zip(GFF_COLUMN,tLine))
            tAttribute=dRef2Content["attribute"].split(";")
            sCurrentGeneId=tAttribute[-1].split(" ")[-1]
            if len(tAttribute)>1:
                sCurrentTrId=tAttribute[-2].split(" ")[-1]
            if len(tAttribute)>2:
                sCurrentExonId=tAttribute[-3].split(" ")[-1]
            if sCurrentGeneId==sGeneId and dRef2Content["feature"].lower()==FEATURE_TRANSCRIPT:
                sTrId=sCurrentTrId
                self.add_transcript(sTrId)
            elif sCurrentGeneId==sGeneId and dRef2Content["feature"].lower()==FEATURE_EXON and sTrId in dRef2Content["attribute"]:
                oExon=ExonContent(int(dRef2Content["start"]),int(dRef2Content["end"]),sTrId)
                self.update_exonList(oExon)
            elif sCurrentGeneId==sGeneId and dRef2Content["feature"].lower()==FEATURE_GENE:
                self.set_geneCoord(int(dRef2Content["start"]),int(dRef2Content["end"]),STRAND_TO_INT[dRef2Content["strand"]])
    
    def fusion_lastCommonStop(self):
        self.sort_exonList()
        iLastStart=None
        tExonCommonStop=[]
        for oExon in self.get_exonList()[::-1]:
            if not iLastStart:
                iLastStart=oExon.get_start()
                tExonCommonStop.append(oExon)
            elif iLastStart==oExon.get_start():
                tExonCommonStop.append(oExon)
        if len(tExonCommonStop)>1:
            oRefExon=tExonCommonStop[0]
            for oExon in tExonCommonStop[1:]:
                oRefExon.update(oExon)
                self.remove_exon(oExon)
    
    def fusion_firstCommonStart(self):
        self.sort_exonList()
        iFirstStop=None
        tExonCommonStart=[]
        for oExon in self.get_exonList():
            if not iFirstStop:
                iFirstStop=oExon.get_stop()
                tExonCommonStart.append(oExon)
            elif iFirstStop==oExon.get_stop():
                tExonCommonStart.append(oExon)
        if len(tExonCommonStart)>1:
            oRefExon=tExonCommonStart[0]
            for oExon in tExonCommonStart[1:]:
                oRefExon.update(oExon)
                self.remove_exon(oExon)
    
    def set_geneCoord(self,iStart,iEnd,iStrand):
        self.gene_start=iStart
        self.gene_end=iEnd
        self.gene_strand=iStrand
        
    def get_geneStart(self):
        return self.gene_start
                
    def add_transcript(self,sId):
        self.transcript_to_exon[sId]=[]
    
    def set_transcriptRelation(self,sTrId,sExId):
        self.transcript_to_exon[sTrId].append(sExId)
    
    def update_transcriptList(self):
        for oExon in self.get_exonList():
            for sTranscript in oExon.get_transcript():
                self.set_transcriptRelation(sTranscript,oExon.get_index())
    
    def correct_exonCoord(self):
        iAbsoluStart=self.get_geneStart()
        for oExon in self.get_exonList():
            iRelativeStart=oExon.get_start()-iAbsoluStart
            iRelativeStop=oExon.get_stop()-iAbsoluStart
            oExon.set_gene_start(iRelativeStart)
            oExon.set_gene_stop(iRelativeStop)
    
    def sort_exonList(self):
        tTemp=[]
        dDict={}
        tListExon=self.get_exonList()
        for oExon in tListExon:
            try:
                dDict[oExon.get_start()].append(oExon)
            except KeyError:
                dDict[oExon.get_start()]=[oExon]
        for iStart in sorted(dDict.keys()):
            for oExon in sorted(dDict[iStart], key=lambda x: x.stop):
                tTemp.append(oExon)
        self.set_exonList(tTemp)
    
    def assign_exonId(self):
        dTrId2Order={}
        dTrId2ExonNumber={}
        iExonId=0
        for oExon in self.get_exonList():
            iExonId+=1
            for iTrId in oExon.get_transcript():
                if iTrId not in dTrId2Order:
                    dTrId2Order[iTrId]=len(dTrId2Order)+1
                if iTrId not in dTrId2ExonNumber:
                    dTrId2ExonNumber[iTrId]=1
                else:
                    dTrId2ExonNumber[iTrId]+=1
            else:
                sTrName=EXON_BASENAME+str(iExonId)
                sTrName+=EXON_SEPARATOR
                sTrName+=EXON_INTERNALSEPARATOIR.join(sorted([str(dTrId2Order[X]) for X in oExon.get_transcript()]))
            oExon.set_index(sTrName)
            
    def set_emptyExonList(self):
        self.exon_list=[]
        
    def get_exonList(self):
        return self.exon_list
        
    def set_exonList(self,tList):
        self.exon_list=list(tList)
        
    def add_exon(self,oExon):
        self.exon_list.append(oExon)
        
    def remove_exon(self,oExon):
        self.exon_list.remove(oExon)
        
    def update_exonList(self,oNewExon):
        iNewStart=oNewExon.get_start()
        iNewStop=oNewExon.get_stop()
        tExonList=self.get_exonList()
        bTheSame=False
        for oOtherExon in tExonList:
            if iNewStart==oOtherExon.get_start() and iNewStop==oOtherExon.get_stop():
                oOtherExon.update(oNewExon)
                bTheSame=True
                break
        if not bTheSame:
            self.add_exon(oNewExon)
        
    def describe_exon(self,strand_dependant=True):
        sContent=""
        if strand_dependant and self.gene_strand == -1:
            exonList = reversed(self.get_exonList())
        else:
            exonList = self.get_exonList()
        for oExon in exonList:
            if len(sContent)!=0:
                sContent+"\n"
            sContent+=oExon.describe_self()
        return sContent
