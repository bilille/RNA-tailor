from blast_filter.ToolContent import *
from blast_filter.AlignContent import *

from Bio.Blast import NCBIXML

class BlastContent(ToolContent): 
       
    def __init__(self,sFile):
        self.strand=None
        self.set_tool("blast")
        oFileContent=open(sFile)
        oAllBlastMatchs=NCBIXML.parse(oFileContent)
        bFirst=True
        for oBlastMatch in oAllBlastMatchs:
            oAlignments=oBlastMatch.alignments
            if bFirst:
                bFirst=False
            else:
                exit("ERROR 48 : multiple blast match ??")
            self.set_queryId(oBlastMatch.query)
            self.set_querySize(oBlastMatch.query_letters)
            self.set_emptyAlignList()
            
            if len(oAlignments)!=0:
                for oUniqAlignment in oAlignments:

                    sRefId=oUniqAlignment.hit_def
                    sRefId=sRefId.replace(" No definition line","")

                    sRefSize=oUniqAlignment.length
                    tHspList=oUniqAlignment.hsps
                    oAlignContent=AlignContent(sRefId,sRefSize,tHspList)
                    self.add_align(oAlignContent)
                    
                oLambdaHsp=tHspList[0]
                iQueryStrand=oLambdaHsp.frame[0]
                self.set_strand(iQueryStrand)