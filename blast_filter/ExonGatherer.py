
class ExonGatherer(object):
	"""
		Take a gff file and try to gather every overlapping exons into a single one.
		None overlapping exons must remain as they are after the gathering is done.
		The exon gathering is done in three steps :
		1 - Collecting every exons from the gff file and save them in a convenient
			data structure for later.
		2 - Doing the computational work to gather exons.
		3 - Writing the results in a txt file which has a different structure than a gff.
	"""
	def __init__(self, exon_gff_file, output_file="exon.txt") :
		assert exon_gff_file != None, "a gff file with exons must be provided"

		print(">> Collecting exons")
		self.collect_exons(exon_gff_file)

		print(">> Gathering exons")
		self.gather_exons()

		print(">> Writing exons")
		self.write_exons(output_file)

	def collect_exons(self, exon_gff_file):
		assert exon_gff_file != None, "a gff file with exons must be provided"

		print(">> read " + exon_gff_file)
		file = open(exon_gff_file)

		self.exon_list = []
		for line in file :
			fields = line.split("\t")
			if len(fields) > 3 and fields[2].lower() == "exon" :
				self.exon_list.append((int(fields[3]), int(fields[4])))
		file.close()

	def gather_exons(self):
		"""
	    This algorithm will merge overlapping exons into a single one.
	    1. Sort the exons in increasing order
	    2. Push the first exon on the stack
	    3. Iterate through intervals and for each one compare current exon
	       with the top of the stack and:
	       A. If current exon does not overlap, push on to stack
	       B. If current exon does overlap, merge both intervals in to one
	          and push on to stack
	    4. At the end return stack
	    """

		sorted_by_lower_bound = sorted(self.exon_list, key=lambda tup: tup[0])
		self.merged_exons = []

		for higher in sorted_by_lower_bound:
		    if not self.merged_exons:
		        self.merged_exons.append(higher)
		    else:
		        lower = self.merged_exons[-1]
		        # test for intersection between lower and higher:
		        # we know via sorting that lower[0] <= higher[0]
		        if higher[0] <= lower[1]:
		            upper_bound = max(lower[1], higher[1])
		            self.merged_exons[-1] = (lower[0], upper_bound)  # replace by merged interval
		        else:
		            self.merged_exons.append(higher)

	def write_exons(self, output_file):
		assert output_file != None, "an ouput file name must be provided"

		print(">> open " + output_file)
		f = open(output_file, "w")

		for i in range(len(self.merged_exons)) :
			f.write("Ex" + str(i) + "\t" + str(self.merged_exons[i][0]) + "\t" + str(self.merged_exons[i][1]) + "\n")

		f.close()