class AlignContent:
    
    def __init__(self,sId,sSize,tObjectList):
        self.align_id=sId
        self.align_size=sSize
        self.align_hsp=tObjectList
        self.exonCovering={}
        
        self.cumul_read_strand=None
        self.cumul_oriented_read_size=None
        self.cumul_gene_strand=None
        self.cumul_oriented_gene_size=None
        self.assign_cumul_strand()
        
        self.read_strand=None
        self.gene_strand=None
        self.assign_major_strand()

    def get_read_size(self):
        return self.align_size
    
    def remove_badStrandHsp(self,dStrand):
        tToRemove=[]
        iMegablastGeneStrand=dStrand["geneStrand"]
        iMegablastReadStrand=dStrand["readStrand"]
        for oHsp in self.get_hspList():
            if oHsp.frame[0]==iMegablastGeneStrand:
                if oHsp.frame[1]!=iMegablastReadStrand:
                    tToRemove.append(oHsp)
            elif oHsp.frame[0]==-iMegablastGeneStrand:
                if oHsp.frame[1]==iMegablastReadStrand:
                    tToRemove.append(oHsp)
        tNewHspList=[X for X in self.get_hspList() if X not in tToRemove]
        self.set_hspList(tNewHspList)
    
    def assign_cumul_strand(self):
        iGeneStrand=0
        iReadStrand=0

        for oHsp in self.get_hspList():
            iGeneStrand+=oHsp.frame[0]
            iReadStrand+=oHsp.frame[1]
        self.set_cumul_gene_strand(iGeneStrand)
        self.set_cumul_read_strand(iReadStrand)
    
    def assign_major_strand(self):
        if self.get_cumul_gene_strand()>0:
            self.set_gene_strand(1)
        elif self.get_cumul_gene_strand()<0:
            self.set_gene_strand(-1)
        if self.get_cumul_read_strand()>0:
            self.set_read_strand(1)
        elif self.get_cumul_read_strand()<0:
            self.set_read_strand(-1)
    
    def set_gene_strand(self,iInt):
        self.gene_strand=iInt
    
    def get_gene_strand(self):
        return self.gene_strand
    
    def set_read_strand(self,iInt):
        self.read_strand=iInt
    
    def get_read_strand(self):
        return self.read_strand
    
    def set_cumul_read_strand(self,iInt):
        self.cumul_read_strand=iInt
    
    def get_cumul_read_strand(self):
        return self.cumul_read_strand
    
    def set_cumul_gene_strand(self,iInt):
        self.cumul_gene_strand=iInt
    
    def get_cumul_gene_strand(self):
        return self.cumul_gene_strand
    
    def get_id(self):
        return self.align_id
    
    def get_hspList(self):
        return self.align_hsp
    
    def set_hspList(self,tObjectList):
        self.align_hsp=list(tObjectList)
    
    def get_alignContent_size(self):
        return self.align_size
        
    def set_alignContent_selfCover(self,fFloat):
        self.align_self_cover=fFloat
        
    def get_alignContent_selfCover(self):
        return self.align_self_cover
    
    def compute_ref_cover(self,iInt):
        '''
        The  method calculates the reference coverage, i.e. the proportion of the reference sequence covered by the alignment.
        '''
        iDelta=max(self.get_globalEnd(),self.get_globalStart())-min(self.get_globalEnd(),self.get_globalStart())
        fRatio=float(iDelta)/iInt*100
        self.set_alignContent_refCover(fRatio)

    def set_alignContent_refCover(self,fFloat):
        self.align_ref_cover=fFloat
        
    def get_alignContent_refCover(self):
        return self.align_ref_cover
        
    def compute_selfGap(self):
        dbValue=self.get_globalSelfPosition()
        #print("dbValue {}".format(dbValue),end=' ')
        iGap=0
        iCover=0
        for iIndex in range(len(dbValue)-1):
            iGap+=1
            dbOne=dbValue[iIndex]
            dbTwo=dbValue[iIndex+1]
            iStop=dbOne[-1]+1
            iStart=dbTwo[0]
            iDelta=iStart-iStop
            iCover+=iDelta
        fCover= float(iCover)/self.get_alignContent_size()*100
        #print(iCover, fCover, iGap, self.get_alignContent_size())
        self.set_number_of_selfGap(iGap)
        self.set_selfGap_covering(fCover)
        
    def set_number_of_selfGap(self,iInt):
        self.nb_self_gap=iInt
        
    def get_number_of_selfGap(self):
        return self.nb_self_gap
    
    def set_selfGap_covering(self,fFloat):
        self.self_gap_covering=fFloat
        
    def get_selfGap_covering(self):
        return self.self_gap_covering
        
    def get_selfStrand(self):
        return self.selfStrand
        
    def set_selfStrand(self,iInt):
        self.selfStrand=iInt
        
    def get_refStrand(self):
        return self.refStrand
        
    def set_refStrand(self,iInt):
        self.refStrand=iInt
    
    def compute_self_cover(self):
        '''
        The method subtracts the sum of the overlaps from the total coverage to obtain the actual self-coverage,
         converts it to a percentage of the reading size, and stores this value.
        '''
        iSelfSize=self.get_alignContent_size() # read length
        bPrevious=False
        iPreviousSelfStop=-1
        iSumSelfOverlap=0
        iSelfCover=0
        
        
        #DEBUG
        iStrand=None
        iRefStrand=None
        for oHsp in self.get_hspList():
            #print(oHsp)
            #print("HSP : {}".format(oHsp.__dict__.keys()))
            #print("HSP : {}".format(oHsp.__dict__.values()))
            if iStrand==None:
                iStrand=oHsp.frame[1] #Read strand
                iRefStrand=oHsp.frame[0]
            elif iStrand!=oHsp.frame[1] or iRefStrand!=oHsp.frame[0]:
                # print("WARNING 499 : divergente strand for the same read alignment")
                # print(self.get_id(),self.get_alignContent_size())
                
                self.set_alignContent_selfCover(-1.0)
                return None
        
        self.set_refStrand(oHsp.frame[0])
        self.set_selfStrand(oHsp.frame[1])
        
        if iStrand==1:        
            for oHsp in sorted(self.get_hspList(),key=lambda x: x.sbjct_start):
                iSelfStart=oHsp.sbjct_start
                iSelfStop=oHsp.sbjct_end
                iSelfRealSize=iSelfStop-iSelfStart+1
                
                if bPrevious:
                    if iSelfStart<=iPreviousSelfStop+1:
                        #iSumSelfOverlap=iPreviousSelfStop-iSelfStart+1
                        iSumSelfOverlap += min(iSelfStop,iPreviousSelfStop)-iSelfStart+1

                bPrevious=True
                iPreviousSelfStop=max(iSelfStop,iPreviousSelfStop)
                iSelfCover+=iSelfRealSize

                #if "20331" in self.align_id:
                #    print("{} from {} to {} : cover {} overlap {} size {}".format(self.align_id,iSelfStart,iSelfStop,iSelfCover,iSumSelfOverlap,iSelfSize))

        elif iStrand==-1:
            for oHsp in sorted(self.get_hspList(),key=lambda x: x.sbjct_end):
                iSelfStart=oHsp.sbjct_end
                iSelfStop=oHsp.sbjct_start
                iSelfRealSize=iSelfStop-iSelfStart+1
               
                if bPrevious:
                    if iSelfStart<=iPreviousSelfStop+1:
                        #iSumSelfOverlap=iPreviousSelfStop-iSelfStart+1
                        iSumSelfOverlap += min(iSelfStop, iPreviousSelfStop) - iSelfStart + 1
                    
                bPrevious=True
                iPreviousSelfStop=max(iSelfStop,iPreviousSelfStop)
                iSelfCover+=iSelfRealSize
        else:
            exit("ERROR 523 : strand in Hsp is not 1 nor -1")

        #if "20331" in self.align_id:
        #    print("{} {} {} {}".format(self.align_id,iSelfCover,iSumSelfOverlap,iSelfSize))

        iSelfCover=iSelfCover-iSumSelfOverlap
        fSelfCover=float(iSelfCover)/iSelfSize*100
        self.set_alignContent_selfCover(fSelfCover)
        
    def define_position(self):
        """
        Scan all Query/Self position in order to fusion element with overlap
        Pre_requisites for assign self_exonCovering
        """
        tAllQueryPosition=[] #Gene/ref
        tAllSelfPosition=[] #sbjct/reads
        
        if self.get_alignContent_selfCover()==-1.0:
            ##Bad items. Assign arbitrary value
            self.set_globalStart(-1)
            self.set_globalEnd(-1)
            self.set_number_of_selfGap(-1)
            self.set_selfGap_covering(-1)
            return None
        
        if self.get_refStrand()==1:
            for oHsp in sorted(self.get_hspList(),key=lambda x: x.query_start):
                iQueryStart=oHsp.query_start
                iQueryStop=oHsp.query_end
                tAllQueryPosition.append((iQueryStart,iQueryStop))
        else:
            for oHsp in sorted(self.get_hspList(),key=lambda x: x.query_end):
                iQueryStart=oHsp.query_end
                iQueryStop=oHsp.query_start
                tAllQueryPosition.append((iQueryStart,iQueryStop))
        
        if self.get_selfStrand()==1:
            for oHsp in sorted(self.get_hspList(),key=lambda x: x.sbjct_start):    
                iSelfStart=oHsp.sbjct_start
                iSelfStop=oHsp.sbjct_end
                tAllSelfPosition.append((iSelfStart,iSelfStop))
        else:
            for oHsp in sorted(self.get_hspList(),key=lambda x: x.sbjct_end):    
                iSelfStart=oHsp.sbjct_end
                iSelfStop=oHsp.sbjct_start
                tAllSelfPosition.append((iSelfStart,iSelfStop))
        
        tGlobalQueryPosition=[]
        for dbQueryPosition in tAllQueryPosition:
            if len(tGlobalQueryPosition)==0:
                tGlobalQueryPosition.append(dbQueryPosition)
            elif dbQueryPosition[0]<tGlobalQueryPosition[-1][1]:
                #Fusion
                tGlobalQueryPosition[-1]=(min(dbQueryPosition[0],tGlobalQueryPosition[-1][0]),max(dbQueryPosition[1],tGlobalQueryPosition[-1][1]))
            else:
                tGlobalQueryPosition.append(dbQueryPosition)
                
        tGlobalSelfPosition=[]
        for dbSelfPosition in tAllSelfPosition:
            if len(tGlobalSelfPosition)==0:
                tGlobalSelfPosition.append(dbSelfPosition)
            elif dbSelfPosition[0]<tGlobalSelfPosition[-1][1]:
                #Fusion
                tGlobalSelfPosition[-1]=(min(dbSelfPosition[0],tGlobalSelfPosition[-1][0]),max(dbSelfPosition[1],tGlobalSelfPosition[-1][1]))
            else:
                tGlobalSelfPosition.append(dbSelfPosition)
        
        dbGlobalQueryPosition=tuple(tGlobalQueryPosition)
        self.set_globalQueryPosition(dbGlobalQueryPosition)
        dbGlobalSelfPosition=tuple(tGlobalSelfPosition)
        self.set_globalSelfPosition(dbGlobalSelfPosition)
        
        self.set_globalStart(dbGlobalQueryPosition[0][0])
        self.set_globalEnd(dbGlobalQueryPosition[-1][-1])
        
        self.compute_selfGap()
        
    def set_globalStart(self,iValue):
        self.globalStart=iValue
        
    def get_globalStart(self):
        return self.globalStart
    
    def set_globalEnd(self,iValue):
        self.globalEnd=iValue
        
    def get_globalEnd(self):
        return self.globalEnd
            
    def set_globalQueryPosition(self,dbTuple):
        self.globalQueryPosition=dbTuple
    
    def get_globalQueryPosition(self):
        return self.globalQueryPosition
        
    def set_globalSelfPosition(self,dbTuple):
        self.globalSelfPosition=dbTuple
        
    def get_globalSelfPosition(self):
        return self.globalSelfPosition
        
    def describe_self(self,oTrContent=None,bStdout=True):        
        sContent=""
        dbTarget=(self.get_gene_strand(),self.get_read_strand(),
                    self.get_id(),self.get_alignContent_size(),
                    self.get_alignContent_selfCover(),
                    self.get_selfGap_covering(),self.get_number_of_selfGap(),
                    self.get_alignContent_refCover())
        for oValue in dbTarget:
            if sContent!="":
                sContent+="\t"
            sContent+="{}".format(oValue)
        dTarget=self.get_exonCovering()
        iSumPart=0
        if oTrContent is None:
            for sKey in sorted(dTarget):
                if dTarget[sKey]>0:
                    iSumPart+=1
                sContent+="\t{}".format(dTarget[sKey]["deltaStart"])
                sContent+="\t{}".format(dTarget[sKey]["ExonCovering"])
                sContent+="\t{}".format(dTarget[sKey]["deltaStop"])
        else:
            for oExonContent in oTrContent.get_exonList():
                sKey=oExonContent.get_index()
                if dTarget[sKey]["ExonCovering"]>0:
                    iSumPart+=1
                sContent+="\t{}".format(dTarget[sKey]["deltaStart"])
                sContent+="\t{}".format(dTarget[sKey]["ExonCovering"])
                sContent+="\t{}".format(dTarget[sKey]["deltaStop"])
                
        sContent+="\t{}".format(iSumPart)
        if bStdout:
            print(sContent)
        sContent+="\n"
        return sContent
        
    def describe_header(self,oTrContent=None,bStdout=True):
        sContent="Tool\tGeneStrand\tReadStrand\tReadId\tReadSize\t%ReadCover\t%ReadGap\t#Gap\t%GapedGeneCover"
        if oTrContent is None:
            for sKey in sorted(self.get_exonCovering()):
                sContent+="\t{}.e5".format(sKey)
                sContent+="\t{}".format(sKey)
                sContent+="\t{}.e3".format(sKey)
        else:
            for oExonContent in oTrContent.get_exonList():
                sContent+="\t{}.e5".format(oExonContent.get_index().split(".")[0])
                sContent+="\t{}".format(oExonContent.get_index())
                sContent+="\t{}.e3".format(oExonContent.get_index().split(".")[0])
        sContent+="\tSumExon"
        if bStdout:
            print(sContent)
        sContent+="\n"
        return sContent
    
    def describe_empty(self,bStdout=True):
        sContent=""
        dbTarget=(self.get_gene_strand(),self.get_read_strand(),
                    self.get_id(),self.get_alignContent_size(),
                    self.get_alignContent_selfCover(),
                    self.get_selfGap_covering(),self.get_number_of_selfGap(),
                    self.get_alignContent_refCover())
        sContent+="{}".format(self.get_id())
        for oValue in dbTarget[1:]:
            if sContent!="":
                sContent+="\t"
            sContent+=""
        sContent+="\t"
        if bStdout:
            print(sContent)
        sContent+="\n"
        return sContent
    
    def assign_exonCovering(self,oTranscriptContent):
        tExonList=oTranscriptContent.get_exonList()
        dExId2Include={}

        for oExon in tExonList:
            dExId2Include[oExon.get_index()]={"ExonCovering":0.00,
                                                "deltaStart":0,
                                                "deltaStop":0}
            
        if self.get_alignContent_selfCover()==-1.0:
            self.set_exonCovering(dExId2Include)
            return None
        
        for dbPosition in self.get_globalQueryPosition():
            iCurrentStart=dbPosition[0]
            iCurrentStop=dbPosition[1]
                        
            for oExon in tExonList:
                sExId=oExon.get_index()
                iExStart=oExon.get_start()
                iExStop=oExon.get_stop()
                iExLength=iExStop-iExStart+1

                if iCurrentStart>iExStop or iCurrentStop<iExStart:
                    continue
                else: #Hsp cover/is include/overlap the exon
                    iDeltaStart=iExStart-iCurrentStart
                    iDeltaStop=iCurrentStop-iExStop
                    fCoveringValue=0.0
                    #Case 01: FusionnedHsp is include into the exon
                    if iCurrentStart>=iExStart and iCurrentStop<=iExStop:
                        fCoveringValue=round((iCurrentStop-iCurrentStart+1)/(iExLength)*100,2)
                    #Case 02: FusionnedHsp cover all the exon
                    if iCurrentStart<=iExStart and iCurrentStop>=iExStop:
                        fCoveringValue=100.00
                    #Case 03: FusionnedHsp overlap the start of the exon
                    if iCurrentStart<=iExStart and iCurrentStop<=iExStop:
                        fCoveringValue=round((iCurrentStop-iExStart+1)/(iExLength)*100,2)
                    #Case 04: FusionnedHsp overlap the end of the exon
                    if iCurrentStart>=iExStart and iCurrentStop>=iExStop:
                        fCoveringValue=round((iExStop-iCurrentStart+1)/(iExLength)*100,2)

                    dExId2Include[sExId]["ExonCovering"]+=fCoveringValue

                    if dExId2Include[sExId]["deltaStart"]==0 or dExId2Include[sExId]["deltaStop"]==0:
                        dExId2Include[sExId]["deltaStart"]=iDeltaStart
                        dExId2Include[sExId]["deltaStop"]=iDeltaStop
                    else:
                        dExId2Include[sExId]["deltaStop"]=iDeltaStop

        self.set_exonCovering(dExId2Include)
            
    def set_exonCovering(self,dDict):
        self.exon_covering=dDict
    
    def get_exonCovering(self):
        return self.exon_covering