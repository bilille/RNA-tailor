class ToolContent:

    def set_tool(self,sString):
        self.tool=sString
    
    def set_queryId(self, sString):
        self.query_id=sString
    
    def get_queryId(self):
        return self.query_id
    
    def set_querySize(self, sString):
        self.query_size=sString
        
    def set_emptyAlignList(self):
        self.align_list=[]
        
    def add_align(self,oAlign):
        if oAlign.get_read_strand() is not None and oAlign.get_gene_strand() is not None:
            self.align_list.append(oAlign)
    
    def set_alignList(self,tObjectList):
        self.align_list=list(tObjectList)
        
    def get_alignList(self):
        return self.align_list
        
    def set_strand(self,iInt):
        self.strand=iInt
        
    def compute_self_cover_alignments(self):
        for oAlignContent in self.get_alignList():
            oAlignContent.compute_self_cover()
            
    def compute_ref_cover_alignments(self):
        for oAlignContent in self.get_alignList():
            oAlignContent.compute_ref_cover(self.get_querySize())
            
    def define_position_alignments(self):
        for oAlignContent in self.get_alignList():
            oAlignContent.define_position()
        self.compute_ref_cover_alignments()
    
    def describe_alignment(self,oTranscriptContent=None,bPrint=True):
        sContent=""
        for oAlignContent in self.get_alignList():
            sContent+="{}\t".format(self.tool)
            sContent+=oAlignContent.describe_self(oTrContent=oTranscriptContent,bStdout=bPrint)
        return sContent
    
    def describe_alignment_header(self,oTranscriptContent=None,bPrint=True):
        sContent=""
        for oAlignContent in self.get_alignList():
            sContent+=oAlignContent.describe_header(oTrContent=oTranscriptContent,bStdout=bPrint)
            break
        return sContent
        
    def assign_exonCovering(self,oTranscriptContent):
        for oAlignContent in self.get_alignList():
            oAlignContent.assign_exonCovering(oTranscriptContent)
        
    def get_querySize(self):
        return self.query_size
    
    def export_data(self):
        sContent=""
        sGeneId=self.get_queryId()
        iGeneSize=self.get_querySize()

        for oAlignContent in self.get_alignList():
            sReadId=oAlignContent.get_id()
            iReadSize=oAlignContent.get_read_size()

            for oHsp in oAlignContent.get_hspList():
                iReadStart=min(oHsp.sbjct_start,oHsp.sbjct_end)
                iReadStop=max(oHsp.sbjct_start,oHsp.sbjct_end)
                iGeneStart=min(oHsp.query_start,oHsp.query_end)
                iGeneStop=max(oHsp.query_start,oHsp.query_end)
                iReadStrand=oHsp.frame[1]
                iGeneStrand=oHsp.frame[0]
                iAlignSize=len(oHsp.sbjct)
                iNumberExactMatch=0
                iNumberIndel=0
                iNumberNotIndel=0
                for iIndex in range(iAlignSize):
                    if oHsp.sbjct[iIndex]==oHsp.query[iIndex]:
                        iNumberExactMatch+=1
                    if oHsp.sbjct[iIndex]=="-" or oHsp.query[iIndex]=="-":
                        iNumberIndel+=1
                    else:
                        iNumberNotIndel+=1
                if iReadStrand*iGeneStrand==1:
                    sReadStrand="+"
                elif iReadStrand*iGeneStrand==-1:
                    sReadStrand="-"
                else:
                    exit("FATAL 235")
                tFormatArgs=[sReadId,iReadSize,iReadStart,iReadStop,sReadStrand,
                            sGeneId,iGeneSize,iGeneStart,iGeneStop,
                            iNumberNotIndel,iAlignSize,
                            255]
                sContent+="\t".join(["{}"]*len(tFormatArgs)).format(*tFormatArgs)
                sContent+="\n"
        return sContent