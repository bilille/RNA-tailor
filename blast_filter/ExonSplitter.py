
class ExonSplitter(object):
    """
        Take a gff file and try to fraction every overlapping exons into a new exon.
        None overlapping exons must remain as they are after the fractioning is done.
        The exon fractionning is done in three steps :
        1 - Collecting every exons from the gff file and save them in a convenient
            data structure for later.
        2 - Doing the computational work to fraction exons.
        3 - Writing the results in a txt file which has a different structure than a gff.
    """
    def __init__(self, exon_gff_file, output_file="exon.txt") :
        assert exon_gff_file != None, "a gff file with exons must be provided"

        print(">> Collecting exons")
        self.collect_exons(exon_gff_file)

        print(">> Splitting exons")
        self.fraction_exons()

        print(">> Writing exons")
        self.write_exons(output_file)

    def collect_exons(self, exon_gff_file):
        assert exon_gff_file != None, "a gff file with exons must be provided"

        print(">> read " + exon_gff_file)
        file = open(exon_gff_file)

        self.exon_list = []
        for line in file :
            fields = line.split("\t")
            if len(fields) > 3 and fields[2].lower() == "exon" :
                self.exon_list.append((int(fields[3]), int(fields[4])))
        #print(self.exon_list)
        file.close()

    def fraction_exons(self):
        """
            This algorithm will fractionate overlapping exons from the gff
            into separate sub-intervals.
            1. Merge overlapping exons into a single one.
            2. For each merged exons, create the sorted list of alternative starts and stops inside the exon.
            3. Build a dictionnary as (key : exon number, values : list of sub intervals of this exon

	    """
        sorted_by_lower_bound = sorted(self.exon_list, key=lambda tup: tup[0])
        self.merged_exons = []
        for higher in sorted_by_lower_bound:
            if not self.merged_exons:
                self.merged_exons.append(higher)
            else:
                lower = self.merged_exons[-1]
                # test for intersection between lower and higher:
                # we know via sorting that lower[0] <= higher[0]
                if higher[0] <= lower[1]:
                    upper_bound = max(lower[1], higher[1])
                    self.merged_exons[-1] = (lower[0], upper_bound)  # replace by merged interval
                else:
                    self.merged_exons.append(higher)

        Dico = {} # Dictionnary containing as keys the exon number and as values
        # the tuple list of all intervals versions of this exon encounteed in the gff.
        i=0
        for (start,stop) in self.merged_exons:
            Dico[i] = [(x,y) for (x,y) in sorted_by_lower_bound if start<=x<=stop or start<=y<=stop]
            i=i+1
        liste_intuplable = []
        self.dico_fractionned_exon = {} # Dictionnary containing as keys the exon number and as values the
        # tuples list of fractionned intervals of all the exons diversity encountered in the gff
        exon_number=-1
        for key in Dico :
            liste_intuplable = []
            exon_number=exon_number+1
            liste_to_add = Dico[key]
            fractionned_list = []
            fractionned_list = [x for exon_alter in liste_to_add for x in exon_alter]
            fractionned_list = set(fractionned_list)
            fractionned_list = list(fractionned_list)
            sorted_fractionned_list = sorted(fractionned_list)
            for i in range(len(sorted_fractionned_list)-2):
                liste_intuplable.append((sorted_fractionned_list[i],sorted_fractionned_list[i+1]-1))
            liste_intuplable.append((sorted_fractionned_list[len(sorted_fractionned_list)-2],sorted_fractionned_list[len(sorted_fractionned_list)-1]))
            self.dico_fractionned_exon[exon_number]=liste_intuplable



    def write_exons(self, output_file):
        assert output_file != None, "an ouput file name must be provided"

        print(">> open " + output_file)
        f = open(output_file, "w")
        for key in self.dico_fractionned_exon:
            liste_intra_exon=[]
            liste_intra_exon=self.dico_fractionned_exon[key]
            for i in range(len(liste_intra_exon)) :
                f.write("Ex" + str(key) + "." + str(i) + "\t" + str(liste_intra_exon[i][0]) + "\t" + str(liste_intra_exon[i][1]) + "\n")

        f.close()
