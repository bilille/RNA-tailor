import unittest
import sys
import Bio

sys.path.append("..") # Adds higher directory to python modules path. Necessary to refer to files in the parent directory

from exonerate_alignment.ExonerateParser import ExonerateParser
from exon_matrix.HSPFragmentSequenceExtracter import HSPFragmentSequenceExtracter

data_directory = "tests_data/HSPFragmentSequenceExtracter_data/"
exonerate_sample_1 = data_directory + "/fake_exonerate_1.txt"

class TestHSPFragmentSequenceExtracter(unittest.TestCase):
	
	def test_extract_sequences_from_reference_positions_returns_good_type(self):
		self.assertTrue(isinstance(h1.extract_sequences_from_reference_positions(0,0), tuple))
		self.assertTrue(isinstance(h1.extract_sequences_from_reference_positions(0,0)[0], str))
		self.assertTrue(isinstance(h1.extract_sequences_from_reference_positions(0,0)[1], str))
		self.assertEqual(len(h1.extract_sequences_from_reference_positions(0,0)), 2)

	def test_is_reference_position_on_fragments(self):
		self.assertFalse(h1.is_reference_position_on_fragments(0))
		self.assertFalse(h1.is_reference_position_on_fragments(1))
		self.assertFalse(h1.is_reference_position_on_fragments(3))
		self.assertFalse(h1.is_reference_position_on_fragments(37))
		self.assertTrue(h1.is_reference_position_on_fragments(4))
		self.assertTrue(h1.is_reference_position_on_fragments(36))

	def test_is_read_position_on_fragments(self):
		self.assertFalse(h1.is_read_position_on_fragments(0))
		self.assertFalse(h1.is_read_position_on_fragments(1))
		self.assertFalse(h1.is_read_position_on_fragments(35))
		self.assertTrue(h1.is_read_position_on_fragments(2))
		self.assertTrue(h1.is_read_position_on_fragments(3))
		self.assertTrue(h1.is_read_position_on_fragments(34))

	def test_get_fragment_from_reference_position(self):
		self.assertEqual(h1.get_fragment_from_reference_position(0), None)
		self.assertEqual(h1.get_fragment_from_reference_position(1), None)
		self.assertEqual(h1.get_fragment_from_reference_position(3), None)
		self.assertEqual(h1.get_fragment_from_reference_position(37), None)
		self.assertTrue(isinstance(h1.get_fragment_from_reference_position(4), Bio.SearchIO.HSPFragment))
		self.assertTrue(isinstance(h1.get_fragment_from_reference_position(36), Bio.SearchIO.HSPFragment))


	def test_get_fragment_from_read_position(self):
		self.assertEqual(h1.get_fragment_from_read_position(0), None)
		self.assertEqual(h1.get_fragment_from_read_position(1), None)
		self.assertEqual(h1.get_fragment_from_read_position(35), None)
		self.assertTrue(isinstance(h1.get_fragment_from_read_position(2), Bio.SearchIO.HSPFragment))
		self.assertTrue(isinstance(h1.get_fragment_from_read_position(3), Bio.SearchIO.HSPFragment))
		self.assertTrue(isinstance(h1.get_fragment_from_read_position(34), Bio.SearchIO.HSPFragment))

if __name__ == '__main__':
	e = ExonerateParser(exonerate_sample_1)
	h1 = HSPFragmentSequenceExtracter(e.getReadResultFromName("fake_read_sequence_1"))
	unittest.main()