import unittest
import sys
import numpy
sys.path.append("..") # Adds higher directory to python modules path. Necessary to refer to files in the parent directory
from exon_matrix.BinaryMatrix import BinaryMatrix
from exonerate_alignment.ExonerateParser import *

# To instantiate an BinaryMatrix object, it is necessary to provide some files.
data_directory = "tests_data/"
sTargetId = data_directory + "ENSMUSG00000020167"
sInputFile = data_directory + "ENSMUSG00000020167.exonerate.xml"
sOutputFile = data_directory + "ENSMUSG00000020167.compact.tsv"
sFastaFile = data_directory + "ENSMUSG00000020167.megablast.fa"
sRefFile = data_directory + "ENSMUSG00000020167.fa.masked"
exon_file = data_directory + "ENSMUSG00000020167.exon.txt"
exonerate_output = data_directory + "ENSMUSG00000020167.exonerate.txt"

exonerateParser = ExonerateParser(exonerate_output)

matrix1 = [
    [0,0,0,0,1,1,1,1,0,0,0,0,0,0],
    [0,0,0,0,1,1,1,1,1,1,0,0,0,0],
    [0,0,0,0,1,1,1,1,0,0,0,0,0,0],
    [0,0,0,0,1,1,1,1,0,0,0,0,0,0],
    [0,0,0,0,1,1,1,1,0,0,0,0,0,0]
]

matrix2 = [
    [0,0,0,0,1,1,1,1,1,1,1,1,1,1],
    [0,0,0,0,0,1,1,1,1,1,1,1,1,1],
    [0,0,0,0,0,0,1,1,1,1,1,1,1,1],
    [0,0,0,0,0,0,0,0,0,1,1,1,1,1],
    [0,0,0,0,0,0,0,0,0,0,1,1,1,1],
    [0,0,0,0,0,0,0,0,0,0,0,1,1,1]
]

matrix3 = [
    [0,0,0,0,1,1,1,1,1,0,0,0,0,0],
    [0,0,0,0,1,1,1,1,1,0,0,0,0,0],
    [0,0,0,0,0,0,1,1,1,1,0,0,0,0],
    [0,0,0,0,1,1,1,1,1,0,0,0,0,0],
    [0,0,0,0,1,1,1,1,0,0,0,0,0,0]
]

matrix4 = [
    [0,0,1,1,1,1,0,0,0,1,1,1,1,1],
    [0,0,1,1,1,1,1,1,0,1,1,1,1,1],
    [0,0,1,1,1,1,1,1,0,1,1,1,1,1],
    [0,0,1,1,1,1,1,1,0,1,1,1,1,1],
    [0,0,1,1,1,1,0,1,0,1,1,1,1,1]
]

matrix5 = [
    [0,0,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,0,0],
    [0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,0,0],
    [0,0,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,0,0,0],
    [0,0,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,0,0,0,0],
    [0,0,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,0,0],
    [0,0,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,0,0]
]
# Test the behaviour of the AlignedMatrixContent class and its methods.
class TestBinaryMatrix(unittest.TestCase) :

    # Verify that an AlignedMatrixContent object can be instantiate with the correct parameters.
    def test_instanciation(self):
        self.assertNotEqual(unmodified_matrix,None)
 
    # Verify that "setup_global_vector" method produces a vector which has the same length as
    # the matrix.
    def test_setup_global_vector_has_same_size_as_matrix(self):
        self.assertEqual(unmodified_matrix.get_gene_length(), len(unmodified_matrix.setup_global_vector()[0]))

    # Verify that "setup_global_vector" method produces a vector which contains only specific
    # symbols. i.e. : 'i' for an intron, '<' for an alignment increase, '>' for an alignment
    # decrease and '=' for the same alignements.
    def test_setup_global_vector_contains_only_some_symbols(self):
        global_vector = unmodified_matrix.setup_global_vector()[0]
        for symbol in global_vector :
            self.assertTrue(symbol in ['i','<','>','='])

    # Verify that "setup_global_vector" method produces a vector where the symbol 'i'
    # can never be right before the symbol '='. At least one increase symbol must be between them.
    def test_setup_global_vector_has_symbol_i_never_come_before_symbol_equal(self):
        global_vector = unmodified_matrix.setup_global_vector()[0]

        previous_index = 0
        for index in range(1, len(global_vector)):
            if global_vector[previous_index] == 'i':
                self.assertTrue(global_vector[index] is not '=')
            previous_index = index

    # Verify that "setup_global_vector" method produces a vector where the symbol 'i'
    # can never be right after the symbol '='. At least one decrease symbol must be between them.
    def test_setup_global_vector_has_symbol_i_never_come_after_symbol_equal(self):
        global_vector = unmodified_matrix.setup_global_vector()[0]

        previous_index = 0
        for index in range(1, len(global_vector)):
            if global_vector[previous_index] == '=':
                self.assertTrue(global_vector[index] is not 'i')
            previous_index = index

    # Verify that setup_global_vector produces two vectors with the same size
    def test_setup_global_vector_produce_two_vectors_with_same_size(self):
        global_vector = unmodified_matrix.setup_global_vector()
        self.assertEqual(len(global_vector[0]),len(global_vector[1]))

    # Verify that setup_global_vector produces two vectors with the accurate content
    def test_setup_global_vector_produce_vector_with_accurate_values(self):
        m.set_matrix(matrix1)
        vectors_1 = m.setup_global_vector()
        expected_value_vector_1 = [0, 0, 0, 0, 5, 5, 5, 5, 1, 1, 0, 0, 0, 0]
        expected_symbol_vector_1 = ['i','i','i','i','<','=','=','=','>','=','>','i','i','i']
        self.assertEqual(expected_symbol_vector_1, vectors_1[0])
        self.assertEqual(expected_value_vector_1, vectors_1[1])

        m.set_matrix(matrix2)
        vectors_2 = m.setup_global_vector()
        expected_value_vector_2 = [0, 0, 0, 0, 1, 2, 3, 3, 3, 4, 5, 6, 6, 6]
        expected_symbol_vector_2 = ['i','i','i','i','<','<','<','=','=','<','<','<','=','=']
        self.assertEqual(expected_symbol_vector_2, vectors_2[0])
        self.assertEqual(expected_value_vector_2, vectors_2[1])

        m.set_matrix(matrix3)
        vectors_3 = m.setup_global_vector()
        expected_value_vector_3 = [0, 0, 0, 0, 4, 4, 5, 5, 4, 1, 0, 0, 0, 0]
        expected_symbol_vector_3 = ['i','i','i','i','<','=','<','=','>','>','>','i','i','i']
        self.assertEqual(expected_symbol_vector_3, vectors_3[0])
        self.assertEqual(expected_value_vector_3, vectors_3[1])

        m.set_matrix(matrix4)
        vectors_4 = m.setup_global_vector()
        expected_value_vector_4 = [0, 0, 5, 5, 5, 5, 3, 4, 0, 5, 5, 5, 5, 5]
        expected_symbol_vector_4 = ['i','i','<','=','=','=','>','<','>','<','=','=','=','=']
        self.assertEqual(expected_symbol_vector_4, vectors_4[0])
        self.assertEqual(expected_value_vector_4, vectors_4[1])



    # Verify that regroup_global_vector does not modify the shape of the matrix even though
    # the content is susceptible to change.
    def test_regroup_global_vector_does_not_affect_matrix_size(self):
        m.set_matrix(matrix1)
        vectors = m.setup_global_vector()
        m.regroup_global_vector(vectors[0], vectors[1], stdout=False)
        for i in range(0,len(m.get_matrix())):
            self.assertEqual(len(m.get_matrix()[i]), len(matrix1[i]))
        self.assertEqual(len(m.get_matrix()), len(matrix1))

    # When there is no window for a symbol in the symbol vector, the values returned
    # by the next_window method must be the same.
    def test_next_window_returns_same_value_when_no_window_detected(self):
        m.set_matrix(matrix1)
        vectors = m.setup_global_vector()
        symbol_vector = vectors[0] # symbol vector is : ['i','i','i','i','<','=','=','=','>','=','>','i','i','i']
        window1 = m.next_window(symbol_vector, 4)
        window2 = m.next_window(symbol_vector, 10)
        self.assertEqual(window1[0], window1[1])
        self.assertEqual(window2[0], window2[1])

        m.set_matrix(matrix4)
        vectors = m.setup_global_vector()
        symbol_vector = vectors[0]
        expected_value_vector_4 = [0, 0, 5, 5, 5, 5, 3, 4, 0, 5, 5, 5, 5, 5]
        expected_symbol_vector_4 = ['i','i','<','=','=','=','>','<','>','<','=','=','=','=']
        window3 = m.next_window(symbol_vector, 2)
        window4 = m.next_window(symbol_vector, 6)
        window5 = m.next_window(symbol_vector, 7)
        window6 = m.next_window(symbol_vector, 8)
        window7 = m.next_window(symbol_vector, 9)
        self.assertEqual(window3[0], window3[1])
        self.assertEqual(window4[0], window4[1])
        self.assertEqual(window5[0], window5[1])
        self.assertEqual(window6[0], window6[1])
        self.assertEqual(window7[0], window7[1])


    # Verify that next_window detects windows correctly.
    def test_next_window_returns_accurate_value_when_window_detected(self):
        m.set_matrix(matrix1)
        vectors = m.setup_global_vector()
        symbol_vector = vectors[0] # symbol vector is : ['i','i','i','i','<','=','=','=','>','=','>','i','i','i']
        window1 = m.next_window(symbol_vector, 8)
        self.assertEqual(window1[0],8)
        self.assertEqual(window1[1],10)

        m.set_matrix(matrix2)
        vectors = m.setup_global_vector()
        symbol_vector = vectors[0] # symbol vector is : ['i','i','i','i','<','<','<','=','=','<','<','<','=','=']
        window2 = m.next_window(symbol_vector, 4)
        window3 = m.next_window(symbol_vector, 5)
        window4 = m.next_window(symbol_vector, 6)
        window5 = m.next_window(symbol_vector, 9)
        window6 = m.next_window(symbol_vector, 10)

        self.assertEqual(window2[0], 4)
        self.assertEqual(window2[1], 10) # this may change with the value of REGROUP_GLOBALVECTOR_VALUE

        self.assertEqual(window3[0], 5)
        self.assertEqual(window3[1], 11)

        self.assertEqual(window4[0], 6)
        self.assertEqual(window4[1], 11)

        self.assertEqual(window5[0], 9)
        self.assertEqual(window5[1], 11)

        self.assertEqual(window6[0], 10)
        self.assertEqual(window6[1], 11)

        m.set_matrix(matrix3)
        vectors = m.setup_global_vector()
        symbol_vector = vectors[0] # symbol vector is ['i','i','i','i','<','=','<','=','>','>','>','i','i','i']
        window7 = m.next_window(symbol_vector, 4)
        window8 = m.next_window(symbol_vector, 8)
        window9 = m.next_window(symbol_vector, 9)

        self.assertEqual(window7[0], 4)
        self.assertEqual(window7[1], 6)

        self.assertEqual(window8[0], 8)
        self.assertEqual(window8[1], 10)

        self.assertEqual(window9[0], 9)
        self.assertEqual(window9[1], 10)

    # Verify that "maximum_delta_in_window" computes the accurate value for windows.
    def test_maximum_delta_in_window_gives_expected_value(self):
        # test for '<' window
        m.set_matrix(matrix3)
        vectors = m.setup_global_vector()
        symbol_vector = vectors[0] # symbol vector is ['i','i','i','i','<','=','<','=','>','>','>','i','i','i']
        values_vector = vectors[1]
        window = m.next_window(symbol_vector, 4)
        index_maximum_delta = m.maximum_delta_in_window(window, values_vector)
        expected_value = 4
        self.assertEqual(expected_value, index_maximum_delta)

        # test for '>' window
        artificial_values_vector = [0, 0, 0, 1, 1, 2, 5, 5, 5, 5, 4, 4, 4, 1, 1, 0, 0]
        artificial_symbol_vector = ['i','i','i','<','=','<','<','=','=','=','>','=','=','>','=','>','i','i']
        window1 = m.next_window(artificial_symbol_vector, 3)
        window2 = m.next_window(artificial_symbol_vector, 10)
        index_maximum_delta1 = m.maximum_delta_in_window(window1, artificial_values_vector)
        index_maximum_delta2 = m.maximum_delta_in_window(window2, artificial_values_vector)
        self.assertEqual(6, index_maximum_delta1)
        self.assertEqual(13, index_maximum_delta2)

        # Test when the symbol vector has '<' and '>' on the edges.
        artificial_values_vector = [10, 11, 11, 15, 15, 15, 11, 11, 11, 11, 0]
        artificial_symbol_vector = ['<','<','=','<','=','=','>','=','=','=','>']
        window1 = m.next_window(artificial_symbol_vector, 0)
        window2 = m.next_window(artificial_symbol_vector, 6)
        index_maximum_delta1 = m.maximum_delta_in_window(window1, artificial_values_vector)
        index_maximum_delta2 = m.maximum_delta_in_window(window2, artificial_values_vector)
        self.assertEqual(3, index_maximum_delta1)
        self.assertEqual(10, index_maximum_delta2)

    # Verify that if in a window, two indexes carry the maximum delta value, the one
    # which is closest to the middle is chosen.
    def test_maximum_delta_in_window_gives_closest_to_middle_of_window_value(self):
        # Test that even though a big value is close to mid, it is indeed the biggest delta that is chosen.
        symbol_vector = ['i','<','=','=','<','<','=','=','=','>','>','i']
        values_vector = [0, 1, 1, 1, 5, 12, 12, 12, 12, 8, 0, 0]
        window = m.next_window(symbol_vector, 1)
        index_maximum_delta = m.maximum_delta_in_window(window, values_vector)
        expected_value = 5
        self.assertEqual(expected_value, index_maximum_delta)

        # Test that if two indexes have the maximum delta, the one that is closest to the middle of
        # the window is chosen (for a window starting with '<').
        symbol_vector = ['i','<','=','=','<','<','=','=','=','>','>','i']
        values_vector = [0, 1, 1, 1, 5, 9, 9, 9, 9, 8, 0, 0]
        window = m.next_window(symbol_vector, 1)
        index_maximum_delta = m.maximum_delta_in_window(window, values_vector)
        expected_value = 4
        self.assertEqual(expected_value, index_maximum_delta)

        # Test that if two indexes have the maximum delta, the one that is closest to the middle of
        # the window is chosen (for a window starting with '>').
        symbol_vector = ['i','<','=','=','=','=','>','=','>','=','>','>','i']
        values_vector = [0, 10, 10, 10, 10, 10, 7, 7, 4, 4, 3, 0, 0]
        window = m.next_window(symbol_vector, 6)
        index_maximum_delta = m.maximum_delta_in_window(window, values_vector)
        expected_value = 8
        self.assertEqual(expected_value, index_maximum_delta)

    # verify that regroup_global_vector correcly adjust the matrix.
    def test_rectify_matrix_on_window_does_modify_matrix(self):
        m.set_matrix(matrix5)
        vectors = m.setup_global_vector()
        symbol_vector = vectors[0]
        values_vector = vectors[1]
        m.regroup_global_vector(symbol_vector, values_vector, stdout=False)

        expected_matrix = [
            [0,0,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,0,0],
            [0,0,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,0,0],
            [0,0,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,0,0],
            [0,0,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,0,0],
            [0,0,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,0,0],
            [0,0,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,0,0]
        ]

        self.assertEqual(expected_matrix, m.get_matrix().tolist())

if __name__ == '__main__':
    # matrix that can be modified through unit tests using set_matrix
    m = BinaryMatrix(sTargetId,sFastaFile,sRefFile,exon_file, exonerateParser)
    
    # matrix that should not be modifier through unit tests
    unmodified_matrix = BinaryMatrix(sTargetId,sFastaFile,sRefFile,exon_file, exonerateParser)
    unittest.main()
