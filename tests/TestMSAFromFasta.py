import unittest
import sys
sys.path.append("..") # Adds higher directory to python modules path. Necessary to refer to files in the parent directory
from exon_matrix.MSAFromFasta import MSAFromFasta

data_directory = "tests_data"
fasta_dir = data_directory + "/" + "bloc_artificial/"

# Test the behaviour of the MSAFromFasta class and its methods.
class TestMSAFromFasta(unittest.TestCase) :

	def test_alignment_filed__are_retrieved(self):
		self.assertEqual(len(m.get_alignments()), 3)

	def test_maximize_reference_sequence(self):
		m.maximize_reference_sequence()
		self.assertEqual(m.get_reference_sequence(), "CGACAATGCACGACAGAGGAAGCAGAACAGATATTTAGATTGCCTCTCATTTTCTCTCCC")

	def test_computed_reference_gaps_length_equals_reference_sequence_length(self):
		m.maximize_reference_sequence()
		m.compute_reference_gaps()
		self.assertTrue(len(m.get_reference_gaps()),len(m.get_reference_sequence()))

	def test_computed_reference_gaps_list_has_correct_values(self):
		m.maximize_reference_sequence()
		m.compute_reference_gaps()
		gaps = m.get_reference_gaps()
		self.assertEqual(sum(gaps[:5]), 0)
		self.assertEqual(gaps[6], 2)
		self.assertEqual(sum(gaps[7:20]), 0)
		self.assertEqual(gaps[21], 4)
		self.assertEqual(gaps[22], 2)
		self.assertEqual(sum(gaps[23:47]), 0)
		self.assertEqual(gaps[48], 4)
		self.assertEqual(sum(gaps[49:0]), 0)

	def test_msa_reference_sequence_has_accurate_content(self):
		e = MSAFromFasta(fasta_dir)
		self.assertEqual("CGACAAT--GCACGACAGAGGAAG----C--AGAACAGATATTTAGATTGCCTCTCA----TTTTCTCTCCC", e.get_msa_reference_sequence())
	def test_msa_reference_sequence_has_accurate_length(self):
		a = MSAFromFasta(fasta_dir)
		self.assertEqual(sum(a.get_reference_gaps()) + len(a.get_reference_gaps()), len(a.get_msa_reference_sequence()))

	def test_accurate_number_of_msa_reads_after_computation(self):
		b = MSAFromFasta(fasta_dir)
		self.assertEqual(len(b.get_alignments()), len(b.get_msa_reads_sequences()))

	def test_msa_reads_have_same_length(self):
		c = MSAFromFasta(fasta_dir)
		for sequence in c.get_msa_reads_sequences() :
			self.assertEqual(len(sequence), len(c.get_msa_reference_sequence()))

	def test_msa_reads_have_accurate_content(self) :
		d = MSAFromFasta(fasta_dir)
		sequences = d.get_msa_reads_sequences()
		expected_seq_1 = "CGACAATAAGCACGACAGAGGAAG----C--AGAACAGATA-----ATTGCCTCTCA----TTTTC-CTCCC"
		expected_seq_2 = "CGACAAT---CACGACAGAGGAAG----CTTAGAACAGATATTTAG---GCCTCTCA----TTTTCTCTCCC"
		expected_seq_3 = "CGACAAT--GCACGACAGAGGAAGTTTTC--AGAACAGATATTTAGATTGCCTCTCAAAAATTTTCTCTCCC"

		self.assertTrue(expected_seq_1 in sequences)
		self.assertTrue(expected_seq_2 in sequences)
		self.assertTrue(expected_seq_3 in sequences)


if __name__ == '__main__':
	m = MSAFromFasta(fasta_dir)
	unittest.main()