import unittest
import sys
import numpy
sys.path.append("..") # Adds higher directory to python modules path. Necessary to refer to files in the parent directory
from exon_matrix.ExonicBinaryVector import ExonicBinaryVector

class TestExonicBinaryVector(unittest.TestCase):
	"""
		Test the good behaviour of ExonicBinaryVector methods.
	"""

	def test_is_included(self):
		# All EBV must be included in themselves
		self.assertTrue(ebv1.is_included(ebv1))
		self.assertTrue(ebv2.is_included(ebv2))
		self.assertTrue(ebv3.is_included(ebv3))
		self.assertTrue(ebv4.is_included(ebv4))
		self.assertTrue(ebv5.is_included(ebv5))
		self.assertTrue(ebv6.is_included(ebv6))

		self.assertFalse(ebv1.is_included(ebv2))
		self.assertFalse(ebv2.is_included(ebv1))
		self.assertFalse(ebv2.is_included(ebv3))
		self.assertFalse(ebv2.is_included(ebv4))
		self.assertFalse(ebv2.is_included(ebv5))
		self.assertFalse(ebv2.is_included(ebv6))

		self.assertTrue(ebv1.is_included(ebv3))
		self.assertTrue(ebv1.is_included(ebv4))
		self.assertTrue(ebv1.is_included(ebv5))
		self.assertTrue(ebv1.is_included(ebv6))



if __name__ == '__main__' :
	ebv1 = ExonicBinaryVector("ebv1", [0, 0, 1, 1, 0, 0, 1, 1, 0, 0]) 
	ebv2 = ExonicBinaryVector("ebv2", [0, 0, 1, 1, 1, 1, 1, 1, 1, 1]) 
	ebv3 = ExonicBinaryVector("ebv3", [0, 1, 1, 1, 0, 0, 1, 1, 0, 0]) 
	ebv4 = ExonicBinaryVector("ebv4", [0, 0, 1, 1, 0, 0, 1, 1, 1, 0]) 
	ebv5 = ExonicBinaryVector("ebv5", [0, 1, 1, 1, 0, 0, 1, 1, 1, 0])
	ebv6 = ExonicBinaryVector("ebv6", [1, 0, 1, 1, 0, 0, 1, 1, 0, 1]) 
	unittest.main()

