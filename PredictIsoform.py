# coding: utf-8
"""Python3.6"""

import argparse
import logging
import subprocess



class Logger:
    def __init__(self, target_id : str):
        self.logger = logging.getLogger(__name__)
        self.debug_logger = logging.getLogger(f"{target_id}_debug")
        self.debug_logger.setLevel(logging.DEBUG)
        debug_handler = logging.FileHandler(f"{target_id}/log/my_log_DEBUG.log")
        debug_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
        self.debug_logger.addHandler(debug_handler)

        self.info_logger = logging.getLogger(f"{target_id}_info")
        self.info_logger.setLevel(logging.INFO)
        info_handler = logging.FileHandler(f"{target_id}/log/my_log_INFO.log")
        info_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
        self.info_logger.addHandler(info_handler)

        self.warning_logger = logging.getLogger(f"{target_id}_warning")
        self.warning_logger.setLevel(logging.WARNING)
        warning_handler = logging.FileHandler(f"{target_id}/log/my_log_WARNING.log")
        warning_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
        self.warning_logger.addHandler(warning_handler)

    def add_comment_debug(self, comment: str):
        self.debug_logger.debug(comment)
    def add_comment_info(self, comment: str):
        self.info_logger.info(comment)
    def add_comment_warning(self, comment: str):
        self.warning_logger.warning(comment)



import config
from seq.Reference import Reference

parser = argparse.ArgumentParser()
parser.add_argument('-a','--export_alignments', action="store_true", help="export every alignments between reference seq and reads in a reads folder")
parser.add_argument('-b','--realignment_scores',action="store_true", help="create a txt file with possible realignment scores")
parser.add_argument('-c','--config', help="import the data stored in the configfile")
parser.add_argument('-d','--dot_file',action="store_true", help="generate a dot file for the exonic binary vector")
parser.add_argument('-e','--exon-file',action="store", required=True, help="file with known exons")
parser.add_argument('-E','--exonerate-output',action="store", required=True, help="Exonerate alignments output between reference sequence and reads")
parser.add_argument('-J','--junction-filtering', action="store_true", help="filter reads on junctions")
parser.add_argument("-z","--export-binary-json",action="store", help="export binary matrix in a json format")
parser.add_argument('-Z',"--export-compact-json", action="store",help="export annotated compact matrix in a json format")
parser.add_argument('-m','--export_msa_alignment', action="store_true", help="export every multiple sequence alignments on each block in compact matrix")
parser.add_argument('-r','--reference-file',action="store", required=True, help="file with reference sequence in fasta format")
parser.add_argument('-R','--reads-file',action="store",required=True, help="file with reads of interest in fasta format")
parser.add_argument('-t','--target-id', action="store", required=True, help="reference sequence id")
parser.add_argument('-f','--reinforce-block-status', action="store_true", help="Reinforce block status before realignment")
parser.add_argument('-T','--export-tsv',action="store", default="", help="export analysis file in a tsv format")
parser.add_argument('-X','--export-xlsx', action="store", default="", help="export analysis file in a xlsx format")
parser.add_argument('-G','--export-gtf-name', action="store", default="", help="export annotation prediction in GTF format")
parser.add_argument('-S','--border', action="store_true", help="apply the border smoothing methode after realignment")
args = parser.parse_args()

if __name__ == "__main__":
    from exon_matrix.GFFExporter import *
    from exon_matrix.AnnotatedCompactMatrix import AnnotatedCompactMatrix
    from exon_matrix.BinaryMatrix import BinaryMatrix
    from exon_matrix.XLSXExporter import XLSXExporter
    from exonerate_alignment.ExonerateParser import *
    from seq.ReadsSet import ReadsSet

    # Set up config
    config.load_or_default_config(args)
    # Set up logging and logging directory and erase previous log
    if not (os.path.exists(f"{args.target_id}/log")):
        subprocess.run(["mkdir", f"{args.target_id}/log"])
    if (os.path.exists(f"{args.target_id}/log/my_log_DEBUG.log")):
        subprocess.run(["rm", f"{args.target_id}/log/my_log_DEBUG.log"])

    if (os.path.exists(f"{args.target_id}/log/my_log_WARNING.log")):
        subprocess.run(["rm", f"{args.target_id}/log/my_log_WARNING.log"])
        # subprocess.run(["rm", f"{args.target_id}/log/my_log_ERROR.log"])
        # subprocess.run(["rm", f"{args.target_id}/log/my_log_CRITICAL.log"])

    # Creates reference object, it contains
    # a Biopython SeqIo object of the fasta file
    # its Id and the filename
    reference = Reference(args.reference_file)
    # Creates the object ReadsSet, containing the list ordered list of reads
    reads = ReadsSet.make_from_fasta(args.reads_file)
    # create the ReadsSet containing only reads that appears in the exonerate result
    # Check that the reads in the Readset are aligned in the Exonerate File
    # by Parsing the exonerate file
    exonerateParser = ExonerateParser(args.exonerate_output,reads)

    # Updates the ReadsSet verified by ExonerateParser
    reads = exonerateParser.get_read_set()

    # build the binary matrix
    binary_matrix = BinaryMatrix(args.target_id, reads, reference, exonerateParser, junction_filter=False
                                 , annotated_compact_matrix=None,
                                 log_object=None)

    # PHASE 1 : build the ACM
    annotated_compact_matrix = AnnotatedCompactMatrix(binary_matrix, args.target_id, known_exons_file=args.exon_file,
                                                      generate_dot_file=args.dot_file,
                                                      export_name=args.export_xlsx.replace("compact", "compact-1-init"))

    GFFExporter(annotated_compact_matrix.do_clustering(), config.read_config(args.config)['DBREADS'], args.target_id,
                args.export_gtf_name.replace(".gtf", "-1-init.gtf"), config.read_config(args.config)['ISOFORM_READ_SUPPORT'])
    # GFFExporter(annotated_compact_matrix.do_clustering(), config.read_config(args.config)['DBREADS'], args.target_id,
    #             args.export_gtf_name.replace("corrected", "compact-1-init"), config.read_config(args.config)['ISOFORM_READ_SUPPORT'])
    # PHASE 2
    annotated_compact_matrix.do_filtering(redemption=config.REDEMPTION)
    annotated_compact_matrix.export_to_xlsx(os.path.join(args.export_xlsx.replace("compact", "compact-2-prefiltering")))

    GFFExporter(annotated_compact_matrix.do_clustering(), config.read_config(args.config)['DBREADS'], args.target_id,
                args.export_gtf_name.replace(".gtf", "-2-prefiltering.gtf"), config.read_config(args.config)['ISOFORM_READ_SUPPORT'])
    # PHASE 2 bis
    annotated_compact_matrix.do_realignment(config.read_config(args.config)['HARD_REALIGNABLE_BLOCK_LENGTH'])
    # rebuild the ACM
    # annotated_compact_matrix = AnnotatedCompactMatrix(binary_matrix, args.target_id, known_exons_file=args.exon_file, generate_dot_file=args.dot_file,  export_name=args.export_xlsx.replace("compact", "compact-2-bis-realign"))
    for exon in annotated_compact_matrix.get_predicted_exons():
        for block in exon.get_blocks():
            print(block)
    # TODO figure out why is the new exon not showing up in the compact output
    annotated_compact_matrix.export_to_xlsx(os.path.join(args.export_xlsx.replace("compact", "compact-2_5-realhard")))

    GFFExporter(annotated_compact_matrix.do_clustering(), config.read_config(args.config)['DBREADS'], args.target_id,
                args.export_gtf_name.replace(".gtf", "-2_5-realhard.gtf"), config.read_config(args.config)['ISOFORM_READ_SUPPORT'])


    annotated_compact_matrix.do_filtering(redemption=False)

    # PHASE 3
    annotated_compact_matrix.do_smoothing()
    annotated_compact_matrix.export_to_xlsx(os.path.join(args.export_xlsx.replace("compact", "compact-3-smoothing")))
    GFFExporter(annotated_compact_matrix.do_clustering(), config.read_config(args.config)['DBREADS'], args.target_id,
                args.export_gtf_name.replace(".gtf", "-3-smoothing.gtf"), config.read_config(args.config)['ISOFORM_READ_SUPPORT'])

    # GFFExporter(annotated_compact_matrix.do_clustering(), config.read_config(args.config)['DBREADS'], args.target_id,
    #             args.export_gtf_name.replace(".gtf", "-3-smoothing.gtf"), config.read_config(args.config)['ISOFORM_READ_SUPPORT'])
    # annotated_compact_matrix.do_pruning()

    #binaryblockmatrix = BinaryBlockMatrix(annotated_compact_matrix)
#    mechanism_and_cluster = ClusterIsoforms(annotated_compact_matrix)

    # TODO: Insertion des knowns exon ici ?
    annotated_compact_matrix.do_insert_known_exons(known_exons_file=args.exon_file,generate_dot_file=True)
                                                      #generate_dot_file=args.dot_file)
    GFFExporter(annotated_compact_matrix.do_clustering(), config.read_config(args.config)['DBREADS'], args.target_id,
                args.export_gtf_name, config.read_config(args.config)['ISOFORM_READ_SUPPORT'])


    if len(args.export_tsv) > 0 :
        # print("écriture du fichier")
        binary_matrix.export_to_tsv(args.export_tsv)

    if len(args.export_xlsx) > 0 :
        xlsx = XLSXExporter(annotated_compact_matrix, os.path.join(args.export_xlsx), args.reference_file)
        xlsx.export()
        # binary_matrix.export_to_xlsx(args.export_xlsx)

    if args.export_binary_json and len(args.export_binary_json) > 0 :
        binary_matrix.get_exonerate_parser().export_to_json(args.export_binary_json, gene_id=binary_matrix.get_geneID(),
                                                            gene_seq=binary_matrix.get_gene_seq())
    #

    if args.export_compact_json and len(args.export_compact_json) > 0 :
        print("let's go json")
        annotated_compact_matrix.export_to_json(args.export_compact_json)


