import json
from typing import Any, Dict

DEFAULT_CONFIG = {
	"FILTER_READ_JUNCTION_PERCENT" : 25,
	"JUNCTION_WINDOW" : 20,
	"SUSPICIOUS_THRESHOLD" : 1,
	"SUSPICIOUS_DELTA_THRESHOLD" : 1,
	"MIN_REALIGNABLE_LENGTH" : 6,
	"REALIGNMENT_ERROR_EDGE" : 5,
	"MAX_SMOOTHING_LEN" : 6,
	"THRESHOLD_SMOOTHING_RATIO" : 0.5,
	"REALIGNABLE_BLOCK_LENGTH" : 10,
	"REGROUP_GLOBALVECTOR_VALUE" : 6,
	"REALIGNABLE_BLOCK_LENGTH" : 10,
	"SOLID_JUNCTION_MINIMUM" : 2 ,
	"PERCENT" : 0.05
}


def read_config(CONFIG_FILE) -> Dict[str, Any]:
    with open(CONFIG_FILE) as f:
        return json.load(f)


def write_config(config: Dict[str, Any]) -> None:
    with open(CONFIG_FILE, 'w') as f:
        json.dump(config, f)


def load_or_default_config(args) -> None:
    try:
        cfg = read_config(args.config)
    except FileNotFoundError:
        cfg = DEFAULT_CONFIG
        write_config(cfg)


    if ('smoothing' not in cfg):
            cfg['smoothing'] = args.border
            
    globals().update(cfg)


