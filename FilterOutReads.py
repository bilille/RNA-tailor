# coding: utf-8
"""Python3.6"""
import logging
from Bio import SeqIO
import re
import argparse
import subprocess
import config
from seq.Reference import Reference

parser = argparse.ArgumentParser()

parser.add_argument('-E', '--exonerate-output', action="store", required=True,
					help="Exonerate alignments output between reference sequence and reads")
parser.add_argument('-R', '--reads-file', action="store", required=True,
					help="file with reads of interest in fasta format")
parser.add_argument('-o', '--output-filtered', action="store", required=True,
					help="name of the output file megablast_filtered")
parser.add_argument('-r', '--reference-file', action="store", required=True,
					help="file with reference sequence in fasta format")

args = parser.parse_args()

if __name__ == "__main__":

	from exonerate_alignment.ExonerateParser import *
	from seq.ReadsSet import ReadsSet
	from Bio import SeqIO

	reference = Reference(args.reference_file)
	reads = ReadsSet.make_from_fasta(args.reads_file)
	exonerateParser = ExonerateParser(args.exonerate_output, reads)
	exonerateParser.discard_monoexonic_reads()
	# exonerateParser.filter_out_unique_intronic_junction()
	# exonerateParser.filter_out_unique_intronic_structure()
	reads = exonerateParser.get_read_set()
	reads_to_keep = reads.get_readnames()


	# Open the input FASTA file and output FASTA file
	with open(args.reads_file, "r") as megablast, open(args.output_filtered, "w") as megablast_filtered:
		# Parse the input file and write the selected reads to the output file
		for record in SeqIO.parse(megablast, "fasta"):
			if record.id in reads_to_keep:
				SeqIO.write(record, megablast_filtered, "fasta")
