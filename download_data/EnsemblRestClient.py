import urllib
import urllib.request
import json
import os
import sys

SERVER="http://rest.ensembl.org"
SERVER_ERROR429=429
VERBOSE=False

class EnsemblRestClient(object):
    """
    NOTE: methods __init__ and perform_rest_action has been taken from the Ensemble REST API website.
    """

    def __init__(self, sServer=SERVER):
        self.server = sServer
        
    def launch_request(self,sRequestString, bVerbose=VERBOSE):
        oData=None
        try:
            if bVerbose:
                print("Request:"+self.server+sRequestString)
            oRequest = urllib.request.Request(self.server + sRequestString)
            oResponse = urllib.request.urlopen(oRequest)
            oContent = oResponse.read()
            sContent=oContent.decode("utf-8")   
            try:         
                oData=json.loads(sContent)
            except json.decoder.JSONDecodeError:
                oData=sContent

        except urllib.request.HTTPError as e:
            # check if we are being rate limited by the server
            if e.code == SERVER_ERROR429:
                if 'Retry-After' in e.headers:
                    retry = e.headers['Retry-After']
                    time.sleep(float(retry))
                    oData=self.launch_request(sRequestString)
            else:
                sys.stderr.write(
                    'Request failed for {0}: Status code: {1.code} Reason: {1.reason}\n'.format(endpoint, e))    
        return oData
            
    def get_geneData(self,sGeneId):
        sRequestSuffix="/lookup/id/"+sGeneId
        sRequestSuffix+="?content-type=application/json" #results as json format
        sRequestSuffix+=";expand=1" #expand all item linked to sGeneId (transcript, exon, etc.)
        sRequestSuffix+=";utr=1" #Get the utr data
        dGene=self.launch_request(sRequestSuffix)
        return dGene
        
    def get_geneSequence(self,sGeneId):
        sRequestSuffix="/sequence/id/"+sGeneId
        sRequestSuffix+="?content-type=text/plain" #results as json format
        sGeneSeq=self.launch_request(sRequestSuffix)
        return sGeneSeq