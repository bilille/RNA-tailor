from utils.AsterUtils import *
from download_data.EnsemblExon import *

GFF_FORMAT=("seqname","source","feature","start","end","score","strand","frame","attribute")

class EnsemblTranscript:
    
    def __init__(self,dDict,oParent):
        dbData=get_itemInfo(dDict)
        self.set_seqname(dbData[0])
        self.set_source(dbData[1])
        self.set_feature(dbData[2])
        self.set_start(dbData[3])
        self.set_end(dbData[4])
        self.set_score(dbData[5])
        self.set_strand(dbData[6])
        self.set_frame(dbData[7])
        self.set_attribute(dbData[8])
        self.add_attribute(oParent.get_attribute())
        self.set_emptyExonList()
        
        for dExon in dDict["Exon"]:
            oExon=EnsemblExon(dExon,self)
            self.add_exon(oExon)

        # for dExon in dDict["Exon"]+dDict["UTR"]:
        #     oExon = EnsemblExon(dExon, self)
        #     self.add_exon(oExon)
    def describe(self):
        sContent=""
        for sGffPart in GFF_FORMAT:
            if sContent!="":
                sContent+="\t"
            sContent+=str(getattr(self,sGffPart))
        sContent+="\n"
        for oExon in sorted(self.get_exonList(),key=lambda x: x.get_start()):
            sContent+=oExon.describe()

        return sContent
    
    def set_emptyExonList(self):
        self.exonList=[]
    
    def add_exon(self,oObject):
        self.exonList.append(oObject)
        
    def get_exonList(self):
        return self.exonList
    
    def set_seqname(self,sString):
        self.seqname=sString
    
    def set_source(self,sString):
        self.source=sString
        
    def set_feature(self,sString):
        self.feature=sString
    
    def set_start(self,iValue):
        self.start=iValue
    
    def set_end(self,iValue):
        self.end=iValue
        
    def set_score(self,sString):
        self.score=sString
        
    def set_strand(self,sString):
        self.strand=sString
        
    def set_frame(self,sString):
        self.frame=sString
        
    def set_attribute(self,sString):
        self.attribute=sString
        
    def add_attribute(self,sString):
        self.attribute+=";"+sString
    
    def get_source(self):
        return self.source
    
    def get_start(self):
        return self.start
        
    def get_attribute(self):
        return self.attribute