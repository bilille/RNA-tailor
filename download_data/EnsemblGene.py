from utils.AsterUtils import *
from download_data.EnsemblTranscript import *

TRANSCRIPT_BIOTYPE=("protein_coding")

class EnsemblGene():
    
    def __init__(self,dDict):
        dbData=get_itemInfo(dDict)
        self.set_seqname(dbData[0])
        self.set_source(dbData[1])
        self.set_feature(dbData[2])
        self.set_start(dbData[3])
        self.set_end(dbData[4])
        self.set_score(dbData[5])
        self.set_strand(dbData[6])
        self.set_frame(dbData[7])
        self.set_attribute(dbData[8])
        self.set_emptyTranscriptList()
        
        for dTr in dDict["Transcript"]:
            if dTr["biotype"] in TRANSCRIPT_BIOTYPE:
                oTranscript=EnsemblTranscript(dTr,self)
                self.add_transcript(oTranscript)
                
    def describe(self):
        sContent=""
        for sGffPart in GFF_FORMAT:
            if sContent!="":
                sContent+="\t"
            sContent+=str(getattr(self,sGffPart))
        sContent+="\n"
        for oTranscript in sorted(self.get_transcriptList(),key=lambda x: x.get_start()):
            sContent+=oTranscript.describe()

        return sContent
    
    def set_emptyTranscriptList(self):
        self.transcriptList=[]
    
    def add_transcript(self,oObject):
        self.transcriptList.append(oObject)
        
    def get_transcriptList(self):
        return self.transcriptList
    
    def set_seqname(self,sString):
        self.seqname=sString
    
    def set_source(self,sString):
        self.source=sString
        
    def set_feature(self,sString):
        self.feature=sString
    
    def set_start(self,iValue):
        self.start=iValue
    
    def set_end(self,iValue):
        self.end=iValue
        
    def set_score(self,sString):
        self.score=sString
        
    def set_strand(self,sString):
        self.strand=sString
        
    def set_frame(self,sString):
        self.frame=sString
        
    def set_attribute(self,sString):
        self.attribute=sString
        
    def get_attribute(self):
        return self.attribute