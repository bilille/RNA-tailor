# RNA-tailor

RNA-tailor is a tool designed to precisely inventory the repertoire of 
alternatively spliced transcripts of a target gene from third-generation
sequencing data.
The aim of this tool is to provide a nucleotide-level precise picture of all possible alternative transcripts of
a given target gene without any annotation data.
This makes the method usable for analysis on both well known and unknown species, and provides unbiased results that better reflect the transcript diversity inside a sample with a great potential for novel isoform discovery.


## Table of contents

* [Installing RNA-tailor](#Installing_RNA-tailor) 
  + [Prerequisite](#Prerequisite)
  + [Setting up](#Setting up config)
* [Run RNA-tailor](#Run_RNA-tailor)
* [How to run ? - Example](#How_to_run_?_-_Example)
* [Project structure](#project_structure)
* [Contributors](#contributors)

## Installing RNA-tailor

RNA-tailor is implemented in a snakemake workflow developed in Python and Biopython.

To clone the repository :
```bash
git clone https://gitlab.univ-lille.fr/bilille/RNA-tailor.git
cd RNA-tailor
```

### Prerequisite

This tool is based on different software and python modules. To execute it, you need the following :

* blastn (included in the [blast+](https://ftp.ncbi.nlm.nih.gov/blast/executables/) collection)
* [exonerate]( https://www.ebi.ac.uk/about/vertebrate-genomics/software/exonerate) 2.3 (this exact version is required, since 2.4 contains dubious results for alignment)
* [isONcorrect](https://github.com/ksahlin/isONcorrect), details in the paper [K Sahlin & Medvedev, 2021](https://www.nature.com/articles/s41467-020-20340-8) (installable with conda).
* [minimap2](https://github.com/lh3/minimap2), details in the paper [Li, 2018](https://academic.oup.com/bioinformatics/article/34/18/3094/4994778).
* [seqtk](https://github.com/lh3/seqtk), we recommend installing from apt package manager.

The necessary python packages are listed in the dependencies.yml file. 
We recommend using a specific [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html) environment for RNA-tailor. 
You can set it up by running the following commands:

```bash
mamba env create -f dependencies.yml
conda activate RNA-tailor-env
```

Some packages may be incompatible between each other in a conda environment, to address this issue install them globally :

```bash
pip install logging
apt-get install seqtk
```
### Setting up config

Before running RNA-Tailor, you need to set up the *config.json* file. For a quick run, we recommend only changing the two
mandatory variables :
```yaml
"DBREADS" : "DBREADS", // usage format "DBREADS" for DBREADS.fq
"SPECIE" : "SPECIE" // usage format "mus_musculus", only needed if you are using minimap2
```

## Run RNA-tailor

Once everything is installed, you can run the pipeline by using Snakemake. 
The Snakefile file sums up all its steps.
You can replace `DBREADS` variable with your own reads database. 
The database must be placed in a`db/`directory.

You can then run a specific pipeline step:

```bash
snakemake Ensembl_gene_id/Ensembl_gene_id.megablast_compact_corrected.xlsx # to run the pipeline using megablast
```

```bash
snakemake Ensembl_gene_id/Ensembl_gene_id.minimap2_compact_corrected.xlsx # to run the pipeline using minimap2
```



By opening this file with Excel or Libreoffice Calc you should obtain something similar to :

![](doc/expected_output.png)

## How to run ? - Example

You can use files in the **example** folder to try out the pipeline. Set your current directory to the 
project root and do the following :


```bash
mkdir db
cp example/reads.fastq db/reads.fastq
```

Before running the workflow, activate your conda environment or make sure that every required modules
are installed. Also, to make the workflow use the example reads file, set **DBREADS** variable in the `config.json`
to **reads**.

```bash
snakemake ENSMUSG00000000827/ENSMUSG00000000827.megablast_compact_corrected.xlsx --cores all
```

## Project structure

Snakemake pipeline structure :

![chart](doc/workflow_chat_minimap.svg)

* **download_data** : This folder contains every source files required to download necessary data from Ensembl.
* **blast_filter** : This folder contains every source files required to apply the megablast filter.
* **exonerate_alignement** : This folder contains every source files required to deal with the Exonerate alignment.
* **exon_matrix** : This folder contains every source files required to deal with the alignment matrix generation.
* **utils** : This folder contains source files with some useful methods call throughout program worklow.
* **doc** : This folder contains graphics for the README.
* **realignment** : contains the scripts required to execute the realignment step.
* **seq** : contains the scripts required to analyse the read alignment.

## RNA-Tailor Method



![chart](doc/pipeline_RNA-tailor.png "Pipeline scheme of the method")
_Pipeline scheme of the method_

RNA-tailor predicts splicing isoforms for a target gene. It takes into input the sequencing data
and either the reference genome and the locus of the gene of interest or the reference sequence of
the gene. Optionally, an annotation file can be added, but it will not be used for the predictions.

Its method integrates six analysis steps:

* **Read selection by sequence similarity alignment:** It is computed with Megablast, to select reads
from a reference sequence of the target gene only or Minimap2, to select reads from the genomic
coordinates of the gene. 
* **Sequence correction of the selected reads:** it is performed by isONcorrect and aims to avoid error in the next sequence similarity alignment steps due to indels in LR
sequences. 
* **Recognition of signature sequence motif from splice sites:** in this step, the corrected
reads are realigned by sequence similarity to the target gene sequence with Exonerate, it performs
a global alignment sensitive to donor and acceptor splice sites motif.
* **Correct misalignments** by realigning isolated alignments and extremities of exons using Exonerate. 
* **Consistency of exonic junction breakpoints betweens reads:** we consider that a transcript from the target gene expression
in a sample, while keeping its breakpoints diversity, conserves a common splice junction structure.
Therefore, if a transcript is not sharing enough breakpoints with the others, it is identified as false
positive and discarded.
* **Correction of splice junction positions:** At this point of the analysis each read has a draft version of its exonic structure. Since transcripts sequence are contaminated by  error rates and sequence alignment biais, the exact positioning of exonic junction at nucleotidic level can remain unclear. In this case, a correction of exon junction positions is applied to all the reads within a restrained nucleotidic window at the most supported position of each breakpoint. 
* **Clustering:** Transcripts are clustered according to their predicted exonic junction, excluding some
first and last breakpoints to account for read completeness issue.

## Contributors

* Aymeric Antoine-Lorquin
* Cyprien Borée
* Lilian Marchand
* Jean-Stéphane Varré
* Hélène Touzet


