# coding: utf-8
"""Python3.6"""

import os
import sys
from optparse import OptionParser

from blast_filter.ExonGatherer import *
from blast_filter.ExonSplitter import *

from utils.AsterUtils import *

sCurrentVersionScript="v8"
########################################################################
'''
V8-2018/12/20

python ExtractExons.py -e EXONFILE -o OUTPUTFILE

BLASTFILE: path to the blast xml file result
TARGETID: gene of interest's EnsemblId
GFFFILE: path to the gff file of reference
EXONFILE: path to store the list of exon and all transcript associated
OUTPUTFILE: path to store the result matrix
'''
########################################################################
#Options
parser = OptionParser(conflict_handler="resolve")
parser.add_option("-g","--gfffile", dest="gfffile")
parser.add_option("-e","--exonlistfile", dest="exonlistfile")
parser.add_option("-t","--targetid", dest="targetid")
parser.add_option("-m", "--method", dest="methodChoice", choices=["gather","splitter"], default = "gather")

(options, args) = parser.parse_args()

sTargetId=options.targetid
if not sTargetId:
    sys.exit("Error : no targetid -t defined, process broken")

sExonListFile=options.exonlistfile
if not sExonListFile:
    sExonListFile="ExonList_"+os.path.basename(sInputFile).split(".")[0]+".txt"
    print("Warning : no exonlistfile -e defined, default {}".format(sExonListFile))
    
sGffFile=options.gfffile
if not sGffFile:
    sys.exit("Error : no gfffile -g defined, process broken")

if __name__ == "__main__":
    if options.methodChoice == "splitter":
        ExonSplitter(sGffFile, sExonListFile)
    else:
        ExonGatherer(sGffFile, sExonListFile)

