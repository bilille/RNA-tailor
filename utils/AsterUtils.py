import gffutils

# This file gathers useful functions for the software.

def Fasta2Dict(sFile):
    dResults={}
    sSeqName=""
    sSeqContent=""
    fasta_file = open(sFile)
    for sLine in fasta_file:
        sStripedLine=sLine.strip()
        if sStripedLine[0]==">":
            if sSeqName!="":
                dResults[sSeqName]=sSeqContent
            sSeqName=sStripedLine[1:]
            sSeqContent=""
        else:
            sSeqContent+=sStripedLine
    fasta_file.close()
    if sSeqName!="":
        dResults[sSeqName]=sSeqContent
    return dResults

def WriteFile(sPath,sContent):
    print("Writing file {}".format(sPath))
    FILE=open(sPath,"w")
    FILE.write(sContent)
    FILE.close()    

def WriteGFF(sFile,oObject):
    sContent=oObject.describe()
    WriteFile(sFile,sContent)

def gff_format_modify(sFile):
    """
    Change the given gff file by modifying the exons coordinates.
    The coordinates will be switched to the relative coordinates according
    to the reference. If the reference is on the negative strand, the order
    of exons will also be modified. 
    """
    print("Change gff format")

    # Create an in-memory sqllite3 database in a hierachical manner.
    # It allows operations which would be complicated or time-consuming
    # using a text-file only approach.
    gff_database = gffutils.create_db(sFile, dbfn=":memory:", id_spec=None, force=True, verbose=False, keep_order=True)
    
    output_filename = sFile
    output_file = open(output_filename, "w")
    
    reference_start = 0
    reference_stop = 0
    reference_strand = "+"
    exon_stack = []
    for record in gff_database.all_features() :

        # gff3 fields according to The Sequence Ontology's specifications.
        # record_seqid = record[0]
        # record_source = record[1]
        record_type = record[2]
        record_start = int(record[3])
        record_stop = int(record[4])
        # record_score = record[5]
        record_strand = record[6]
        # record_phase = record[7]
        # record_attributes = record[8]

        if record_type == "Gene" :
            reference_start = record_start
            reference_stop = record_stop
            reference_strand = record_strand

            relative_start = reference_start
            relative_stop = reference_stop
        elif record_type == "Transcript" :            
            if record_strand == "+" :
                relative_start = record_start - reference_start
                relative_stop = record_stop - reference_start
            else:
                relative_start = reference_stop - record_stop
                relative_stop = reference_stop - record_start

        else : # Exons
            assert record_strand == reference_strand, "strand differ from the gene"
            # We treat every features that are not on the negative strand as features
            # on the positive strand
            if record_strand == "+" :
                relative_start = record_start - reference_start
                relative_stop = record_stop - reference_start
            else:
                relative_start = reference_stop - record_stop
                relative_stop = reference_stop - record_start


        record[2] = record_type
        record[3] = str(relative_start) 
        record[4] = str(relative_stop)
        
        # write in the output file. If the strand is reversed, we need also to reverse 
        # the order in which the exons will be written. That's why a stack is used.
        if reference_strand == "-" :
            if record_type == "Exon" :
                exon_stack.append(str(record))
            elif record_type == "Transcript" :
                while len(exon_stack) > 0 :
                    output_file.write(exon_stack.pop() + "\n")
                output_file.write(str(record) + "\n")
            else :
                output_file.write(str(record) + "\n")
        else:
            output_file.write(str(record)+"\n")
    
    # Empty the stack if it still contains unwritten exons.
    while len(exon_stack) > 0 :
        output_file.write(exon_stack.pop() + "\n")
    output_file.close()

    


def WriteMonoFasta(sSeqTitle,sFileName,sSeqContent):
    sContent=">"+sSeqTitle+"\n"
    iBase=1
    iLineSize=60
    while iBase*iLineSize<len(sSeqContent):
        sContent+=sSeqContent[(iBase-1)*iLineSize:iBase*iLineSize]+"\n"
        iBase+=1
    sContent+=sSeqContent[(iBase-1)*iLineSize:]+"\n"
    WriteFile(sFileName,sContent)
    
def get_itemInfo(dDict,oParent=None):
    sStrand="+"
    if dDict["strand"]==-1:
        sStrand="-"
    sObjectType=dDict["object_type"]
    sObjectTypeId=sObjectType.lower()
    sObjectTypeId=sObjectTypeId.replace("_prime","'")
    sObjectTypeId=sObjectTypeId.replace("three","3")
    sObjectTypeId=sObjectTypeId.replace("five","5")
    if "UTR" in sObjectType:
        sObjectType="UTR"
    try:
        sSource=dDict["source"]
    except KeyError:
        sSource="."
    return(dDict["seq_region_name"],
            sSource,
            sObjectType,
            dDict["start"],
            dDict["end"],
            ".",
            sStrand,
            ".",
            "{}_id {}".format(sObjectTypeId,dDict["id"])
            )
