# coding: utf-8
"""Python3.6"""

import os
import sys
from optparse import OptionParser
from Bio.Seq import Seq
#from Bio.Alphabet import generic_dna
#import matplotlib.pyplot as plt

sCurrentVersionScript="v5"
########################################################################
'''
Write the interesting (found by megablast) reads sequences into a fasta file.
If the found read is in the opposite strand (of the gene), the sequence is 
reverted.
'''
########################################################################
#Options
parser = OptionParser(conflict_handler="resolve")
parser.add_option("-f","--fastafile", dest="fastafile")
parser.add_option("-t","--targetread", dest="targetread")
parser.add_option("-s","--sourcefile", dest="sourcefile")
parser.add_option("-o","--outputfile", dest="outputfile")
parser.add_option("-r","--readcoverthreshold", dest="readcoverthreshold")
parser.add_option("-g","--gapedgenecoverthreshold", dest="gapedgenecoverthreshold")

(options, args) = parser.parse_args()

sFastaFile=options.fastafile
if not sFastaFile:
    sys.exit("Error : no fastafile -f defined, process broken")
    
sTargetRead=options.targetread
sSourceFile=options.sourcefile
if not sTargetRead and not sSourceFile:
    sys.exit("Error : no targetread -t or sourcefile -s defined, process broken")
elif sTargetRead and sSourceFile:
    sys.exit("Error : only one among targetread -t and sourcefile -s must be defined, process broken")
elif sTargetRead:
    bTargetRead=True
elif sSourceFile:
    bTargetRead=False
else:
    sys.exit("FATAL : L41")

sOutputFile=options.outputfile
if not sOutputFile:
    sys.exit("Error : no outputfile -o defined, process broken")

sReadCoverThreshold=options.readcoverthreshold
try:
    fReadCoverThreshold=float(sReadCoverThreshold)
except:
    fReadCoverThreshold=0.0
    print("Warning :  no readcoverthreshold -r defined, default value will be {} %")

sGeneCoverThreshold=options.gapedgenecoverthreshold
try:
    fGeneCoverThreshold=float(sGeneCoverThreshold)
except:
    fGeneCoverThreshold=0.0
    print("Warning :  no gapedgenecoverthreshold -g defined, default value will be {} %")


########################################################################
#DEBUG
# print("sFastaFile",sFastaFile)
# print("sTargetRead",sTargetRead)
# print("sSourceFile",sSourceFile)
# print("sOutputFile",sOutputFile)

########################################################################
#CONSTANT
COLUMN_READCOVER_INDEX=5
COLUMN_GAPEDGENECOVER_INDEX=8
COLUMN_READID_INDEX=3
#BAD_READCOVER_VALUE=-1

READCOVER_THRESHOLD=fReadCoverThreshold
GENECOVER_THRESHOLD=fGeneCoverThreshold
########################################################################
#Function
def LoadFastaFile(sPath):
    dDict={}
    sSeqName=""
    sSeqContent=""
    for sLine in open(sPath):
        sContent=sLine.strip()
        if sContent[0]==">":
            if sSeqName!="":
                try:
                    dDict[sSeqName].append(sSeqContent)
                except KeyError:
                    dDict[sSeqName]=[sSeqContent]
            sSeqName=sContent.replace(">","")
            sSeqContent=""
        else:
            sSeqContent+=sContent
    try:
        dDict[sSeqName].append(sSeqContent)
    except KeyError:
        dDict[sSeqName]=[sSeqContent]
    return dDict

def GetTrIdListFromSourceFile(sFile):
    tList=[]
    bHeader=True
    for sLine in open(sFile):
        if bHeader:
            bHeader=False
            continue
        tLine=sLine.split("\t")
        #print(tLine)
        #if float(tLine[COLUMN_READCOVER_INDEX])!=BAD_READCOVER_VALUE:
        if float(tLine[COLUMN_READCOVER_INDEX])>=READCOVER_THRESHOLD \
        and float(tLine[COLUMN_GAPEDGENECOVER_INDEX])>=GENECOVER_THRESHOLD:
            tList.append(tLine[COLUMN_READID_INDEX])
    return tList

def ExtractRead2Strand(sPath):
    dResult={}
    bHeader=True
    for sLine in open(sPath):
        if bHeader:
            bHeader=False
            continue
        tLine=sLine.split("\t")
        iGeneStrand=int(tLine[1])
        iReadStrand=int(tLine[2])
        sReadId=tLine[3]
        dResult[sReadId]={"geneStrand":iGeneStrand,"readStrand":iReadStrand}
    return dResult

def WriteFasta(dDict,sPath,tList=None,revert_reads=None):
    if tList!=None:
        tTarget=list(tList)
    else:
        tTarget=dDict.keys()
    FILE=open(sPath,"w")
    for sKey in tTarget:
        try:
            oContent=dDict[sKey]
        except KeyError:
            print("WARNING : target {} not present into the fasta".format(sKey))
        if len(oContent)!=1:
            print("WARNING : target {} is referenced {} time into the fasta".format(sKey,len(oContent)))
            continue
        

        if sKey in revert_reads :
            read_seq = Seq(dDict[sKey][0]) #, generic_dna)
            FILE.write(">{}".format(sKey) + " (reversed)\n")
            FILE.write("{}\n".format(read_seq.reverse_complement()))
        else :
            FILE.write(">{}\n".format(sKey))
            FILE.write("{}\n".format(dDict[sKey][0]))
    FILE.close()

########################################################################
#MAIN
dFasta=LoadFastaFile(sFastaFile)
# print(f"dFasta {dFasta}")
if bTargetRead:
    # print(f"sTargetRead {sTargetRead}")
    tTargetRead=sTargetRead.split(";")
else:
    tTargetRead=GetTrIdListFromSourceFile(sSourceFile)
    # print(f"tTargetRead {tTargetRead}")
strands_per_read = ExtractRead2Strand(sSourceFile)
# print(f"strands_per_read {strands_per_read}")
reads_to_revert = []
for target_read in tTargetRead :
    gene_strand = strands_per_read[target_read]["geneStrand"]
    read_strand = strands_per_read[target_read]["readStrand"]

    if gene_strand != read_strand :
        # mark read to be reversed
        reads_to_revert.append(target_read)

WriteFasta(dFasta,sOutputFile,tTargetRead,reads_to_revert)

