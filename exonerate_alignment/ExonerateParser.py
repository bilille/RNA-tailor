import collections
import copy

from Bio.SearchIO._model import *
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import sys
from exonerate_alignment.Fragment import *
from seq.Read import Read
from seq.ReadsSet import ReadsSet
from exon_matrix.Block import *
from realignment.RealignmentArea import *
import logging
import json

class ExonerateParser(object):
	"""
		Parse an exonerate output and save its content.
		The output must be produced with '-n 1' option to make sure that only
		the best alignment is chosen.
	"""
	def __init__(self, filename : str, readset : ReadsSet):
		self.filename = filename
		# This list contains for each read a list of Fragment
		self.read_set = readset
		self.reads_fragments = []
		self.query_results = []
		self.query_indexes = {}
		if self.filename is not None :
			self.read_set = self.__parse(self.filename)

		import os

		log_dir = f"{filename.split('/')[0]}/log"
		if not os.path.exists(log_dir):
			os.makedirs(log_dir)
		open(f"{filename.split('/')[0]}/log/my_log_INFO.log", 'w').close()
		# logging.getLogger(f"{filename.split('/')[0]}/log/my_log_INFO.log")
		self.info_logger = logging.getLogger(f"{filename.split('/')[0]}/log/my_log_INFO.log")
		self.info_logger.setLevel(logging.INFO)
		info_handler = logging.FileHandler(f"{filename.split('/')[0]}/log/my_log_INFO.log")
		info_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
		self.info_logger.addHandler(info_handler)

		# print("ExonerateParser object created")
		# self.discard_monoexonic_reads()

	def __parse(self, filename : str) -> ReadsSet:

		with open(filename) as handle:
			self.query_results = list(SearchIO.parse(handle, 'exonerate-text'))

		# create the ReadsSet containing only reads that appears in the exonerate result
		read_set = ReadsSet()
		for query in self.query_results:
			# check for readname
			assert query.id in self.read_set.get_readnames(), query.id + " not found in " + str(self.read_set.get_readnames())
			# we make a copy of the read, in order to be able to change its properties, notably its hits
			read = copy.copy(self.read_set.get_read(query.id))
			#
			read.set_exonerate_result(query) # TODO
			read_set.add(read)

		# index query_indexes
		self.__index_queries()

		# Create custom Fragment objects from biopython objects
		for query in self.query_results :
			assert len(query) == 1, f"query {query.id} has results for multiple targets"
			#assert len(query[0]) == 1, f"query {query.id} has more than one result"
			read_fragments = []
			for f in query[0][0] :
				# convert base class to child class to use extended behaviour
				f.__class__ = Fragment
				read_fragments.append(f)
			#if read_fragments != [] : self.reads_fragments.append(read_fragments)
			self.reads_fragments.append(read_fragments)

		return read_set


	def filter_out_unique_intronic_structure(self):
		"""
		Filter out reads with unique intronic structures, keeping only those
		that share their intronic structure with others (count > max(1, 1% of total reads)).
		Uses removeReadResultsFromName to remove reads from the read set.
		"""
		# Dictionary to count intronic structures
		intron_structure_counts = collections.defaultdict(int)
		read_introns = {}

		# Build and count intronic structures
		for query_result in self.query_results:
			hit_positions = []
			for hit in query_result.hits:
				for hsp in hit.hsps:
					for exon_fragment in hsp.fragments:
						hit_positions.append(exon_fragment.hit_start)
						hit_positions.append(exon_fragment.hit_end)

			# Sort positions and remove first and last elements
			hit_positions = sorted(hit_positions)
			intronic_structure = frozenset(hit_positions[1:-1])

			# Store the intronic structure for this read
			read_introns[query_result.id] = intronic_structure
			intron_structure_counts[intronic_structure] += 1

		# Determine the minimum count to retain (at least 1% of total reads or more than 1)
		min_count = max(1, len(self.query_results)*0.01)

		# Identify reads to be removed based on unique intronic structures
		reads_to_remove = []
		for read_id, intronic_structure in read_introns.items():
			if intron_structure_counts[intronic_structure] <= min_count:
				reads_to_remove.append(read_id)


		# Log the number of reads removed and retained
		self.info_logger.info(
			f"Removed {len(reads_to_remove)} reads out of {len(self.query_results)} based on unique intronic structures (count < {min_count}).")
		print(
			f"Removed {len(reads_to_remove)} reads out of {len(self.query_results)} based on unique intronic structures (count < {min_count}).")

		# Remove reads with unique intronic structures
		for read_id in reads_to_remove:
			self.removeReadResultsFromName(read_id)

	def filter_out_unique_intronic_junction(self):
		"""
        Filter out reads with unique intronic junction, keeping only those
        that share their intronic junction with others (intron count >= max(1, 1% of total reads)).
        Uses removeReadResultsFromName to remove reads from the read set.
        """
		# Dictionary to count each intron's occurrence
		intron_counts = collections.defaultdict(int)
		read_introns = {}

		# Build and count introns
		for query_result in self.query_results:
			hit_positions = []
			for hit in query_result.hits:
				for hsp in hit.hsps:
					for exon_fragment in hsp.fragments:
						hit_positions.append(exon_fragment.hit_start)
						hit_positions.append(exon_fragment.hit_end)

			# Sort positions and extract introns (pairs of consecutive positions)
			hit_positions = sorted(hit_positions)
			intronic_structure = []
			for i in range(1, len(hit_positions) - 1, 2):
				intron = (hit_positions[i], hit_positions[i + 1])
				intronic_structure.append(intron)
				intron_counts[intron] += 1

			# Store the intronic structure for this read
			read_introns[query_result.id] = intronic_structure

		# Determine the minimum count to retain (at least 5% of total reads or more than 1)
		min_count = max(1, len(self.query_results)*0.3)

		# Identify reads to be removed based on intronic structure counts
		reads_to_remove = []
		for read_id, intronic_structure in read_introns.items():
			if any(intron_counts[intron] <= min_count for intron in intronic_structure):
				reads_to_remove.append(read_id)

		# Remove reads with unique intronic structures
		for read_id in reads_to_remove:
			self.removeReadResultsFromName(read_id)

		# Log the number of reads removed and retained
		self.info_logger.info(
			f"Removed {len(reads_to_remove)} reads out of {len(self.query_results)} based on unique intronic junctions.")
		print(
			f"Removed {len(reads_to_remove)} reads out of {len(self.query_results)} based on unique intronic junctions.")


	def discard_monoexonic_reads(self):
		'''
		Checks if a hit on a read has only one fragment in its hsp in the Exonerate Parser.
		'''
		to_be_removed = []
		for query_result in self.query_results:
			#check if alignment has only one hit, therefore is monoexonic
			for hit in query_result.hits:
				if query_result.id == "SRR15899612.170177.1":
					print(hit)
					for hsp in hit.hsps:
						print(hsp)
				for hsp in hit.hsps:
					if len(hsp.fragments) == 1:
						to_be_removed.append(query_result.id)
						# self.add_comment_info("Removed monoexonic read {} from the ExonerateParser object.".format(query_result.id))
		for read_name in to_be_removed:
			# print(("Removed monoexonic read {} from the ExonerateParser object.".format(read_name)))
			self.removeReadResultsFromName(read_name)


	def __index_queries(self):
		self.query_indexes = {}
		index = 0
		for query in self.query_results:
			self.query_indexes[query.id] = index
			index += 1

	def get_read_set(self):
		return self.read_set

	def add_comment_info(self, comment: str):
		self.info_logger.info(comment)

	@classmethod
	def insert_HSPFragment(cls, read: Read, new_begin: int, new_end: int, \
						   read_begin: int, read_end: int, fragment: SearchIO.HSPFragment, area : RealignmentArea):
		"""
			For a given read, insert if possible a new fragment corresponding to a realignment
			(a read part is realigned over another gene area).
			The conditions to make this insertion possible are :
			- source fragment(s) has to exist for the given read name and coordinates
			- the gene area for the realignment must be all free of an alignment.

			If the insertion is done sucessfully, the read HSP will be altered noticeably and
			thus previous HSPFragments will be cut out, while new ones may appear. An integrity
			check is ran afterward to verify the correct HSP composition.

			read_name : The name of read for which the fragment will be inserted
			new_begin : The begin position on the gene area where to realign on
			new_end : The end position on the gene are where to realign on
			read_begin : The begin position on the read part that is realigned
			read_end : The end position on the read part that is realigned
			fragment : The fragment to insert itself.

			Return:
				3 if the new alignement m-overlaps multiple old alignements
				2 if the new alignment disrupt colinearity
				1 if the new alignment overlaps an existing fragment in the HSP in the reference
				0 the new alignment has been considered and old overlaping alignments have been corrected
		"""
		#read_to_monitor = "ch212" #"ch332"
		blocks = area.get_blocks()

		read_name = read.id

		# if read_to_monitor in read_name:
		# 	print(f" Realignement on {read_name}\n new_begin {new_begin}\n new_end {new_end}\n read_begin {read_begin}\n read_end {read_end}\n fragment {fragment}")
		# 	print(f" Block - prev : {blocks[0].get_previous_block_containing_read_name(read_name)}")
		# 	for b in blocks:
		# 		print(f" Block - real : {b}")
		# 	print(f" Block - next : {blocks[-1].get_next_block_containing_read_name(read_name)}")

		assert isinstance(fragment, SearchIO.HSPFragment), "Given parameter is not an HSPFragment"

		read_results = read.get_hits() #self.getReadResultFromName(read_name)

		assert read_results != None, "No data found for {} in the ExonerateParser object.".format(read_name)

		# get HSP : the list of fragments currently recorded for the read `read_name`
		HSP = read_results[0][0]

		# if read_to_monitor in read_name:
		# 	print("-" * 40 + "\n" + f"\tfragment overlapping in reference:\n")
		# 	for x in ExonerateParser.get_fragments_from_gene_positions(HSP, new_begin, new_end):
		# 		print(x)
		# 	print("-" * 40 + "\n" + f"\tfragment overlapping in read:\n")
		# 	for x in ExonerateParser.get_fragments_from_read_positions(HSP, read_begin, read_end):
		# 		print(x)

		# Do not realign if another fragment already exists between the given
		# realignment coordinates. Another behaviour might be better here.
		if len(ExonerateParser.get_fragments_from_gene_positions(HSP, new_begin, new_end)) > 0:
			# TODO how to split a fragement that spread over multiple fragments
			return 1, None

		# the fragments that overlap the new fragment
		read_fragments = ExonerateParser.get_fragments_from_read_positions(HSP, read_begin, read_end)
		#
		# if read_to_monitor in read_name:
		# 	ExonerateParser.add_comment_info("-" * 40 + "\n" + f"\tnew fragment: {fragment}")
		# 	ExonerateParser.add_comment_info("-"*40+"\n"+f"\tread_fragments between {read_begin} and {read_end}:")
		# 	for rf in read_fragments:
		# 		ExonerateParser.add_comment_info("\t\t{}".format(rf))

		# sanity check : there must be fragments overlapping the new alignement in the reference,
		# otherwise the alignment area should not exist
		assert len(read_fragments) > 0, "No HSPFragments contains the realignable read sequence."

		# Easiest case, the read sequence is only in 1 HSPFragment. It means, the old
		# alignment part has to be cut out only for one HSPFragment.
		if len(read_fragments) == 1:

			# step 1 : cut out the realignable read sequence from old HSPFragment
			old_fragment = read_fragments[0]
			if read_begin > old_fragment.query_start and read_end+1 < old_fragment.query_end:
				# ExonerateParser.add_comment_info("Colinearity violated : ({},{}) vs ({},{})".format(read_begin,read_end,old_fragment.query_start,old_fragment.query_end))
				return 2, (old_fragment.query_start,old_fragment.query_end,old_fragment.hit_start,old_fragment.hit_end)

			# Get the new fragment(s) after the cutting.
			fragment_residues = ExonerateParser.cut_out_fragment_part_from_read_pos(old_fragment, read_begin, read_end)

			# add the residuals fragments into the HSP
			for residue in fragment_residues:
				residue.hit_description = old_fragment.hit_description
				residue.query_description = old_fragment.query_description
				residue.hit_strand = old_fragment.hit_strand
				residue.query_strand = old_fragment.query_strand
				HSP._validate_fragment(residue)
				HSP._items.append(residue)

			fragment.hit_id = old_fragment.hit_id
			fragment.query_id = old_fragment.query_id
			fragment.hit_description = old_fragment.hit_description
			fragment.query_description = old_fragment.query_description
			fragment.query_strand = old_fragment.query_strand
			fragment.hit_strand = old_fragment.hit_strand

			# necessary use of biopython property decorator
			q_offset = fragment.query_start
			fragment.query_start = None
			fragment.query_end = None
			fragment.query_start = read_begin
			fragment.query_end = read_end

			# necessary use of biopython property decorator
			fragment.hit_start = None
			fragment.hit_end = None
			fragment.hit_start = new_begin
			fragment.hit_end = new_end

			HSP._validate_fragment(fragment)
			HSP._items.append(fragment)

			HSP._items.sort(key=SearchIO.HSPFragment._hit_start_get)

			HSP._items.remove(old_fragment)

		else:
			# The read sequence spreads over more than 1 HSPFragment. It means,
			# many HSPFragments have to be corrected/deleted.
			# [-- old alignment1 --] [-- old alignment2 --] [-- old alignment3 --]
			#       [---------------- new alignement -----------]
			# In such a case :
			# - the first old alignment will be cut
			# - inner old alignements will be simply deleted
			# - the last old alignement will be cut

			# for now on, we skip this case
			return 3,None


			# necessary use of biopython property decorator
			fragment.query_end = None
			fragment.query_start = None
			fragment.query_start = read_begin
			fragment.query_end = read_end

			# necessary use of biopython property decorator
			fragment.hit_start = None
			fragment.hit_end = None
			fragment.hit_start = new_begin
			fragment.hit_end = new_end

			HSP._validate_fragment(fragment)
			HSP._items.append(fragment)

			HSP._items.sort(key=SearchIO.HSPFragment._hit_start_get)

		assert ExonerateParser.HSP_integrity(HSP), \
			"HSP integrity for alignement ({},{})/({},{}) is wrong : \n {}".format(read_begin, read_end, new_begin,
																				   new_end, HSP)


		# all worked well
		return 0, None

	@classmethod
	def HSP_integrity(cls, HSP) -> bool:
		"""
			Check that every fragments of a given HSP does not overlap on each other.
		"""
		integrity_broken = False

		f_index = 0
		while f_index < len(HSP) and not integrity_broken:
			g_index = f_index + 1
			while g_index < len(HSP) and not integrity_broken:
				integrity_broken = ExonerateParser.are_fragments_overlapping(HSP[f_index],
																  HSP[g_index]) or ExonerateParser.are_fragments_overlapping(
					HSP[g_index], HSP[f_index])
				g_index += 1
			f_index += 1
		return not integrity_broken

	@classmethod
	def are_fragments_overlapping(cls, f1, f2) -> bool:
		"""
			Return True if the first given fragment overlap
			with the second one. To verify the reciprocity call the same
			function by switching fragments.
		"""
		return (f1.hit_start < f2.hit_end and f2.hit_start < f1.hit_end) or \
			   (f1.query_start < f2.query_end and f2.query_start < f1.query_end) or \
			   (f1.hit_start == f2.hit_start and f1.hit_end == f2.hit_end) or \
			   (f1.query_start == f2.query_start and f1.query_end == f2.query_end)

	@classmethod
	def cut_out_fragment_part_from_read_pos(cls, fragment: SearchIO.HSPFragment, cut_begin: int, cut_end: int) -> [
		SearchIO.HSPFragment]:
		"""
			Cut out a part of a given HSPFragment and return the residuals fragments in a list.
			The cut coordinates must be on the Query (the read). Both of the coordinates are included
			in the cut.
			Depending on the cutting, the residuals could be one, two or nothing.

			fragment : the fragment to be cut
			cut_begin : the position where to cut from in the read
			cut_end : the position where to cut to in the read

		"""

		# print("Cutting the following fragment between position {} and {}".format(cut_begin,cut_end))
		# print(fragment)

		# sanity check
		assert fragment.query_start <= cut_begin < fragment.query_end, \
			"cut begin coordinate {} is out of fragment({},{})".format(cut_begin, fragment.query_start,
																	   fragment.query_end)
		# sanity check
		assert fragment.query_start <= cut_end < fragment.query_end, \
			"cut end coordinate {} is out of fragment({},{})".format(cut_end, fragment.query_start, fragment.query_end)

		# will be the list of fragments after cutting
		residuals = []

		if cut_end == fragment.query_start:
			return []

		# the sequence of the reference
		hit_seq = str(fragment.hit.seq)
		# the sequence of the read
		query_seq = str(fragment.query.seq)

		# If the cut starts after the fragment beginning position, add this portion to
		# the residuals
		if cut_begin > fragment.query_start:
			# cut out the left remaining part of the fragment
			ct_count = 0
			nt_count = cut_begin - fragment.query_start

			while ct_count < len(query_seq) and nt_count > 0:
				if query_seq[ct_count] != '-':
					nt_count -= 1
				ct_count += 1

			assert nt_count <= 0, "fragment query_seq is full of gaps"

			ct_count = ExonerateParser.get_alignment_position_from_read_position(cut_begin,fragment)

			# create fragment
			first_hit = fragment.hit[:ct_count]
			first_query = fragment.query[:ct_count]
			first_fragment = HSPFragment(hit_id=fragment.hit_id, query_id=fragment.query_id, hit=first_hit,
										 query=first_query)

			first_fragment.hit_start = fragment.hit_start
			first_fragment.hit_end = ExonerateParser.get_gene_position_from_alignment_position(ct_count,fragment,1)


			# query = read
			first_fragment.query_start = fragment.query_start
			first_fragment.query_end = cut_begin #fragment.query_start + (cut_begin - fragment.query_start)

			first_fragment.aln_annotation["similarity"] = fragment.aln_annotation["similarity"][:ct_count]

			residuals.append(first_fragment)


		# If cut ends before the fragment last position, add this portion to the residuals
		if cut_end < fragment.query_end - 1:
			# cut out the right remaining part of the fragment
			ct_count = 0
			nt_count = cut_end - fragment.query_start
			while ct_count < len(query_seq) and nt_count > 0:
				if query_seq[ct_count] != '-':
					nt_count -= 1
				ct_count += 1

			assert nt_count <= 0, "fragment hit seq is full of gaps"

			ct_count = ExonerateParser.get_alignment_position_from_read_position(cut_end, fragment)

			# create fragment
			last_hit = fragment.hit[ct_count:]
			last_query = fragment.query[ct_count:]
			last_fragment = HSPFragment(hit_id=fragment.hit_id, query_id=fragment.query_id, hit=last_hit,
										query=last_query)

			last_fragment.hit_start = ExonerateParser.get_gene_position_from_alignment_position(ct_count,fragment,1)
			last_fragment.hit_end = fragment.hit_end
			last_fragment.query_start = cut_end
			last_fragment.query_end = fragment.query_end

			last_fragment.aln_annotation["similarity"] = fragment.aln_annotation["similarity"][ct_count:]

			residuals.append(last_fragment)

		return residuals

	@classmethod
	def get_fragments_from_read_positions(cls, HSP: SearchIO.HSP, read_begin: int, read_end: int) -> [SearchIO.HSPFragment]:
		"""
			Return every HSPFragments containing a part of the read sequence
		"""
		res = []
		for fragment in HSP:
			if (fragment.query_start <= read_begin < fragment.query_end) or \
					(fragment.query_start <= read_end < fragment.query_end) or \
					(read_begin <= fragment.query_start <= fragment.query_end <= read_end):
				res.append(fragment)
		return res

	@classmethod
	def get_fragments_from_gene_positions(cls, HSP: SearchIO.HSP, gene_begin: int, gene_end: int) -> [SearchIO.HSPFragment]:
		"""
			Return every HSPFragments carrying a part of the gene sequence
		"""
		res = []
		for fragment in HSP:
			if (fragment.hit_start <= gene_begin < fragment.hit_end) or \
					(fragment.hit_start <= gene_end < fragment.hit_end) or \
					(gene_begin <= fragment.hit_start <=  fragment.hit_end <= gene_end):
				res.append(fragment)
		return res


	@classmethod
	def get_alignment_position_from_absolute_position(cls, position: int, sequence_with_gap: str):
		"""
		@param position: position in sequence_with_gap without gaps
		@param sequence_with_gap: string corresponding to a sequence aligned
		@return: the corresponding position of position in sequence_with_gap including gaps

		>>> ExonerateParser.get_alignment_position_from_absolute_position(3,"XXXXXX---XXXX")
		3
		>>> ExonerateParser.get_alignment_position_from_absolute_position(6,"XXXXXX---XXXX")
		9
		>>> ExonerateParser.get_alignment_position_from_absolute_position(0,"XXXXXX---XXXX")
		0
		>>> ExonerateParser.get_alignment_position_from_absolute_position(9,"XXXXXX---XXXX")
		12
		>>> ExonerateParser.get_alignment_position_from_absolute_position(10,"XXXXXX---XXXX")
		Traceback (most recent call last):
    		...
    		AssertionError: Not enough nucleotides (10 required) in the fragment : XXXXXX---XXXX
    	>>> ExonerateParser.get_alignment_position_from_absolute_position(-1,"XXXXXX---XXXX")
		Traceback (most recent call last):
    		...
    		AssertionError: Position should be greater than 0
		"""
		assert position >= 0, "Position {position} should be greater than 0"
		# we search for the good position, accounting for gaps
		# nt_count is the number of nucleotides we have to count before finding the position in the alignment
		nt_count = position+1
		# ct_count will be the right position in the alignment
		ct_count = 0
		while ct_count < len(sequence_with_gap) and nt_count > 0:
			# we count up to nt_count nucleotides
			if sequence_with_gap[ct_count] != '-':
				nt_count -= 1
			ct_count += 1
		ct_count -= 1
		assert nt_count == 0, "Not enough nucleotides ({} required) in the fragment : {}".format(
			position, sequence_with_gap)

		return ct_count


	@classmethod
	def get_alignment_position_from_gene_position(cls, gene_position: int, fragment: SearchIO.HSPFragment):
		"""

		@param gene_position:
		@param fragment:
		@return:

		>>> f = HSPFragment(hit_id="ref",query_id="read",hit="XXX--XXX",query="XXXXXXXX")
		>>> ExonerateParser.get_alignment_position_from_gene_position(0,f)
		0
		>>> ExonerateParser.get_alignment_position_from_gene_position(2,f)
		2
		>>> ExonerateParser.get_alignment_position_from_gene_position(3,f)
		5
		>>> ExonerateParser.get_alignment_position_from_gene_position(5,f)
		7
		>>> f = HSPFragment(hit_id="ref",query_id="read",hit="XXXXXXXX",query="XXX--XXX")
		>>> ExonerateParser.get_alignment_position_from_gene_position(0,f)
		0
		>>> ExonerateParser.get_alignment_position_from_gene_position(2,f)
		2
		>>> ExonerateParser.get_alignment_position_from_gene_position(3,f)
		3
		>>> ExonerateParser.get_alignment_position_from_gene_position(4,f)
		4
		>>> ExonerateParser.get_alignment_position_from_gene_position(5,f)
		5
		>>> ExonerateParser.get_alignment_position_from_gene_position(7,f)
		7
		"""
		# recall that hit is the reference/gene sequence
		hit_seq = str(fragment.hit.seq)
		starting_position = fragment.hit_start
		# we search for the good position, accounting for gaps
		ct_count = ExonerateParser.get_alignment_position_from_absolute_position(gene_position - starting_position, hit_seq)
		return ct_count


	@classmethod
	def get_alignment_position_from_read_position(cls, read_position: int, fragment: SearchIO.HSPFragment):
		"""

		@param read_position:
		@param fragment:
		@return:

		>>> f = HSPFragment(hit_id="ref",query_id="read",hit="XXX--XXX",query="XXXXXXXX")
		>>> ExonerateParser.get_alignment_position_from_read_position(0,f)
		0
		>>> ExonerateParser.get_alignment_position_from_read_position(2,f)
		2
		>>> ExonerateParser.get_alignment_position_from_read_position(3,f)
		3
		>>> ExonerateParser.get_alignment_position_from_read_position(4,f)
		4
		>>> ExonerateParser.get_alignment_position_from_read_position(5,f)
		5
		>>> ExonerateParser.get_alignment_position_from_read_position(7,f)
		7
		>>> f = HSPFragment(hit_id="ref",query_id="read",hit="XXXXXXXX",query="XXX--XXX")
		>>> ExonerateParser.get_alignment_position_from_read_position(0,f)
		0
		>>> ExonerateParser.get_alignment_position_from_read_position(2,f)
		2
		>>> ExonerateParser.get_alignment_position_from_read_position(3,f)
		5
		>>> ExonerateParser.get_alignment_position_from_read_position(5,f)
		7
		"""
		# recall that query is the read sequence
		query_seq = str(fragment.query.seq)
		starting_position = fragment.query_start
		# we search for the good position, accounting for gaps
		ct_count = ExonerateParser.get_alignment_position_from_absolute_position(read_position - starting_position, query_seq)
		return ct_count


	@classmethod
	def get_read_position_from_alignment_position(cls, alignment_position: int, fragment: SearchIO.HSPFragment,
												  extension: int = 0):
		"""

		extension = 0 : search for exact position
		extension = -1 : leftmost position without an indel
		extension = 1 : rightmost position without an indel

		@param alignment_position:
		@param fragment:
		@param extension:
		@return:

		>>> f = HSPFragment(hit_id="ref",query_id="read",hit="XXX--XXX",query="X-XXXXXX")
		>>> ExonerateParser.get_read_position_from_alignment_position(0,f)
		0
		>>> ExonerateParser.get_read_position_from_alignment_position(1,f)
		Error
		>>> ExonerateParser.get_read_position_from_alignment_position(2,f)
		1
		>>> ExonerateParser.get_read_position_from_alignment_position(3,f)
		2
		>>> ExonerateParser.get_read_position_from_alignment_position(4,f)
		3
		>>> ExonerateParser.get_read_position_from_alignment_position(5,f)
		4
		>>> ExonerateParser.get_read_position_from_alignment_position(7,f)
		6
		>>> f = HSPFragment(hit_id="ref",query_id="read",hit="XXXXXXXX",query="XXX--XXX")
		>>> ExonerateParser.get_read_position_from_alignment_position(0,f)
		0
		>>> ExonerateParser.get_read_position_from_alignment_position(2,f)
		2
		>>> ExonerateParser.get_read_position_from_alignment_position(3,f)
		Error
		>>> ExonerateParser.get_read_position_from_alignment_position(4,f)
		Error
		>>> ExonerateParser.get_read_position_from_alignment_position(5,f)
		4
		>>> ExonerateParser.get_read_position_from_alignment_position(7,f)
		5
		"""
		# recall that query is the read sequence
		# recall that hit is the reference/gene sequence

		# TODO it does not make sense to check the hit(ref)
		assert fragment.hit.seq[alignment_position] != '-', "Can't retrieve read position from a gap in gene"

		if extension == 0: #or fragment.hit.seq[alignment_position] == '-':
			assert fragment.query.seq[alignment_position] != '-', "Can't retrieve read position from a gap in read"
			return fragment.query_start + alignment_position - fragment.query.seq[:alignment_position].count('-')
		elif extension < 0:
			# look for the first position in the read (query) that is not a '-' (from left to right)
			offset = 0
			while alignment_position + offset < len(fragment) and \
					fragment.query.seq[alignment_position + offset] == '-':
				offset += 1
			return fragment.query_start + alignment_position + offset - fragment.query.seq[
																		:alignment_position + offset].count('-')
		else:  # extension = 1
			# look for the first position in the gene (hit) that is not a '-' (from left to right)
			offset_gene = 1
			while alignment_position + offset_gene < len(fragment) and \
					fragment.hit.seq[alignment_position + offset_gene] == '-':
				offset_gene += 1
			# look for the first position in the read (query) that is not a '-' (from right to left)
			offset = offset_gene-1
			while alignment_position + offset >= 0 and \
					fragment.query.seq[alignment_position + offset] == '-':
				offset += -1
			return fragment.query_start + alignment_position + offset - fragment.query.seq[
																		:alignment_position + offset].count('-')


	# this method is used for cutting HSP fragments during realignement
	@classmethod
	def get_gene_position_from_alignment_position(cls, alignment_position: int, fragment: SearchIO.HSPFragment,
												  extension: int = 0):
		# recall that query is the read sequence
		# recall that hit is the reference/gene sequence

		# TODO it does not make sense to check the query(read)
		assert fragment.query.seq[alignment_position] != '-', "Can't retrieve gene position from a gap in read"

		if extension == 0: # or fragment.query.seq[alignment_position] == '-':
			assert fragment.hit.seq[alignment_position] != '-', "Can't retrieve gene position from a gap in gene"
			return fragment.hit_start + alignment_position - fragment.query.seq[:alignment_position].count('-')
		elif extension == -1:
			offset = 0
			if alignment_position + offset > 0 and fragment.hit.seq[alignment_position + offset] == '-':
				offset += 1
			#offset += 1
			return fragment.hit_start + alignment_position + offset - fragment.hit.seq[
																		:alignment_position + offset].count('-')
		else:  # extension = 1
			offset = 0
			if alignment_position + offset < len(fragment) and fragment.hit.seq[alignment_position + offset] == '-':
				offset += -1
			#offset -= 1
			return fragment.hit_start + alignment_position + offset - fragment.hit.seq[
																		:alignment_position + offset].count('-')


	@classmethod
	def get_read_positions_from_gene_positions(cls, gene_begin : int, gene_end: int, fragment: SearchIO.HSPFragment):
		"""

		@param gene_begin:
		@param gene_end:
		@param fragment:
		@return:
		"""
		alignment_begin = cls.get_alignment_position_from_gene_position(gene_begin,fragment)
		read_begin = cls.get_read_position_from_alignment_position(alignment_begin,fragment,-1)
		alignment_end = cls.get_alignment_position_from_gene_position(gene_end, fragment)
		read_end = cls.get_read_position_from_alignment_position(alignment_end,fragment,1)
		# print(f"gene : {gene_begin},{gene_end}, \
		# 	alignment : {alignment_begin},{alignment_end}, \
		# 	read : {read_begin},{read_end}, \
		# 	read letters {fragment.query[alignment_begin]},{fragment.query[alignment_end]} \
		# 	gene letters {fragment.hit[alignment_begin]},{fragment.hit[alignment_end]} \
		# 	")
		return (read_begin,read_end)

	def getAllQueryResults(self) -> [SearchIO.QueryResult]:
		"""
			Return query results from Exonerate output or None if nothing has been parsed.
		"""
		return self.query_results


	def getReadResultFromName(self, read_name : str) :
		"""
			Return query results from a given read or None object if nothing is found.
		"""
		assert read_name in self.query_indexes
		return self.query_results[self.query_indexes[read_name]]


	def getReadResultFromIndex(self, read_index : int) :
		assert read_index < len(self.query_results)
		return self.query_results[read_index]


	def getReadIndexFromName(self, read_name : str) -> int :
		"""
			Returns the index of a read from its name or -1 if the read could not
			be found.
		"""
		assert read_name in self.query_indexes
		return self.query_indexes[read_name]

	def removeReadResultsFromName(self, read_name : str) :
		"""
			remove a read from the results from a given name.
		"""
		read_index = 0
		while read_index < len(self.query_results) and self.query_results[read_index].id != read_name :
			read_index = read_index + 1
		assert read_index < len(self.query_results)
		del self.query_results[read_index]
		if self.read_set.get_index_from_readname(read_name) != -1:
			self.read_set.remove(read_name)

	def removeReadResultsFromIndex(self, read_index : int) :
		"""
			Remove a read from the results from a given index.
		"""
		if 0 <= read_index < len(self.query_results) :
			read_to_delete = self.query_results[read_index].id
			del self.query_results[read_index]
			self.read_set.remove(read_to_delete)
		self.__index_queries()

	def get_read_coverage(self, read : str) -> float :
		"""
			From a given read name, return the read coverage.
			The read coverage is the ratio between the read alignment and the read size.
		"""
		return (self.getReadResultFromName(read)[-1][-1][-1].query_range[1] - self.getReadResultFromName(read)[-1][-1][0].query_range[0]) / (self.getReadResultFromName(read)[-1][-1][-1].query_range[1])

	def export_to_json(self, output_filename, gene_id: str = None, gene_seq: str = None):
		"""
			Write a json file containing every alignments from the Fragment objects.
		"""
		results = {}
		for read in self.reads_fragments:
			read_dict = {}
			current_read = ""
			for fragment in read:
				read_dict['{}-{}'.format(fragment.hit_start, fragment.hit_end)] = fragment.as_dict()
				current_read = fragment.query_id
			if read_dict != {}:
				results[current_read] = read_dict

		if results != {}:
			if gene_id: results["gene_id"] = gene_id
			if gene_seq: results["gene_seq"] = gene_seq

			results["data-type"] = "binary"

			with open(output_filename, "w+") as f:
				f.write(json.dumps(results))