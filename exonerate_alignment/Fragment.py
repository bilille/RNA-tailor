from Bio import SearchIO

class Fragment(SearchIO._model.hsp.HSPFragment):

	def as_dict(self) -> dict :
		"""
			Return the current Fragment in a dictionary form that can
			be used to export into json.
		"""
		d = {}

		d['hit_name'] = self.hit_id
		d['query_name'] = self.query_id
		d['hit_strand'] = self.hit_strand
		d['query_strand'] = self.hit_strand
		d['hit'] = str(self.hit.seq)
		d['query'] = str(self.query.seq)
		d['hit_start'] = self.hit_start
		d['hit_end'] = self.hit_end
		d['query_start'] = self.query_start
		d['query_end'] = self.query_end
		return d
