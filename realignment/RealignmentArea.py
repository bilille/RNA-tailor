"""
A realignment area corresponds to
- a segment of the reference file
- a list of consecutive blocks
for a given read
"""
from exon_matrix.Block import Block

class RealignmentArea:

    def __init__(self, readname: str, list_of_blocks: [Block], reference_begin: int, reference_end: int, consecutive_length: int, available_reference_blocks: [Block], reference_blocks: [Block]):
        """

        @param readname:
        @param list_of_blocks: blocks that contain the read
        @param reference_begin:
        @param reference_end:
        @param consecutive_length: sum of lengths of blocks in list_of_blocks
        @param available_reference_blocks: blocks that do not contain the read
        @param reference_blocks: all blocks (i.e. list_of_blocks + available_reference_blocks)
        """
        self.readname = readname
        self.list_of_blocks = list_of_blocks
        self.reference_begin = reference_begin
        self.reference_end = reference_end
        self.consecutive_length = consecutive_length
        self.available_reference_blocks = available_reference_blocks
        self.reference_blocks = reference_blocks
        for b in self.list_of_blocks:
            assert b.is_uncertain()

    def number_of_blocks (self):
        return len(self.list_of_blocks)

    def get_first_block(self) -> Block:
        return self.list_of_blocks[0]

    def get_last_block(self) -> Block:
        return self.list_of_blocks[-1]

    def get_blocks(self) -> [Block]:
        return self.list_of_blocks

    def get_consecutive_block_length(self):
        return self.consecutive_length

    def get_reference_begin(self):
        return self.reference_begin

    def get_reference_end(self):
        return self.reference_end

    def get_available_reference_blocks(self) -> [Block]:
        return self.available_reference_blocks

    def get_reference_blocks(self) -> [Block]:
        return self.reference_blocks

    def has_enough_space_to_realign(self):
        """
        Do not realign if the available area is the same as the area occupied by the uncertain blocks.
        This apply only if the uncertain blocks are following each other.
        """
        realignable_area_begin = self.reference_begin
        realignable_area_end = self.reference_end
        # we first check that the bound of the blocks correponds to bounds on the reference sequence
        if realignable_area_end == self.get_last_block().get_end() and realignable_area_begin == self.get_first_block().get_begin():
            # the we check that blocks are not all consecutive (i.e. there's space between at least two blocks)
            r = self.list_of_blocks
            block_index = 1
            while block_index < len(r) and \
                    r[block_index].get_begin() == r[block_index - 1].get_end() + 1:
                block_index += 1
            # Do not realign because blocks follow each other
            if block_index >= len(r):
                return False
        return True