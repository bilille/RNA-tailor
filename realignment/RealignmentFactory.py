import os
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from exon_matrix.PredictedExon import PredictedExon
from exon_matrix.BlockAlignment import BlockAlignment
from exon_matrix.AnnotatedCompactMatrix import AnnotatedCompactMatrix
from exon_matrix.BinaryMatrix import BinaryMatrix
from exon_matrix.Block import Block, BlockReadStatus
from exonerate_alignment.ExonerateParser import ExonerateParser
from realignment.RealignmentArea import RealignmentArea
import config
from seq.ReadsSet import ReadsSet


class RealignmentFactory:

    @classmethod
    def compute_realignable_areas(cls, annotated_matrix: AnnotatedCompactMatrix, count_realignable_read: bool = False) -> {}:
        """
            Write every realignable area for each read in a .txt file.
            The first and last exon will not be considered while looking for realignable areas.
        """
        read_names = annotated_matrix.get_read_names()

        # a dictionary that contains for each read (key) a list of RealignmentArea (values)
        result = {}

        # reset the amount for every block
        if count_realignable_read:
            for exon in annotated_matrix.predicted_exons:
                for block in exon.get_blocks():
                    block.set_realignable_read_amount(0)

        # Find realignable areas for each read
        for read in read_names:

            # build for that read a list of list of consecutive uncertain blocks
            # a list consecutive uncertain blocks define realignment ranges
            consecutive_realignable_blocks = []
            available_reference_blocks = []
            reference_blocks = []
            realignable_ranges = []
            predicted_exons = annotated_matrix.get_read_predicted_exons(read)
            for exon in predicted_exons: #[1:-1]:  # First and last exon are ignored
                for block in exon.get_blocks():
                    if block.contain_read(read):
                        if block.is_uncertain():
                            consecutive_realignable_blocks.append(block)
                            reference_blocks.append(block)
                        else:
                            if consecutive_realignable_blocks != []:

                                realignable_ranges.append((consecutive_realignable_blocks,available_reference_blocks,reference_blocks))
                            consecutive_realignable_blocks = []
                            available_reference_blocks = []
                            reference_blocks = []
                    else:
                        available_reference_blocks.append(block)
                        reference_blocks.append(block)

            if consecutive_realignable_blocks != []:

                realignable_ranges.append((consecutive_realignable_blocks,available_reference_blocks,reference_blocks))

            realignment_areas = []
            for r,a,b in realignable_ranges:

                # Compute the length of every consecutive realignable blocks to decide
                # wether or not it is necessary to realign.
                consecutive_blocks_length = sum((b.get_end() - b.get_begin() + 1) for b in r)
                # consecutive blocks of length greater than MAX_SMOOTHING_LEN could be realigned
                # those of length less than HARD_REALIGNEMENT_LENGTH will be hard realigned
                # those of length greater than MIN_REALIGNABLE_LENGTH will be soft realigned
                # those between HARD_REALIGNEMENT_LENGTH and MIN_REALIGNABLE_LENGTH will not be considered
                if consecutive_blocks_length >= config.MAX_SMOOTHING_LEN: #config.MIN_REALIGNABLE_LENGTH

                    # Get the solid block containing the read right before the first realignable block.
                    # If no solid block has been found, the realignable area starts on 0.
                    realignable_area_begin_block = annotated_matrix.get_previous_solid_block(read, r[0].get_begin())
                    realignable_area_begin = 0 if realignable_area_begin_block == None else realignable_area_begin_block.get_end() + 1

                    # Get the solid block containing the read right after the last realignable block.
                    # If no solid block has been found, the realignable area ends on gene's length.
                    realignable_area_end_block = annotated_matrix.get_next_solid_block(read, r[-1].get_end())
                    realignable_area_end = annotated_matrix.binary_matrix.get_width() if realignable_area_end_block == None else realignable_area_end_block.get_begin() - 1

                    reference_blocks = []
                    print(f"{read} {reference_blocks}")
                    realignable_area_begin_block = realignable_area_begin_block.get_next_block() if realignable_area_begin_block != None else annotated_matrix.get_predicted_exons()[0].get_blocks()[0]
                    if realignable_area_begin_block is None:
                        realignable_area_begin_block = r[0]
                    realignable_area_end_block = realignable_area_end_block.get_previous_block() if realignable_area_end_block != None else annotated_matrix.get_predicted_exons()[-1].get_blocks()[-1]
                    if realignable_area_end_block is None:
                        realignable_area_end_block = r[-1]
                    b = realignable_area_begin_block
                    print(b)
                    print(realignable_area_end_block)
                    while b is not realignable_area_end_block:
                        reference_blocks.append(b)
                        b = b.get_next_block()
                    reference_blocks.append(b)
                    print(f"{read} {reference_blocks}")

                    ra = RealignmentArea(read, r, realignable_area_begin, realignable_area_end, consecutive_blocks_length,a,reference_blocks)
                    # check if the realignment area concerns all the begining of the reference or all the end of the reference
                    if ra.reference_begin == 0 or ra.reference_end == annotated_matrix.binary_matrix.get_width():
                        print(f"{ra} discarded")
                        continue
                    if ra.has_enough_space_to_realign():
                        realignment_areas.append(ra)

                    if count_realignable_read:
                        for block in r:
                            block.set_realignable_read_amount(block.get_realignable_read_amount() + 1)

            if realignment_areas != []:
                result[read] = realignment_areas

        # print("* Realignable areas")
        # for read in result:
        #     print("** {}".format(read))
        #     for a in result[read]:
        #         print(f"*** {a.get_consecutive_block_length()} ({a.get_reference_begin()}/{a.get_reference_end()})")
        #         try:
        #             print("    blocks:")
        #             for b in a.get_blocks():
        #                 print("      {}".format(b))
        #         except:
        #             print("      no block")
        #         try:
        #             print("    available reference blocks:")
        #             for b in a.get_available_reference_blocks():
        #                 print("      {}".format(b))
        #         except:
        #             print("      no block")
        #         try:
        #             print("    reference blocks:")
        #             for b in a.get_reference_blocks():
        #                 print("      {}".format(b))
        #         except:
        #             print("      no block")

        return result


    @classmethod
    def write_realignment_scores(cls,realignable_areas: dict, reads:ReadsSet, reference: SeqRecord, gene_filename: str, bm: BinaryMatrix):
        """
            Apply a splice alignment to realignable areas using Exonerate and write scores into
            a file.
        """
        realign_dir = os.path.join(gene_filename,"realign")
        if not os.path.exists(realign_dir):
            os.mkdir(realign_dir)

        synthesis_filename = os.path.join(realign_dir,f"{gene_filename}_synthesis.txt")
        synthesis_file = open(synthesis_filename, "a")
        for readname in realignable_areas:
            synthesis_file.write("\n{}\n".format(readname))
            areas = realignable_areas[readname]

            for area in [ a for a in areas if config.MIN_REALIGNABLE_LENGTH <= a.get_consecutive_block_length() ]:
                realignable_blocks = area.get_blocks()  # list of blocks

                # check whether the blocks are contiguous or not, this will serve
                # when we have to decide if new fragements are better
                continuous_blocks = True
                block_index = 1
                while block_index < len(realignable_blocks) and continuous_blocks:
                    continuous_blocks = \
                        realignable_blocks[block_index].get_begin() == \
                        realignable_blocks[block_index - 1].get_end() + 1
                    block_index += 1

                # The previous coordinates on the reference where the read is aligned with the gene
                previous_gene_begin = realignable_blocks[0].get_begin()
                previous_gene_end = realignable_blocks[-1].get_end()

                # The read coordinates where the read is aligned with the gene
                read_begin = realignable_blocks[0].get_read_coordinates(readname)[0]
                read_end = realignable_blocks[-1].get_read_coordinates(readname)[1]

                # The gene coordinates where the realignment is possible
                gene_begin = area.get_reference_begin()
                gene_end = area.get_reference_end()

                # TODO 23_5_rea
                print(f"\n# Realignments for read {readname}")
                print(f"Realignment area ({read_begin},{read_end})/({gene_begin},{gene_end})/({previous_gene_begin},{previous_gene_end})")
                print(f"Length on read {read_end-read_begin+1}, on gene {previous_gene_end-previous_gene_begin+1}")
                read_number = reads.get_index_from_readname(readname)
                # get the current binary vector of the realignment area
                previous_line = bm.inner_matrix[read_number][previous_gene_begin:previous_gene_end+1]
                print(previous_line)

                # discard realignement if area on the reference is equal to area on the read at config.REALIGNMENT_ERROR_EDGE
                if abs((read_end-read_begin+1) - (gene_end-gene_begin+1)) <= config.REALIGNMENT_ERROR_EDGE:
                    synthesis_file.write(f"Realignment area ({read_begin},{read_end})/({gene_begin},{gene_end}) discarded because lengths on read and reference are almost the same\n")
                    continue

                #  Write a fasta file for the gene subpart
                subgene_filename = os.path.join(realign_dir,"{}_{}_{}.fa".format(gene_filename, gene_begin, gene_end))
                with open(subgene_filename,"w+") as subgene_file:
                    subgene_file.write(">{} {} {}\n".format(gene_filename, gene_begin, gene_end))
                    subgene_sequence = reference[gene_begin:gene_end]
                    subgene_file.write("{}\n".format(str(subgene_sequence)))
                    subgene_file.close()
                # Write a fasta file for the read subpart
                subread_filename = os.path.join(realign_dir,"{}_{}_{}.fa".format(readname, read_begin, read_end))
                with open(subread_filename, "w+") as subread_file:
                    subread_file.write(">{} {} {}\n".format(readname, read_begin, read_end))
                    subread_sequence = reads.get_read(readname)[read_begin:read_end] #self.binary_matrix.get_read_sequence(read)[read_begin:read_end]
                    subread_file.write(str(subread_sequence.seq) + "\n")
                    subread_filename.close()
                # Run Exonerate on the previously created files
                exonerate_filename = os.path.join(realign_dir,"{}_{}_{}_exonerate.txt".format(readname, gene_begin, gene_end))
                c = os.system(
                    "./exonerate2.3.sh -S no --revcomp false --dnahspthreshold 30 -m affine:local -E -s 50 --showvulgar no --showtargetgff y --bestn 1 {} {} > {}".format(
                        subread_filename, subgene_filename, exonerate_filename))
                if c != 0:
                    synthesis_file.write(
                        f"Realignment area ({read_begin},{read_end})/({gene_begin},{gene_end}) could not be realigned because of Exonerate termination status. \n".format(
                            (read_begin, read_end)))
                    continue

                # Parse Exonerate and write down the alignment results.

                # get exonerate results from the new run of exonerate
                e = ExonerateParser(exonerate_filename,reads)
                # these assert are false if exonerate gives no result
                #assert readname in e.get_read_set().get_readnames()
                #assert len(e.get_read_set()) == 1

                results = e.getAllQueryResults()
                # print(" exonerate results for realignment on " + str((readname, read_begin, read_end)) + " "+ str(results))
                if results != []:
                    # we should have only one SearchIO.QueryResult and only one hit for it
                    hit = results[0][0]

                    # for each HSP of the hit, we check whether old HSP has to be changed or not
                    # To do that, we try to change the old framgent list of the HSP.
                    # The new fragment is discarded if its corresponds approximately to the old one.
                    for hsp in hit:
                        #print(f"HSP : {hsp}")
                        # TODO what does that mean if a hsp has multiple fragments
                        for fragment in hsp:
                            #print(f"  fragment : {fragment}")

                            new_gene_begin = fragment.hit_start + gene_begin
                            new_gene_end = fragment.hit_end + gene_begin

                            new_read_begin = read_begin + fragment.query_start
                            new_read_end = read_begin + fragment.query_end

                            # TODO 23_5_rea
                            # get the binary vector on which we could realign
                            print(f"New alignment ({new_read_begin},{new_read_end})/({new_gene_begin},{new_gene_end})")
                            new_line = bm.inner_matrix[read_number][new_gene_begin:new_gene_end + 1]
                            print(f"Length on read {new_read_end - new_read_begin + 1}, on gene {new_gene_end - new_gene_begin + 1}")
                            print(new_line)

                            # Do not realign if the position found is pretty much the same and blocks follow each other
                            if continuous_blocks and realignable_blocks[
                                0].get_begin() - config.REALIGNMENT_ERROR_EDGE < new_gene_begin < new_gene_end < \
                                    realignable_blocks[-1].get_end() + config.REALIGNMENT_ERROR_EDGE:
                                synthesis_file.write(
                                    "Exonerate realigment region is almost the same : {}/{} vs {}/{}, realigment aborted\n".format(
                                        new_gene_begin, new_gene_end, realignable_blocks[0].get_begin(),
                                        realignable_blocks[-1].get_end()))
                                # skip this fragment
                                continue

                            # if "ch178" in read :
                            # print("read {} realign {}/{} onto {}/{}".format(readname, read_begin, read_end, gene_begin, gene_end))
                            # print("read {} new coordinates: {}/{} onto {}/{}".format(readname, new_read_begin, new_read_end, new_gene_begin, new_gene_end))
                            # print("fragment.query_start: {} fragment.query_end: {}".format(fragment.query_start, fragment.query_end))

                            # TODO 23_5_rea
                            s = sum(new_line)

                            if s == 0:
                                left_remaining_length = new_read_begin - read_begin
                                right_remaining_length = read_end - new_read_end
                                print(f"left_remaining_length={left_remaining_length} right_remaining_length={right_remaining_length}")

                                if left_remaining_length == 0 and right_remaining_length == 0:
                                    # 0 the new alignment has been considered and old overlaping alignments have been corrected
                                    res = 0
                                elif right_remaining_length == 0:
                                    # TODO check the total length is ok
                                    print(f"|l| == {left_remaining_length}, |r|={right_remaining_length}, new_gene_begin:{new_gene_begin}->{new_gene_begin-left_remaining_length} ")
                                    if sum(bm.inner_matrix[read_number][
                                           new_gene_begin - left_remaining_length:new_gene_end+ 1]) == 0:
                                        new_gene_begin -= left_remaining_length
                                        res = 0
                                    else:
                                        res = 2
                                elif left_remaining_length == 0:
                                    # TODO check the total length is ok
                                    print(f"|l| == {left_remaining_length}, |r|={right_remaining_length}, new_gene_end:{new_gene_end}->{new_gene_end+right_remaining_length} ")
                                    if sum(bm.inner_matrix[read_number][
                                           new_gene_begin:new_gene_end + right_remaining_length + 1]) == 0:
                                        new_gene_end += right_remaining_length
                                        res = 0
                                    else:
                                        res = 2
                                else:
                                    # left_remaining_length != 0 and right_remaining_length != 0
                                    print(
                                        f"|l| == {left_remaining_length}, |r|={right_remaining_length}, new_gene_begin:{new_gene_begin}->{new_gene_begin-left_remaining_length} , new_gene_end:{new_gene_end}->{new_gene_end + right_remaining_length} ")
                                    if sum(bm.inner_matrix[read_number][
                                           new_gene_begin - left_remaining_length:new_gene_end + 1]) == 0 \
                                            and \
                                            sum(bm.inner_matrix[read_number][
                                            new_gene_begin:new_gene_end + right_remaining_length + 1]) == 0:
                                        new_gene_begin -= left_remaining_length
                                        new_gene_end += right_remaining_length
                                        res = 0
                                    else:
                                        res = 2
                                    # 2 if the new alignment disrupt colinearity
                            elif s != len(new_line):
                                # 3 if the new alignement m-overlaps multiple old alignements
                                res = 3
                            elif s == len(new_line):
                                # 1 if the new alignment overlaps an existing fragment in the HSP in the reference
                                res = 1
                            else:
                                raise Error()


                            if res == 0:
                                cls.change_inner_matrix(bm, read_number,
                                                        (previous_gene_begin, previous_gene_end),
                                                        (new_gene_begin, new_gene_end))
                                print(
                                    f"changed in {bm.inner_matrix[read_number][new_gene_begin:new_gene_end + 1]}")

                            old_positions = [None,None,None,None]

                            # res, old_positions = ExonerateParser.insert_HSPFragment(reads.get_read(readname), new_gene_begin, new_gene_end, new_read_begin,new_read_end, fragment, area)

                            if res == 0:
                                synthesis_file.write(
                                    "Exonerate alignment ({},{})/({},{})  vs. ({},{})/({},{}) validated\n".format(
                                        new_read_begin, new_read_end, new_gene_begin, new_gene_end, read_begin,
                                        read_end, gene_begin, gene_end))
                            elif res == 1:
                                synthesis_file.write(
                                    "Exonerate alignment ({},{})/({},{}) vs. ({},{})/({},{})  canceled because realignment occurs on already occupied area in the gene\n".format(
                                        new_read_begin, new_read_end, new_gene_begin, new_gene_end, read_begin,
                                        read_end, gene_begin, gene_end))
                            elif res == 2:
                                synthesis_file.write(
                                    "Exonerate alignment ({},{})/({},{}) vs. ({},{})/({},{}) canceled because realignment violates colinearity\n".format(
                                        new_read_begin, new_read_end, new_gene_begin, new_gene_end, old_positions[0],
                                        old_positions[1], old_positions[2], old_positions[3]))
                            elif res == 3:
                                synthesis_file.write(
                                    "Exonerate alignment ({},{})/({},{}) canceled because realignment spreads multiple old alignements in the read\n".format(
                                        new_read_begin, new_read_end, new_gene_begin, new_gene_end))
                            else:
                                raise Error()


                else :
                    synthesis_file.write("Exonerate found no alignment for read part ({},{}) on gene part({},{})".format(read_begin, read_end, gene_begin, gene_end))
            synthesis_file.write("\n")
        synthesis_file.close()
        #exit(0)


    @classmethod
    def write_realignment_scores_on_ACM(cls,realignable_areas: dict, reads:ReadsSet, reference: SeqRecord, gene_filename: str,  bm: BinaryMatrix, exons : [PredictedExon]):
        """
            Apply a splice alignment to realignable areas using Exonerate and write scores into
            a file.

            Only realignable_areas of length greater than MIN_REALIGNABLE_LENGTH are considered.
        """
        realign_dir = os.path.join(gene_filename,"realign")
        if not os.path.exists(realign_dir):
            os.mkdir(realign_dir)

        synthesis_filename = os.path.join(realign_dir,f"{gene_filename}_synthesis.txt")
        synthesis_file = open(synthesis_filename, "a")
        for readname in realignable_areas:
            synthesis_file.write("\n{}\n".format(readname))
            areas = realignable_areas[readname]

            for area in [ a for a in areas if config.MIN_REALIGNABLE_LENGTH <= a.get_consecutive_block_length() ]:
                realignable_blocks = area.get_blocks()  # list of blocks

                # check whether the blocks are contiguous or not, this will serve
                # when we have to decide if new fragements are better
                continuous_blocks = True
                block_index = 1
                while block_index < len(realignable_blocks) and continuous_blocks:
                    continuous_blocks = \
                        realignable_blocks[block_index].get_begin() == \
                        realignable_blocks[block_index - 1].get_end() + 1
                    block_index += 1

                # The previous coordinates on the reference where the read is aligned with the gene
                previous_gene_begin = realignable_blocks[0].get_begin()
                previous_gene_end = realignable_blocks[-1].get_end()

                # The read coordinates where the read is aligned with the gene
                read_begin = realignable_blocks[0].get_read_coordinates(readname)[0]
                read_end = realignable_blocks[-1].get_read_coordinates(readname)[1]

                # The gene coordinates where the realignment is possible
                gene_begin = area.get_reference_begin()
                gene_end = area.get_reference_end()

                read_number = reads.get_index_from_readname(readname)



                # discard realignement if area on the reference is equal to area on the read at config.REALIGNMENT_ERROR_EDGE
                if abs((read_end-read_begin+1) - (gene_end-gene_begin+1)) <= config.REALIGNMENT_ERROR_EDGE:
                    synthesis_file.write(f"Realignment area ({read_begin},{read_end})/({gene_begin},{gene_end}) discarded because lengths on read and reference are almost the same\n")
                    continue

                #  Write a fasta file for the gene subpart
                subgene_filename = os.path.join(realign_dir,"{}_{}_{}.fa".format(gene_filename, gene_begin, gene_end))
                with open(subgene_filename,"w+") as subgene_file:
                    subgene_file.write(">{} {} {}\n".format(gene_filename, gene_begin, gene_end))
                    subgene_sequence = reference[gene_begin:gene_end]
                    subgene_file.write("{}\n".format(str(subgene_sequence)))

                # Write a fasta file for the read subpart
                subread_filename = os.path.join(realign_dir,"{}_{}_{}.fa".format(readname, read_begin, read_end))
                with open(subread_filename, "w+") as subread_file:
                    subread_file.write(">{} {} {}\n".format(readname, read_begin, read_end))
                    subread_sequence = reads.get_read(readname)[read_begin:read_end] #self.binary_matrix.get_read_sequence(read)[read_begin:read_end]
                    subread_file.write(str(subread_sequence.seq) + "\n")

                # Run Exonerate on the previously created files
                exonerate_filename = os.path.join(realign_dir,"{}_{}_{}_exonerate.txt".format(readname, gene_begin, gene_end))
                c = os.system(
                    "./exonerate2.3.sh -S no --revcomp false --dnahspthreshold 30 -m affine:local -E -s 50 --showvulgar no --showtargetgff y --bestn 1 {} {} > {}".format(
                        subread_filename, subgene_filename, exonerate_filename))
                if c != 0:
                    synthesis_file.write(
                        f"Realignment area ({read_begin},{read_end})/({gene_begin},{gene_end}) could not be realigned because of Exonerate termination status. \n".format(
                            (read_begin, read_end)))
                    continue

                # Parse Exonerate and write down the alignment results.

                # get exonerate results from the new run of exonerate
                e = ExonerateParser(exonerate_filename,reads)
                # these assert are false if exonerate gives no result
                #assert readname in e.get_read_set().get_readnames()
                #assert len(e.get_read_set()) == 1

                results = e.getAllQueryResults()

                if results != []:
                    # we should have only one SearchIO.QueryResult and only one hit for it
                    hit = results[0][0]

                    # for each HSP of the hit, we check whether old HSP has to be changed or not
                    # To do that, we try to change the old framgent list of the HSP.
                    # The new fragment is discarded if its corresponds approximately to the old one.

                    for hsp in hit:
                        #print(f"HSP : {hsp}")
                        # TODO what does that mean if a hsp has multiple fragments

                        for fragment in hsp:
                            #print(f"  fragment : {fragment}")

                            new_gene_begin = fragment.hit_start + gene_begin
                            new_gene_end = fragment.hit_end + gene_begin

                            new_read_begin = read_begin + fragment.query_start
                            new_read_end = read_begin + fragment.query_end

                            # TODO 23_5_rea
                            # get the binary vector on which we could realign
                            print(f"New alignment ({new_read_begin},{new_read_end})/({new_gene_begin},{new_gene_end})")
                            new_line = bm.inner_matrix[read_number][new_gene_begin:new_gene_end + 1]
                            print(f"Length on read {new_read_end - new_read_begin + 1}, on gene {new_gene_end - new_gene_begin + 1}")
                            print(new_line)

                            # Do not realign if the position found is pretty much the same and blocks follow each other
                            if continuous_blocks and realignable_blocks[
                                0].get_begin() - config.REALIGNMENT_ERROR_EDGE < new_gene_begin < new_gene_end < \
                                    realignable_blocks[-1].get_end() + config.REALIGNMENT_ERROR_EDGE:
                                synthesis_file.write(
                                    "Exonerate realigment region is almost the same : {}/{} vs {}/{}, realigment aborted\n".format(
                                        new_gene_begin, new_gene_end, realignable_blocks[0].get_begin(),
                                        realignable_blocks[-1].get_end()))
                                # skip this fragment
                                continue

                            # if readname == "SRR15899612.127091.1" and read_begin == 821:
                            #     print(" exonerate results for realignment on " + str(
                            #         (readname, read_begin, read_end)) + " " + str(results))
                            #     print(hit)
                            #     print(f"HSP : {hsp}")
                            #     print(f"  fragment : {fragment}")
                            #     print("sum newline")
                            #     print(new_line)
                            #     print(sum(new_line))
                            #     left_remaining_length = new_read_begin - read_begin
                            #     right_remaining_length = read_end - new_read_end
                            #     print(
                            #         f"left_remaining_length={left_remaining_length} right_remaining_length={right_remaining_length}")
                            #     print(reads.get_read_from_index(read_number).id)
                            #
                            #     new_gene_begin = fragment.hit_start + gene_begin
                            #     new_gene_end = fragment.hit_end + gene_begin
                            #     print(f"new_gene_begin = fragment.hit_start + gene_begin")
                            #     print(f"{new_gene_begin} = {fragment.hit_start} + {gene_begin}")
                            #     print(f"new_gene_end = fragment.hit_end + gene_begin")
                            #     print(f"{new_gene_end} = {fragment.hit_end} + {gene_begin}")
                            #     new_read_begin = read_begin + fragment.query_start
                            #     new_read_end = read_begin + fragment.query_end
                            #     print(frjhui)
                            #     new_line = bm.inner_matrix[read_number][new_gene_begin:new_gene_end + 1]
                            # TODO 23_5_rea
                            s = sum(new_line)

                            if s == 0:
                                left_remaining_length = new_read_begin - read_begin
                                right_remaining_length = read_end - new_read_end
                                print(f"left_remaining_length={left_remaining_length} right_remaining_length={right_remaining_length}")

                                if left_remaining_length == 0 and right_remaining_length == 0:
                                    # 0 the new alignment has been considered and old overlaping alignments have been corrected
                                    res = 0
                                elif right_remaining_length == 0:
                                    # TODO check the total length is ok
                                    print(f"|l| == {left_remaining_length}, |r|={right_remaining_length}, new_gene_begin:{new_gene_begin}->{new_gene_begin-left_remaining_length} ")
                                    if sum(bm.inner_matrix[read_number][
                                           new_gene_begin - left_remaining_length:new_gene_end+ 1]) == 0:
                                        new_gene_begin -= left_remaining_length
                                        res = 0
                                    else:
                                        res = 2
                                elif left_remaining_length == 0:
                                    # TODO check the total length is ok
                                    print(f"|l| == {left_remaining_length}, |r|={right_remaining_length}, new_gene_end:{new_gene_end}->{new_gene_end+right_remaining_length} ")
                                    if sum(bm.inner_matrix[read_number][
                                           new_gene_begin:new_gene_end + right_remaining_length + 1]) == 0:
                                        new_gene_end += right_remaining_length
                                        res = 0
                                    else:
                                        res = 2
                                else:
                                    # left_remaining_length != 0 and right_remaining_length != 0
                                    print(
                                        f"|l| == {left_remaining_length}, |r|={right_remaining_length}, new_gene_begin:{new_gene_begin}->{new_gene_begin-left_remaining_length} , new_gene_end:{new_gene_end}->{new_gene_end + right_remaining_length} ")
                                    if sum(bm.inner_matrix[read_number][
                                           new_gene_begin - left_remaining_length:new_gene_end + 1]) == 0 \
                                            and \
                                            sum(bm.inner_matrix[read_number][
                                            new_gene_begin:new_gene_end + right_remaining_length + 1]) == 0:
                                        new_gene_begin -= left_remaining_length
                                        new_gene_end += right_remaining_length
                                        res = 0
                                    else:
                                        res = 2
                                    # 2 if the new alignment disrupt colinearity
                            elif s != len(new_line):
                                # 3 if the new alignement m-overlaps multiple old alignements
                                res = 3
                            elif s == len(new_line):
                                # 1 if the new alignment overlaps an existing fragment in the HSP in the reference
                                res = 1
                            else:
                                raise Error()


                            if res == 0:
                                # cls.change_inner_matrix(bm, read_number,
                                #                         (previous_gene_begin, previous_gene_end),
                                #                         (new_gene_begin, new_gene_end))
                                print(
                                    f"changed in {bm.inner_matrix[read_number][new_gene_begin:new_gene_end + 1]}")
                                # GET blocks between previous_gene_begin and previous_gene_end
                                donors_blocks = cls.get_blocks_between_positions(target_read_name= readname, begin= previous_gene_begin, end=previous_gene_end, predicted_exons= exons)
                                # Get blocks between new_gene_begin and new_gene_end
                                acceptors_block = cls.get_blocks_between_positions_and_fills_gaps_with_block(begin= new_gene_begin, end=new_gene_end, predicted_exons= exons)
                                mapped_acceptors_blocks = cls.map_read_to_new_blocks_normal_tag(target_read_name= readname, donor_blocks= donors_blocks, acceptor_blocks= acceptors_block)
                                exons = cls.insert_new_blocks_in_PredictedExons(mapped_acceptors_blocks= mapped_acceptors_blocks, predicted_exons_list= exons)


                            old_positions = [None,None,None,None]

                            # res, old_positions = ExonerateParser.insert_HSPFragment(reads.get_read(readname), new_gene_begin, new_gene_end, new_read_begin,new_read_end, fragment, area)

                            if res == 0:
                                synthesis_file.write(
                                    "Exonerate alignment ({},{})/({},{})  vs. ({},{})/({},{}) validated\n".format(
                                        new_read_begin, new_read_end, new_gene_begin, new_gene_end, read_begin,
                                        read_end, gene_begin, gene_end))
                            elif res == 1:
                                synthesis_file.write(
                                    "Exonerate alignment ({},{})/({},{}) vs. ({},{})/({},{})  canceled because realignment occurs on already occupied area in the gene\n".format(
                                        new_read_begin, new_read_end, new_gene_begin, new_gene_end, read_begin,
                                        read_end, gene_begin, gene_end))
                            elif res == 2:
                                synthesis_file.write(
                                    "Exonerate alignment ({},{})/({},{}) vs. ({},{})/({},{}) canceled because realignment violates colinearity\n".format(
                                        new_read_begin, new_read_end, new_gene_begin, new_gene_end, old_positions[0],
                                        old_positions[1], old_positions[2], old_positions[3]))
                            elif res == 3:
                                synthesis_file.write(
                                    "Exonerate alignment ({},{})/({},{}) canceled because realignment spreads multiple old alignements in the read\n".format(
                                        new_read_begin, new_read_end, new_gene_begin, new_gene_end))
                            else:
                                raise Error()


                else :
                    synthesis_file.write("Exonerate found no alignment for read part ({},{}) on gene part({},{})".format(read_begin, read_end, gene_begin, gene_end))
            synthesis_file.write("\n")
        synthesis_file.close()


    @classmethod
    def hard_realignment(cls, realignable_areas: dict, threshold: int, reads:ReadsSet, reference: SeqRecord, gene_filename: str, bm: BinaryMatrix):
        """
        A hard realignment in a realignment which is done without computing any new alignment.

        We only check if the segment to be realigned is less than MIN_REALIGNMENT_LENGTH.
        realignable_areas should not contain segments of length less than MAX_SMOOTHING_LEN

        @param realignable_areas:
        @param threshold: maximal length for realignment areas to be considered as hard realigned
        @param reads:
        @param reference:
        @param gene_filename:
        @param bm: BinaryMatrix modified
        @return:

        side effects:
        - realignable_areas is modified, at the end, only realignable_areas of length greater than threshold are kept

        """
        realign_dir = os.path.join(gene_filename, "realign")
        if not os.path.exists(realign_dir):
            os.mkdir(realign_dir)

        synthesis_filename = os.path.join(realign_dir, f"{gene_filename}_synthesis.txt")
        synthesis_file = open(synthesis_filename, "w")

        for readname in realignable_areas:
            synthesis_file.write("\nHard realignments for {}\n".format(readname))
            read_number = reads.get_index_from_readname(readname)
            areas = realignable_areas[readname]
            kept_areas = []
            for area in areas:
                if area.get_consecutive_block_length() < threshold:
                    realignable_blocks = area.get_blocks()  # list of blocks
                    previous_gene_begin = realignable_blocks[0].get_begin()
                    previous_gene_end = realignable_blocks[-1].get_end()

                    # remapped_blocks = cls.__choose_blocks(readname,area.reference_blocks,area.consecutive_length)
                    remapped_blocks = cls.__choose_blocks_from_PE(readname,area.reference_blocks,area.consecutive_length)
                    # TODO check if not empty ?
                    if remapped_blocks != []:
                        if cls.Are_donor_and_acceptors_blocks_different(donor_blocks= realignable_blocks,
                                                    acceptor_blocks = remapped_blocks):
                        # bm.inner_matrix[read_number][previous_gene_begin:previous_gene_end + 1] = 0
                            cls.map_read_to_new_blocks_hard_tag(readname,donor_blocks= realignable_blocks,
                                                        acceptor_blocks = remapped_blocks)
                            # for b in remapped_blocks:
                            #     bm.inner_matrix[read_number][b.get_begin():b.get_end() + 1] = 1
                            synthesis_file.write(
                                f"({[(b.get_begin(),b.get_end()) for b in realignable_blocks]}) validated to {[(b.get_begin(),b.get_end()) for b in remapped_blocks]}\n")
                        else:
                            synthesis_file.write(
                                f"({[(b.get_begin(), b.get_end()) for b in realignable_blocks]}) , stopped included into {[(b.get_begin(), b.get_end()) for b in remapped_blocks]}\n")
                    else:
                        synthesis_file.write(
                            f"({[(b.get_begin(), b.get_end()) for b in realignable_blocks]}) , empty acceptor blocks  {[(b.get_begin(), b.get_end()) for b in remapped_blocks]}\n")
                else:
                    kept_areas.append(area)
                    synthesis_file.write(
                        f"(fragment length above {threshold} < {area.get_consecutive_block_length()}\n")

            realignable_areas[readname] = kept_areas

        synthesis_file.close()

    @classmethod
    def __choose_blocks(cls, readname:str, list_of_blocks: [Block], length: int):

        b = [ len(block) for block in list_of_blocks]
        en = [ jp.delta_read if jp is not None else -1 for jp in [ block.get_entering_junction_point() for block in list_of_blocks] ]
        le = [jp.delta_read if jp is not None else -1 for jp in
              [block.get_leaving_junction_point() for block in list_of_blocks]]
        w = [ x+y for x,y in zip(en,le) ]
        l = length
        best = (-1,0,0)
        print(f"* {readname}\nlength: {length}\nblocks: {b}\nweight: {w}")

        for i in range(0, len(b) + 1):
            s = 0
            j = i
            # While l'index j inf à len de list_b et que la sequence de b ne dépasse pas la taille l + 6
            while j < len(b) and s + b[j] <= l + 6:
            # while j < len(b) and s + b[j] <= min(l + config.MAX_SMOOTHING_LEN, l * 1.2):
                s += b[j]
                j += 1
            #if j > i and s >= min(l + config.MAX_SMOOTHING_LEN, l*1.2):
            if j > i and s >= l+6:
                print(i, j, s, b[i:j], sum(w[i:j]))
                if sum(w[i:j]) > best[0]:
                    best = (sum(w[i:j]),i,j)
        return list_of_blocks[best[1]:best[2]]


    @classmethod
    def get_sublist_of_contiguous_blocks(cls,list_of_block : [Block]):
        sorted_blocks = sorted(list_of_block, key=lambda b: b.begin)
        sublists = []
        current_sublist = [sorted_blocks[0]]
        for i in range(1, len(sorted_blocks)):
            if sorted_blocks[i - 1].end + 1 == sorted_blocks[i].begin:
                current_sublist.append(sorted_blocks[i])
            else:
                sublists.append(current_sublist)
                current_sublist = [sorted_blocks[i]]
        if current_sublist:
            sublists.append(current_sublist)
        return sublists

    @classmethod
    def Search_for_solid_block(cls,list_block : [Block]):
        '''
        Search for a solid block :
        - inside the list
        - adjacent to the list
        '''
        for b in list_block:
            if b.is_solid():
                return True
        if list_block[0].get_previous_block().is_solid() and \
            list_block[0].get_previous_block().end + 1 == list_block[0].begin:
            return True
        if list_block[-1].get_next_block().is_solid() and \
            list_block[-1].end + 1 == list_block[-1].get_next_block().begin:
            return True
        return False
    @classmethod
    def __choose_blocks_from_PE(cls, readname:str, uncontigous_list_of_blocks: [Block], length: int):
        '''
        Apply the methods from __choose_blocks to a modified list of blocks, so they are separated as exons.
        '''

        target_b_per_exons = cls.get_sublist_of_contiguous_blocks(uncontigous_list_of_blocks)
        best = (-1, 0, 0)
        choosed_blocks = []
        for list_of_blocks in target_b_per_exons:
            b = [ len(block) for block in list_of_blocks]
            en = [ jp.delta_read if jp is not None else -1 for jp in [ block.get_entering_junction_point() for block in list_of_blocks] ]
            le = [jp.delta_read if jp is not None else -1 for jp in
                  [block.get_leaving_junction_point() for block in list_of_blocks]]
            w = [ x+y for x,y in zip(en,le) ]
            l = length

            print(f"* {readname}\nlength: {length}\nblocks: {b}\nweight: {w}")

            for i in range(0, len(b) + 1):
                s = 0
                j = i
                # While l'index j inf à len de list_b et que la sequence de b ne dépasse pas la taille l + 6
                while j < len(b) and (s <= min(l + config.MAX_SMOOTHING_LEN, l * 1.2)
                                      and s >= min(l - config.MAX_SMOOTHING_LEN, l * 0.8)):
                # while j < len(b) and s + b[j] <= min(l + config.MAX_SMOOTHING_LEN, l * 1.2):

                    s += b[j]
                    j += 1
                #if j > i and s >= min(l + config.MAX_SMOOTHING_LEN, l*1.2):
                if j > i and (s <= l + config.MAX_SMOOTHING_LEN and s >= l - config.MAX_SMOOTHING_LEN):
                    # print(i, j, s, b[i:j], sum(w[i:j]))
                    # print("list_of_blocks[i:j}")
                    for bl in list_of_blocks[i: j]:
                        print(bl)
                    if sum(w[i:j]) > best[0]:
                        if cls.Search_for_solid_block(list_of_blocks[i:j]):
                            best = (sum(w[i:j]),i,j)
                            choosed_blocks = list_of_blocks[best[1]:best[2]]


        return choosed_blocks
    @classmethod
    def change_inner_matrix (cls,bm: BinaryMatrix, read_number : int, old: (int,int), new:(int,int)):
        previous_gene_begin, previous_gene_end = old
        new_gene_begin, new_gene_end = new
        bm.inner_matrix[read_number][previous_gene_begin:previous_gene_end + 1] = 0
        bm.inner_matrix[read_number][new_gene_begin:new_gene_end + 1] = 1

    @classmethod
    def map_read_to_new_blocks_normal_tag(cls, target_read_name: str, donor_blocks: [Block], acceptor_blocks: [Block]):
        # merge the BA if there is several blocks to realign
        if donor_blocks == acceptor_blocks:
            print("donors blocks and acceptors blocks are the same.")
            pass
        else:
            #print(f"Updating blocks for {target_read_name}")
            #for b in donor_blocks:
            #    print(f"\t{b}")
            if len(donor_blocks) > 1:
                BA_to_move = cls.fusion_block_alignment([b.get_block_alignment(target_read_name) for b in donor_blocks])
                for donor_block in donor_blocks:
                    donor_block.remove_read(target_read_name)
                # remove  read from the BA
                cls.realign_donor_BA_into_acceptor_block(BA_to_move, acceptor_blocks, target_read_name)
            else:
                BA_to_move = donor_blocks[0].get_block_alignment(target_read_name)
                for donor_block in donor_blocks:
                    donor_block.remove_read(target_read_name)
                cls.realign_donor_BA_into_acceptor_block(BA_to_move, acceptor_blocks, target_read_name)


            for b in acceptor_blocks:
                b.reads_status[target_read_name] = BlockReadStatus.EXON

            return acceptor_blocks

    @classmethod
    def map_read_to_new_blocks_hard_tag(cls, target_read_name: str, donor_blocks: [Block], acceptor_blocks: [Block]):
        # merge the BA if there is several blocks to realign
        if donor_blocks == acceptor_blocks:
            print("donors blocks and acceptors blocks are the same.")
            pass
        else:
            if len(donor_blocks) > 1:
                BA_to_move = cls.fusion_block_alignment([b.get_block_alignment(target_read_name) for b in donor_blocks])

                for donor_block in donor_blocks:
                    donor_block.remove_read(target_read_name)
                # remove  read from the BA
                cls.realign_donor_BA_into_acceptor_block(BA_to_move, acceptor_blocks, target_read_name)
            else:
                BA_to_move = donor_blocks[0].get_block_alignment(target_read_name)

                for donor_block in donor_blocks:
                    donor_block.remove_read(target_read_name)
                cls.realign_donor_BA_into_acceptor_block(BA_to_move, acceptor_blocks, target_read_name)

            for b in acceptor_blocks:
                b.reads_status[target_read_name] = BlockReadStatus.EXON



    @classmethod
    def divide_BA_in_two(cls, BA : BlockAlignment, dividing_point : int, len_of_acceptors_block : int)-> [BlockAlignment]:
        '''
        Divides a BlockAlignement object in two parts.
        len_of_acceptors_block is needed to properly scale the division, in case of an iterative division.
        '''
        import math
        if BA != {}:
            len_BA = BA.reference_end - BA.reference_begin + 1
            ratio = len_BA /len_of_acceptors_block
            corrected_dividing_point = math.floor(dividing_point * (ratio))

            BA1 = BlockAlignment(BA.reference_name, BA.reference_sequence[0:corrected_dividing_point],BA.reference_begin,
                                 BA.reference_begin + corrected_dividing_point, BA.read_name,
                                 BA.read_sequence[0:corrected_dividing_point],BA.read_begin, BA.read_begin+ corrected_dividing_point)

            BA2 = BlockAlignment(BA.reference_name, BA.reference_sequence[corrected_dividing_point::],
                                 BA.reference_begin + corrected_dividing_point+1,
                                 BA.reference_end , BA.read_name,
                                 BA.read_sequence[corrected_dividing_point::],BA.read_begin+ corrected_dividing_point+1, BA.read_end)
            return BA1, BA2
        else:
            return {}, {}

    @classmethod
    def realign_donor_BA_into_acceptor_block(cls, BA_to_divide : BlockAlignment, acceptor_blocks : [Block], read : str):
        if len(acceptor_blocks) > 1:
            len_acpt = sum([(ab.get_end() - ab.get_begin() +1) for ab in acceptor_blocks ])

            for acceptor_block in sorted(acceptor_blocks, key=lambda block: block.get_begin()):
                BA_to_insert, BA_to_divide = cls.divide_BA_in_two(BA_to_divide, acceptor_block.get_length(), len_acpt)
                len_acpt = len_acpt - (acceptor_block.get_end() - acceptor_block.get_begin() +1)
                BA_to_insert.reference_begin = acceptor_block.get_begin()
                BA_to_insert.reference_end = acceptor_block.get_end()
                acceptor_block.add_read(read)
                acceptor_block.add_read_positions(read,BA_to_insert.read_begin,BA_to_insert.read_end)
                acceptor_block.add_alignment(BA_to_insert)
                # acceptor_block.block_alignments[read]=BA_to_insert
                # acceptor_block.reads_positions[read]=(BA_to_insert.read_begin,BA_to_insert.read_end)
        else:
            BA_to_divide.reference_begin =acceptor_blocks[0].get_begin()
            BA_to_divide.reference_end = acceptor_blocks[0].get_end()
            acceptor_blocks[0].add_read(read)
            acceptor_blocks[0].add_read_positions(read, BA_to_divide.read_begin, BA_to_divide.read_end)
            acceptor_blocks[0].add_alignment(BA_to_divide)

    @classmethod
    def fusion_block_alignment(cls, ba_list: [BlockAlignment]) -> BlockAlignment:
        '''
        Return the merged BlockAlignment
        '''
        sorted_ba_list = sorted(ba_list, key=lambda x: x.read_begin)
        reference_name = sorted_ba_list[0].reference_name
        reference_sequence = "".join([ba.reference_sequence for ba in sorted_ba_list])
        reference_begin = sorted_ba_list[0].reference_begin
        reference_end = sorted_ba_list[-1].reference_end
        read_name = sorted_ba_list[0].read_name
        read_sequence = "".join([ba.read_sequence for ba in sorted_ba_list])
        read_begin = sorted_ba_list[0].read_begin
        read_end = sorted_ba_list[-1].read_end
        return BlockAlignment(reference_name, reference_sequence, reference_begin, reference_end, read_name,
                              read_sequence, read_begin, read_end)

    @classmethod
    def get_blocks_between_positions(cls, target_read_name: str, begin: int, end: int, predicted_exons : [PredictedExon]):
        """
            Return every blocks between two genes positions.
        """

        print("search blocks between {} and {}".format(begin, end))
        results = []
        for exon in predicted_exons:
            for block in exon.get_blocks():
                # print((block.get_begin(), block.get_end()))

                # if block.get_begin() <= begin <= block.get_end() \
                #         or  block.get_begin() <= end <= block.get_end() \
                #         or  begin <= block.get_begin() and end >= block.get_end():
                #     if block.contain_read(target_read_name):
                if begin <= block.get_begin() and block.get_end() <= end and block.contain_read(target_read_name):
                        results.append(block)
        return results

    @classmethod
    def get_blocks_between_positions_and_fills_gaps_with_block(cls, begin: int, end: int, predicted_exons : [PredictedExon]):
        results = []
        for exon in predicted_exons:
            for block in exon.get_blocks():
                if block.get_begin() <= begin <= block.get_end() \
                        or  block.get_begin() <= end <= block.get_end() \
                        or  begin <= block.get_begin() and end >= block.get_end():
                    results.append(block)

        print("Target existing blocks")
        for b in results:
            print(b)

        return cls.insert_new_blocks_after_realignement(begin, end, results)
    #@classmethod
    # def get_blocks_between_positions_and_fills_gaps_with_block(cls, begin: int, end: int, predicted_exons : [PredictedExon]):
    #     """
    #
    #     """
    #
    #     results = []
    #     for exon in predicted_exons:
    #         for block in exon.get_blocks():
    #             # print((block.get_begin(), block.get_end()))
    #             if (begin <= block.get_begin() and end >= block.get_end()):
    #                 results.append(block)
    #
    #     return cls.insert_new_blocks_after_realignement(begin, end, results)
    @classmethod
    def insert_new_blocks_after_realignement(cls, begin: int, end: int, results : [Block]):
        # print(f"insert_new_blocks_after_realignement {begin}/{end} in {[b.get_represented_genomic_interval() for b in results]}")

        r = []

        if results == []:
            # if there is no acceptor block, we have to create a new one
            r.append(Block(begin=begin, end=end))
        else:

            # first block
            if begin < results[0].get_begin():
                # we need to insert a new block before the first acceptor block
                # this block will contain only this read
                r.append(Block(begin=begin, end=results[0].get_begin() - 1))
            elif results[0].get_begin() != begin:
                # results[0].get_begin() < begin
                # we need to split the first block
                # [ old block : from r[0].b to begin-1 ] [ new block : from begin to r[0].e ]

                results[0:1] = cls.split_block(results[0],begin)

            # Last block
            if results[-1].get_end() < end:
                # we need to insert a new block after the last acceptor block
                r.append(Block(begin=results[-1].get_end() + 1, end=end))
            elif results[-1].get_end() != end:
                # end < results[-1].get_end()
                # we need to split the last block
                # [ old block : from r[-1].b to end ] [ new block : from end+1 to r[-1].e ]

                results[-1:] = cls.split_block(results[-1],end+1)


        print("New ones")
        if r:
            for b in sorted(r, key=lambda b: b.get_begin()):
                print(b)

        print("All blocks")
        for b in sorted(r + results, key=lambda b: b.get_begin()):
            print(b)

        return sorted(r + results, key=lambda b: b.get_begin())

    @classmethod
    def split_block(cls, block_to_split : Block, genomic_pos : int):
        '''
        Given a Block(start, end) object and a genomic position g inside the block range,
        Returns a list of two Blocks (start, g-1)(g,end)
        '''
        assert block_to_split.get_begin() <= genomic_pos and genomic_pos <= block_to_split.get_end(), \
            f"Splitting point {genomic_pos} outside of block {block_to_split}."
        splitted_block_list = [None,None]
        splitted_block_list[0] = Block(block_to_split.get_begin(),genomic_pos-1)
        splitted_block_list[1] = Block(genomic_pos, block_to_split.get_end())
        #Divide BA in 2 from splitted_block
        splitted_BAs = [{}, {}]
        for BA in block_to_split.block_alignments.values():
             a, b = cls.divide_BA_in_two(BA,genomic_pos-block_to_split.get_begin(), block_to_split.get_length())
             splitted_BAs[0][a.read_name]= a
             splitted_BAs[1][b.read_name] = b
        # Add read_name to initialize block_alignement, read_status and read_positions in new Blocks
        for read_name in block_to_split.get_reads_name():
            splitted_block_list[0].add_read(read_name)
            splitted_block_list[1].add_read(read_name)

        # add alignments and read_position to the new blocks
        for spltd_BA in splitted_BAs[0].values():
            splitted_block_list[0].add_read_positions(spltd_BA.get_read_name(),spltd_BA.get_read_begin(),spltd_BA.get_read_end() )
            splitted_block_list[0].add_alignment(spltd_BA)

        for spltd_BA in splitted_BAs[1].values():
            splitted_block_list[1].add_read_positions(spltd_BA.get_read_name(),spltd_BA.get_read_begin(),spltd_BA.get_read_end() )
            splitted_block_list[1].add_alignment(spltd_BA)
        return splitted_block_list
    # @classmethod
    # def insert_new_blocks_after_realignement(cls, begin: int, end: int, results : [Block]):
    #     # first block
    #     if results == []:
    #         results.append(Block(begin=begin, end=end))
    #
    #     if results[0].get_begin() > begin:
    #         results.append(Block(begin= begin, end= results[0].get_begin()))
    #
    #     # Last block
    #     if results[-1].get_end() < end and len(results) > 1:
    #         results.append(Block(begin= results[-1].get_end()+1, end= end))
    #
    #     # begin in between block
    #     if begin not in [b.get_begin() for b in results]:
    #         for b in results:
    #             if b.get_begin() < begin and b.get_end() > begin:
    #                 results.append(Block(begin= begin, end=b.get_end()))
    #                 b.set_end(begin-1)
    #
    #     # end in between block
    #     if end not in [b.get_end() for b in results]:
    #         for b in results:
    #             if b.get_begin() < end and b.get_end() > end:
    #                 results.append(Block(begin= begin, end=b.get_end()))
    #                 b.set_end(begin-1)
    #     return sorted(results, key=lambda b: b.get_begin())

    @classmethod
    def insert_new_blocks_in_PredictedExons(cls, mapped_acceptors_blocks : [Block], predicted_exons_list : [PredictedExon]):
        '''
        This method inserts the newly created acceptors blocks in the PredictedExons.
        Those blocks may be totally new and creating a new PredictedExon,
        or they may be added to or overwritting older blocks.

        The mapped_acceptors_blocks should be a list of contiguous Blocks, within one PredictedExon
        '''
        # sorted_acceptors_blocks
        sorted_acceptors_blocks = sorted(mapped_acceptors_blocks, key=lambda b: b.get_begin())

        # we assume that any block in predicted_exons_list can overlap only one predicted exon
        for predicted_exon in predicted_exons_list:
            # if blocks is overlapping an existing PE, or contiguous
            list_of_blocks_to_be_inserted = []
            for block in sorted_acceptors_blocks:
                # Check if block and exon overlap
                if block.end >= predicted_exon.begin and block.begin <= predicted_exon.end \
                        or block.begin + 1 == predicted_exon.end or block.end + 1 == predicted_exon.begin:
                    list_of_blocks_to_be_inserted.append(block)
            if list_of_blocks_to_be_inserted != []:
                for b in list_of_blocks_to_be_inserted:
                    sorted_acceptors_blocks.remove(b)
                # all blocks in list_of_blocks_to_be_inserted must be inserted in this PE
                print(f"Inserting {block} in {predicted_exon}")
                predicted_exon.insert_blocks_in_exon(acceptor_blocks=list_of_blocks_to_be_inserted)
                predicted_exon.blocks=sorted(predicted_exon.blocks, key=lambda b: b.begin)
                predicted_exon.begin = predicted_exon.blocks[0].begin
                predicted_exon.end = predicted_exon.blocks[-1].end

        # TODO : create PredictedExons for each sequence of contiguous blocks
        if sorted_acceptors_blocks != []:
            new_exon = PredictedExon(begin=block.begin, end=block.end, blocks=sorted_acceptors_blocks)
            predicted_exons_list.append(new_exon)

        predicted_exons_list.sort(key=lambda e: e.begin)

        # print(f"Predicted exons and blocks after inserting {mapped_acceptors_blocks}")
        # last_pe_begin  = -1
        # for exon in predicted_exons_list:
        #     print(f"PE {exon.id} {exon.begin} {exon.end}")
        #     last_begin_b = -1
        #     for block in exon.get_blocks():
        #         print(f"\t{block}")
        #         assert last_begin_b != block.begin
        #         last_begin_b = block.begin
        #     assert last_pe_begin != exon.begin
        #     last_pe_begin = exon.begin

        return predicted_exons_list



    @classmethod
    def change_alignment(cls, read_name: str, new:(int,int), acm : AnnotatedCompactMatrix, ra: RealignmentArea):

        # currently allows only to replace all the old alignment by a new one
        print(ra.get_blocks()[0].get_read_coordinates(read_name))
        read_begin = ra.get_blocks()[0].get_read_coordinates(read_name)[0]
        read_end = ra.get_blocks()[-1].get_read_coordinates(read_name)[1]
        print(f"Change alignment for {read_name} {(read_begin,read_end)} to {new} in {str(ra)}")
        # remove occurrences of read_name in blocks within old positions
        for block in ra.get_blocks():
            print(f"Removing {read_name} from {block}")
            block.remove_read(read_name)
        # update blocks
        new_gene_begin, new_gene_end = new
        # check if we need to add a new block in front of the blocks
        block = ra.get_reference_blocks()[0]
        if new_gene_end < block.get_begin():
            block_end = min(new_gene_end, block.get_end())
            b = Block(new_gene_begin,block_end)
            b.add_read(read_name)
            b.add_read_positions(read_name,read_begin,read_end)
            # insert b before block
            block.set_previous_block(b)
            b.set_next_block(block)
            # TODO change the way to change blocks in the ACM
            if block is acm.binary_matrix.blocks[0]:
                # insert b in front of the blocks
                acm.binary_matrix.blocks[0:0] = b
            new_gene_begin = block_end + 1
        for block in ra.get_reference_blocks():
            if new_gene_begin <= new_gene_end:
                if block.get_begin() <= new_gene_begin <= block.get_end():
                    # a block starts within an existing block
                    block_end = min(new_gene_end, block.get_end())
                    # the block may be replaced by three blocks
                    # [block.get_begin()..new_gene_begin-1][new_gene_begin..block_end][block_end+1..block.get_end()]
                    if block.get_begin() < new_gene_begin:
                        b1,b2 = block.split(new_gene_begin)
                    else:
                        b1,b2 = None, block
                    if block_end < block.get_end():
                        b2,b3 = b2.split(block_end+1)
                    else:
                        b2,b3 = block, None
                    b2.add_read(read_name)
                    b2.add_read_positions(read_name, read_begin, read_end)
                    b123 = [ b for b in [b1,b2,b3] if b is not None]
                    # TODO replace block with b1,b2,b3
                    print(f"  Block {block}\n  changed to {[str(b) for b in b123]}")
                    # chaining blocks
                    block.get_previous_block().set_next_block(b123[0])
                    b123[0].set_previous_block(block.get_previous_block())
                    block.get_next_block().set_previous_block(b123[-1])
                    b123[-1].set_next_block(block.get_next_block())
                    for x,y in zip(b123[0:-1],b123[1:]):
                        x.set_next_block(y)
                        y.set_previous_block(x)
                    # TODO effectively add b123 in the blocks of the acm
                    new_gene_begin = block_end+1
                elif block.get_end() < new_gene_begin and block.get_next_block() is not None and new_gene_begin < block.get_next_block().get_begin():
                    # a new block starts in between two existing blocks
                    block_end = min(new_gene_end, block.get_next_block().get_begin()-1)
                    b = Block(new_gene_begin, block_end)
                    b.add_read(read_name)
                    b.add_read_positions(read_name, read_begin, read_end)
                    # insert b after block
                    block.set_next_block(b)
                    b.set_previous_block(block)
                    # TODO change the way to change blocks in the ACM
                    if block is acm.binary_matrix.blocks[0]:
                        # insert b in front of the blocks
                        acm.binary_matrix.blocks.append(b)
                    new_gene_begin = block_end + 1
        # check if we need to a add a new block after the blocks
        block = ra.get_reference_blocks()[-1]
        if new_gene_begin <= new_gene_end and new_gene_begin > block.get_end():

            block_end = new_gene_end
            b = Block(new_gene_begin, block_end)
            b.add_read(read_name)
            b.add_read_positions(read_name, read_begin, read_end)
            # TODO insert b after block
            new_gene_begin = block_end + 1
        assert new_gene_begin > new_gene_end

        # add occurrences of read_name in blocks within new positions
        # predicted exons have to be recomputed

    @classmethod
    def check_fragment_validity(cls):
        pass

    @classmethod
    def Are_donor_and_acceptors_blocks_different(cls, donor_blocks: [Block], acceptor_blocks : [Block]):
        '''
        Check if donor_blocks are fully included into acceptors blocks.
        '''
        # TODO Est-ce que cela marcherait avec: if a_block in donor_blocks
        for ab in acceptor_blocks:
            if not ab.get_begin() in [do_b.get_begin() for do_b in donor_blocks]:
                return True
        return False
