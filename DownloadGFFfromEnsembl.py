# coding: utf-8
"""Python3.6"""

import os
import sys
from optparse import OptionParser
import ast
import time

from utils.AsterUtils import *
from download_data.EnsemblRestClient import *
from download_data.EnsemblGene import *
from download_data.EnsemblTranscript import *

sCurrentVersionScript="v2"
########################################################################
'''
V2-2018/07/20
Explicit outputfile for fasta and gff

V1-2018/07/13
Download data for all transcript of a given GeneId and recreate the gff

python DownloadGFFfromEnsembl.py -t TARGETID -f FASTAFILE -g GFFFILE

TARGETID: gene of interest's EnsemblId
FASTAFILE: output path for fasta file
GFFFILE: output path for gff file
'''
########################################################################
#Options
parser = OptionParser(conflict_handler="resolve")
parser.add_option("-t","--targetid", dest="targetid")
parser.add_option("-f","--fastafile", dest="fastafile")
parser.add_option("-g","--gfffile", dest="gfffile")

(options, args) = parser.parse_args()

sTargetId=options.targetid
if not sTargetId:
    sys.exit("Error : no targetid -t defined, process broken")

sFastaFile=options.fastafile
if not sTargetId:
    sFastaFile=sTargetId+".fa"
    print("Warning : no fastafile -f defined, default : {}".format(sFastaFile))
    
sGffFile=options.gfffile
if not sTargetId:
    sGffFile=sTargetId+".gff"
    print("Warning : no gfffile -g defined, default : {}".format(sGffFile))

if __name__ == "__main__":
    er = EnsemblRestClient()
    dGeneData=er.get_geneData(sTargetId)
    sGeneSeq=er.get_geneSequence(sTargetId)
    oEnsemblGene=EnsemblGene(dGeneData)
    WriteGFF(sGffFile,oEnsemblGene)
    gff_format_modify(sGffFile)
    WriteMonoFasta(sTargetId,sFastaFile,sGeneSeq)
